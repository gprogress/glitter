#######################################################################################################################
# Copyright 2020-2023 Geometric Progress LLC
#
# This file is part of the Glitter project.
#
# Glitter is free software : you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Glitter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
#######################################################################################################################
if(NOT TARGET EXPAT::EXPAT)
	include(ManifestFind)

	MANIFEST_SHARED_LIBRARY(EXPAT "expat.h" expat libexpat)

	if(WIN32)
		MANIFEST_ENTRY(EXPAT DLL "" expat-1.dll libexpat-1.dll)
		MANIFEST_ENTRY(EXPAT DLL "DEBUG" expat-1d.dll libexpat-1d.dll)
		
		MANIFEST_ENTRY(EXPAT PDB "" expat-1.pdb)
		MANIFEST_ENTRY(EXPAT PDB "DEBUG" expat-1d.pdb)
	endif()

	MANIFEST_FIND_PACKAGE(EXPAT)
endif()

MANIFEST_VALIDATE_FOUND_PACKAGE(EXPAT)
