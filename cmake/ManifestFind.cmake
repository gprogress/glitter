#######################################################################################################################
# Copyright 2020-2023 Geometric Progress LLC
#
# This file is part of the Glitter project.
#
# Glitter is free software : you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Glitter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
#######################################################################################################################
set(MANIFEST_BUILD_TYPES RELEASE DEBUG)
set(MANIFEST_BUILD_TYPE_RELEASE 1)
set(MANIFEST_BUILD_TYPE_DEBUG 1)
set(MANIFEST_USE_PROGRAM 1)
set(MANIFEST_USE_DEFINITIONS 1)
set(MANIFEST_USE_SHARED 1)
set(MANIFEST_USE_STATIC 1)
set(MANIFEST_USE_LIBRARY 1)
set(MANIFEST_USE_INCLUDE 1)
set(MANIFEST_USE_DATA 1)

if(WIN32)
	set(MANIFEST_USE_DLL 1)
	if(NOT MSVC)
		set(MANIFEST_DLL_PREFIX "lib")
	endif()
	set(MANIFEST_DLL_SUFFIX ".dll")
	if(MSVC)
		set(MANIFEST_USE_PDB 1)
		set(MANIFEST_PDB_SUFFIX ".pdb")
	endif()
endif()

macro(MANIFEST_COMPONENT FIND_TARGET COMPONENT COMMANDS)
	if(NOT ${FIND_TARGET}_${COMPONENT})
		list(APPEND ${FIND_TARGET}_COMPONENTS ${COMPONENT})
		set(${FIND_TARGET}_${COMPONENT} 1)
	endif()
	
	MANIFEST(${FIND_TARGET}_${COMPONENT} ${COMMANDS} ${ARGN})
endmacro()

macro(MANIFEST_PROGRAM FIND_TARGET NAMES)
	foreach(_name ${NAMES} ${ARGN})
		MANIFEST_ENTRY(${FIND_TARGET} PROGRAM "" ${_name})
		MANIFEST_ENTRY(${FIND_TARGET} PROGRAM "DEBUG" ${_name}d)
		if(MANIFEST_USE_PDB)
			MANIFEST_ENTRY(${FIND_TARGET} PDB "" ${_name}${MANIFEST_PDB_SUFFIX})
			MANIFEST_ENTRY(${FIND_TARGET} PDB "DEBUG" ${_name}d${MANIFEST_PDB_SUFFIX})
		endif()
	endforeach()
endmacro()

macro(MANIFEST_SHARED_LIBRARY FIND_TARGET HEADER NAMES)
	MANIFEST_ENTRY(${FIND_TARGET} INCLUDE "" "${HEADER}")
	foreach(_name ${NAMES} ${ARGN})
		MANIFEST_ENTRY(${FIND_TARGET} SHARED "" ${_name} lib${_name})
		MANIFEST_ENTRY(${FIND_TARGET} SHARED "DEBUG" ${_name}d lib${_name}d)
		if(MANIFEST_USE_DLL)
			MANIFEST_ENTRY(${FIND_TARGET} DLL "" ${_name}${MANIFEST_DLL_SUFFIX} lib${_name}${MANIFEST_DLL_SUFFIX})
			MANIFEST_ENTRY(${FIND_TARGET} DLL "DEBUG" ${_name}d${MANIFEST_DLL_SUFFIX} lib${_name}d${MANIFEST_DLL_SUFFIX})
		endif()
		if(MANIFEST_USE_PDB)
			MANIFEST_ENTRY(${FIND_TARGET} PDB "" ${_name}${MANIFEST_PDB_SUFFIX} lib${_name}${MANIFEST_PDB_SUFFIX})
			MANIFEST_ENTRY(${FIND_TARGET} PDB "DEBUG" ${_name}d${MANIFEST_PDB_SUFFIX} lib${_name}d${MANIFEST_PDB_SUFFIX})
		endif()
	endforeach()
endmacro()

macro(MANIFEST_SHARED_COMPONENT FIND_TARGET COMPONENT HEADER NAMES)
	if(NOT ${FIND_TARGET}_${COMPONENT})
		list(APPEND ${FIND_TARGET}_COMPONENTS ${COMPONENT})
		set(${FIND_TARGET}_${COMPONENT} 1)
	endif()
	MANIFEST_SHARED_LIBRARY(${FIND_TARGET}_${COMPONENT} ${HEADER} ${NAMES} ${ARGN})
endmacro()

macro(MANIFEST_PROGRAM_COMPONENT FIND_TARGET COMPONENT NAMES)
	if(NOT ${FIND_TARGET}_${COMPONENT})
		list(APPEND ${FIND_TARGET}_COMPONENTS ${COMPONENT})
		set(${FIND_TARGET}_${COMPONENT} 1)
	endif()
	MANIFEST_PROGRAM(${FIND_TARGET}_${COMPONENT} ${NAMES} ${ARGN})
endmacro()

macro(MANIFEST_VALIDATE_FOUND_PACKAGE FIND_TARGET)
	if(${FIND_TARGET}_COMPONENTS)
		set(${FIND_TARGET}_FOUND 1)
		if(${FIND_TARGET}_FIND_COMPONENTS)
			foreach(_comp ${${FIND_TARGET}_FIND_COMPONENTS})
				if(NOT TARGET ${FIND_TARGET}::${_comp})
					set(${FIND_TARGET}_FOUND 0)
					list(APPEND ${FIND_TARGET}_COMPONENTS_MISSING ${_comp})
				endif()
			endforeach()
		else()
			foreach(_comp ${${FIND_TARGET}_COMPONENTS})
				if(NOT TARGET ${FIND_TARGET}::${_comp})
					set(${FIND_TARGET}_FOUND 0)
					list(APPEND ${FIND_TARGET}_COMPONENTS_MISSING ${_comp})
				endif()
			endforeach()
		endif()
	else()
		if (TARGET ${FIND_TARGET}::${FIND_TARGET})
			set(${FIND_TARGET}_FOUND 1)
		else()
			set(${FIND_TARGET}_FOUND 0)
		endif()
	endif()
endmacro()

macro(MANIFEST FIND_TARGET COMMANDS)
	set(_current_command )
	set(_build_type )
	foreach (_command ${COMMANDS} ${ARGN})
		# message(STATUS "Command: ${_command}")
		# message(STATUS "Current Command: ${_current_command}")
		# message(STATUS "Checking MANIFEST_USE_${_command}: ${MANIFEST_USE_${_command}}")
		if ("${MANIFEST_USE_${_command}}")
			set(_current_command ${_command})
			set(_build_type )
			# message(STATUS "Setting names for ${_current_command}")
		elseif("${MANIFEST_BUILD_TYPE_${_command}}")
			set(_build_type ${_command})
			# message(STATUS "Setting build type to ${_build_type}")
		else()
			# message(STATUS "Processing ${_command} for ${_current_command} ${_build_type}")
			MANIFEST_ENTRY(${FIND_TARGET} ${_current_command} "${_build_type}" ${_command})
		endif()
	endforeach()
endmacro()

macro(MANIFEST_ENTRY FIND_TARGET TYPE BUILD_TYPE NAMES)
	if("${BUILD_TYPE}" STREQUAL "")
		list(APPEND ${FIND_TARGET}_${TYPE}_NAMES ${NAMES} ${ARGN})
		# message(STATUS "Set ${FIND_TARGET}_${TYPE}_NAMES")
	else()
		list(APPEND ${FIND_TARGET}_${TYPE}_NAMES_${BUILD_TYPE} ${NAMES} ${ARGN})
		# message(STATUS "Set ${FIND_TARGET}_${TYPE}_NAMES_${BUILD_TYPE}")
	endif()
	set(${FIND_TARGET}_HAS_${TYPE} 1)
	# message(STATUS "${_var_prefix}_HAS_${TYPE}: ${${_var_prefix}_HAS_${TYPE}}")	
endmacro()

macro(MANIFEST_HINT_DIRS FIND_TARGET)
	if(NOT ${FIND_TARGET}_HINTS)
		set(${FIND_TARGET}_INCLUDE_HINTS )
		set(${FIND_TARGET}_RUNTIME_HINTS )
		set(${FIND_TARGET}_LIBRARY_HINTS )

		# Get hints from ${FIND_TARGET}_ROOT and MANIFEST_ROOT if set
		# Note it can be multiple paths and configuration-specific
		foreach(_DIR ${${FIND_TARGET}_ROOT} ${MANIFEST_ROOT})
			list(APPEND ${FIND_TARGET}_INCLUDE_HINTS "${_DIR}/include")
			list(APPEND ${FIND_TARGET}_RUNTIME_HINTS "${_DIR}/bin")
			list(APPEND ${FIND_TARGET}_LIBRARY_HINTS "${_DIR}/lib")
		endforeach()
		
		foreach(_DIR ${${FIND_TARGET}_ROOT_RELEASE})
			list(APPEND ${FIND_TARGET}_RUNTIME_HINTS_RELEASE "${_DIR}/bin")
			list(APPEND ${FIND_TARGET}_LIBRARY_HINTS_RELEASE "${_DIR}/lib")
		endforeach()
		
		foreach(_DIR ${${FIND_TARGET}_ROOT_DEBUG})
			list(APPEND ${FIND_TARGET}_RUNTIME_HINTS_DEBUG "${_DIR}/bin")
			list(APPEND ${FIND_TARGET}_LIBRARY_HINTS_DEBUG "${_DIR}/lib")
		endforeach()
		
		set(${FIND_TARGET}_HINTS 1)
	endif()
endmacro()

function(MANIFEST_FIND_PACKAGE FIND_TARGET)	
	MANIFEST_HINT_DIRS(${FIND_TARGET})
	
	# Different behavior depending on if this is a component-based package or not
	if (${FIND_TARGET}_COMPONENTS)
		# message(STATUS "${FIND_TARGET}: we have components")
		if(${FIND_TARGET}_FIND_COMPONENTS)
			# message(STATUS "${FIND_TARGET}: user asked for specific compponents")
			foreach(_component ${${FIND_TARGET}_FIND_COMPONENTS})
				# message(STATUS "${FIND_TARGET}::${_component}: initiating search")
				MANIFEST_FIND_COMPONENT("${FIND_TARGET}" "${_component}")
			endforeach()
		else()
			# message(STATUS "${FIND_TARGET}: getting all components")
			foreach(_component ${${FIND_TARGET}_COMPONENTS})
				MANIFEST_FIND_COMPONENT("${FIND_TARGET}" "${_component}")
			endforeach()		
		endif()
	else()
		MANIFEST_FIND_STANDALONE("${FIND_TARGET}")
	endif()
endfunction()

macro(MANIFEST_FIND_INCLUDE FIND_TARGET VAR_PREFIX)
	# message(STATUS "${VAR_PREFIX}: Searching for includes")
	set(${VAR_PREFIX}_INCLUDE_MISSING )
	# message(STATUS "${VAR_PREFIX}_HAS_INCLUDE: ${${VAR_PREFIX}_HAS_INCLUDE}")
	if(${VAR_PREFIX}_HAS_INCLUDE)
		set(${VAR_PREFIX}_INCLUDE_MISSING 1)
		find_path(${VAR_PREFIX}_INCLUDE_DIR
			NAMES 
			${${VAR_PREFIX}_INCLUDE_NAMES} 
			HINTS 
			${${FIND_TARGET}_INCLUDE_HINTS}
			PATH_SUFFIXES
			${${FIND_TARGET}_INCLUDE_SUFFIXES})
		
		# message(STATUS "Include Names: ${${VAR_PREFIX}_INCLUDE_NAMES}")
		# message(STATUS "Include Hints: ${${VAR_PREFIX}_INCLUDE_HINTS}")
		# message(STATUS "Include Suffixes: ${${VAR_PREFIX}_INCLUDE_SUFFIXES}")
		# message(STATUS "${VAR_PREFIX}: Include dir is ${${VAR_PREFIX}_INCLUDE_DIR}")	
		if(${VAR_PREFIX}_INCLUDE_DIR)
			set(${VAR_PREFIX}_INCLUDE_MISSING 0)
		endif()
	endif()
endmacro()

macro(MANIFEST_FIND_BINARY FIND_TARGET VAR_PREFIX)
	set(${VAR_PREFIX}_LIBRARY_MISSING )
	if(${VAR_PREFIX}_HAS_SHARED)
		# message(STATUS "${VAR_PREFIX}: Searching for shared libraries")
		set(${VAR_PREFIX}_LIBRARY_MISSING 1)
		
		if(${VAR_PREFIX}_SHARED_NAMES)
			find_library(${VAR_PREFIX}_LIBRARY
				NAME
				${${VAR_PREFIX}_SHARED_NAMES}
				HINTS
				${${FIND_TARGET}_LIBRARY_HINTS} 
			)
			#message(STATUS "${_var_prefix}: Looking for names ${${VAR_PREFIX}_SHARED_NAMES}")
			#message(STATUS "${_var_prefix}: Found ${${_var_prefix}_LIBRARY_${_build_type}}")
			if(${VAR_PREFIX}_LIBRARY)
				# message(STATUS "Found: ${_var_prefix}_LIBRARY_${_build_type}")
				set(${VAR_PREFIX}_LIBRARY_MISSING 0)
				set(${VAR_PREFIX}_TYPE SHARED)
			endif()
		endif()
		
		foreach(_build_type ${MANIFEST_BUILD_TYPES})
			if(${VAR_PREFIX}_SHARED_NAMES_${_build_type})
				#message(STATUS "${VAR_PREFIX}: Searching for ${_build_type} library with names ${${VAR_PREFIX}_SHARED_NAMES_${_build_type}}")
				#message(STATUS "Hints for build type ${FIND_TARGET}_LIBRARY_HINTS_${_build_type}: ${${FIND_TARGET}_LIBRARY_HINTS_${_build_type}}")
				#message(STATUS "General hints: ${FIND_TARGET}_LIBRARY_HINTS ${${FIND_TARGET}_LIBRARY_HINTS}")

				find_library(${VAR_PREFIX}_LIBRARY_${_build_type}
					NAMES
					${${VAR_PREFIX}_SHARED_NAMES_${_build_type}}
					HINTS
					${${FIND_TARGET}_LIBRARY_HINTS_${_build_type}}
					${${FIND_TARGET}_LIBRARY_HINTS}
				)
				
				# message(STATUS "${VAR_PREFIX}: Found ${${_var_prefix}_LIBRARY_${_build_type}}")
				if(${VAR_PREFIX}_LIBRARY_${_build_type})
					# message(STATUS "Found: ${_var_prefix}_LIBRARY_${_build_type}")
					set(${VAR_PREFIX}_LIBRARY_MISSING 0)
					set(${VAR_PREFIX}_TYPE SHARED)
				endif()
			endif()
		endforeach()
		
		if(${VAR_PREFIX}_DLL_NAMES)
			# message(STATUS "Searching for ${${VAR_PREFIX}_DLL_NAMES}")
			find_program(${VAR_PREFIX}_PROGRAM
					NAMES
					${${VAR_PREFIX}_DLL_NAMES}
					HINTS 
					${${FIND_TARGET}_RUNTIME_HINTS} 
			)
				
			if(${VAR_PREFIX}_PROGRAM)
				set(${VAR_PREFIX}_PROGRAM_MISSING 0)
			endif()
		endif()

		foreach(_build_type ${MANIFEST_BUILD_TYPES})
			if(${VAR_PREFIX}_DLL_NAMES_${_build_type})
				# message(STATUS "Searching for ${${VAR_PREFIX}_DLL_NAMES_${_build_type}}")
				find_program(${VAR_PREFIX}_PROGRAM_${_build_type}
					NAMES
					${${VAR_PREFIX}_DLL_NAMES_${_build_type}}
					HINTS 
					${${FIND_TARGET}_RUNTIME_HINTS_${_build_type}}
					${${FIND_TARGET}_RUNTIME_HINTS}
				)
					
				if(${VAR_PREFIX}_PROGRAM_${_build_type})
					set(${VAR_PREFIX}_PROGRAM_MISSING 0)
				endif()
			endif()
		endforeach()
	elseif(${VAR_PREFIX}_HAS_PROGRAM)
		set(${VAR_PREFIX}_PROGRAM_MISSING 1)
		if(${VAR_PREFIX}_PROGRAM_NAMES)
			find_program(${VAR_PREFIX}_PROGRAM
				NAMES
				${${VAR_PREFIX}_PROGRAM_NAMES} 
				HINTS 
				${${FIND_TARGET}_RUNTIME_HINTS} 
			)
				
			if(${VAR_PREFIX}_PROGRAM)
				set(${VAR_PREFIX}_PROGRAM_MISSING 0)
				set(${VAR_PREFIX}_TYPE EXECUTABLE)
			endif()
		endif()
		
		foreach(_build_type ${MANIFEST_BUILD_TYPES})
			if(${VAR_PREFIX}_PROGRAM_NAMES_${_build_type})
				find_program(${VAR_PREFIX}_PROGRAM_${_build_type}
					NAMES
					${${VAR_PREFIX}_PROGRAM_NAMES_${_build_type}}
					HINTS 
					${${FIND_TARGET}_RUNTIME_HINTS_${_build_type}}
					${${FIND_TARGET}_RUNTIME_HINTS}
				)
					
				if(${VAR_PREFIX}_PROGRAM_${_build_type})
					set(${VAR_PREFIX}_PROGRAM_MISSING 0)
					set(${VAR_PREFIX}_TYPE EXECUTABLE)
				endif()
			endif()
		endforeach()
	endif()
	
	if(${VAR_PREFIX}_HAS_PDB)
		if(${VAR_PREFIX}_PDB_NAMES)
			find_program(${VAR_PREFIX}_PDB
				NAMES
				${${VAR_PREFIX}_PDB_NAMES} 
				HINTS 
				${${FIND_TARGET}_PDB_HINTS} 
			)
			mark_as_advanced(${VAR_PREFIX}_PDB)
		endif()
	
		foreach(_build_type ${MANIFEST_BUILD_TYPES})
			if(${VAR_PREFIX}_PDB_NAMES_${_build_type})
				find_program(${VAR_PREFIX}_PDB_${_build_type}
					NAMES 
					${${VAR_PREFIX}_PDB_NAMES_${_build_type}}
					HINTS
					${${FIND_TARGET}_RUNTIME_HINTS_${_build_type}}
					${${FIND_TARGET}_RUNTIME_HINTS} 
				)
				mark_as_advanced(${VAR_PREFIX}_PDB_${_build_type})
			endif()
		endforeach()
	endif()
endmacro()

function(MANIFEST_FIND_STANDALONE FIND_TARGET)
	set(_var_prefix "${FIND_TARGET}")
	set(_var_target "${FIND_TARGET}::${FIND_TARGET}")
	MANIFEST_FIND_PREFIX(${FIND_TARGET} ${_var_prefix} ${_var_target})
endfunction()

function(MANIFEST_FIND_COMPONENT FIND_TARGET COMPONENT)
	set(_var_prefix "${FIND_TARGET}_${COMPONENT}")
	set(_var_target "${FIND_TARGET}::${COMPONENT}")
	MANIFEST_FIND_PREFIX(${FIND_TARGET} ${_var_prefix} ${_var_target})
endfunction()

function(MANIFEST_FIND_PREFIX FIND_TARGET VAR_PREFIX VAR_TARGET)
	if(TARGET ${VAR_TARGET})
		if(NOT ${FIND_TARGET}_FIND_QUIETLY)
			message(STATUS "${VAR_TARGET} found (cached)")
		endif() 
		return()
	endif()

	# Look for the header file.
	MANIFEST_FIND_INCLUDE("${FIND_TARGET}" "${VAR_PREFIX}")
	MANIFEST_FIND_BINARY("${FIND_TARGET}" "${VAR_PREFIX}")
	
	# message(STATUS "Include missing: ${${_var_prefix}_INCLUDE_MISSING}")
	# message(STATUS "Library missing: ${${_var_prefix}_LIBRARY_MISSING}")
	# message(STATUS "Program missing: ${${_var_prefix}_PROGRAM_MISSING}")

	if(NOT "${${VAR_PREFIX}_INCLUDE_MISSING}"
		AND NOT "${${VAR_PREFIX}_LIBRARY_MISSING}"
		AND NOT "${${VAR_PREFIX}_PROGRAM_MISSING}")

		set(${_var_prefix}_FOUND 1)
		if(${_var_prefix}_TYPE STREQUAL "EXECUTABLE")
			add_executable(${VAR_TARGET} IMPORTED GLOBAL)
		else()
			# message(STATUS "_var_prefix: ${_var_prefix}")
			# message(STATUS "target type:" ${${_var_prefix}_TYPE})
			add_library(${VAR_TARGET} ${${VAR_PREFIX}_TYPE} IMPORTED GLOBAL)
		endif()
		
		if(${VAR_PREFIX}_DEFINITIONS_NAMES)
			message("Setting target compile definitions: ${VAR_TARGET} INTERFACE ${${VAR_PREFIX}_DEFINITIONS_NAMES}")
			target_compile_definitions(${VAR_TARGET} INTERFACE ${${VAR_PREFIX}_DEFINITIONS_NAMES})
		endif()
		
		if(${VAR_PREFIX}_INCLUDE_DIR)
			if(NOT ${FIND_TARGET}_FIND_QUIETLY)
				message(STATUS "Found ${VAR_TARGET} include directory: ${${VAR_PREFIX}_INCLUDE_DIR}")			
			endif()
			set_target_properties(${VAR_TARGET} PROPERTIES
				INTERFACE_INCLUDE_DIRECTORIES ${${VAR_PREFIX}_INCLUDE_DIR})
		endif()
		
		if(WIN32)
			if(${VAR_PREFIX}_LIBRARY)
				if(NOT ${FIND_TARGET}_FIND_QUIETLY)
					message(STATUS "Found ${VAR_TARGET} library: ${${VAR_PREFIX}_LIBRARY}")
				endif()
				set_target_properties(${VAR_TARGET} PROPERTIES IMPORTED_IMPLIB "${${VAR_PREFIX}_LIBRARY}")
			endif()
		
			if(${VAR_PREFIX}_LIBRARY_RELEASE)
				if(NOT ${FIND_TARGET}_FIND_QUIETLY)
					message(STATUS "Found ${VAR_TARGET} release library: ${${VAR_PREFIX}_LIBRARY_RELEASE}")
				endif()
				set_target_properties(${VAR_TARGET} PROPERTIES IMPORTED_IMPLIB_RELEASE "${${VAR_PREFIX}_LIBRARY_RELEASE}")
			endif()
			
			if(${VAR_PREFIX}_LIBRARY_DEBUG)
				if(NOT ${FIND_TARGET}_FIND_QUIETLY)
					message(STATUS "Found ${VAR_TARGET} debug library: ${${VAR_PREFIX}_LIBRARY_DEBUG}")
				endif()
				set_target_properties(${VAR_TARGET} PROPERTIES IMPORTED_IMPLIB_DEBUG "${${VAR_PREFIX}_LIBRARY_DEBUG}")
			endif()
			
			if(${VAR_PREFIX}_PROGRAM)
				if(NOT ${FIND_TARGET}_FIND_QUIETLY)
					message(STATUS "Found ${VAR_TARGET} runtime: ${${VAR_PREFIX}_PROGRAM}")
				endif()
				set_target_properties(${VAR_TARGET} PROPERTIES IMPORTED_LOCATION "${${VAR_PREFIX}_PROGRAM}")
			endif()
			
			if(${VAR_PREFIX}_PROGRAM_RELEASE)
				if(NOT ${FIND_TARGET}_FIND_QUIETLY)
					message(STATUS "Found ${VAR_TARGET} release runtime: ${${VAR_PREFIX}_PROGRAM_RELEASE}")
				endif()
				set_target_properties(${VAR_TARGET} PROPERTIES IMPORTED_LOCATION_RELEASE "${${VAR_PREFIX}_PROGRAM_RELEASE}")
			endif()
			
			if(${VAR_PREFIX}_PROGRAM_DEBUG)
				if(NOT ${FIND_TARGET}_FIND_QUIETLY)
					message(STATUS "Found ${VAR_TARGET} debug runtime: ${${VAR_PREFIX}_PROGRAM_DEBUG}")
				endif()			
				set_target_properties(${VAR_TARGET} PROPERTIES IMPORTED_LOCATION_DEBUG "${${VAR_PREFIX}_PROGRAM_DEBUG}")
			endif()
	
			if(${VAR_PREFIX}_PDB)
				if(NOT ${FIND_TARGET}_FIND_QUIETLY)
					message(STATUS "Found ${VAR_TARGET} symbols: ${${VAR_PREFIX}_PDB}")
				endif()				
				set_target_properties(${VAR_TARGET} PROPERTIES IMPORTED_PDB "${${VAR_PREFIX}_PDB}")
			endif()
				
			if(${VAR_PREFIX}_PDB_RELEASE)
				if(NOT ${FIND_TARGET}_FIND_QUIETLY)
					message(STATUS "Found ${VAR_TARGET} release symbols: ${${VAR_PREFIX}_PDB_RELEASE}")
				endif()				
				set_target_properties(${VAR_TARGET} PROPERTIES IMPORTED_PDB_RELEASE "${${VAR_PREFIX}_PDB_RELEASE}")
			endif()
			
			if(${VAR_PREFIX}_PDB_DEBUG)
				if(NOT ${FIND_TARGET}_FIND_QUIETLY)
					message(STATUS "Found ${VAR_TARGET} debug symbols: ${${VAR_PREFIX}_PDB_DEBUG}")
				endif()		
				set_target_properties(${VAR_TARGET} PROPERTIES IMPORTED_PDB_DEBUG "${${VAR_PREFIX}_PDB_DEBUG}")
			endif()
		else()
			if(${VAR_PREFIX}_LIBRARY)
				if(NOT ${FIND_TARGET}_FIND_QUIETLY)
					message(STATUS "Found ${VAR_TARGET} library: ${${VAR_PREFIX}_LIBRARY}")
				endif()		
				set_target_properties(${VAR_TARGET} PROPERTIES IMPORTED_LOCATION "${${VAR_PREFIX}_LIBRARY}")
			elseif(${VAR_PREFIX}_PROGRAM)
				if(NOT ${FIND_TARGET}_FIND_QUIETLY)
					message(STATUS "Found ${VAR_TARGET} runtime: ${${VAR_PREFIX}_PROGRAM}")
				endif()	
				set_target_properties(${VAR_TARGET} PROPERTIES IMPORTED_LOCATION "${${VAR_PREFIX}_PROGRAM}")
			endif()
		
			if(${VAR_PREFIX}_LIBRARY_RELEASE)
				if(NOT ${FIND_TARGET}_FIND_QUIETLY)
					message(STATUS "Found ${VAR_TARGET} release library: ${${VAR_PREFIX}_LIBRARY_RELEASE}")
				endif()		
				set_target_properties(${VAR_TARGET} PROPERTIES IMPORTED_LOCATION_RELEASE "${${VAR_PREFIX}_LIBRARY_RELEASE}")
			elseif(${VAR_PREFIX}_PROGRAM_RELEASE)
				if(NOT ${FIND_TARGET}_FIND_QUIETLY)
					message(STATUS "Found ${VAR_TARGET} release runtime: ${${VAR_PREFIX}_PROGRAM_RELEASE}")
				endif()	
				set_target_properties(${VAR_TARGET} PROPERTIES IMPORTED_LOCATION_RELEASE "${${VAR_PREFIX}_PROGRAM_RELEASE}")
			endif()
			
			if(${VAR_PREFIX}_LIBRARY_DEBUG)
				if(NOT ${FIND_TARGET}_FIND_QUIETLY)
					message(STATUS "Found ${VAR_TARGET} debug library: ${${VAR_PREFIX}_LIBRARY_DEBUG}")
				endif()	
				set_target_properties(${VAR_TARGET} PROPERTIES IMPORTED_LOCATION_DEBUG "${${VAR_PREFIX}_LIBRARY_DEBUG}")
			elseif(${VAR_PREFIX}_PROGRAM_DEBUG)
				set_target_properties(${VAR_TARGET} PROPERTIES IMPORTED_LOCATION_RELEASE "${${VAR_PREFIX}_PROGRAM_DEBUG}")
				if(NOT ${FIND_TARGET}_FIND_QUIETLY)
					message(STATUS "Found ${VAR_TARGET} debug runtime: ${${VAR_PREFIX}_PROGRAM_DEBUG}")
				endif()	
			endif()
		endif()
	else()
		if(NOT ${FIND_TARGET}_FIND_QUIETLY)
			set(${VAR_PREFIX}_STATUS "STATUS")
			if(${FIND_TARGET}_FIND_REQUIRED)
				set(${VAR_PREFIX}_STATUS "SEND_ERROR")
			endif()
			
			set(_message )
			
			if(${VAR_PREFIX}_INCLUDE_MISSING)
				set(_message " includes")
			endif()
			
			if(${VAR_PREFIX}_LIBRARY_MISSING)
				set(_message "${_message} library")
			endif()
			
			if(${VAR_PREFIX}_PROGRAM_MISSING)
				set(_message "${_message} program")
			endif()
			
			message(${${VAR_PREFIX}_STATUS} "Could NOT find ${VAR_TARGET} (missing:${_message})")
		endif()
	endif()
endfunction()

# Optional manifest settings for stock configuration of external libraries
# This is primarily convenient for automating builds on Windows
# This file just sets up the main variables and macros

function(MANIFEST_DETECT_ROOT)
	set(MANIFEST_DEFAULT )
	set(MANIFEST_SYSTEM )
	set(MANIFEST_ARCH )
	set(MANIFEST_COMPILER ) 

	if(WIN32)
		set(MANIFEST_DEFAULT "C:/Packages")
		set(MANIFEST_SYSTEM "windows")

		if(CMAKE_SIZEOF_VOID_P EQUAL 4)
			set(MANIFEST_ARCH "x86")
		elseif(CMAKE_SIZEOF_VOID_P EQUAL 8)
			set(MANIFEST_ARCH "x64")
		endif()
		
		if(MSVC_TOOLSET_VERSION EQUAL 140)
			set(MANIFEST_COMPILER "vs2015")
		elseif(MSVC_TOOLSET_VERSION EQUAL 141)
			set(MANIFEST_COMPILER "vs2017")
		elseif(MSVC_TOOLSET_VERSION EQUAL 142)
			set(MANIFEST_COMPILER "vs2019")
		elseif(MSVC_TOOLSET_VERSION GREATER 142)
			set(MANIFEST_COMPILER "vs2022")
		elseif(MINGW)
			if(CMAKE_CXX_COMPILER_ID)
				message(STATUS "CMake Compiler: ${CMAKE_CXX_COMPILER_ID}")
				if (CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
					set(MANIFEST_COMPILER "mingw64-clang")
				else()
					set(MANIFEST_COMPILER "mingw64-gcc")
				endif()
			else()
			
			endif()
		endif()
		
		set(MANIFEST_DEFAULT_ROOT "${MANIFEST_DEFAULT}/${MANIFEST_SYSTEM}-${MANIFEST_ARCH}/${MANIFEST_COMPILER}")
	else()
		set(MANIFEST_DEFAULT "~/Packages")
		set(MANIFEST_DEFAULT_ROOT "${MANIFEST_DEFAULT}/${CMAKE_LIBRARY_ARCHITECTURE}")
	endif()

	# Set variable, appending force is passed as argument
	set(MANIFEST_ROOT "${MANIFEST_DEFAULT_ROOT}" CACHE PATH "Path to manifest packages root folder" ${ARGN})
	message(STATUS "Manifest root: ${MANIFEST_ROOT}")
endfunction()

macro(MANIFEST_PACKAGE_ROOT PACKAGE NAME VERSION)
	if(MANIFEST_ROOT)
		set(${PACKAGE}_ROOT "${MANIFEST_ROOT}/${NAME}/${VERSION}" CACHE PATH "Path to ${PACKAGE} root folder")
	endif()
	set(${PACKAGE}_MANIFEST_FOLDER ${NAME})
	set(${PACKAGE}_MANIFEST_VERSION ${VERSION})
endmacro()

