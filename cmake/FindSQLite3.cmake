#######################################################################################################################
# Copyright 2020-2023 Geometric Progress LLC
#
# This file is part of the Glitter project.
#
# Glitter is free software : you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Glitter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
#######################################################################################################################
set(SQLite3_ROOT "" CACHE PATH "Hint path to locate desired XLNT installation")

include(ManifestFind)

message (STATUS "Attempting to find SQLite3 at ${SQLite3_ROOT}")

MANIFEST_SHARED_COMPONENT(SQLite3 SQLite3 "sqlite3.h" sqlite3 sqlite)
MANIFEST_PROGRAM_COMPONENT(SQLite3 Shell sqlite)

MANIFEST_FIND_PACKAGE(SQLite3)
MANIFEST_VALIDATE_FOUND_PACKAGE(SQLite3)
