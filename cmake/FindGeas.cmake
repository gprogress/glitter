#######################################################################################################################
# Copyright 2020-2023 Geometric Progress LLC
#
# This file is part of the Glitter project.
#
# Glitter is free software : you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Glitter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
#######################################################################################################################
include(ManifestFind)

MANIFEST_SHARED_COMPONENT(Geas geas "geas/Dictionary.h" geas)
MANIFEST_SHARED_COMPONENT(Geas editor "geas/editor/MainWindow.h" geas-editor)
MANIFEST_SHARED_COMPONENT(Geas excel "geas/excel/GGDMReader.h" geas-excel geas) # may be integrated into geas
MANIFEST_SHARED_COMPONENT(Geas json "geas/json/MaxarOwtWriter.h" geas-json geas) # may be integrated into geas
MANIFEST_SHARED_COMPONENT(Geas xml "geas/xml/NCVReader.h" geas-xml geas) # may be integrated into geas

MANIFEST_FIND_PACKAGE(Geas)
MANIFEST_VALIDATE_FOUND_PACKAGE(Geas)
