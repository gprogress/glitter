#######################################################################################################################
# Copyright 2020-2023 Geometric Progress LLC
#
# This file is part of the Glitter project.
#
# Glitter is free software : you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Glitter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
#######################################################################################################################
include(ManifestFind)

set(CIO_ROOT "" CACHE PATH "Hint path to locate desired CIO installation")

MANIFEST_SHARED_COMPONENT(CIO cio "cio/Channel.h" cio)
MANIFEST_SHARED_COMPONENT(CIO bluetooth "cio/bluetooth/Device.h" cio-bluetooth cio)
MANIFEST_SHARED_COMPONENT(CIO net "cio/net/Socket.h" cio-net cio)
MANIFEST_SHARED_COMPONENT(CIO test "cio/test/Test.h" cio-test cio)
MANIFEST_SHARED_COMPONENT(CIO xml "cio/xml/Parser.h" cio-xml cio)
MANIFEST_SHARED_COMPONENT(CIO zip "cio/zip/Inflator.h" cio-zip cio)

MANIFEST_FIND_PACKAGE(CIO)
MANIFEST_VALIDATE_FOUND_PACKAGE(CIO)
