#######################################################################################################################
# Copyright 2020-2023 Geometric Progress LLC
#
# This file is part of the Glitter project.
#
# Glitter is free software : you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Glitter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
#######################################################################################################################
find_program(AStyle_PROGRAM astyle)

if(AStyle_PROGRAM)
	set(AStyle_FOUND ON)
endif()

function(ADD_ASTYLE_TARGET NAME CONFIG PATHS)
	get_filename_component(_cfg_full_path "${CONFIG}" ABSOLUTE)
	
	set(_inputs )
	foreach(_input ${PATHS} ${ARGN})
		get_filename_component(_input_full_path "${_input}" ABSOLUTE)
		list(APPEND _inputs "${_input_full_path}")
	endforeach()

	add_custom_target("${NAME}" COMMAND
		${AStyle_PROGRAM} "--options=${_cfg_full_path}"
		"--suffix=none" "--recursive" ${_inputs})
endfunction()
