#######################################################################################################################
# Copyright 2020-2023 Geometric Progress LLC
#
# This file is part of the Glitter project.
#
# Glitter is free software : you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Glitter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
#######################################################################################################################
include(ManifestFind)

if(NOT MANIFEST_ROOT)
	MANIFEST_DETECT_ROOT()
endif()

MANIFEST_PACKAGE_ROOT(CIO cio 0.5.0)
MANIFEST_PACKAGE_ROOT(EXPAT expat 2.5.0)
MANIFEST_PACKAGE_ROOT(Geas geas 0.5.0)
MANIFEST_PACKAGE_ROOT(XLNT xlnt 1.5.0)
MANIFEST_PACKAGE_ROOT(SQLite3 sqlite 3.40.0)
MANIFEST_PACKAGE_ROOT(ZLIB zlib 1.2.13)
