#######################################################################################################################
# Copyright 2020-2023 Geometric Progress LLC
#
# This file is part of the Glitter project.
#
# Glitter is free software : you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Glitter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
#######################################################################################################################
cmake_minimum_required(VERSION 3.1)

# Enable use of ROOT variables for find_package
# Note that CIO will still work fine without it, but this silences a warning
if(POLICY CMP0074)
	cmake_policy(SET CMP0074 NEW)
endif()

cmake_policy(SET CMP0054 NEW)


list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake")

set(GLITTER_MAJOR_VERSION 0)
set(GLITTER_MINOR_VERSION 4)
set(GLITTER_PATCH_VERSION 1)
set(GLITTER_SUBPATCH_VERSION 0)

set(GLITTER_ABI_VERSION "${GLITTER_MAJOR_VERSION}.${GLITTER_MINOR_VERSION}")
set(GLITTER_VERSION "${GLITTER_ABI_VERSION}.${GLITTER_PATCH_VERSION}")
set(GLITTER_COPYRIGHT "Copyright 2020-2023 Geometric Progress LLC")
set(GLITTER_HOMEPAGE_URL "https://bitbucket.org/gprogress/glitter")

if(NOT CMAKE_BUILD_TYPE)
	set(CMAKE_BUILD_TYPE Release)
endif()

project(Glitter LANGUAGES CXX)

message(STATUS "Glitter Version: ${GLITTER_VERSION}")
message(STATUS "Glitter ABI Version: ${GLITTER_ABI_VERSION}")
message(STATUS "Glitter Architecture: ${CMAKE_LIBRARY_ARCHITECTURE}")

option(BUILD_SHARED_LIBS "Compile as shared libraries" ON)
if(BUILD_SHARED_LIBS)
	message(STATUS "Library Default: Shared")
else()
	message(STATUS "Library Default: Static")
endif()

set(CMAKE_RELEASE_POSTFIX "" CACHE STRING "Suffix for release builds")
set(CMAKE_DEBUG_POSTFIX "d" CACHE STRING "Suffix for debug builds")

if(CMAKE_RELEASE_POSTFIX)
	message(STATUS "Release Build Suffix: ${CMAKE_RELEASE_POSTFIX}")
else()
	message(STATUS "Release Build Suffix: (none)")
endif()

if(CMAKE_DEBUG_POSTFIX)
	message(STATUS "Debug Build Suffix: ${CMAKE_DEBUG_POSTFIX}")
else()
	message(STATUS "Debug Build Suffix: (none)")
endif()

# Pull in Unix standard installation dirs and associated variables
# This will notably set the following in a way that handles Linux differences:
# CMAKE_INSTALL_BINDIR - runtime destination
# CMAKE_INSTALL_LIBDIR - library and archive destination
# CMAKE_INSTALL_INCLUDEDIR - header include destination
# CMAKE_INSTALL_DATAROOTDIR - data root folder
# CMAKE_INSTALL_DOCDIR - doc root folder
include(GNUInstallDirs)

include(DefaultManifest OPTIONAL)
include(CopyFoundRuntime)

if(NOT WIN32)
	option(CMAKE_RELATIVE_RPATH "Whether to use relative rpaths or absolute rpaths (default is OFF because of setcap issues)" OFF)
	if(CMAKE_RELATIVE_RPATH)
		message(STATUS "RPATH: Relative using \$ORIGIN")
		file(RELATIVE_PATH CMAKE_PROGRAM_RELATIVE_RPATH "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_BINDIR}" "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}")
		set(CMAKE_PROGRAM_RPATH "\$ORIGIN/${CMAKE_PROGRAM_RELATIVE_RPATH}")
		set(CMAKE_LIBRARY_RPATH "\$ORIGIN")
	else()
		message(STATUS "RPATH: Absolute using ${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}")
		set(CMAKE_PROGRAM_RPATH "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}")
		set(CMAKE_LIBRARY_RPATH "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}")
	endif()
endif()

include_directories("${CMAKE_CURRENT_SOURCE_DIR}/src"
	"${CMAKE_CURRENT_SOURCE_DIR}/src/fx-gltf")

add_subdirectory("src")

option(GLITTER_BUILD_TESTS "Whether to build automated tests" ON)
if(GLITTER_BUILD_TESTS)
	add_subdirectory("test")
endif()

find_package(AStyle)
if(AStyle_FOUND)
	message(STATUS "AStyle formatting: available (build 'style' target)")
	ADD_ASTYLE_TARGET("style" "astyle.cfg"  "src/*.cpp,*.h" "test/*.cpp,*.h")
else()
	message(STATUS "AStyle formatting: disabled (AStyle not found)")
endif()

find_package(Doxygen)
if(Doxygen_FOUND)
	message(STATUS "Doxygen HTML generation: available (build 'doc' target)")
	
	# Tune Doxygen settings for C++
	set(DOXYGEN_JAVADOC_AUTOBRIEF YES)
	set(DOXYGEN_WARN_NO_PARAMDOC YES)
	
	doxygen_add_docs(doc "${CMAKE_CURRENT_SOURCE_DIR}/src")
	install(DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/html" DESTINATION "${CMAKE_INSTALL_DOCDIR}/${GEAS_VERSION}" OPTIONAL)
else()
	message(STATUS "Doxygen HTML generation: disabled (Doxygen not found)")
endif()
