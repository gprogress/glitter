/*=====================================================================================================================
 * Copyright 2022-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_KEYVALUELIST_H
#define GLITTER_KEYVALUELIST_H

#include "Types.h"

#include "Provider.h"

#include <geas/Application.h>

#include <cio/Variant.h>

#include <vector>
#include <iosfwd>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace glitter
{
	/**
	* The Key Value List is a Provider implemented as a sequence of attribute indices as keys and values stored as cio::Variants.
	*
	* In the typical use case, it is expected that only non-default values will be stored in the Key Value List and
	* default values may be obtained from the application schema or elsewhere.
	*/
	class GLITTER_ABI KeyValueList : public Provider
	{
		public:
			/** Iterator type. */
			using iterator = std::vector<cio::Variant>::iterator;

			/** Const Iterator type. */
			using const_iterator = std::vector<cio::Variant>::const_iterator;

			/**
			 * Construct a new key-value list.
			 */
			KeyValueList() noexcept;

			/**
			 * Construct a copy of an key-value list.
			 *
			 * @param in The key-value list to copy
			 */
			KeyValueList(const KeyValueList &in);

			/**
			 * Move constructor.
			 *
			 * @param in The key-value list to move
			 */
			KeyValueList(KeyValueList &&in) noexcept;

			/**
			 * Copy an key-value list.
			 *
			 * @param rhs The key-value list to copy
			 * @return this key-value list
			 */
			KeyValueList &operator=(const KeyValueList &rhs);

			/**
			 * Move assignment.
			 *
			 * @param rhs The key-value list to transfer
			 * @return this key-value list
			 */
			KeyValueList &operator=(KeyValueList &&rhs) noexcept;

			/**
			 * Destructor.
			 */
			virtual ~KeyValueList() noexcept override;

			/**
			 * Clears the key-value list.
			 * The attributes are removed and the indices are invalidated.
			 */
			virtual void clear() noexcept override;

			/**
			 * Adds a new attribute override (non-default value) into the key-value list
			 * and returns the created value object.  If an attribute value for the
			 * given attribute index already exists in this key-value list,
			 * the existing value object will be returned.
			 *
			 * @param id The attribute index
			 * @return the Variant object (possibly newly created) which can
			 * store the value for the given attribute index
			 */
			cio::Variant &defineAttribute(geas::Index id);

			/**
			 * Adds a new attribute override (non-default value) into the key-value list
			 * and returns the created value object.  If an attribute value for the
			 * given attribute already exists in this key-value list,
			 * the existing value object will be returned.
			 *
			 * @param attr The attribute
			 * @return the attribute Value object (possibly newly created) which can
			 * store the value for the given attribute index
			 */
			cio::Variant &defineAttribute(const geas::Attribute &attr);

			/**
			 * Adds new attribute override (non-default value)
			 * that is a copy of the given variant and returns the created variant object.
			 * If a value for the given attribute index already exists in this key-value list,
			 * that variant object will be returned with its contents overwritten with the new value.
			 *
			 * @param k The attribute index
			 * @param toCopy The value to copy
			 * @return the variant object (possibly newly created) which is now
			 * a copy of the input value
			 */
			cio::Variant &defineAttribute(geas::Index k, cio::Variant toCopy);

			/**
			 * Removes the attribute override for a given attribute index.
			 * This is logically equivalent to returning the attribute to its default
			 * value according to the data model.
			 *
			 * @param id The attribute index for the attribute to remove
			 * @return whether an attribute with the attribute index existed before removal
			 */
			bool removeAttributeById(geas::Index id);

			/**
			 * Removes the attribute at the given local field index in this set.
			 * This may change the field index of the remaining attribute overrides.
			 *
			 * @param pos The position to remove
			 * @return whether an attribute value existed at that position
			 */
			bool removeAttributeByPosition(std::size_t pos);

			/**
			 * Gets the number of attributes override values in this attribution set.
			 * All position indices lower than this number will reference valid
			 * attribute overrides.
			 *
			 * @return the number of attributes in this key-value list
			 */
			std::size_t getAttributeCount() const;

			/**
			 * Gets an attribute value by its local index in this key-value list.
			 * This will only return a valid attribute if the index is less than
			 * the attribute override count. Otherwise a sentinel value is returned
			 * with an invalid attribute index and type.
			 *
			 * @warning attribute overrides can be reordered by insertion and deletion so
			 * local index values for a particular attribute index may change unexpectedly.
			 * Only cache a position index for an attribute index if you know the attribute
			 * Set will not be modified.
			 *
			 * @param idx The position index
			 * @return The attribute value, which is only valid if the position index was
			 * less than the attribute override count
			*/
			const cio::Variant &getAttributeByPosition(std::size_t idx) const;

			/**
			 * Gets an attribute override from this key-value list by its index.
			 * This will only return a valid attribute if such an override was previously
			 * added by defineAttribute or was copied from another key-value list.
			 * Otherwise a sentinel value is returned with an invalid attribute index and type.
			 *
			 * @param id The attribute index
			 * @return The attribute value, which is only valid if an attribute override
			 * existed for the specified attribute index
			 */
			const cio::Variant &getAttributeById(geas::Index id) const;

			/**
			 * Gets the position of the attribute value with the given attribute index.
			 *
			 * @param id The index
			 * @return The position of the attribute value, or INVALID_POSITION if not found
			 */
			std::size_t getAttributePosition(geas::Index id) const;

			/**
			 * Removes all overridden attribute values in this key-value list.
			 * Logically this is equivalent to returning all attributes for the defined
			 * Concept to their default value. Unlike clear() this does not modify the IDs.
			 *
			 * @post this->getAttributeCount() returns 0
			 */
			void clearAllValues();

			/**
			 * Gets the memory use of this key-value list.
			 *
			 * @return the memory use of this key-value list
			 */
			std::size_t getMemoryUse() const;

			/**
			 * Get the start iterator.
			 *
			 * @return the start iterator
			 */
			KeyValueList::iterator begin();

			/**
			 * Get the end iterator.
			 *
			 * @return the end iterator
			 */
			KeyValueList::iterator end();

			/**
			 * Get the start iterator.
			 *
			 * @return the start iterator
			 */
			KeyValueList::const_iterator begin() const;

			/**
			 * Get the end iterator.
			 *
			 * @return the end iterator
			 */
			KeyValueList::const_iterator end() const;

			/**
			 * Erases an attribute value by iterator.
			 *
			 * @param ii The iterator
			 * @return the next iterater after the one erased
			 */
			KeyValueList::iterator erase(KeyValueList::iterator ii);

			/**
			 * Inserts an attribute value.
			 *
			 * @param k The attribute index
			 * @param v The value
			 * @return a pair containing the iterator to the inserted value and whether it was a new value
			 */
			std::pair<KeyValueList::iterator, bool> insert(geas::Index k, cio::Variant v);

			/**
			 * Gets the number of attribute values.
			 *
			 * @return the number of attribute values
			 */
			virtual geas::Index size() const override;

			/**
			 * Gets whether no attribute values are defined.
			 *
			 * @return whether the key-value list is empty
			 */
			bool empty() const;

			/**
			 * Tests if two KeyValueList objects are identical.
			 * This verifies that they have the same key-value list index,
			 * reference the same Concept index and have the same attribute overrides
			 * in the exact same order.
			 *
			 * @param other The object to compare against this one
			 * @return True if the objects are identical, false otherwise
			 */
			bool operator==(const KeyValueList &other) const;

			/**
			 * Tests if two KeyValueList object are different.
			 * @see KeyValueList::operator==
			 *
			 * @param other The object to compare against this one
			 * @return True if the objects are different, false otherwise
			 */
			bool operator!=(const KeyValueList &other) const;

		private:
			/** An invalid Variant that can be returned for missing read-only values. */
			static const cio::Variant sInvalidValue;

			/** attribute IDs. */
			std::vector<geas::Index> mKeys;

			/** attribute values. */
			std::vector<cio::Variant> mAttributes;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
