/*=====================================================================================================================
 * Copyright 2022-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_REPOSITORY_H
#define GLITTER_REPOSITORY_H

#include "Types.h"

#include "Catalog.h"

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace glitter
{
	/**
	 * The Repository represents some grouping of Collections with a common purpose.
	 *
	 * Each Collection may implement a different Catalog and Application Schema.
	 */
	class GLITTER_ABI Repository : public geas::Definition
	{
		public:
			/**
			 * Construct an empty repository.
			 */
			Repository() noexcept;

			/**
			 * Construct a deep copy of a repository.
			 * All resources will be copied fully.
			 * The two repositories will point to the same catalog.
			 *
			 * @param in The repository to copy
			 */
			Repository(const Repository &in);

			/**
			 * Construct a repository by transferring the contents of an existing repository.
			 *
			 * @param in The repository to move
			 */
			Repository(Repository &&in) noexcept;

			/**
			 * Performs a deep copy of a repository.
			 * All resources will be copied fully.
			 * The two repositories will point to the same catalog.
			 *
			 * @param in The repository to copy
			 * @return this repository
			 */
			Repository &operator=(const Repository &in);

			/**
			 * Transfers the contents of an existing repository.
			 *
			 * @param in The repository to move
			 * @return this repository
			 */
			Repository &operator=(Repository &&in) noexcept;

			/**
			 * Destructor. Deallocates all contained resources.
			 */
			virtual ~Repository() noexcept override;

			/**
			 * Gets the runtime metaclass for this object instance.
			 *
			 * @return the runtime metaclass for this object
			 */
			virtual const cio::Metaclass &getMetaclass() const noexcept override;

			/**
			 * Clears all contained resources and sets the referenced catalog to null.
			 */
			virtual void clear() noexcept override;

			/**
			 * Gets the full list of collections in this repository.
			 *
			 * @return the list of all collections in this repository
			 */
			geas::Dictionary<Collection> &getCollections() noexcept;

			/**
			 * Gets the full list of collections in this repository.
			 *
			 * @return the list of all collections in this repository
			 */
			const geas::Dictionary<Collection> &getCollections() const noexcept;

			/**
			 * Sets the full list of collections in this repository.
			 *
			 * @param collections the full collection list
			 */
			void setCollections(geas::Dictionary<Collection> collections) noexcept;

			/**
			 * Adds a collection to this repository.
			 *
			 * @param collection The collection to add
			 */
			void addCollection(Collection collection);

			/**
			 * Clears all collections on this repository.
			 */
			void clearCollections() noexcept;

		private:
			/** The metaclass for this class. */
			static cio::Class<Repository> sMetaclass;

			/** The set of all resources. */
			geas::Dictionary<Collection> mCollections;
	};
}

namespace geas
{
	/** Instantiation of geas::Dictionary<geas::Repository> for other containers to use. */
	extern template class GLITTER_TEMPLATE geas::Dictionary<glitter::Repository>;
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
