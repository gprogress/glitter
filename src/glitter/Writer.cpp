/*=====================================================================================================================
 * Copyright 2022-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "Writer.h"

#include <cio/Class.h>

namespace glitter
{
	cio::Class<Writer> Writer::sMetaclass("glitter::Writer");

	Writer::Writer() noexcept
	{
		// nothing more to do
	}

	Writer::~Writer() noexcept = default;

	const cio::Metaclass &Writer::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	void Writer::storeRepository(const Repository &repo, const cio::Path &path, const cio::ProtocolFactory &protocols)
	{
		// nothing to do in base class
	}

	void Writer::storeCatalog(const Catalog &catalog, const cio::Path &path, const cio::ProtocolFactory &protocols)
	{
		// nothing to do in base class
	}

	void Writer::storeCollection(const Collection &collection, const cio::Path &path, const cio::ProtocolFactory &protocols)
	{
		// nothign to do in base class
	}

	void Writer::storeNode(const Node &res, const cio::Path &path, const cio::ProtocolFactory &protocols)
	{
		// nothing to do in base class
	}
}
