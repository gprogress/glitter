/*=====================================================================================================================
 * Copyright 2022-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_ATTRIBUTETYPE_H
#define GLITTER_ATTRIBUTETYPE_H

#include "Types.h"

namespace glitter
{
	/**
	 * The Geodex Model Attribute Type enumerates the list of standard vertex attributes commonly used
	 * in 3D modeling to make it convenient to read, process, and write conventional model formats.
	 *
	 * These standard attributes can be linked to more general schema definitions, and also can suggest
	 * their own default definitions if you need one.
	 *
	 * Non-standard vertex attributes can still be used with the Attribute::Unknown type.
	 */
	enum class AttributeType : std::uint8_t
	{
		// General attributes

		/** No vertex attribute defined. */
		None,

		/** Vertex attribute is defined but not one of the standard attributes. */
		Unknown,

		/** Vertex attribute is defined as a semantic application schema property not otherwise specified in this list. */
		Semantic,

		// Basic 3D model vertex attributes

		/** Vertex attribute for spatial position. */
		Position,

		/** Vertex attribute for normal vector. */
		Normal,

		/** Vertex attribute for tangent vector. */
		Tangent,

		/** Vertex attribute for RGBA color. */
		Color,

		/** Vertex attribute for grayscale luminance only. */
		Luminance,

		/** Vertex attribute for opacity / alpha only. */
		Opacity,

		/** Vertex attribute for texture coordinate. */
		Texture,

		// Vertex references

		/** Index Attribute for vertex index selecting a vertex from a buffer. */
		Index,

		/** geas::Index Attribute for specifying count of vertex indices selected from a buffer. */
		Count,

		/** geas::Index Attribute for specifying index of which buffer to use for a vertex value. */
		Buffer,

		// Schema references

		/** Attribute for an entity type reference. */
		Entity,

		/** Attribute for a geometry type refrence. */
		Geometry,

		/** Attribute for an attribute type reference. */
		Attribute,

		/** Attribute for a projection type reference. */
		Projection,

		// Object references

		/** Attribute for a repository reference. */
		Repository,

		/** Attribute for a collection reference. */
		Collection,

		/** Attribute for a node reference. */
		Node,

		/** Attribute for an object instance reference (aka feature ID). */
		Object
	};

	/**
	 * Gets the default attribute schema for the given vertex attribute.
	 *
	 * @param type The vertex attribute type
	 * @return the default attribute schema
	 */
	const geas::Attribute *getDefaultAttribute(AttributeType type) noexcept;

	/**
	 * Gets the default datatype schema for the given vertex attribute.
	 *
	 * @param type The vertex attribute type
	 * @return the default datatype schema
	 */
	const geas::Attribute *getDefaultDatatype(AttributeType type) noexcept;
}

#endif
