/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_LOCATION_H
#define GLITTER_LOCATION_H

#include "Types.h"

#include "Degree.h"

#include <cio/Interval.h>

namespace glitter
{
	/**
	 * The Location class represents a simple geographic location specified by decimal degree longitude, latitude, and altitude in meters.
	 */
	class Location
	{
		public:
			/**
			 * Gets the standard longitude interval.
			 * This is the right-open interval [-180, +180).
			 *
			 * @return the longitude interval range
			 */
			static inline cio::RightOpenInterval<Degree<double>> longitudeRange() noexcept;

			/**
			 * Gets the standard latitude interval.
			 * This is the closed interval [-90, +90].
			 *
			 * @return the latitude interval range
			 */
			static inline cio::ClosedInterval<Degree<double>> latitudeRange() noexcept;

			/** The longitude in decimal degrees. */
			Degree<double> longitude;

			/** The latitude in decimal degrees. */
			Degree<double> latitude;

			/** The altitude in meters. */
			double altitude;

			/**
			 * Construct a location with all coordinates set to unknown (NaN).
			 */
			inline Location() noexcept;

			/**
			 * Clears a location so all coordinates are set to unknown (NaN).
			 */
			inline void clear() noexcept;

			/**
			 * Checks whether the location is valid.
			 * This is true if at least the longitude and latitude are set.
			 */
			inline explicit operator bool() const noexcept;
	};
}

/* Inline implementation */

namespace glitter
{
	inline cio::RightOpenInterval<Degree<double>> Location::longitudeRange() noexcept
	{
		return cio::RightOpenInterval<Degree<double>>(-180, +180);
	}

	inline cio::ClosedInterval<Degree<double>> Location::latitudeRange() noexcept
	{
		return cio::ClosedInterval<Degree<double>>(-90, +90);
	}

	inline Location::Location() noexcept :
		longitude(NAN),
		latitude(NAN),
		altitude(NAN)
	{
		// nothing more to do
	}

	inline void Location::clear() noexcept
	{
		this->longitude = NAN;
		this->latitude = NAN;
		this->altitude = NAN;
	}

	inline Location::operator bool() const noexcept
	{
		return std::isfinite(this->longitude.value) && std::isfinite(this->latitude.value);
	}
}

#endif