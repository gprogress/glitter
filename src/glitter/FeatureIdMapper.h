/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_FEATUREIDMAPPER_H
#define GLITTER_FEATUREIDMAPPER_H

#include "Types.h"

#include "Entity.h"
#include "FeatureIdMap.h"

#include <array>
#include <vector>
#include <iostream>
#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace glitter
{
	/**
	 * The Feature ID Mapper is a convenience class for getting feature IDs for vertices, vertex indices, and higher level primitives.
	 */
	class GLITTER_ABI FeatureIdMapper
	{
		public:
			/**
			 * Construct an empty feature mapper.
			 * This will always return feature ID 0 for any query.
			 */
			FeatureIdMapper();

			/**
			 * Provides the given mapping settings to determine the feature table, offset, constant, and attribute accessor.
			 * 
			 * @param map The map settings
			 */
			inline void setMap(const FeatureIdMap &map);

			/**
			 * Gets the mapping settings to determine the feature table, offset, constant, and attribute accessor.
			 *
			 * @return The map settings
			 */
			inline const FeatureIdMap &getMap() const;

			inline std::uint32_t size() const;

			void resize(std::uint32_t count);
			
			inline void bindSchema(std::shared_ptr<geas::Application> schema);

			inline std::shared_ptr<geas::Application> getSchema();

			inline void bindNode(Node *Node);

			inline Node *getNode();

			void bindVertexIndices(Scalar<std::uint32_t, void> accessor);

			void unbindVertexIndices();

			inline Scalar<std::uint32_t, void> &getVertexIndices();

			void bindFeatureIds(Scalar<std::uint32_t, void> accessor);

			inline Scalar<std::uint32_t, void> &getFeatureIds();

			void unbindFeatureIds();

			inline std::uint32_t getVertexFeatureId(std::uint32_t v) const;

			/**
			* Sets the constant and divisor value to calculate the fid in getVertexFeatureId
			* 
			* @param constant The offset
			* @param divisor The number of repetitions vertices that have the same feature ID
			*/
			inline void setProceduralValues(std::uint32_t constant, std::uint32_t divisor);

			std::array<std::uint32_t, 3> getTriangleFeatureIds(std::uint32_t t) const;

			std::vector<std::uint32_t> getPrimitiveSetFeatureIds() const;

			std::uint32_t getTriangleFeatureId(std::uint32_t t) const;

			std::uint32_t getPrimitiveSetFeatureId() const;

			Entity getVertexEntity(std::uint32_t v) const;

			Entity getTriangleEntity(std::uint32_t t) const;

			std::array<Entity, 3> getTriangleEntities(std::uint32_t t) const;

			Entity getPrimitiveSetEntity() const;

			std::vector<Entity> getPrimitiveSetEntities() const;

			explicit operator bool() const;

		private:
			static std::uint32_t sConstantZero;
			FeatureIdMap mMap;
			std::shared_ptr<geas::Application> mSchema;
			Node *mNode;
			Scalar<std::uint32_t, void> mFeatureIds;
			Scalar<std::uint32_t, void> mVertexIndices;
			std::uint32_t mVertexMultiplier;
	};
}

/* inline implementation */

namespace glitter
{
	inline void FeatureIdMapper::bindSchema(std::shared_ptr<geas::Application> schema)
	{
		mSchema = schema;
	}

	inline std::shared_ptr<geas::Application> FeatureIdMapper::getSchema()
	{
		return mSchema;
	}

	inline void FeatureIdMapper::bindNode(Node *provider)
	{
		mNode = provider;
	}

	inline Node *FeatureIdMapper::getNode()
	{
		return mNode;
	}

	inline void FeatureIdMapper::setMap(const FeatureIdMap &map)
	{
		mMap = map;
	}

	inline const FeatureIdMap &FeatureIdMapper::getMap() const
	{
		return mMap;
	}

	inline std::uint32_t FeatureIdMapper::size() const
	{
		return mVertexIndices.size();
	}

	inline Scalar<std::uint32_t, void> &FeatureIdMapper::getVertexIndices()
	{
		return mVertexIndices;
	}

	inline Scalar<std::uint32_t, void> &FeatureIdMapper::getFeatureIds()
	{
		return mFeatureIds;
	}

	inline void FeatureIdMapper::setProceduralValues(std::uint32_t c, std::uint32_t d)
	{
		mMap.offset = c;
		mMap.divisor = d;
	}

	inline std::uint32_t FeatureIdMapper::getVertexFeatureId(std::uint32_t v) const
	{	
		std::uint32_t idx = mVertexIndices.get(v) + mVertexMultiplier * v;
		std::uint32_t base = mFeatureIds.get(idx);
		return base + mMap.procedural(v);
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
