/*=====================================================================================================================
 * Copyright 2022-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_LASREADER_H
#define GLITTER_LASREADER_H

#include "Types.h"

#include "ModelReader.h"

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace glitter
{
	/**
	 * The Geodex Model LAS Reader refines the Geodex Model Reader to load point clouds defined in the LAS format.
	 */
	class GLITTER_ABI LasReader : public ModelReader
	{
		public:
			/**
			 * Gets the metaclass for this class.
			 *
			 * @return the metaclass for this class
			 */
			static const cio::Class<LasReader> &getDeclaredMetaclass() noexcept;

			/** User text for projection node in VLR record. */
			static const cio::FixedText<16> VLR_PROJECTION_USER;

			/** Type code for OGC WKT projection node in VLR record. */
			static const std::uint16_t VLR_PROJECTION_WKT_TYPE;

			/** Type code for GeoTIFF projection node in VLR record. */
			static const std::uint16_t VLR_PROJECTION_TIFF_TYPE;

			/**
			 * Default constructor.
			 */
			LasReader() noexcept;

			/**
			 * Constructs a reader with the given profile.
			 * This does NOT call onProfileChanged() since virtual methods cannot be used in a constructor.
			 *
			 * @param profile The profile to use
			 */
			explicit LasReader(std::shared_ptr<Profile> profile) noexcept;

			/**
			 * Copy constructor.
			 *
			 * @param in The reader to copy
			 */
			LasReader(const LasReader &in);

			/**
			 * Move constructor.
			 *
			 * @param in The reader to move
			 */
			LasReader(LasReader &&in) noexcept;

			/**
			 * Copy assignment.
			 * This does NOT call onProfileChanged() since subclasses are expected to override it.
			 *
			 * @param in The reader to copy
			 * @return this reader
			 */
			LasReader &operator=(const LasReader &in);

			/**
			 * Move assignment.
			 * This does NOT call onProfileChanged() since subclasses are expected to override it.
			 *
			 * @param in The reader to move
			 * @return this reader
			 */
			LasReader &operator=(LasReader &&in) noexcept;

			/**
			 * Destructor.
			 */
			virtual ~LasReader() noexcept override;

			/**
			 * Clears the reader state.
			 * This does NOT call onProfileChanged() since subclasses are expected to override it.
			 */
			virtual void clear() noexcept override;

			/**
			 * Gets the runtime metaclass for this object instance.
			 *
			 * @return the runtime metaclass for this object
			 */
			virtual const cio::Metaclass &getMetaclass() const noexcept override;

			/**
			 * Loads a catalog from the given input stream.
			 *
			 * The LAS Reader parses out the header and sets up the application schema to contain
			 * the necessary concepts to represent the LAS file.
			 *
			 * The input stream is left at the start of the actual point cloud data.
			 *
			 * @param catalog the catalog to update
			 * @param input The input to read
			 */
			virtual void loadCatalogFromInput(Catalog &catalog, cio::Input &input) override;;

			/**
			 * Loads a collection from the given input stream.
			 *
			 * The LAS Reader parses out the header, sets up the collection's catalog, then sets up a
			 * single Node to receive the actual point cloud data.
			 *
			 * @param collection The collection to update
			 * @param input The input to read
			 */
			virtual void loadCollectionFromInput(Collection &collection, cio::Input &input) override;

			/**
			 * Loads a node from the given input stream.
			 *
			 * The LAS Reader parses out the header, sets up a one-off application schema, then
			 * puts the point cloud data on the node.
			 *
			 * @param node the node to update
			 * @param input The input to load
			 */
			virtual void loadNodeFromInput(Node &node, cio::Input &input) override;

			/**
			 * Decodes the point record in LAS point format #3 to set the attributes for the given point object
			 * in the data provider.
			 *
			 * @param object The object index to set
			 * @param provider The data provider
			 * @param record The point record in format #3
			 */
			void decodePointFormat3(geas::Index object, Provider &provider, cio::Buffer &record);

		private:
			/** The metaclass for this class. */
			static cio::Class<LasReader> sMetaclass;

			/** Current LAS format type. */
			unsigned mLasFormat;

			/** Schema currently being processed. */
			std::shared_ptr<Catalog> mCatalog;

			/** Node currently being processed. */
			Node *mNode;

			/** Data table currently being processed. */
			std::shared_ptr<Table> mTable;

			/** Projection read from header, if any. */
			const geas::Projection *mProjection;

			/** Current number of bytes processed from ongoing input. */
			std::uint64_t mCurrentOffset;

			/** Byte offset to point data. */
			std::uint64_t mPointOffset;

			/** Total number of points. */
			std::uint64_t mPointCount;

			/** Size of each point record. */
			std::uint16_t mPointLength;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
