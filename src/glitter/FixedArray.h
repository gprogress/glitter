/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_FIXEDARRAY_H
#define GLITTER_FIXEDARRAY_H

#include "Scalar.h"

#include <cio/Chunk.h>
#include <cio/Types.h>

namespace glitter
{
		template <typename T, typename U /*= void*/>
		class FixedArray
		{
		public:
			/**
			* Constructs a fixed array of a different type than the data type in the given fixed array and
			* uses the same referenced data of the given fixed array.
			*
			* @param in The given fixed array that contains data and values needed to construct new fixed array.
			* @return A fixed array that references the same data as the given fixed array.
			*/
			template <typename V, typename W>
			static inline FixedArray<T, U> reference(const FixedArray<V, W>& in);

			/**
			* Constructs a fixed array of a different type with the values in the given fixed array and
			* uses the same referenced data of the given fixed array.  The given scalar fixed array has a given
			* number of elements.  Default is 1.
			*
			* @param in The given fixed array that contains data and values needed to construct new fixed array.
			* @param columns The number of elements to consider from the given scalar fixed array.  Default
			* is 1.
			* @return A fixed array that references the same data as the given fixed array.
			*/
			template <typename V, typename W>
			static inline FixedArray<T, U> reference(const Scalar<V, W>& in, std::size_t columns = 1);

			/**
			* Get a subset of the overall data of the fixed array.
			*
			* @param in The given fixed array that contains data.
			* @param offset The location of the start of the data you wish to retrieve.
			* @param count  The number of elements (in bytes) of data to retrieve.
			* @return A fixed array that contains a subset of the overall data.
			*/
			template <typename V, typename W>
			static inline FixedArray<T, U> slice(FixedArray<V, W>& in, std::size_t offset, std::size_t count);

			/**
			* Get a subset of the overall data of the fixed array.
			*
			* @param in The given fixed array that contains data.
			* @param offset The location of the start of the data you wish to retrieve.
			* @param count  The number of elements (in bytes) of data to retrieve.
			* @return A fixed array that contains a subset of the overall data.
			*/
			template <typename V, typename W>
			static inline FixedArray<const T, const U> slice(const FixedArray<V, W>& in, std::size_t offset, std::size_t count);

			/**
			* Construct a new fixed array with size, offset and element count of zero, stride, element stride and element size as the
			* size of the data type.
			*/
			inline FixedArray() noexcept;

			/**
			* Construct a fixed array with the given data, size is the number of
			* logical data elements in the data, element count is 1, offset is set to zero.  Stride
			* element stride and element size are the size of the data type of the fixed array.

			* @param data The data that the fixed array is going to parse and discern.
			*/
			inline explicit FixedArray(cio::Chunk<U> data);

			/**
			* Construct a fixed array with the given data, number of elements, size and offset.
			*
			* @param data The data that the fixed array is going to parse.
			* @param columns The number of columns in each row.
			* @param count The number of sets of elements (or indices in the data).
			* @param offset The number of bytes from the beginning of the data to begin reading the data, default of zero.
			*/
			inline FixedArray(cio::Chunk<U> data, std::size_t columns, std::size_t count, std::size_t offset = 0);

			/**
			* Construct a fixed array with the given data, number of elements, size, offset, stride and elementSize.
			*
			* @param data The data that the fixed array is going to parse.
			* @param columns The number of elements in each row.
			* @param count The number of sets of elements (or indices in the data).
			* @param offset The number of bytes from the beginning of the data to begin reading the data, default of zero.
			* @param stride The number of bytes between each index of data.
			* @param elementSize The number of bytes of each element.  Default is the size of the templated type T.
			*/
			inline FixedArray(cio::Chunk<U> data, std::size_t columns, std::size_t count, std::size_t offset, std::size_t stride, std::size_t elementSize = sizeof(T));

			/**
			* Construct a fixed array with the given data, number of elements, size, offset, stride and elementSize.
			*
			* @param data The data that the fixed array is going to parse.
			* @param columns The number of elements in each row.
			* @param count The number of sets of elements (or indices in the data).
			* @param offset The number of bytes from the beginning of the data to begin reading the data, default of zero.
			* @param stride The number of bytes between each index of data.
			* @param elementSize The number of bytes of each element.
			* @param elementStride The number of bytes between each element.
			*/
			inline FixedArray(cio::Chunk<U> data, std::size_t columns, std::size_t count, std::size_t offset, std::size_t stride, std::size_t elementSize, std::size_t elementStride);

			/**
			* Construct a default, compiler generated, read-only copy of the fixed array.
			*
			* @param in The read-only fixed array to copy.
			*/
			inline FixedArray(const FixedArray<T, U>& in) = default;

			/**
			* Move the data and copy the attributes into a new instance of this class.
			*
			* @param in The fixed array to copy.
			*/
			inline FixedArray(FixedArray<T, U>&& in) noexcept;

			/**
			* Copy the data and attributes into a new instance of this class.
			*
			* @param in The read-only fixed array to copy.
			*/
			template <typename V, typename W>
			inline FixedArray(const FixedArray<V, W>& in);

			/**
			* Move the data and copy the attributes into a new instance of this class.
			*
			* @param in The fixed array to copy.
			*/
			template <typename V, typename W>
			inline FixedArray(FixedArray<V, W>&& in) noexcept;

			/**
			* Create a read-only copy of the given fixed array.
			*
			* @param in The given fixed array to copy.
			*/
			inline FixedArray<T, U>& operator=(const FixedArray<T, U>& in) = default;

			/**
			* Constructs copy of the given fixed array.
			*
			*@param in The given fixed array of a templated type to copy.
			*/
			inline FixedArray<T, U>& operator=(FixedArray<T, U>&& in) noexcept;

			/**
			* Default destructor.
			*/
			inline ~FixedArray() noexcept = default;

			/**
			* Set the data in the fixed array to the data given.  Inizializes the attributes,
			* size is the number of logical data elements and offset is zero, stride and
			* element size are set to the size of the data type of the fixed array.
			*
			* @param data The data for the fixed array to parse.
			*/
			inline void bind(cio::Chunk<U> data);

			/**
			* Retrieve a single value from the given location index in
			* the underlying data.
			*
			* @param idx The location of the desired value.
			* @return The value at the desired location.
			*/
			inline cio::Value<T> get(std::size_t idx, std::size_t element) const;

			/**
			* Sets the given value at the given data location in the
			* underlying data structure.
			*
			* @param idx The index at which to place the given value.
			* @param value The value to insert at the given location.
			*/
			inline void set(std::size_t idx, std::size_t element, cio::Value<T> value);

			/**
			* Sets the given value at the given data location in the
			* underlying data structure.
			*
			* @param idx The index at which to place the given value.
			* @param value The value to insert at the given location.
			* @param elementSize the size of the value.
			*/
			inline void set(std::size_t idx, std::size_t element, cio::Value<T> value, std::size_t elementSize);

			/**
			* Get the offset, the location to begin reading calculated from the beginning
			* of the underlying data.
			*
			* @return The offset in bytes
			*/
			inline std::size_t getOffset() const;

			/**
			* Sets the offset, or the location from the beginning of the
			* underlying data array to begin parsing data.
			*
			* @param offset The size, in bytes, of the offset.
			*/
			inline void setOffset(std::size_t offset);

			/**
			* Gets the number of bytes from one index to the next.
			*
			* @return The size, in bytes, between indices.
			*/
			inline std::size_t getStride() const;

			/**
			* Set the size, in bytes, from one data index to the next.
			*
			* @param stride The number of bytes between one index and the next.
			*/
			inline void setStride(std::size_t stride);

			/**
			* Gets the size of the number of elements in each row of the underlying data.
			*
			* @return The number of elements per row.
			*/
			inline std::size_t getColumnCount() const;

			/**
			* Sets the size of the number of elements in each row of the underlying data.
			*
			* @param count The number of elements per row.
			*/
			inline void setColumnCount(std::size_t count);

			/**
			* Gets the size of the number of rows of the underlying data.
			*
			* @return The number of rows in the data.
			*/
			inline std::size_t getRowCount() const;

			/**
			* Sets the size of the number of rows of the underlying data.
			*
			* @param count The number of rows.
			*/
			inline void setRowCount(std::size_t count);

			/**
			* Gets the size of the elements that comprise the underlying data.
			*
			* @return The size of the data types that comprise the data.
			*/
			inline std::size_t getElementSize() const;

			/**
			* Sets the size of the elements within the data.
			*
			* @param count The size of the data type within the underlying data.
			*/
			inline void setElementSize(std::size_t count);

			/**
			* Gets the number of bytes from one element to the next.
			*
			* @return The size, in bytes, between elements.
			*/
			inline std::size_t getElementStride() const;

			/**
			* Set the size, in bytes, from one data element to the next.
			*
			* @param count The number of bytes between one element and the next.
			*/
			inline void setElementStride(std::size_t count);

			/**
			* Get the number of elements in the data array.
			*
			* @return The number of elements.
			*/
			inline std::size_t size() const;

			/**
			* Resize the fixed array.
			*
			* @param length  The desired size of the underlying data.
			*/
			inline void resize(std::size_t length);

			/**
			* Clears the underlying data, resets the size, offset, element size, element
			* count, element stride and stride.
			*/
			inline void clear();

			/**
			* Get the underlying chunk of data.
			*
			* @return The data the fixed array is parsing.
			*/
			inline cio::Chunk<U>& getData();

			/**
			* Gets a read-only reference to the underlying chunk of data.
			*
			* @return the read-only reference to the underlying data.
			*/
			inline const cio::Chunk<U>& getData() const;

			/**
			* Sets the underlying data for the fixed array.
			*
			* @param data The data to be moved to be owned by the fixed array.
			*/
			inline void setData(cio::Chunk<U> data);

			/**
			* Gets a pointer to the underlying data.
			*
			* @return a pointer to the underlying data of templated type U.
			*/
			inline U* data();

			/**
			* Gets a const pointer to the underlying data.
			*
			* @return a const pointer to the data in the fixed array.
			*/
			inline const U* data() const;


			/**
			* Returns a scalar fixed array to a given data element
			*
			* @param element The index of the element to access.
			* @return a scalar fixed array to the given element.
			*/
			Scalar<T, U> select(std::size_t element);

			/**
			* Returns a read-only scalar fixed array to a given data element
			*
			* @param element The index of the element to access.
			* @return a read-only scalar fixed array to the given element.
			*/
			Scalar<const T, const U> select(std::size_t element) const;

		private:
			/*  The data that the fixed array is interested in parsing.  */
			cio::Chunk<U> mData;

			/** The total number of elements in the entire fixed array */
			std::size_t mSize;

			/** The number of vectors in the vector fixed array */  //was size
			std::size_t mRows;

			/** The number of elements per vector or the number of columns per row */  //was mElementCount
			std::size_t mColumns;

			/*  The offset in bytes from the beginning of the data */
			std::size_t mOffset;

			/** The distance between vectors, generally the number of columns times the element size*/
			std::size_t mStride;

			/** The size of each element, generally the size of the data type of the element */
			std::size_t mElementSize;

			/** The distance between data elements  */
			std::size_t mElementStride;
		};
}

/* Inline implementation */
namespace glitter
{
		template <typename T, typename U>
		template <typename V, typename W>
		inline FixedArray<T, U> FixedArray<T, U>::reference(const FixedArray<V, W>& in)
		{
			return FixedArray<T, U>(in.getData().reference(), in.getColumnCount(), in.getRowCount(), in.getOffset(), in.getStride(),
				in.getElementSize(), in.getElementStride());
		}

		template <typename T, typename U>
		template <typename V, typename W>
		inline FixedArray<T, U> FixedArray<T, U>::reference(const Scalar<V, W>& in, std::size_t columns)
		{
			return FixedArray<T, U>(in.getData().reference(), columns, in.size() / columns, in.getOffset(), in.getStride() * columns,
				in.getElementSize());
		}

		template <typename T, typename U>
		template <typename V, typename W>
		inline FixedArray<T, U> FixedArray<T, U>::slice(FixedArray<V, W>& in, std::size_t offset, std::size_t rows)
		{
			return FixedArray<T, U>(in.getData().reference(), in.getColumnCount(), rows, in.getOffset() + offset * in.getStride(),
				in.getStride(), in.getElementSize(), in.getElementStride());
		}

		template <typename T, typename U>
		template <typename V, typename W>
		inline FixedArray<const T, const U> FixedArray<T, U>::slice(const FixedArray<V, W>& in, std::size_t offset, std::size_t rows)
		{
			return FixedArray<const T, const U>(in.getData().reference(), in.getColumnCount(), rows, in.getOffset() + offset * in.getStride(), in.getStride(),
				in.getElementSize(), in.getElementStride());
		}

		template <typename T, typename U>
		inline FixedArray<T, U>::FixedArray() noexcept :
			mSize(0),
			mRows(0),
			mOffset(0),
			mColumns(0),
			mStride(sizeof(cio::Value<T>)),
			mElementSize(sizeof(cio::Value<T>)),
			mElementStride(mElementSize)
		{
			// nothing more to do
		}

		template <typename T, typename U>
		inline FixedArray<T, U>::FixedArray(cio::Chunk<U> data) :
			mData(std::move(data)),
			mSize(1),
			mRows(1),
			mOffset(0),
			mColumns(1),
			mStride(sizeof(cio::Value<T>)),
			mElementSize(sizeof(cio::Value<T>)),
			mElementStride(mElementSize)
		{
			// nothing more to do
		}

		template <typename T, typename U>
		inline FixedArray<T, U>::FixedArray(cio::Chunk<U> data, std::size_t columns, std::size_t rows, std::size_t offset) :
			mData(std::move(data)),
			mSize(rows* columns),
			mRows(rows),
			mOffset(offset),
			mColumns(columns),
			mStride(sizeof(cio::Value<T>)* columns),
			mElementSize(sizeof(cio::Value<T>)),
			mElementStride(sizeof(cio::Value<T>))
		{
			// nothing more to do
		}

		template <typename T, typename U>
		inline FixedArray<T, U>::FixedArray(cio::Chunk<U> data, std::size_t columns, std::size_t rows, std::size_t offset, std::size_t stride, std::size_t elementSize) :
			mData(std::move(data)),
			mSize(rows* columns),
			mRows(rows),
			mOffset(offset),
			mColumns(columns),
			mStride(stride),
			mElementSize(elementSize),
			mElementStride(mElementSize)
		{
			// nothing more to do
		}


		template <typename T, typename U>
		inline FixedArray<T, U>::FixedArray(cio::Chunk<U> data, std::size_t columns, std::size_t rows, std::size_t offset, std::size_t stride, std::size_t elementSize, std::size_t elementStride) :
			mData(std::move(data)),
			mSize(rows* columns),
			mRows(rows),
			mOffset(offset),
			mColumns(columns),
			mStride(stride),
			mElementSize(elementSize),
			mElementStride(elementStride)
		{
			// nothing more to do
		}

		template <typename T, typename U>
		inline FixedArray<T, U>::FixedArray(FixedArray<T, U>&& in) noexcept :
			mData(std::move(in.mData)),
			mSize(in.mSize),
			mRows(in.mRows),
			mOffset(in.mOffset),
			mColumns(in.mColumns),
			mStride(in.mStride),
			mElementSize(in.mElementSize),
			mElementStride(in.mElementStride)
		{
			in.clear();
		}

		template <typename T, typename U>
		template <typename V, typename W>
		inline FixedArray<T, U>::FixedArray(const FixedArray<V, W>& in) :
			mData(in.getData()),
			mSize(in.size()),
			mRows(in.getRowCount()),
			mOffset(in.getOffset()),
			mColumns(in.getColumnCount()),
			mStride(in.getStride()),
			mElementSize(in.getElementSize()),
			mElementStride(in.getElementStride())
		{
			// nothing more to do
		}

		template <typename T, typename U>
		template <typename V, typename W>
		inline FixedArray<T, U>::FixedArray(FixedArray<V, W>&& in) noexcept :
			mData(std::move(in.getData())),
			mSize(in.size()),
			mRows(in.getRowCount()),
			mOffset(in.getOffset()),
			mColumns(in.getColumnCount()),
			mStride(in.getStride()),
			mElementSize(in.getElementSize()),
			mElementStride(in.getElementStride())
		{
			in.clear();
		}

		template <typename T, typename U>
		inline FixedArray<T, U>& FixedArray<T, U>::operator=(FixedArray<T, U>&& in) noexcept
		{
			if (this != &in)
			{
				mData = std::move(in.mData);
				mSize = in.mSize;
				mRows = in.mRows;
				mOffset = in.mOffset;
				mColumns = in.mColumns;
				mStride = in.mStride;
				mElementSize = in.mElementSize;
				mElementStride = in.mElementStride;
				in.clear();
			}

			return *this;
		}

		template <typename T, typename U>
		inline void FixedArray<T, U>::bind(cio::Chunk<U> data)
		{
			mData = std::move(data);
			mSize = 1;
			mRows = 1;
			mOffset = 0;
			mColumns = 1;
			mStride = sizeof(cio::Value<T>);
			mElementStride = sizeof(cio::Value<T>);
			mElementSize = sizeof(cio::Value<T>);
		}

		template <typename T, typename U>
		inline cio::Value<T> FixedArray<T, U>::get(std::size_t idx, std::size_t element_idx) const
		{
			T result = T();
			std::size_t loc = mOffset + mStride * idx + mElementStride * element_idx;
			std::size_t chunk = loc / sizeof(cio::Value<U>);
			std::size_t off = loc % sizeof(cio::Value<U>);
			const U* data = static_cast<const cio::Value<U> *>(mData.data()) + chunk;
			std::memcpy(&result, reinterpret_cast<const cio::Value<U>*>(data) + off, mElementSize);
			return result;
		}

		template <typename T, typename U>
		inline void FixedArray<T, U>::set(std::size_t idx, std::size_t element, cio::Value<T> value)
		{
			std::size_t loc = mOffset + mStride * idx + mElementStride * element;
			std::size_t chunk = loc / sizeof(cio::Value<U>);
			std::size_t off = loc % sizeof(cio::Value<U>);
			U* data = static_cast<cio::Value<U> *>(mData.data()) + chunk;
			std::memcpy(reinterpret_cast<cio::Value<U>*>(data) + off, &value, mElementSize);
		}

        template <typename T, typename U>
        inline void FixedArray<T, U>::set(std::size_t idx, std::size_t element, cio::Value<T> value, std::size_t elementSize)
        {
            std::size_t loc = mOffset + mStride * idx + mElementStride * element;
            std::size_t chunk = loc / sizeof(cio::Value<U>);
            std::size_t off = loc % sizeof(cio::Value<U>);
            U* data = static_cast<cio::Value<U> *>(mData.data()) + chunk;
            std::memcpy(reinterpret_cast<cio::Value<U>*>(data) + off, &value, elementSize);
        }

		template <typename T, typename U>
		inline std::size_t FixedArray<T, U>::getOffset() const
		{
			return mOffset;
		}

		template <typename T, typename U>
		inline void FixedArray<T, U>::setOffset(std::size_t offset)
		{
			mOffset = offset;
		}

		template <typename T, typename U>
		inline std::size_t FixedArray<T, U>::getStride() const
		{
			return mStride;
		}

		template <typename T, typename U>
		inline void FixedArray<T, U>::setStride(std::size_t stride)
		{
			mStride = stride;
		}

		template <typename T, typename U>
		inline std::size_t FixedArray<T, U>::getColumnCount() const
		{
			return mColumns;
		}

		template <typename T, typename U>
		inline void FixedArray<T, U>::setColumnCount(std::size_t count)
		{
			mColumns = count;
		}

		template <typename T, typename U>
		inline std::size_t FixedArray<T, U>::getRowCount() const
		{
			return mRows;
		}

		template <typename T, typename U>
		inline void FixedArray<T, U>::setRowCount(std::size_t count)
		{
			mRows = count;
		}

		template <typename T, typename U>
		inline std::size_t FixedArray<T, U>::getElementSize() const
		{
			return mElementSize;
		}

		template <typename T, typename U>
		inline void FixedArray<T, U>::setElementSize(std::size_t count)
		{
			mElementSize = count;
		}

		template <typename T, typename U>
		inline std::size_t FixedArray<T, U>::getElementStride() const
		{
			return mElementStride;
		}

		template <typename T, typename U>
		inline void FixedArray<T, U>::setElementStride(std::size_t count)
		{
			mElementStride = count;
		}

		template <typename T, typename U>
		inline std::size_t FixedArray<T, U>::size() const
		{
			return mSize;
		}

		template <typename T, typename U>
		inline void FixedArray<T, U>::resize(std::size_t length)
		{
			mSize = length;
		}

		template <typename T, typename U>
		inline void FixedArray<T, U>::clear()
		{
			mData.clear();
			mSize = 0;
			mRows = 0;
			mOffset = 0;
			mColumns = 0;
			mStride = sizeof(cio::Value<T>);
			mElementSize = sizeof(cio::Value<T>);
			mElementStride = sizeof(cio::Value<T>);
		}

		template <typename T, typename U>
		Scalar<T, U> FixedArray<T, U>::select(std::size_t element)
		{
			return Scalar<T, U>(mData.reference(), mRows, mOffset + element * mElementStride, mStride, mElementSize);
		}

		template <typename T, typename U>
		Scalar<const T, const U> FixedArray<T, U>::select(std::size_t element) const
		{
			return Scalar<const T, const U>(mData.reference(), mRows, mOffset + element * mElementStride, mStride, mElementSize);
		}

		template <typename T, typename U>
		inline cio::Chunk<U>& FixedArray<T, U>::getData()
		{
			return mData;
		}

		template <typename T, typename U>
		inline const cio::Chunk<U>& FixedArray<T, U>::getData() const
		{
			return mData;
		}

		template <typename T, typename U>
		inline void FixedArray<T, U>::setData(cio::Chunk<U> data)
		{
			mData = std::move(data);
		}

		template <typename T, typename U>
		inline U* FixedArray<T, U>::data()
		{
			return mData.data();
		}

		template <typename T, typename U>
		inline const U* FixedArray<T, U>::data() const
		{
			return mData.data();
		}
}


#endif
