/*=====================================================================================================================
 * Copyright 2022-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "TiffReader.h"

#include "Catalog.h"
#include "Collection.h"
#include "Node.h"

#include <cio/Buffer.h>
#include <cio/Class.h>
#include <cio/Input.h>
#include <cio/UniqueId.h>

#include <geas/Projection.h>

namespace glitter
{
	const std::uint16_t TiffReader::GEOKEY_DIRECTORY_TAG = 34735;

	const std::uint16_t TiffReader::GEO_DOUBLE_PARAM_TAG = 34736;

	const std::uint16_t TiffReader::GEO_ASCII_PARAM_TAG = 34737;

	const std::uint16_t TiffReader::GEO_PROJECTED_CS_KEY = 3072;

	cio::Class<TiffReader> TiffReader::sMetaclass("glitter::TiffReader");

	const cio::Class<TiffReader> &TiffReader::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}

	TiffReader::TiffReader() noexcept
	{
		// nothing more to do
	}

	TiffReader::TiffReader(std::shared_ptr<Profile> profile) noexcept :
		ModelReader(std::move(profile))
	{
		// nothing more to do
	}

	TiffReader::TiffReader(const TiffReader &in) = default;

	TiffReader::TiffReader(TiffReader &&in) noexcept = default;

	TiffReader &TiffReader::operator=(const TiffReader &in) = default;

	TiffReader &TiffReader::operator=(TiffReader &&in) noexcept = default;

	TiffReader::~TiffReader() noexcept = default;

	void TiffReader::clear() noexcept
	{
		Reader::clear();
	}

	const cio::Metaclass &TiffReader::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	geas::Projection *TiffReader::loadGeokeyDirectory(geas::Application &schema, cio::Buffer &buffer)
	{
		geas::Projection *projection = nullptr;

		std::uint16_t keyVersion = buffer.getLittle<std::uint16_t>();
		std::uint16_t keyRevision = buffer.getLittle<std::uint16_t>();
		std::uint16_t keyMinor = buffer.getLittle<std::uint16_t>();
		std::uint16_t keyCount = buffer.getLittle<std::uint16_t>();

		for (std::size_t k = 0; k < keyCount; ++k)
		{
			std::uint16_t keyId = buffer.getLittle<std::uint16_t>();
			std::uint16_t tiffLocation = buffer.getLittle<std::uint16_t>();
			std::uint16_t valueCount = buffer.getLittle<std::uint16_t>();
			std::uint16_t value = buffer.getLittle<std::uint16_t>();

			if (keyId == GEO_PROJECTED_CS_KEY)
			{
				geas::Index index = value;
				geas::Code code;
				std::snprintf(code.data(), code.capacity(), "EPSG:%u", index);
				cio::Path path = "https://epsg.io/" + std::to_string(value);

				projection = schema.getProjections().getOrCreate(index, geas::Label(), code, path);

				geas::Standard *standard = schema.getOrCreateVocabulary()->getStandards().getOrCreate(geas::Code("EPSG"));
				projection->setStandard(standard);
			}
		}

		return projection;
	}
}
