/*=====================================================================================================================
 * Copyright 2021-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "GarbageMarker.h"

namespace glitter
{
	GarbageMarker::GarbageMarker() noexcept :
		mInputCount(0),
		mMarkedCount(0),
		mOutputCount(0)
	{
		// nothing more to do
	}

	GarbageMarker::~GarbageMarker() noexcept = default;

	void GarbageMarker::clear() noexcept
	{
		mRemovals.clear();
		mMarkedIndices.clear();
		mInputCount = 0;
		mMarkedCount = 0;
		mOutputCount = 0;
	}

	void GarbageMarker::setInputCount(std::int32_t count)
	{
		mInputCount = count;
		mMarkedIndices.resize(count);
		mOutputCount = count;
	}

	std::int32_t GarbageMarker::getInputCount() const noexcept
	{
		return mInputCount;
	}

	bool GarbageMarker::mark(std::int32_t idx)
	{
		bool marked = false;
		if (idx != -1)
		{
			if (idx >= mInputCount)
			{
				marked = true;
				this->setInputCount(idx + 1);
			}
			else
			{
				marked = !mMarkedIndices[idx];
			}

			mMarkedCount += static_cast<std::int32_t>(marked);
			mMarkedIndices[idx] = true;
		}

		return marked;
	}

	bool GarbageMarker::isMarked(std::int32_t idx) const
	{
		return idx != -1 && idx < mInputCount && mMarkedIndices[idx];
	}

	std::int32_t GarbageMarker::getMarkedCount() const noexcept
	{
		return mMarkedCount;
	}

	std::int32_t GarbageMarker::sweep()
	{
		std::int32_t cumulative = 0;

		// Special case - everything is unused so create one big removal range
		if (mMarkedCount == 0)
		{
			Removal current = { 0, mInputCount, mInputCount };
			mRemovals.emplace_back(current);
			mOutputCount = 0;
			cumulative = mInputCount;
		}
		// Special case - everything is used, so we have no removals and set output count to input count
		else if (mMarkedCount >= mInputCount)
		{
			mOutputCount = mInputCount;
		}
		// General case - have to process the bitsets to figure out removal ranges
		else
		{
			Removal current = { -1, 0, 0 };

			// Go through each index to find unused indices to remove
			for (std::int32_t i = 0; i < mInputCount; ++i)
			{
				// Is the current index marked as in use?
				bool marked = mMarkedIndices[i];
				if (marked)
				{
					// The current index is in use, so if the previous index was removed as unused, we have finished a removal range
					// Calculate the cumulative removed then add to the removal list
					if (current.start != -1)
					{
						current.end = i;
						cumulative += current.end - current.start;
						current.total = cumulative;
						mRemovals.emplace_back(current);

						current.start = -1;
						current.end = 0;
						current.total = 0;
					}
				}
				else
				{
					// The current index is unused, so if the previous index was used, start a new removal range
					if (current.start == -1)
					{
						current.start = i;
					}
				}
			}

			// We removed the end of the index list, or the entire range, and did not finish the removal inside the loop
			if (current.start != -1)
			{
				current.end = mInputCount;
				cumulative += current.end - current.start;
				current.total = cumulative;
				mRemovals.emplace_back(current);
			}

			mOutputCount = mInputCount - cumulative;
		}

		return cumulative;
	}

	std::int32_t GarbageMarker::getOutputCount() const noexcept
	{
		return mOutputCount;
	}

	std::int32_t GarbageMarker::remap(std::int32_t index) const noexcept
	{
		std::int32_t output = index;
		if (index != -1)
		{
			Removal nearest = this->findNearestRemoval(index);
			if (nearest.start != -1)
			{
				if (index < nearest.end)
				{
					output = -1;
				}
				else
				{
					output = index - nearest.total;
				}
			}
		}
		return output;
	}

	std::int32_t GarbageMarker::getRemovalCount() const noexcept
	{
		return static_cast<std::int32_t>(mRemovals.size());
	}

	GarbageMarker::Removal GarbageMarker::findNearestRemoval(std::int32_t idx) const noexcept
	{
		GarbageMarker::Removal removal = { -1, 0, 0 };

		// Conduct a binary search through the ordered removals list
		// We start with the search range spanning the whole list
		std::size_t low = 0;
		std::size_t high = mRemovals.size();

		// While we still have a search range, keep processing
		while (low < high)
		{
			std::size_t cur = (high + low) / 2;

			// Look at the current midpoint of the search range
			const GarbageMarker::Removal &candidate = mRemovals[cur];

			// If the midpoint removal range starts before the index, then it is a possible candidate for the answer
			if (candidate.start < idx)
			{
				// If the removal range actually contains the index, we're done
				// Also, if the current candidate is the also the lowest possible candidate, that means we've narrowed the range down to one item
				// So this is the last one and pick it
				if (candidate.end >= idx || cur == low)
				{
					removal = candidate;
					break;
				}
				// Otherwise we've found a possible removal range candidate, but there might be a higher one
				// So set this one as the lower bound
				else
				{
					low = cur;
				}
			}
			// If the midpoint removal range starts after the given index, it is not a valid candidate
			// Set it as the upper bound of the possible candidates to look at lower candidates
			else
			{
				high = cur;
			}
		}

		return removal;
	}

	GarbageMarker::Removal GarbageMarker::getRemoval(std::int32_t position) const noexcept
	{
		GarbageMarker::Removal removal = { -1, 0, 0 };
		if (position >= 0 && static_cast<std::size_t>(position) < mRemovals.size())
		{
			removal = mRemovals[position];
		}
		return removal;
	}

	bool GarbageMarker::needsRemapping() const noexcept
	{
		return !mRemovals.empty() && (mRemovals.size() > 1 || mRemovals.back().end < mInputCount);
	}
}