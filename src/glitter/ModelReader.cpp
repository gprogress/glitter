/*=====================================================================================================================
 * Copyright 2022-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "ModelReader.h"

#include "Profile.h"

#include <cio/Class.h>

namespace glitter
{
	cio::Class<ModelReader> ModelReader::sMetaclass("glitter::ModelReader");

	const cio::Class<ModelReader> &ModelReader::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}

	ModelReader::ModelReader() noexcept
	{
		// nothing more to do
	}

	ModelReader::ModelReader(std::shared_ptr<Profile> profile) noexcept :
		mProfile(std::move(profile))
	{
		// nothing more to do
	}

	ModelReader::ModelReader(const ModelReader &in) = default;

	// VS 2017 does not allow us to default this
	ModelReader::ModelReader(ModelReader &&in) noexcept :
		Reader(std::move(in)),
		mProfile(std::move(in.mProfile))
	{
		// nothing more to do
	}

	ModelReader &ModelReader::operator=(const ModelReader &in) = default;

	// VS 2017 does not allow us to default this
	ModelReader &ModelReader::operator=(ModelReader &&in) noexcept
	{
		Reader::operator=(std::move(in));
		mProfile = std::move(in.mProfile);
		return *this;
	}

	ModelReader::~ModelReader() noexcept = default;

	void ModelReader::clear() noexcept
	{
		Reader::clear();
		mProfile.reset();
	}

	const cio::Metaclass &ModelReader::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	void ModelReader::onProfileChanged()
	{
		// nothing to do in base class
	}

	std::shared_ptr<Profile> ModelReader::getProfile() noexcept
	{
		return mProfile;
	}

	std::shared_ptr<const Profile> ModelReader::getProfile() const noexcept
	{
		return mProfile;
	}

	std::shared_ptr<Profile> ModelReader::getOrCreateProfile() noexcept
	{
		if (!mProfile)
		{
			mProfile.reset(new Profile());
			this->onProfileChanged();
		}

		return mProfile;
	}

	void ModelReader::setProfile(std::shared_ptr<Profile> profile) noexcept
	{
		if (mProfile || profile)
		{
			mProfile = std::move(profile);
			this->onProfileChanged();
		}
	}

	void ModelReader::clearProfile() noexcept
	{
		if (mProfile)
		{
			mProfile.reset();
			this->onProfileChanged();
		}
	}
}