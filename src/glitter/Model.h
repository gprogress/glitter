/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_MODEL_H
#define GLITTER_MODEL_H

#include "Types.h"

#include "Array.h"
#include "Collection.h"
#include "Entity.h"
#include "FeatureIdMapper.h"
#include "Weight.h"

#include <cio/ProtocolFactory.h>
#include <cio/Path.h>
#include <cio/UniqueId.h>

#include <memory>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace glitter
{
	/**
	 * The Model represents a single glTF document plus any entity metadata and schema assocaited with it to form
	 * a complete model of a tile or physical object.
	 */
	class GLITTER_ABI Model
	{
		public:
			/**
			 * Gets the default extension name for the glTF metadata extension.
			 * This is currently "EXT_feature_metadata"
			 * 
			 * @return the default metadata extension name
			 */
			static const char *getDefaultMetadataExtensionName();

			/**
			 * Construct an empty model.
			 * No glTF document, entity schema, or entity collection is created.
			 */
			Model() noexcept;

			/**
			 * Creates a new model by transferring the contents of an existing one.
			 * 
			 * @param in The model to transfer into this one
			 */
			Model(Model &&in) noexcept;

			/**
			 * Transfers the contents of a model into this one.
			 * The existing contents, if any, are discarded. Any unsaved changes are lost.
			 * 
			 * @param in The model to transfer into this one
			 * @return this model
			 */
			Model &operator=(Model &&in) noexcept;

			/**
			 * Destructor. Any unsaved changes are lost.
			 */
			virtual ~Model() noexcept;

			/**
			 * Removes all content from this model.
			 * Any unsaved changes are lost.
			 */
			virtual void clear() noexcept;

			/**
			*  Uses the given EntityReference to create a new feature
			* 
			* @param in The entity reference that holds the Node information needed to create a new feature
			*/
			void finalizeEntityCreation(const Entity &in);

			/**
			* Updates the mDocument to have the current catalog information.
			*/
			void synchronizeCatalogToDocument();

			/**
			 * Creates a new feature table that can be used to create new features.
			 * If the feature table already exists, the existing definition is returned.
			 * Otherwise, a new Node is created and the appropriate part of the JSON schema extension is updated.
			 *
			 * @param tableName The feature table name
			 * @return the Node for the feature table
			 */
			Node *getOrCreateFeatureTable(const geas::Label &tableName);

			/**
			 * Creates a new feature type that can be used to create new features.
			 * If the feature type already exists, the existing definition is returned.
			 * Otherwise, a new entity definition is created and the appropriate part of the JSON schema extension is updated.
			 * 
			 * @param entityTypeName The feature type name
			 * @return the entity definition for the feature type
			 */
			geas::Entity *getOrCreateFeatureType(const geas::Label &entityTypeName);

			/**
			* Searches the collection for the given table name, if not found, create a new table with the given name. Searches the
			* collection for the given entity type name, if found, set the entity type, and if not found, create an entity with the given name.
			* 
			* @param tableName The name of the table to find in the collection
			* @param entityTypeName The name of the entity to find in the collection
			* @return The entity reference
			*/
			Entity createFeature(const geas::Label &tableName, const geas::Label &entityTypeName);

			/**
			* Uses the entityId to find the table and the entity. If no entity is found using the entityId, check if an entity can be found
			* using entityTypeName, if yes, set the entity type, if not create an entity using the given name.
			* 
			* @param entityId The id used to find the information of the table and catalog
			* @param entityTypeName The name of the entity to find in the collection
			* @return The entity reference
			*/
			Entity createFeature(const cio::UniqueId &entityId, const geas::Label &entityTypeName);

			/**
			 * Loads a glTF document from disk. This may be a text .gltf file or a binary .glb file.
			 * If a binary buffer file is referenced, it is automatically loaded into memory.
			 * Externally referenced textures are not automatically loaded into memory, but embedded textures are.
			 * If the glTF document has the EXT_feature_metadata extension, a Catalog is created to represent the discovered entity schemas
			 * and an entity collection is created to represent the content.
			 * 
			 * The parent resource path is set by default to the parent directory of the glTF file.
			 * 
			 * @param path The path to the glTF document
			 */
			virtual void loadFromFile(const std::string &path);

			/**
			 * Loads a glTF document from a resource URI. This may be a text .gltf file or a binary .glb file.
			 * If a binary buffer file is referenced, it is automatically loaded into memory.
			 * Externally referenced textures are not automatically loaded into memory, but embedded textures are.
			 * If the glTF document has the EXT_feature_metadata extension, a Catalog is created to represent the discovered entity schemas
			 * and an entity collection is created to represent the content.
			 *
			 * The parent resource path is set by default to the parent directory of the glTF file if one exists.
			 * 
			 * If a protocol factory has been set, it will be used to work with the resource path and obtain the input.
			 * Otherwise, the model will be limited to processing local files.
			 * 
			 * @param path The path to the glTF document
			 */
			virtual void loadFromPath(const cio::Path &path);

			/**
			 * Stores a glTF document to disk. The file extension controls whether it is stored as a text .gltf file or a binary .glb file.
			 * If an entity collection is specified and has changed since the document was loaded (or was not originally part of the document)
			 * the document is first updated to capture the metadata extension data.
			 * 
			 * @param path The path to the glTF document
			 */
			virtual void storeToFile(const std::string &path);

			/**
			 * Stores a glTF document to disk. The file extension controls whether it is stored as a text .gltf file or a binary .glb file.
			 * If an entity collection is specified and has changed since the document was loaded (or was not originally part of the document)
			 * the document is first updated to capture the metadata extension data.
			 * 
			 * If a protocol factory has been set, it will be used to work with the resource path and obtain the output.
			 * Otherwise, the model will be limited to processing local files.
			 * 
			 * This does not change the current main file path or parent resource path.
			 *
			 * @param path The path to the glTF document
			 */
			virtual void storeToPath(const cio::Path &path);

			/**
			 * Processes the loaded glTF document to determine what data is actually used and to remove all unused data.
			 * Data is considered to be used if any of the scenes in the document reference it directly or indirectly.
			 * Note that this can remove entire node trees, feature tables, cameras, skins, meshes, materials, textures, images, samplers, as well as underlying data
			 * such as accessors, buffer views, and buffers.
			 * 
			 * Since animations are stored in glTF but not referenced by anything else inside the document and expected to be used by runtime clients,
			 * by the default this method assumes all animations are used so that they are preserved along with all node and accessor data they need. 
			 * 
			 * If data is removed in such a way that it affects index or pointer cross-references or JSON extension text, the affected references are also updated
			 * to reflect the changes so the document is still valid.
			 * 
			 * @warning This method is not aware of extensions other than the feature metadata extension currently, so it can remove data that is used
			 * by unknown extensions.
			 * 
			 * @see glitter::GarbageCollector If you want more precise control over which objects are considered used vs. unused and want to manually run the removal process.
			 * 
			 * @note This method currently does not track used vs. unused URIs so external files will not be removed yet.
			 * 
			 * @return whether the document was modified by removing any data
			 */
			virtual bool removeUnusedData();

			/**
			 * Stores a glTF document to disk at the same file location it was originally opened, or to the new main file location
			 * if changed by setMainFile().
			 * 
			 * After saving, the parent resource path will be the parent of the saved location.
			 */
			void save();

			/**
			 * Gets the resource path to the main file, typically a .gltf or .glb file.
			 * 
			 * @return the main file
			 */
			const cio::Path &getMainFile() const;

			/**
			 * Sets the resource path to the main file, which should typically be a .gltf or .glb file.
			 * This can be used to change the save() location, or to designate a file location for a model obtained from non-file sources.
			 */
			void setMainFile(const cio::Path path);

			/**
			 * Gets the parent resource URI for the glTF model, used to resolve relative external references.
			 * 
			 * @return the parent resource path
			 */
			const cio::Path &getParentNode() const;

			/**
			 * Sets the parent resource URI for the glTF model, used to resolve relative external references.
			 *
			 * @param path the parent resource path
			 */
			void setParentNode(cio::Path path);

			/**
			 * Gets the protocol factory, if any, that been set on the model.
			 * 
			 * @return the protocol factory or nullptr if none was set
			 */
			const cio::ProtocolFactory *getProtocolFactory() const;

			/**
			 * Sets the protocol factory to use for subsequent load and store operations.
			 * 
			 * @param protocols The protocol factory to use, or nullptr to disable support for everything except local files
			 */
			void setProtocolFactory(const cio::ProtocolFactory *protocols);

			/**
			 * Gets the currently loaded glTF document.
			 * This represents the physical structure and geometry of the glTF model.
			 * 
			 * @return the glTF document, or nullptr if none has been set
			 */
			fx::gltf::Document *getDocument();

			/**
			 * Gets the currently loaded glTF document.
			 * This represents the physical structure and geometry of the glTF model.
			 *
			 * @return the glTF document, or nullptr if none has been set
			 */
			const fx::gltf::Document *getDocument() const;
			
			/**
			 * Gets the currently loaded glTF document, creating a new one if none is present.
			 * This represents the physical structure and geometry of the glTF model.
			 *
			 * @return the glTF document, either the existing one or a newly created one
			 */
			fx::gltf::Document *getOrCreateDocument();

			/**
			 * Sets the currently loaded glTF document.
			 * No changes to the entity catalog or collection are made automatically - call synchronizeEntities() to perform that step.
			 * Any changes to the existing glTF document, if any, are lost.
			 * 
			 * @param document The glTF document to adopt
			 */
			void setDocument(std::unique_ptr<fx::gltf::Document> document);

			/**
			 * Removes the currently loaded glTF document from this model and returns it.
			 * This may be used to transfer ownership of the glTF model to some other object.
			 * 
			 * @return the removed glTF document
			 */
			std::unique_ptr<fx::gltf::Document> takeDocument();

			/**
			 * Gets the entity catalog currently in use for this model.
			 * This represents the schema of any entity collection data present in the model.
			 * The schema may be loaded from a data model standard or automatically derived from the glTF content.
			 * 
			 * @return the entity catalog, or nullptr if none has been set
			 */
			std::shared_ptr<Catalog> getCatalog();

			/**
			 * Gets the entity catalog currently in use for this model.
			 * This represents the schema of any entity collection data present in the model.
			 * The schema may be loaded from a data model standard or automatically derived from the glTF content.
			 * 
			 * @return the entity catalog, or nullptr if none has been set
			 */
			std::shared_ptr<const Catalog> getCatalog() const;

			/**
			 * Gets the entity catalog currently in use for this model, creating a new one if none is present.
			 * This represents the schema of any entity collection data present in the model.
			 * The schema may be loaded from a data model standard or automatically derived from the glTF content.
			 *
			 * @return the entity catalog, either the existing one or a new one
			 */
			std::shared_ptr<Catalog> getOrCreateCatalog();

			/**
			 * Sets the entity catalog to use.
			 * No changes to the glTF document or collection are made automatically - call synchronizeEntities() to perform that step.
			 * Any changes to the existing catalog, if any, are lost.
			 *
			 * @param document The entity catalog to adopt
			 */
			void setCatalog(std::shared_ptr<Catalog> document);

			/**
			 * Removes the currently loaded entity catalog from this model and returns it.
			 * This may be used to transfer ownership of the catalog to some other object.
			 *
			 * @return the removed entity catalog
			 */
			std::shared_ptr<Catalog> takeCatalog();

			/**
			 * Gets the entity collection currently active for this model.
			 * This represents the hierarchy of Nodes for logical entities and attributes present in this model.
			 * The collection may be loaded from the EXT_feature_metadata extension in glTF, or programmatically created.
			 * 
			 * @return the entity collection currently associated with this model, or null if none
			 */
			Collection *getEntityCollection();

			/**
			 * Gets the entity collection currently active for this model.
			 * This represents the hierarchy of Nodes for logical entities and attributes present in this model.
			 * The collection may be loaded from the EXT_feature_metadata extension in glTF, or programmatically created.
			 *
			 * @return the entity collection currently associated with this model, or null if none
			 */
			const Collection *getEntityCollection() const;

			/**
			 * Gets the entity collection currently active for this model, creating a new one if none is present.
			 * This represents the hierarchy of Nodes for logical entities and attributes present in this model.
			 * The collection may be loaded from the EXT_feature_metadata extension in glTF, or programmatically created.
			 *
			 * @return the entity collection currently associated with this model, either the existing one or a newly created one
			 */
			Collection *getOrCreateEntityCollection();

			/**
			 * Sets the entity collection to use.
			 * No changes to the glTF document or catalog are made automatically - call synchronizeEntities() to perform that step.
			 * Any changes to the existing collection, if any, are lost.
			 *
			 * @param document The entity collection to adopt
			 */
			void setEntityCollection(std::unique_ptr<Collection> collection);

			/**
			 * Removes the currently loaded entity collection from this model and returns it.
			 * This may be used to transfer ownership of the entity collection to some other object.
			 *
			 * @return the removed entity collection
			 */
			std::unique_ptr<Collection> takeEntityCollection();

			/**
			 * Evaluate whether this model is valid in a Boolean context.
			 * This is true if a glTF document is currently active.
			 * 
			 * @return whether a glTF document is active
			 */
			explicit operator bool() const;

			/**
			* Sets attibute values on specific objects such as vertex, indices, primitive sets, meshes, and nodes
			* 
			* @param objectId The id of the object to look up
			* @param entityId The id of the entity to look up
			* @param layerIndex Which feature layer the entity should be bound to
			*/
			void setEntity(const cio::UniqueId& objectId, const cio::UniqueId &entityId, std::uint32_t layerIndex = 0);

			void setEntityForNode(fx::gltf::Node &mesh, const Entity &entity, std::uint32_t layerIndex = 0);

			void setEntityForMesh(fx::gltf::Mesh &mesh, const Entity &entity, std::uint32_t layerIndex = 0);

			void setEntityForPrimitiveSet(fx::gltf::Primitive &primitive, const Entity &entity, std::uint32_t layerIndex = 0);

			void setEntityForVertex(fx::gltf::Primitive &primitive, std::uint32_t vertexIdx, const Entity &entity, std::uint32_t layerIndex = 0);

			/**
			 * Gets the total unique vertex count of the given primitive set.
			 * This is calculated from one of two techniques:
			 * 1.) Check each existing vertex attribute and calculate the number of values given the accessor parameters
			 * 2.) Check the index array for the highest defined vertex index
			 * 
			 * @return the vertex count
			 */
			std::uint32_t getVertexCount(const fx::gltf::Primitive &primitive) const;

			/**
			 * Gets the unique ID of the entity data directly associated with the given object ID.
			 * 
			 * If the id is an entity, then the input ID is returned.
			 * If the id is a vertex or primitive index, the first vertex attribute array for feature IDs will be consulted to get the proper entity ID
			 * If the id is a vertex attribute array, this will get the proper ID element from the array and then resolve it against the proper feature table
			 * If the id is a primitive or primitive set, this will check if the primitive or primitive set is defined to have a uniform feature ID and if so returns that
			 * If the id is a mesh or node, this will check if every primitive set under the object is defined to have a uniform feature ID and if so returns that
			 * Other types are not currently supported and will return the invalid unique ID.
			 * 
			 * @param id The ID of the object to locate
			 * @param layer Which attribute layer to search
			 * @return the entity ID of the entity associated with the given object
			 */
			FeatureIdWeight getEntityId(const cio::UniqueId &id, unsigned layer) const;

			/**
			 * Gets the unique ID of the entity data directly associated with the primitive set's primitive element index.
			 *
			 * @param meshIdx The mesh index
			 * @param primitiveSetIdx The primitive set index relative to the mesh
			 * @param idx The primitive index's index relative to the primitive set
			 * @param layer Which feature layer to consult
			 * @return the entity ID of the entity associated with the primitive index
			 */
			cio::UniqueId getEntityIdForPrimitiveIndex(std::int32_t meshIdx, std::int32_t primitiveSetIdx, std::uint32_t idx, unsigned layer = 0) const;

			/**
			 * Gets the entity data directly associated with the given object ID, if any, for all feature layers.
			 * This is equivalent to using getEntityData layer separately on each feature layer, and then returning a vector of all of them in sequence.
			 * Empty placeholders (null references) will be returned if a particular layer does not reference an entity.
			 *
			 * @param id The unique ID of the object to locate
			 * @param layer which feature layer to get
			 * @return the logical entity data for the best or most widely used entity associated with that object
			 */
			std::vector<EntityWeight> getEntityData(const cio::UniqueId &id);

			/**
			 * Gets the entity data directly associated with the given object ID, if any, for all feature layers.
			 * This is equivalent to using getEntityData layer separately on each feature layer, and then returning a vector of all of them in sequence.
			 * Empty placeholders (null references) will be returned if a particular layer does not reference an entity.
			 *
			 * @param id The unique ID of the object to locate
			 * @param layer which feature layer to get
			 * @return the logical entity data for the entity associated with that object
			 */
			std::vector<EntityWeight> getEntityData(const cio::UniqueId &id) const;

			/**
			 * Gets the entity data directly associated with the given object ID, if any, for a particular feature layer.
			 * 
			 * If the id is an entity, that entity's data is returned if present
			 * If the id is anything else, then the best entity for that object is returned.
			 * For geas::Index and Vertex, this gets the single feature associated either the feature attribute or the procedural feature ID.
			 * For Triangle, this gets the feature IDs of the three vertices. If at least two vertices reference the same feature, that is returned. Otherwise, the lowest feature ID is used.
			 * For PrimitiveSet, this examines all vertices in the primitive set and returns the most frequently referenced feature, or the lowest feature ID if none is used more than once.
			 * For Mesh and Node, this gets the predominant feature contained inside the data.
			 * 
			 * 
			 * @param id The unique ID of the object to locate
			 * @param layer which feature layer to get
			 * @return the logical entity data for the best or most widely used entity associated with that object
			 */
			EntityWeight getEntityData(const cio::UniqueId &id, unsigned layer);

			/**
			 * Gets the entity data directly associated with the given object ID, if any, for a particular feature layer.
			 *
			 * If the id is an entity, that entity's data is returned if present
			 * If the id is anything else, this calls getEntityId
			 *
			 * @param id The unique ID of the object to locate
			 * @param layer which feature layer to get
			 * @return the logical entity data for the entity associated with that object
			 */
			EntityWeight getEntityData(const cio::UniqueId &id, unsigned layer) const;

			/**
			 * Gets the number of entity layers present on the model.
			 *
			 * @return the number of entity layers
			 */
			std::uint32_t getEntityLayerCount() const;

			/**
			 * Gets the number of entity layers present on the identified object.
			 * 
			 * @param id The ID of the object of interest
			 * @return the number of entity layers
			 */
			std::uint32_t getEntityLayerCount(const cio::UniqueId &id) const;

			std::uint32_t getEntityLayerCount(const fx::gltf::Primitive &prim) const;

			std::uint32_t getEntityLayerCount(const fx::gltf::Mesh &mesh) const;

			std::uint32_t getEntityLayerCount(const fx::gltf::Node &node) const;

			std::uint32_t getEntityLayerCount(const fx::gltf::Scene &node) const;

			/**
			 * Gets the list of all entity IDs associated with the given object specified by unique ID in all feature layers.
			 *
			 * @param id The ID of the object to locate
			 * @return the list of layers of entity IDs of the entities associated with the given object
			 */
			std::vector<std::vector<FeatureIdWeight>> getAllEntityIds(const cio::UniqueId &id);

			/**
			 * Gets the list of all entity IDs associated with the given object specified by unique ID in a particular feature layer.
			 * 
			 * @param id The ID of the object to locate
			 * @return the list of entity IDs of the entities associated with the given object
			 */ 
			std::vector<FeatureIdWeight> getAllEntityIds(const cio::UniqueId &id, unsigned layer) const;

			cio::UniqueId getVertexEntityId(std::int32_t meshIdx, std::int32_t primIdx, std::uint32_t vertexIdx, unsigned layer) const;

			cio::UniqueId getIndexEntityId(std::int32_t meshIdx, std::int32_t primIdx, std::uint32_t indexIdx, unsigned layer) const;

			FeatureIdWeight getBestTriangleEntityId(std::int32_t meshIdx, std::int32_t primIdx, std::uint32_t triangleIdx, unsigned layer) const;

			FeatureIdWeight getBestPrimitiveSetEntityId(std::int32_t meshIdx, std::int32_t primIdx, unsigned layer) const;

			FeatureIdWeight getBestMeshEntityId(std::int32_t meshIdx, unsigned layer) const;

			FeatureIdWeight getBestNodeEntityId(std::int32_t nodeIdx, unsigned layer) const;

			FeatureIdWeight getBestModelEntityId(std::uint32_t layer) const;

			FeatureIdMapper makeFeatureIdMapper(std::int32_t meshIdx, std::int32_t primIdx, std::uint32_t layer) const;

			FeatureIdMapper makeFeatureIdMapper(const fx::gltf::Primitive &prim, std::uint32_t layer) const;

			FeatureIdMapper makeFeatureIdMapper(const cio::UniqueId &id, std::uint32_t layer) const;

			/**
			* Adds the given attribute to the Node attribute values if it is not already present.
			* 
			* @param node The Node used to find the list of attributes
			* @param type The attribute to be added if not found in the list of attributes
			* @return status indicating either the attribute given already exists or the attribute has been added
			*/
			cio::State addAttribute(Node *node, const geas::Attribute *type);
			
			/**
			* Adds the given attribute to the Node attribute values if it is not already present.
			*
			* @param node The Node used to find the list of attributes
			* @param attributeName
			* @return status indicating either the attribute given already exists or the attribute has been added
			*/
			cio::State addAttribute(Node *node, const geas::Label &attributeName);
			
			/**
			* Adds the given attribute to the Node attribute values if it is not already present.
			*
			* @param nodeName The name of Node used to find the list of attributes
			* @param attributeName The name attribute to be added if not found in the list of attributes
			* @return status indicating either the attribute given already exists or the attribute has been added
			*/
			cio::State addAttribute(const geas::Label &nodeName, const geas::Label &attributeName);
			
			/**
			* Adds the given attribute to the Node attribute values if it is not already present.
			*
			* @param nodeName The name of Node used to find the list of attributes
			* @param type The attribute to be added if not found in the list of attributes
			* @return status indicating either the attribute given already exists or the attribute has been added
			*/
			cio::State addAttribute(const geas::Label &nodeName, geas::Attribute *type);
			
			/**
			* Adds the given attribute to the Node attribute values if it is not already present.
			*
			* @param nodeId The id of Node used to find the list of attributes
			* @param type The attribute to be added if not found in the list of attributes
			* @return status indicating either the attribute given already exists or the attribute has been added
			*/
			cio::State addAttribute(const cio::UniqueId &nodeId, geas::Attribute *type);
			
			/**
			* Adds the given attribute to the Node attribute values if it is not already present.
			*
			* @param nodeId The id of Node used to find the list of attributes
			* @param attributeName The name attribute to be added if not found in the list of attributes
			* @return status indicating either the attribute given already exists or the attribute has been added
			*/
			cio::State addAttribute(const cio::UniqueId &nodeId, const geas::Label &attributeName);

			/**
			 * Gets the logical element type based on the accessor.
			 * In many cases, this will correspond to the physical layout type - for example, Float matches up with Real.
			 * However, for integer types, if the accessor is normalized, this actually generates a Real type rather than Integer.
			 * 
			 * Currently it is not possible for an accessor to generate a type other than Integer or Real.
			 *
			 * @param accessor The accessor to assess
			 * @return the element type for the accessor
			 */
			cio::Type getElementType(const fx::gltf::Accessor &accessor) const;

			/**
			 * Gets the number layout and number of elements for a particular glTF accessor.
			 * The number layout will be based on the Accessor's component type and the element count will be the total element count per item.
			 * For this method, matrix accessors (Mat2, Mat3, and Mat4) will be flattend out into vector accessors (4, 9, and 16 respectively).
			 * 
			 * @param accessor The accessor to assess
			 * @return the number layout and element count for this accessor
			 */
			std::pair<cio::Encoding, unsigned> getElementLayout(const fx::gltf::Accessor &accessor) const;

			/**
			 * Gets a memory data chunk that references the given glTF buffer index.
			 * 
			 * @param bufferIdx The buffer index
			 * @return the memory chunk
			 */
			cio::Chunk<void> getBuffer(std::int32_t bufferIdx);

			/**
			 * Gets a memory data chunk that references the given glTF buffer index.
			 *
			 * @param bufferIdx The buffer index
			 * @return the memory chunk
			 */
			cio::Chunk<const void> getBuffer(std::int32_t bufferIdx) const;

			/**
			 * Gets a memory data chunk configured to represent the given glTF buffer view.
			 * 
			 * Most users will not need this and will want to call getDataView to get an accessor formatted with the underlying scalar/vector/matrix type information.
			 * 
			 * @param bufferViewIdx the index of the buffer view
			 * @return the accessor for the strided access pattern for the buffer view
			 */
			cio::Chunk<void> getBufferView(std::int32_t bufferViewIdx);

			/**
			 * Gets a memory data chunk configured to represent the given glTF buffer view.
			 *
			 * Most users will not need this and will want to call getDataView to get an accessor formatted with the underlying scalar/vector/matrix type information.
			 *
			 * @param bufferViewIdx the index of the buffer view
			 * @return the accessor for the strided access pattern for the buffer view
			 */
			cio::Chunk<const void> getBufferView(std::int32_t bufferViewIdx) const;

			/**
			 * Gets an attribute-level data view of a particular glTF accessor and buffer view.
			 * This will consult the accessor to determine the numeric data type (if relevant), the number of elements per attribute value, and the binary access pattern.
			 * The buffer view will be used to obtain the actual data pointer and initial offset.
			 * 
			 * The given value array view can then be further used to set up accessors for particular data types either natively or by conversion.
			 * 
			 * @param accessorIdx The index of the glTF accessor
			 * @param viewIdx The index of the glTF buffer view
			 * @return an attribute value accessor for obtaining the given data
			 */
			Array getDataView(std::int32_t accessorIdx);

			/**
			 * Gets an attribute-level data view of a particular glTF accessor and buffer view.
			 * This will consult the accessor to determine the numeric data type (if relevant), the number of elements per attribute value, and the binary access pattern.
			 * The buffer view will be used to obtain the actual data pointer and initial offset.
			 *
			 * The given value array view can then be further used to set up accessors for particular data types either natively or by conversion.
			 *
			 * @param accessorIdx The index of the glTF accessor
			 * @param viewIdx The index of the glTF buffer view
			 * @return an attribute value accessor for obtaining the given data
			 */
			Array getDataView(std::int32_t accessorIdx) const;

			/**
			 * Gets an attribute-level data view of a particular glTF accessor and buffer view.
			 * This will consult the accessor to determine the numeric data type (if relevant), the number of elements per attribute value, and the binary access pattern.
			 * The buffer view will be used to obtain the actual data pointer and initial offset.
			 *
			 * The given value array view can then be further used to set up accessors for particular data types either natively or by conversion.
			 *
			 * @param accessor The glTF accessor
			 * @param view The glTF buffer view
			 * @return an attribute value accessor for obtaining the given data
			 */
			Array getDataView(fx::gltf::Accessor &accessor);

			/**
			 * Gets an attribute-level data view of a particular glTF accessor and buffer view.
			 * This will consult the accessor to determine the numeric data type (if relevant), the number of elements per attribute value, and the binary access pattern.
			 * The buffer view will be used to obtain the actual data pointer and initial offset.
			 *
			 * The given value array view can then be further used to set up accessors for particular data types either natively or by conversion.
			 *
			 * @param accessor The glTF accessor
			 * @param view The glTF buffer view
			 * @return an attribute value accessor for obtaining the given data
			 */
			Array getDataView(const fx::gltf::Accessor &accessor) const;

			/**
			 * Creates a unique ID representing the given material.
			 * The material must currently be present on the glTF document.
			 * 
			 * @param material The material
			 * @return the ID of the material
			 */
			cio::UniqueId makeId(const fx::gltf::Material &material) const;

			/**
			 * Creates a unique ID representing the given node.
			 * The node must currently be present on the glTF document.
			 *
			 * @param node The node
			 * @return the ID of the node
			 */
			cio::UniqueId makeId(const fx::gltf::Node &node) const;

			/**
			 * Creates a unique ID representing the given mesh.
			 * The mesh must currently be present on the glTF document.
			 *
			 * @param mesh The mesh
			 * @return the ID of the mesh
			 */
			cio::UniqueId makeId(const fx::gltf::Mesh &mesh) const;

			/**
			 * Creates a unique ID representing the given primitive set.
			 * The primitive set must currently be present on the given mesh in the glTF document.
			 *
			 * @param mesh The mesh
			 * @param prim The primitive set
			 * @return the ID of the primitive set
			 */
			cio::UniqueId makeId(const fx::gltf::Mesh &mesh, const fx::gltf::Primitive &prim) const;

			/**
			 * Creates a unique ID representing the vertex.
			 * The vertex must be a valid vertex index on the given primitive set.
			 *
			 * @param mesh The mesh
			 * @param prim The primitive set
			 * @param vtx The vertex index
			 * @return the ID of the vertex
			 */
			cio::UniqueId makeVertexId(const fx::gltf::Mesh &mesh, const fx::gltf::Primitive &prim, std::uint32_t vtx) const;

			/**
			 * Creates a unique ID representing a single primitive (typically a triangle) on the primitive set.
			 * The index must be a valid primitive count index. In the triangle case, the triangle ID is the initial vertex index offset divided by 3.
			 *
			 * @param mesh The mesh
			 * @param prim The primitive set
			 * @param idx The index of the primitive relative to the primitive set
			 * @return the ID of the primitive
			 */
			cio::UniqueId makePrimitiveId(const fx::gltf::Mesh &mesh, const fx::gltf::Primitive &prim, std::uint32_t idx) const;

			/**
			 * Creates a unique ID representing a single index of a single primitive on the primitive set, regardless of its interpretation.
			 * The index must be a valid primitive index's index.
			 *
			 * @param mesh The mesh
			 * @param prim The primitive set
			 * @param idx The primitive index's index relative to the primitive set
			 * @return the ID of the primitive index
			 */
			cio::UniqueId makePrimitiveIndexId(const fx::gltf::Mesh &mesh, const fx::gltf::Primitive &prim, std::uint32_t idx) const;

			/**
			 * Creates a unique ID representing the given material.
			 * This method does not validate that such an object exists.
			 *
			 * @param materialIdx The index of the material
			 * @return the ID of the material
			 */
			cio::UniqueId makeMaterialId(std::uint32_t materialIdx) const;

			/**
			 * Creates a unique ID representing the given node.
			 * This method does not validate that such an object exists.
			 *
			 * @param nodeIdx The index of the node
			 * @return the ID of the node
			 */
			cio::UniqueId makeNodeId(std::uint32_t nodeIdx) const;

			/**
			 * Creates a unique ID representing the given mesh.
			 * This method does not validate that such an object exists.
			 * 
			 * @param meshIdx The index mesh
			 * @return the ID of the mesh
			 */
			cio::UniqueId makeMeshId(std::uint32_t meshIdx) const;

			/**
			 * Creates a unique ID representing the given primitive set.
			 * This method does not validate that such an object exists.
			 * 
			 * @param meshIdx The index of the mesh
			 * @param primIdx The index of the primitive set relative to the mesh
			 * @return the ID of the primitive set
			 */
			cio::UniqueId makePrimitiveSetId(std::uint32_t meshIdx, std::uint32_t primIdx) const;

			/**
			 * Creates a unique ID representing a single primitive (typically a triangle) on the primitive set.
			 * The triangle ID is the initial vertex index offset divided by 3.
			 * This method does not validate that such an object exists.
			 *
			 * @param meshIdx The index of the mesh
			 * @param primSetIdx The index of the primitive set relative to the mesh
			 * @param primIdx The index of the primitive relative to the primitive set
			 * @return the ID of the primitive
			 */
			cio::UniqueId makePrimitiveId(std::uint32_t meshIdx, std::uint32_t primSetIdx, std::uint32_t primIdx) const;

			/**
			 * Creates a unique ID representing a single index of a single primitive on the primitive set, regardless of its interpretation.
			 * This method does not validate that such an object exists.
			 *
			 * @param meshIdx The index of the mesh
			 * @param primSetIdx The index of the primitive set relative to the mesh
			 * @param primIdxIdx The primitive index's index relative to the primitive set
			 * @return the ID of the primitive index
			 */
			cio::UniqueId makePrimitiveIndexId(std::uint32_t meshIdx, std::uint32_t primSetIdx, std::uint32_t primIdxIdx) const;
			
			/**
			* Creates a unique ID representing the vertex.
			* This method does not validate that such an object exists.
			*
			* @param meshIdx The index of the mesh
			* @param primIdx The index of the primitive set
			* @param vtx The vertex index
			* @return the ID of the vertex
			*/
			cio::UniqueId makeVertexId(std::uint32_t meshIdx, std::uint32_t primIdx, std::uint32_t vertexOffset) const;

			cio::UniqueId makeEntityId(const geas::Index dsid, std::uint32_t fid) const;

			cio::UniqueId makeEntityId(const geas::Label &node, std::uint32_t fid) const;

			cio::UniqueId makeEntityId(const Node &node, std::uint32_t fid) const;

			/**
			 * Processes the current glTF document for the metadata extensions and populates the catalog and collection from the current data.
			 * Existing catalog and collection data will not be modified.
			 */
			void loadFeatureMetadata();

			/**
			 * Cross-references the current glTF document with the current entity collection and catalog to ensure they are consistent.
			 */
			void synchronizeEntities();

			/**
			 * Gets the extension name in use for working with feature metadata.
			 * The default name is the empty string if no metadata extension is detected or in use, otherwise it is the name of the detected extension.
			 * It can also be manually set to use a particular extension name.
			 * 
			 * @return the metadata extension name
			 */
			const std::string &getMetadataExtensionName() const;

			/**
			 * Sets the extension name in use for working with feature metadata.
			 * This can also be set to the empty string to result in default behavior.
			 *
			 * @param name The metadata extension name
			 */
			void setMetadataExtensionName(std::string name);

			void setPreserveCatalog(bool value);

			bool getPreserveCatalog() const;

			void setAutoloadCatalog(bool value);

			bool getAutoloadCatalog() const;

			void setAutoloadCollection(bool value);

			bool getAutoloadCollection() const;
			
			void updateFeatureWeightMap(std::map<cio::UniqueId, Weight<void>> &weights, unsigned layer) const;

			void updateFeatureWeightMap(const fx::gltf::Scene &scene, std::map<cio::UniqueId, Weight<void>> &weights, unsigned layer) const;

			void updateFeatureWeightMap(const fx::gltf::Node &node, std::map<cio::UniqueId, Weight<void>> &weights, unsigned layer) const;

			void updateFeatureWeightMap(const fx::gltf::Mesh &mesh, std::map<cio::UniqueId, Weight<void>> &weights, unsigned layer) const;

			void updateFeatureWeightMap(const fx::gltf::Primitive &prim, std::map<cio::UniqueId, Weight<void>> &weights, unsigned layer) const;

			FeatureIdWeight extractBestFeature(const std::map<cio::UniqueId, Weight<void>> &weights) const;
			
		private:
			/** The default metadata extension name, currently "EXT_feature_metadata" */
			static const char sDefaultMetadataExtensionName[];

			/**
			 * Common method to ensure the model is set up for processing feature metadata.
			 * 
			 * This will ensure that there is a valid Catalog, Collection, and Document.
			 * The Collection will refer to the Catalog.
			 * The metadata extension will use the default name (if not otherwise set) and be created.
			 */
			void ensureMetadata();

			/** The currently loaded glTF document */
			std::unique_ptr<fx::gltf::Document> mDocument;

			/** The currently active entity catalog */
			std::shared_ptr<Catalog> mCatalog;

			/** The currently active entity colleciton */
			std::unique_ptr<Collection> mEntityCollection;

			/** The protocol factory to use for working with external resources, or nullptr to only use local file resources */
			const cio::ProtocolFactory *mProtocolFactory;

			/** The resource path to the main file (.gltf or .glb) */
			cio::Path mMainFile;

			/** The parent resource to use to resolve for relative paths */
			cio::Path mParentNode;

			/** The name of the metadata extension to use for this document */
			std::string mMetadataExtensionName;

			/** Whether to preserve and augment the assigned catalog when one is loaded from the glTF */
			bool mPreserveCatalog;

			/** Whether to automatically load the catalog from a glTF document extension when loaded or assigned */
			bool mAutoloadCatalog;

			/** Whether to automatically load the collection from a glTF document extension when loaded or assigned */
			bool mAutoloadCollection;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
