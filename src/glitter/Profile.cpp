/*=====================================================================================================================
 * Copyright 2022-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "Profile.h"

#include "AttributeType.h"
#include "GeometryType.h"
#include "Primitive.h"

#include <cio/Class.h>

namespace glitter
{
	cio::Class<Profile> Profile::sMetaclass("glitter::Profile");

	const cio::Class<Profile> &Profile::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}

	Profile::Profile() noexcept
	{
		// nothing more to do
	}

	Profile::Profile(const Profile &in) = default;

	Profile::Profile(Profile &&in) noexcept = default;

	Profile &Profile::operator=(const Profile &in) = default;

	Profile &Profile::operator=(Profile &&in) noexcept = default;

	Profile::~Profile() noexcept = default;

	void Profile::clear() noexcept
	{
		// nothing to do yet
	}

	const cio::Metaclass &Profile::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	void Profile::applyToSchema(geas::Application &schema) const
	{
		// TODO implement
	}

	void Profile::matchFromSchema(const geas::Application &schema)
	{
		// TODO implement
	}

	const geas::Attribute *Profile::getAttribute(AttributeType attribute) const
	{
		return getDefaultAttribute(attribute);
	}

	const geas::Geometry *Profile::getGeometry(GeometryType geometry, Primitive primitive) const
	{
		return getDefaultGeometry(geometry, primitive);
	}
}
