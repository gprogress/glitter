/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "Provider.h"

#include <geas/Attribute.h>

#include <cio/Class.h>
#include <cio/Variant.h>
#include "Table.h"

namespace glitter
{
	cio::Class<Provider> Provider::sMetaclass("Provider");

	const cio::Metaclass &Provider::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	Provider::Provider() noexcept
	{
		// nothing to do
	}

	Provider::Provider(const Provider &in) = default;

	Provider::Provider(Provider &&in) noexcept = default;

	Provider &Provider::operator=(const Provider &in) = default;

	Provider &Provider::operator=(Provider &&in) noexcept = default;

	Provider::~Provider() noexcept = default;

	void Provider::clear() noexcept
	{
		// nothing to do
	}

	geas::Index Provider::size() const
	{
		return 0;
	}

	geas::Index Provider::resize(geas::Index count)
	{
		return this->size();
	}

	// Schema management

	const geas::Attribute *Provider::getSizeProperties() const noexcept
	{
		return nullptr;
	}

	const geas::Attribute *Provider::getAttributeCountProperties() const noexcept
	{
		return nullptr;
	}

	bool Provider::isUniformFieldLayout() const noexcept
	{
		return true;
	}

	geas::Index Provider::create(const geas::Entity *entity, const geas::Geometry *geometry)
	{
		geas::Index current = this->size();
		geas::Index i = this->resize(1 + current);
		if (i > current)
		{
			if (entity || geometry)
			{
				this->setEntityType(i, entity, geometry);
			}
		}

		return i;
	}

	geas::Index Provider::getAttributeCount(geas::Index object) const
	{
		return 0;
	}

	const geas::Entity *Provider::getEntityType(geas::Index object) const
	{
		return nullptr;
	}

	const geas::Geometry *Provider::getGeometryType(geas::Index object) const
	{
		return nullptr;
	}

	void Provider::setEntityType(geas::Index object, const geas::Entity *entity, const geas::Geometry *geometry)
	{
		throw std::invalid_argument("Cannot set entity type");
	}

	const geas::Attribute *Provider::getAttribute(geas::Index object, geas::Index column) const
	{
		return nullptr;
	}

	void Provider::setAttribute(geas::Index object, geas::Index column, const geas::Attribute *defn)
	{
		throw std::invalid_argument("Cannot set attribute definition");
	}

	geas::Index Provider::addAttribute(geas::Index object, const geas::Attribute *defn)
	{
		throw std::invalid_argument("Cannot set attribute definition");
	}

	void Provider::removeAttribute(geas::Index object, geas::Index column)
	{
		throw std::invalid_argument("Cannot remove attribute definition");
	}

	geas::Index Provider::findAttributeByLabel(geas::Index object, const geas::Label &label) const
	{
		geas::Index field = INT32_MIN;
		geas::Index count = this->getAttributeCount(object);

		for (geas::Index i = 0; i < count; ++i)
		{
			const geas::Attribute *a = this->getAttribute(object, i);
			if (a && a->getLabel() == label)
			{
				field = i;
				break;
			}
		}

		return field;
	}

	geas::Index Provider::findAttributeByCode(geas::Index object, const geas::Code &code) const
	{
		geas::Index field = INT32_MIN;
		geas::Index count = this->getAttributeCount(object);

		for (geas::Index i = 0; i < count; ++i)
		{
			const geas::Attribute *a = this->getAttribute(object, i);
			if (a && a->getCode() == code)
			{
				field = i;
				break;
			}
		}

		return field;
	}

	geas::Index Provider::findAttributeByIndex(geas::Index object, geas::Index idx) const
	{
		geas::Index field = INT32_MIN;
		geas::Index count = this->getAttributeCount(object);

		for (geas::Index i = 0; i < count; ++i)
		{
			const geas::Attribute *a = this->getAttribute(object, i);
			if (a && a->getIndex() == idx)
			{
				field = i;
				break;
			}
		}

		return field;
	}

	geas::Index Provider::findMatchingAttribute(geas::Index object, const geas::Attribute &attribute) const
	{
		geas::Index field = INT32_MIN;
		geas::Index count = this->getAttributeCount(object);

		for (geas::Index i = 0; i < count; ++i)
		{
			const geas::Attribute *a = this->getAttribute(object, i);
			if (a && a->matches(attribute))
			{
				field = i;
				break;
			}
		}

		return field;
	}

	Array Provider::getValue(geas::Index object, geas::Index column) const
	{
		return Array();
	}

	bool Provider::setValue(geas::Index object, geas::Index column, const Array &value)
	{
		return false;
	}

	geas::AttributeFault Provider::validateCurrentValue(const geas::Attribute &constraint, geas::Index object, geas::Index attr) const
	{
		return geas::AttributeFault(cio::Validation::NotSupported);
	}

	std::size_t Provider::estimateValueMemoryUse() const noexcept
	{
		return this->estimateNewValueMemoryUse(this->size());
	}

	std::size_t Provider::estimateNewValueMemoryUse(std::size_t count) const noexcept
	{
		const geas::Attribute *attribute = this->getAttribute(geas::Index(0), count);
		std::size_t values = attribute->getRange().upper.bits() * count;
		return values;
	}
}
