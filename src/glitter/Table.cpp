/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "Table.h"

#include "Column.h"

#include <geas/Application.h>
#include <geas/Attribute.h>
#include <geas/Enumerant.h>
#include <geas/Enumeration.h>
#include <geas/Measures.h>
#include <geas/Unit.h>

#include <cio/Class.h>
#include <cio/Type.h>

namespace glitter
{
	cio::Class<Table> Table::sMetaclass("glitter::Table");

	geas::Attribute Table::sSizeProperties;

	geas::Attribute Table::sColumnProperties;

	Table::Table() noexcept :
		mSize(0)
	{
		sSizeProperties.setValueType(cio::Type::Unsigned);

		sColumnProperties.setValueType(cio::Type::Unsigned);
		sColumnProperties.setRange(cio::VariantInterval(0u, 1u));
	}

	const cio::Metaclass &Table::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	Table::Table(const Table &in) = default;

	Table::Table(Table &&in) noexcept :
		Provider(std::move(in)),
		mSize(in.mSize),
		mDataModel(std::move(in.mDataModel)),
		mTableEntity(std::move(in.mTableEntity)),
		mColumns(std::move(in.mColumns))
	{
		in.mSize = 0;
	}

	Table &Table::operator=(const Table &in) = default;

	Table &Table::operator=(Table &&in) noexcept
	{
		if (this != &in)
		{
			Provider::operator=(std::move(in));
			mSize = in.mSize;
			mDataModel = std::move(in.mDataModel);
			mTableEntity = std::move(in.mTableEntity);
			mColumns = std::move(in.mColumns);
		}
		return *this;
	}

	Table::~Table() noexcept = default;

	void Table::clear() noexcept
	{
		Provider::clear();
		mSize = 0;
		mDataModel.reset();
		mTableEntity.clear();
		mColumns.clear();
	}

	geas::Index Table::size() const
	{
		return static_cast<geas::Index>(mSize);
	}

	geas::Index Table::resize(geas::Index count)
	{
		mSize = count;

		for (std::shared_ptr<Provider> &value : mColumns)
		{
			value->resize(count);
		}

		return count;
	}

	geas::Index Table::create(const geas::Entity *entity, const geas::Geometry *geometry)
	{
		return this->resize(mSize + 1);
	}

	const geas::Attribute *Table::getSizeProperties() const noexcept
	{
		return &sSizeProperties;
	}

	const geas::Attribute *Table::getAttributeCountProperties() const noexcept
	{
		return &sColumnProperties;
	}

	bool Table::isUniformFieldLayout() const noexcept
	{
		return true;
	}

	geas::Index Table::getAttributeCount(geas::Index object) const
	{
		return mTableEntity.getAttributes().size();
	}

	const geas::Attribute *Table::getAttribute(geas::Index object, geas::Index field) const
	{
		return mTableEntity.getAttributes().get(field);
	}

	void Table::setAttribute(geas::Index object, geas::Index field, const geas::Attribute *defn)
	{
		mTableEntity.getAttributes().merge(defn);

		while (field >= mColumns.size())
		{
			mColumns.emplace_back(new Column());
		}

		mColumns[field]->setAttribute(object, 0, defn);
	}

	geas::Index Table::addAttribute(geas::Index object, const geas::Attribute *defn)
	{
		geas::Index field = mTableEntity.getAttributes().size();
		mTableEntity.getAttributes().merge(defn);
		mColumns.emplace_back(new Column());
		mColumns.back()->addAttribute(object, defn);
		return field;
	}

	void Table::removeAttribute(geas::Index object, geas::Index field)
	{
		auto ii = mTableEntity.getAttributes().find(field);
		if (ii != mTableEntity.getAttributes().end())
		{
			mColumns.erase(mColumns.begin() + (*ii)->getIndex());
			mTableEntity.getAttributes().erase(ii);
		}
	}

	geas::Index Table::findAttributeByLabel(geas::Index object, const geas::Label &label) const
	{
		geas::Index fieldIdx = INT32_MIN;

		const geas::Attribute *field = mTableEntity.getAttributes().get(label);

		if (field)
		{
			fieldIdx = field->getIndex();
		}

		return fieldIdx;
	}

	geas::Index Table::findAttributeByCode(geas::Index object, const geas::Code &code) const
	{
		geas::Index fieldIdx = INT32_MIN;

		const geas::Attribute *field = mTableEntity.getAttributes().get(code);

		if (field)
		{
			fieldIdx = field->getIndex();
		}

		return fieldIdx;
	}

	geas::Index Table::findAttributeByIndex(geas::Index object, geas::Index idx) const
	{
		geas::Index fieldIdx = INT32_MIN;

		const geas::Attribute *field = mTableEntity.getAttributes().get(idx);

		if (field)
		{
			fieldIdx = field->getIndex();
		}

		return fieldIdx;
	}

	geas::Index Table::findMatchingAttribute(geas::Index object, const geas::Attribute &attribute) const
	{
		geas::Index fieldIdx = INT32_MIN;

		const geas::Attribute *field = mTableEntity.getAttributes().get(attribute);

		if (field)
		{
			fieldIdx = field->getIndex();
		}

		return fieldIdx;
	}

	Array Table::getValue(geas::Index object, geas::Index field) const
	{
		// TODO this is not yet const correct, but we need accessor support and a const value array to make that happen
		Array result;
		if (object < mSize && field < mColumns.size())
		{
			result = mColumns[field]->getValue(object, 0);
		}

		return result;
	}

	bool Table::setValue(geas::Index object, geas::Index field, const Array &value)
	{
		bool found = false;

		if (object < mSize && field < mColumns.size())
		{
			found = mColumns[field]->setValue(object, 0, value);
		}

		return found;
	}

	geas::AttributeFault Table::validateCurrentValue(const geas::Attribute &constraint, geas::Index object, geas::Index field) const
	{
		geas::AttributeFault result;
		if (field >= mColumns.size())
		{
			result.setOverallStatus(cio::fail(cio::Reason::Missing));
			result.setSeverity(cio::Severity::Error);
			result.setMessage("geas::Attribute not present");
		}
		else
		{
			std::shared_ptr<Provider> base = mColumns[field];
			result = base->validateCurrentValue(constraint, object, 0);
		}

		return result;
	}

	std::shared_ptr<geas::Application> Table::getDataModel() noexcept
	{
		return mDataModel;
	}

	std::shared_ptr<const geas::Application> Table::getDataModel() const noexcept
	{
		return mDataModel;
	}

	std::shared_ptr<geas::Application> Table::getOrCreateDataModel()
	{
		if (!mDataModel)
		{
			mDataModel.reset(new geas::Application());
		}

		return mDataModel;
	}

	void Table::setDataModel(std::shared_ptr<geas::Application> schema) noexcept
	{
		mDataModel = std::move(schema);
	}

	void Table::clearDataModel() noexcept
	{
		mDataModel.reset();
	}


	geas::Attribute *Table::addCustomDatatype(geas::Attribute datatype)
	{
		std::shared_ptr<geas::Application> dm = this->getOrCreateDataModel();

		return dm->getAttributes().create(std::move(datatype));
	}

	geas::Attribute *Table::addCustomDatatype(geas::Label label, geas::Code code, cio::Type type)
	{
		std::shared_ptr<geas::Application> dm = this->getOrCreateDataModel();

		geas::Attribute datatype;
		datatype.setLabel(std::move(label));
		datatype.setCode(code);
		datatype.setValueType(type);

		return dm->getAttributes().create(std::move(datatype));
	}

	geas::Attribute *Table::addCustomDatatype(geas::Label label, geas::Code code, cio::Type type, const geas::Quantity *quantity)
	{
		geas::Attribute *datatype = this->addCustomDatatype(label, code, type);

		if (quantity)
		{
			datatype->setUnit(this->addCustomUnit(quantity));
		}

		return datatype;
	}

	geas::Attribute *Table::addCustomField(geas::Label label, geas::Code code, const geas::Attribute *datatype)
	{
		std::shared_ptr<geas::Application> dm = this->getOrCreateDataModel();

		geas::Index idx = mTableEntity.getAttributes().size();
		geas::Attribute field(idx);
		field.setLabel(std::move(label));
		field.setCode(code);
		field.setSemantic(datatype->getSemantic());
		
		geas::Attribute *created = dm->getAttributes().create(std::move(field));
		mTableEntity.getAttributes().merge(created);
		return created;
	}

	geas::Unit *Table::addCustomUnit(const geas::Quantity *quantity)
	{
		std::shared_ptr<geas::Application> dm = this->getOrCreateDataModel();
		std::shared_ptr<geas::Measures> ms = dm->getOrCreateMeasures();

		geas::Unit *unit = ms->getUnits().create();
		unit->setQuantity(quantity);
		return unit;
	}

	std::size_t Table::estimateValueMemoryUse() const noexcept
	{
		return this->estimateNewValueMemoryUse(mSize);
	}

	std::size_t Table::estimateNewValueMemoryUse(std::size_t count) const noexcept
	{
		geas::Index total = 0;
		for (const geas::Attribute *field : mTableEntity.getAttributes())
		{
			std::size_t added;
			if (field->getEnumeration())
			{
				added = field->getEnumeration()->getEnumerants().size() *cio::bits(field->getEnumeration()->getComponentType());
			}
			else
			{
				added = field->getRange().lower.bits();
			}
			total += added;
		}

		return count * total;
	}

	std::vector<std::shared_ptr<Provider>> &Table::getColumns() noexcept
	{
		return mColumns;
	}

	const std::vector<std::shared_ptr<Provider>> &Table::getColumns() const noexcept
	{
		return mColumns;
	}

	void Table::setColumns(std::vector<std::shared_ptr<Provider>> values) noexcept
	{
		mColumns = std::move(values);
	}

	std::shared_ptr<Provider> Table::getColumn(geas::Index field)
	{
		return mColumns.at(field);
	}

	std::shared_ptr<const Provider> Table::getColumn(geas::Index field) const
	{
		return mColumns.at(field);
	}

	void Table::setColumn(geas::Index field, std::shared_ptr<Provider> values)
	{
		mColumns.at(field) = std::move(values);
	}

	bool Table::hasColumn(geas::Index field) const noexcept
	{
		return field < mColumns.size();
	}

	bool Table::hasObject(geas::Index object) const noexcept
	{
		return object < mSize;
	}

	bool Table::hasObjectColumn(geas::Index object, geas::Index field) const noexcept
	{
		return field < mColumns.size() && object < mSize;
	}
}

