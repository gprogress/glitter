/*=====================================================================================================================
 * Copyright 2022-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_MODELREADER_H
#define GLITTER_MODELREADER_H

#include "Types.h"

#include "Reader.h"

#include <memory>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace glitter
{
	/**
	 * The Model Reader refines the Reader to support easily loading conventional 3D formats such as meshes, point clouds, and shapefiles.
	 * It uses the Profile to mediate between the Application Schema and the standard 3D definitions.
	 *
	 * When possible, readers should try to allow any resource type and protocol - relying on a protocol factory to mediate this -
	 * and primarily focus on the decoding of the data from generic streams.
	 */
	class GLITTER_ABI ModelReader : public Reader
	{
		public:
			/**
			 * Gets the metaclass for this class.
			 *
			 * @return the metaclass for this class
			 */
			static const cio::Class<ModelReader> &getDeclaredMetaclass() noexcept;

			/**
			 * Default constructor.
			 */
			ModelReader() noexcept;

			/**
			 * Constructs a reader with the given profile.
			 * This does NOT call onProfileChanged() since virtual methods cannot be used in a constructor.
			 *
			 * @param profile The profile to use
			 */
			explicit ModelReader(std::shared_ptr<Profile> profile) noexcept;

			/**
			 * Copy constructor.
			 *
			 * @param in The reader to copy
			 */
			ModelReader(const ModelReader &in);

			/**
			 * Move constructor.
			 *
			 * @param in The reader to move
			 */
			ModelReader(ModelReader &&in) noexcept;

			/**
			 * Copy assignment.
			 * This does NOT call onProfileChanged() since subclasses are expected to override it.
			 *
			 * @param in The reader to copy
			 * @return this reader
			 */
			ModelReader &operator=(const ModelReader &in);

			/**
			 * Move assignment.
			 * This does NOT call onProfileChanged() since subclasses are expected to override it.
			 *
			 * @param in The reader to move
			 * @return this reader
			 */
			ModelReader &operator=(ModelReader &&in) noexcept;

			/**
			 * Destructor.
			 */
			virtual ~ModelReader() noexcept override;

			/**
			 * Clears the reader state.
			 * This does NOT call onProfileChanged() since subclasses are expected to override it.
			 */
			virtual void clear() noexcept override;

			/**
			 * Gets the runtime metaclass for this object instance.
			 *
			 * @return the runtime metaclass for this object
			 */
			virtual const cio::Metaclass &getMetaclass() const noexcept override;

			/**
			 * Called when the model profile is modified or removed.
			 */
			virtual void onProfileChanged();

			/**
			 * Gets the model profile currently in use.
			 *
			 * @return the model profile, or nullptr if none
			 */
			std::shared_ptr<Profile> getProfile() noexcept;

			/**
			 * Gets the model profile currently in use.
			 *
			 * @return the model profile, or nullptr if none
			 */
			std::shared_ptr<const Profile> getProfile() const noexcept;

			/**
			 * Gets the model profile currently in use, creating a new one if none was set.
			 * This calls onProfileChanged() if a new one was created.
			 *
			 * @return the model profile
			 * @throw std::bad_alloc If memory for a new profile could not be allocated
			 */
			std::shared_ptr<Profile> getOrCreateProfile() noexcept;

			/**
			 * Sets the model profile to use.
			 * This calls onProfileChanged() if the current or new profile is not null.
			 *
			 * @param profile The new profile
			 */
			void setProfile(std::shared_ptr<Profile> profile) noexcept;

			/**
			 * Clears the model profile.
			 * This calls onProfileChanged() if the current one is not null.
			 */
			void clearProfile() noexcept;

		protected:
			/** The model profile in use. */
			std::shared_ptr<Profile> mProfile;

		private:
			/** The metaclass for this class. */
			static cio::Class<ModelReader> sMetaclass;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
