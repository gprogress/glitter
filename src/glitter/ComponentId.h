/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_COMPONENTID_H
#define GLITTER_COMPONENTID_H

#include "Types.h"

#include "ComponentType.h"

namespace glitter
{
	/** 
	 * The component ID is used to generically represent a piece of data in a conforming glTF document.
	 * This is generally not directly exposed to the user but instead packed into a geas::UniqueId.
	 */
	struct ComponentId
	{
		/** The document-level type of data that contains the desired information */
		ComponentType base;

		/** The specific type of data being referenced */
		ComponentType type;

		/** geas::Index of an attribute relative to an entity or primitive set */
		std::uint16_t attribute;

		/** The index of the document-level data type to access */
		std::uint32_t item;

		/** If the type is a primitive set or an element one, the primitive set index to the mesh */
		std::uint32_t primitiveset;

		/** The element within the primitive set or otherwise referenced data type to access */
		std::uint32_t element;
	};

	static_assert(sizeof(ComponentId) <= 16, "ComponentId byte layout won't fit into geas::UniqueId");

	GLITTER_ABI std::string toString(const ComponentId &id);

	GLITTER_ABI std::ostream &operator<<(std::ostream &s, const ComponentId &id);
}

#endif
