/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "Collection.h"

#include <geas/Dictionary.cpp>

#include <cio/Class.h>

namespace glitter
{
	cio::Class<Collection> Collection::sMetaclass("glitter::Collection");

	Collection::Collection() noexcept :
		mProfile(nullptr),
		mProjection(nullptr)
	{
		// nothing more to do
	}

	Collection::Collection(const Collection &in) = default;

	Collection::Collection(Collection &&in) noexcept :
		geas::Definition(std::move(in)),
		mCatalog(in.mCatalog),
		mProfile(in.mProfile),
		mNodes(std::move(in.mNodes)),
		mProjection(in.mProjection),
		mCoverage(in.mCoverage)
	{
		in.mCatalog = nullptr;
	}

	Collection &Collection::operator=(const Collection &in) = default;

	Collection &Collection::operator=(Collection &&in) noexcept
	{
		if (this != &in)
		{
			geas::Definition::operator=(std::move(in));

			mCatalog = in.mCatalog;
			mProfile = in.mProfile;

			mNodes = std::move(in.mNodes);
			mProjection = in.mProjection;
			mCoverage = in.mCoverage;
		}

		return *this;
	}

	Collection::~Collection() noexcept = default;

	const cio::Metaclass &Collection::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	void Collection::clear() noexcept
	{
		geas::Definition::clear();

		mNodes.clear();
		mCatalog.reset();
		mProfile = nullptr;
		mProjection = nullptr;
		mCoverage.clear();
	}

	std::shared_ptr<Catalog> Collection::getCatalog() noexcept
	{
		return mCatalog;
	}

	std::shared_ptr<const Catalog> Collection::getCatalog() const noexcept
	{
		return mCatalog;
	}

	std::shared_ptr<Catalog> Collection::getOrCreateCatalog()
	{
		if (!mCatalog)
		{
			mCatalog.reset(new Catalog());

			mCatalog->create();

			std::shared_ptr<const geas::Application> schema = mCatalog->getApplicationSchema();

			for (Node &node : mNodes)
			{
				node.setApplicationSchema(schema);
			}
		}

		return mCatalog;
	}

	void Collection::setCatalog(std::shared_ptr<Catalog> catalog) noexcept
	{
		if (catalog != mCatalog)
		{
			mCatalog = std::move(catalog);

			if (mCatalog)
			{
				std::shared_ptr<const geas::Application> schema = mCatalog->getApplicationSchema();

				for (Node &node : mNodes)
				{
					node.setApplicationSchema(schema);
				}
			}
			else
			{
				for (Node &node : mNodes)
				{
					node.clearApplicationSchema();
				}
			}
		}
	}

	void Collection::clearCatalog() noexcept
	{
		mCatalog.reset();

		for (Node &node : mNodes)
		{
			node.clearApplicationSchema();
		}
	}

	const geas::ApplicationProfile *Collection::getActiveProfile() const noexcept
	{
		return mProfile;
	}

	void Collection::setActiveProfile(const geas::ApplicationProfile *profile) noexcept
	{
		mProfile = profile;
	}

	geas::Dictionary<Node> &Collection::getNodes() noexcept
	{
		return mNodes;
	}

	const geas::Dictionary<Node> &Collection::getNodes() const noexcept
	{
		return mNodes;
	}

	void Collection::setNodes(geas::Dictionary<Node> nodes) noexcept
	{
		mNodes = std::move(nodes);
	}

	void Collection::addNode(Node node)
	{
		mNodes.insert(std::move(node));
	}

	void Collection::clearNodes() noexcept
	{
		mNodes.clear();
	}

	const geas::Projection *Collection::getProjection() const noexcept
	{
		return mProjection;
	}

	void Collection::setProjection(const geas::Projection *projection) noexcept
	{
		mProjection = projection;
	}

	const Region &Collection::getCoverage() const noexcept
	{
		return mCoverage;
	}

	void Collection::setCoverage(Region &coverage) noexcept
	{
		mCoverage = coverage;
	}

	void Collection::clearCoverage() noexcept
	{
		mCoverage.clear();
	}
}

/* Explicit instantiations */

#if defined _MSC_VER
#pragma warning(disable: 4251)
#endif

namespace geas
{
	template class GLITTER_TEMPLATE_ABI geas::Dictionary<glitter::Collection>;
}

