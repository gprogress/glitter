/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_CATALOG_H
#define GLITTER_CATALOG_H

#include "Types.h"

#include "Region.h"

#include <geas/Application.h>
#include <geas/Measures.h>
#include <geas/Vocabulary.h>

#include <memory>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace glitter
{
	/**
	 * The Catalog is a vocabulary of vocabularies that provides the complete set of information to describe
	 * real world geospatial datasets. It consists of an application schema, a semantic vocabulary, and a measurement system.
	 *
	 * The Catalog manages the memory of all underlying vocabularies and ensures they all properly cross-reference each other.
	 */
	class GLITTER_ABI Catalog : public geas::Definition
	{
		public:
			/**
			 * Construct an empty catalog.
			 */
			Catalog() noexcept;

			/**
			 * Fully copy a catalog.
			 * This includes deep copying the application schema, measure sytem, and vocabulary.
			 *
			 * @param in The catalog to deep copy
			 */
			Catalog(const Catalog &in);

			/**
			 * Move a catalog.
			 *
			 * @param in The catalog to move
			 */
			Catalog(Catalog &&in) noexcept;

			/**
			 * Fully copy a catalog.
			 * This includes deep copying the application schema, measure sytem, and vocabulary.
			 *
			 * @param in The catalog to deep copy
			 * @return this catalog
			 */
			Catalog &operator=(const Catalog &in);

			/**
			 * Move a catalog.
			 *
			 * @param in The catalog to move
			 * @return this catalog
			 */
			Catalog &operator=(Catalog &&in) noexcept;

			/**
			 * Destructor.
			 */
			virtual ~Catalog() noexcept override;

			/**
			 * Gets the runtime metaclass for this object instance.
			 *
			 * @return the runtime metaclass for this object
			 */
			virtual const cio::Metaclass &getMetaclass() const noexcept override;

			/**
			 * Clears the entire catalog state.
			 */
			virtual void clear() noexcept override;

			/**
			 * Creates new content for the catalog.
			 * This creates a new vocabulary and measurement system, then creates
			 * a new application schema and updates it to use them.
			 *
			 * Any existing content on the catalog will be lost.
			 *
			 * @throw std::bad_alloc If memory for any new objects could not be allocated
			 */
			void create();

			/**
			 * Gets the application schema used by this catalog if one is set.
			 *
			 * @return the application schema or null if none is in use
			 */
			std::shared_ptr<geas::Application> getApplicationSchema() noexcept;

			/**
			 * Gets the application schema used by this catalog if one is set.
			 *
			 * @return the application schema or null if none is in use
			 * @throw std::bad_alloc If memory for any new objects could not be allocated
			 */
			std::shared_ptr<const geas::Application> getApplicationSchema() const noexcept;

			/**
			 * Gets the application schema used by this catalog if one is set, creating an empty one if not.
			 * If a new schema is created, it will use the vocabulary and measure system currently on the catalog.
			 *
			 * @return the application schema
			 * @throw std::bad_alloc If memory for a new schema could not be allocated
			 */
			std::shared_ptr<geas::Application> getOrCreateApplicationSchema();

			/**
			 * Sets the current application schema.
			 * If non-null and the given schema already has a vocabulary or measure system, this catalog
			 * will also be updated to use the new vocabulary and/or measure system.
			 * Otherwise, the schema will be updated to use the vocabulary and/or measure system currently on the catalog.
			 *
			 * @param schema The application schema to set
			 */
			void setApplicationSchema(std::shared_ptr<geas::Application> schema) noexcept;

			/**
			 * Removes the current application schema in use.
			 */
			void clearApplicationSchema() noexcept;

			// Profile Manager

			/**
			 * Gets application profile manager used by this catalog if any.
			 *
			 * @return the profile manager or nullptr if none was set
			 */
			std::shared_ptr<geas::Profiles> getProfiles() noexcept;

			/**
			 * Gets the application profile manager used by this catalog if one is set.
			 *
			 * @return the profile manager or null if none is in use
			 */
			std::shared_ptr<const geas::Profiles> getProfiles() const noexcept;

			/**
			 * Gets the application profile manager if one is set, creating a new one if not.
			 * If a new one is created, it is configured to use the current application schema.
			 *
			 * @return the profile manager
			 * @throw std::bad_alloc If memory for a new vocabulary could not be allocated
			 */
			std::shared_ptr<geas::Profiles> getOrCreateProfiles();

			/**
			 * Sets the application profile manager to use for this catalog.
			 * If the catalog has an application schema set, the profile manager will be configured to use it.
			 *
			 * @param profiles The profile manager to use
			 */
			void setProfiles(std::shared_ptr<geas::Profiles> profiles) noexcept;

			/**
			 * Clears the low-level vocabulary. This will also remove it from the application schema if set.
			 */
			void clearProfiles() noexcept;

			/**
			 * Gets the default recommended application profile.
			 *
			 * @return the default profile
			 */
			const geas::ApplicationProfile *getDefaultProfile() const noexcept;

			/**
			 * Sets the default recommended application profile.
			 *
			 * @param profile The default profile
			 */
			void setDefaultProfile(const geas::ApplicationProfile *profile) noexcept;

			// Vocabulary

			/**
			 * Gets the low-level vocabulary used by this catalog if one is set.
			 *
			 * @return the vocabulary or null if none is in use
			 */
			std::shared_ptr<geas::Vocabulary> getVocabulary() noexcept;

			/**
			 * Gets the application schema used by this catalog if one is set.
			 *
			 * @return the application schema or null if none is in use
			 */
			std::shared_ptr<const geas::Vocabulary> getVocabulary() const noexcept;

			/**
			 * Gets the low-level vocabulary used by this catalog if one is set, creating a new one if not.
			 * If a new one is created, it is assigned to the application schema if that is set.
			 *
			 * @return the vocabulary
			 * @throw std::bad_alloc If memory for a new vocabulary could not be allocated
			 */
			std::shared_ptr<geas::Vocabulary> getOrCreateVocabulary();

			/**
			 * Sets the low-level vocabulary used by this catalog.
			 * If the application schema is set, it will also be updated to use the vocabulary.
			 *
			 * @param vocabulary The vocabulary to set
			 */
			void setVocabulary(std::shared_ptr<geas::Vocabulary> vocabulary) noexcept;

			/**
			 * Clears the low-level vocabulary. This will also remove it from the application schema if set.
			 */
			void clearVocabulary() noexcept;

			/**
			 * Gets the measure system used by this catalog if one is set.
			 *
			 * @return the measure system or null if none is in use
			 */
			std::shared_ptr<geas::Measures> getMeasures() noexcept;

			/**
			 * Gets the measure system used by this catalog if one is set.
			 *
			 * @return the measure system or null if none is in use
			 */
			std::shared_ptr<const geas::Measures> getMeasures() const noexcept;

			/**
			 * Gets the measure system used by this catalog if one is set, creating a new one otherwise.
			 * If a new one is created, it is assigned to the application schema if that is set.
			 *
			 * @return the measure system
			 * @throw std::bad_alloc If memory for a new measure system could not be allocated
			 */
			std::shared_ptr<geas::Measures> getOrCreateMeasures();

			/**
			 * Sets the measurement system used by this catalog.
			 * If the application schema is set, it will also be updated to use the measurement system.
			 *
			 * @param system The measurement system to set
			 */
			void setMeasures(std::shared_ptr<geas::Measures> system) noexcept;

			/**
			 * Clears the measurement system. This will also remove it from the application schema if set.
			 */
			void clearMeasures() noexcept;

			/**
			 * Gets the geographic coverage of all content using this catalog if known.
			 *
			 * @return the geographic coverage
			 */
			const Region &getCoverage() const noexcept;

			/**
			 * Sets the geographic coverage of all content using this catalog if known.
			 *
			 * @param coverage the new geographic coverage
			 */
			void setCoverage(Region &coverage) noexcept;

			/**
			 * Clears the geographic coverage of this catalog.
			 */
			void clearCoverage() noexcept;

			/**
			 * Copies the contents of the given catalog.
			 *
			 * @param in The catalog to copy
			 * @throw std::bad_alloc If memory for any new objects or fields could not be allocated
			 */
			void copyCatalog(const Catalog &in);

			/**
			 * Merges the contents of the given catalog by copying.
			 * Copies of all merged data will be made.
			 *
			 * @param in The catalog to copy
			 * @throw std::bad_alloc If memory for any new objects or fields could not be allocated
			 */
			void mergeCatalog(const Catalog &in);

			/**
			 * Merges the contents of the given catalog by moving.
			 * Where possible, moves will be used to optimize merges.
			 *
			 * @param in The catalog to move
			 * @throw std::bad_alloc If memory for any new objects or fields could not be allocated
			 */
			void mergeCatalog(Catalog &&in);

		private:
			/** The metaclass for this class. */
			static cio::Class<Catalog> sMetaclass;

			/** The shared vocabulary. */
			std::shared_ptr<geas::Vocabulary> mVocabulary;

			/** The shared measure system. */
			std::shared_ptr<geas::Measures> mMeasures;

			/** The shared application schema. */
			std::shared_ptr<geas::Application> mApplicationSchema;

			/** Known application profiles for this catalog. */
			std::shared_ptr<geas::Profiles> mProfiles;

			/** Active default profile. */
			const geas::ApplicationProfile *mDefaultProfile;

			/** The geographic bounds of the collection if known. */
			Region mCoverage;
	};
}

namespace geas
{
	/** Instantiate geas::Dictionary<Catalog> for use in other containers. */
	extern template class GLITTER_TEMPLATE geas::Dictionary<glitter::Catalog>;
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
