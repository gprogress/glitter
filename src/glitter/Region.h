/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_REGION_H
#define GLITTER_REGION_H

#include "Types.h"

#include "Location.h"

namespace glitter
{
	/**
	 * The Region class represents a simple geographic region defined by a southwest and northeast location.
	 * Note that since this is a geographic region, the west longitude may actually be greater than the east longitude
	 * if the region crosses the international date line.
	 */
	class Region
	{
		public:
			/** The southwest corner of the region. */
			Location southwest;

			/** The northwest corner of the region. */
			Location northeast;

			/** Clears the region to its default invalid state. */
			inline void clear() noexcept;

			/**
			 * Interprets whether the region is set.
			 * This is true if both underlying locations are set.
			 */
			inline explicit operator bool() const noexcept;
	};
}

/* inline implementation */

namespace glitter
{
	inline void Region::clear() noexcept
	{
		this->southwest.clear();
		this->northeast.clear();
	}

	inline Region::operator bool() const noexcept
	{
		return this->southwest && this->northeast;
	}
}

#endif