/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_DEGREE_H
#define GLITTER_DEGREE_H

#include "Types.h"

#include <cmath>

namespace glitter
{
	/**
	 * The Degree template class is used to model an angle quantity using decimal degrees.
	 * It is used to ensure safety of angle units with automatic conversions to and from radians.
	 *
	 * @tparam <T> The underlying number type, typically double or float
	 */
	template <typename T>
	class Degree
	{
		public:
			/** Numeric value in degrees. */
			T value;

			/**
			 * Default constructor.
			 */
			Degree();

			/**
			 * Construct the angle with the given value in degrees.
			 *
			 * @param value the value
			 */
			explicit Degree(T value);

			/**
			 * Construct by copying from a different Degree instantiation.
			 *
			 * @tparam <U> The other degree type
			 * @param in The angle to copy
			 */
			template <typename U>
			Degree(const Degree<U> &in);

			/**
			 * Construct by copying from a Radian instantiation,
			 * converting from radians to degrees.
			 *
			 * @tparam <U> The radian type
			 * @param in The angle to convert
			 */
			template <typename U>
			Degree(const Radian<U> &in);

			/**
			 * Sets the angle equal to the given value.
			 *
			 * @param value The value to set
			 * @return this angle
			 */
			Degree<T> &operator=(T value);

			/**
			 * Clears the angle, resetting it to default value.
			 */
			void clear();

			/**
			 * Explicitly casts to a different Degree instantiation,
			 * avoiding downcast warnings.
			 *
			 * @tparam <U> The desired value type
			 * @return the casted angle in degrees
			 */
			template <typename U>
			explicit operator Degree<U>() const;

			/**
			 * Explicitly casts to a Radian instantiation,
			 * avoiding downcast warnings while converting from degrees to radians.
			 *
			 * @tparam <U> The desired value type
			 * @return the casted angle in radians
			 */
			template <typename U>
			explicit operator Radian<U>() const;

			/**
			 * Automatically casts this degree angle to its underlying numeric type.
			 *
			 * @return the value
			 */
			operator T() const;

			/**
			 * Interprets whether the angle is set.
			 * This is true if it is nonzero and false if it is zero or NaN.
			 *
			 * @return whether this angle is set
			 */
			explicit operator bool() const;
	};
}

#include "Radian.h"

/* Inline implementation */

namespace glitter
{
	template <typename T>
	Degree<T>::Degree() :
		T()
	{
		// nothing more to do
	}

	template <typename T>
	Degree<T>::Degree(T value) :
		value(value)
	{
		// nothing more to do
	}

	template <typename T>
	template <typename U>
	Degree<T>::Degree(const Degree<U> &in) :
		value(in.value)
	{
		// nothing more to do
	}

	template <typename T>
	template <typename U>
	Degree<T>::Degree(const Radian<U> &in) :
		value(in.value * 180 / M_PI)
	{
		// nothing more to do
	}

	template <typename T>
	Degree<T> &Degree<T>::operator=(T value)
	{
		this->value = value;
		return *this;
	}

	template <typename T>
	void Degree<T>::clear()
	{
		this->value = T();
	}

	template <typename T>
	template <typename U>
	Degree<T>::operator Degree<U>() const
	{
		return Degree<U>(static_cast<U>(this->value));
	}

	template <typename T>
	template <typename U>
	Degree<T>::operator Radian<U>() const
	{
		return Radian<U>(static_cast<U>(this->value * M_PI / 180));
	}

	template <typename T>
	Degree<T>::operator T() const
	{
		return this->value;
	}

	template <typename T>
	Degree<T>::operator bool() const
	{
		// check is formulated to return false on 0 and NaN
		// but not to assume the type actually has NaN
		return !(this->value == T());
	}
}

#endif
