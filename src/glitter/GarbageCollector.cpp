/*=====================================================================================================================
 * Copyright 2021-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "GarbageCollector.h"

#include "Model.h"

#include <fx/gltf.h>

#include "Column.h"
#include "Table.h"

namespace glitter
{
	GarbageCollector::GarbageCollector() noexcept
	{
		// nothing more to do
	}

	GarbageCollector::~GarbageCollector() noexcept = default;

	void GarbageCollector::clear() noexcept
	{
		mBuffers.clear();
		mBufferViews.clear();
		mAccessors.clear();
		mFeatureTables.clear();
		mImages.clear();
		mSamplers.clear();
		mTextures.clear();
		mMaterials.clear();
		mMeshes.clear();
		mCameras.clear();
		mSkins.clear();
		mNodes.clear();
		mScenes.clear();
		mAnimations.clear();
	}

	void GarbageCollector::markDefaultScene(const Model &model)
	{
		const fx::gltf::Document *doc = model.getDocument();
		if (doc)
		{
			this->markScene(model, doc->scene);
		}
	}

	void GarbageCollector::markScenes(const Model &model)
	{
		const fx::gltf::Document *doc = model.getDocument();
		if (doc)
		{
			for (std::size_t i = 0; i < doc->scenes.size(); ++i)
			{
				this->markScene(model, i);
			}
		}
	}

	void GarbageCollector::markScene(const Model &model, std::int32_t index)
	{
		if (mScenes.mark(index))
		{
			const fx::gltf::Scene &scene = model.getDocument()->scenes[index];
			for (auto node : scene.nodes)
			{
				this->markNode(model, node);
			}
		}
	}

	void GarbageCollector::markNode(const Model &model, std::int32_t index)
	{
		if (mNodes.mark(index))
		{
			const fx::gltf::Node &node = model.getDocument()->nodes[index];
			this->markMesh(model, node.mesh);
			for (auto children : node.children)
			{
				this->markNode(model, children);
			}
		}
	}

	void GarbageCollector::markMesh(const Model &model, std::int32_t index)
	{
		if (mMeshes.mark(index))
		{
			const fx::gltf::Mesh &mesh = model.getDocument()->meshes[index];
			for (const fx::gltf::Primitive &prim : mesh.primitives)
			{
				// Process material
				this->markMaterial(model, prim.material);

				// Process vertex attributes
				for (const std::pair<std::string, std::uint32_t> &attr : prim.attributes)
				{
					this->markAccessor(model, attr.second);
				}

				// Process index array accessor
				this->markAccessor(model, prim.indices);

				// process morph targets
				for (const fx::gltf::Attributes &target : prim.targets)
				{
					for (const std::pair<std::string, std::uint32_t> &attr : target)
					{
						this->markAccessor(model, attr.second);
					}
				}

				// process associated feature tables if present
				auto extlist = prim.extensionsAndExtras.find("extensions");
				if (extlist != prim.extensionsAndExtras.end())
				{
					auto ext = extlist->find(model.getMetadataExtensionName());
					if (ext != extlist->end())
					{
						auto attr = ext->find("featureIdAttributes");
						if (attr != ext->end())
						{
							for (auto item : *attr)
							{
								auto ft = item.find("featureTable");
								if (ft != item.end())
								{
									geas::Label name = ft->get<std::string>();
									const Node *ds = model.getEntityCollection()->getNodes().get(name);
									if (ds)
									{
										this->markFeatureTable(model, ds->getIndex());
									}
								}
							}
						}

						auto texlist = ext->find("featureIdTextures");
						if (texlist != ext->end())
						{
							for (auto item : *texlist)
							{
								auto fid = item.find("featureIds");
								if (fid != item.end())
								{
									auto tx = fid->find("texture");
									if (tx != fid->end())
									{
										auto idx = tx->find("index");
										if (idx != tx->end())
										{
											std::int32_t value = idx->get<std::int32_t>();
											this->markTexture(model, value);
										}
									}
								}

								auto ft = item.find("featureTable");
								if (ft != item.end())
								{
									geas::Label name = ft->get<std::string>();
									const Node *ds = model.getEntityCollection()->getNodes().get(name);
									if (ds)
									{
										this->markFeatureTable(model, ds->getIndex());
									}
								}
							}
						}
					}
				}
			}
		}
	}

	void GarbageCollector::markAccessor(const Model &model, std::int32_t index)
	{
		if (mAccessors.mark(index))
		{
			const fx::gltf::Accessor &accessor = model.getDocument()->accessors[index];
			this->markBufferView(model, accessor.bufferView);
			this->markBufferView(model, accessor.sparse.indices.bufferView);
			this->markBufferView(model, accessor.sparse.values.bufferView);
		}
	}

	void GarbageCollector::markBufferView(const Model &model, std::int32_t index)
	{
		if (mBufferViews.mark(index))
		{
			const fx::gltf::BufferView &view = model.getDocument()->bufferViews[index];
			this->markBuffer(model, view.buffer);
		}
	}

	void GarbageCollector::markBuffer(const Model &model, std::int32_t index)
	{
		mBuffers.mark(index);
	}

	void GarbageCollector::markAllFeatureTables(const Model &model)
	{
		for (const Node &Node : model.getEntityCollection()->getNodes())
		{
			this->markFeatureTable(model, Node.getIndex());
		}
	}

	void GarbageCollector::markFeatureTable(const Model &model, std::int32_t index)
	{
		// feature tables indices start with 1, so we have to subtract 1 to use for arrays
		if (mFeatureTables.mark(index - 1))
		{
			// Currently Geodex does not support all possible cross-references in a feature table
			// So instead we manually process the JSON here
			const fx::gltf::Document *doc = model.getDocument();
			if (doc)
			{
				auto extlist = doc->extensionsAndExtras.find("extensions");
				if (extlist != doc->extensionsAndExtras.end())
				{
					auto ext = extlist->find(model.getMetadataExtensionName());
					if (ext != extlist->end())
					{
						// get the feature table list
						auto fts = ext->find("featureTables");
						if (fts != ext->end())
						{
							// Get the feature table by index
							// Note JSON does not directly allow this so we have to manually do it
							auto ft = fts->begin();
							std::advance(ft, index - 1);

							// Get properties
							auto pp = ft->find("properties");
							if (pp != ft->end())
							{
								for (auto attr : *pp)
								{
									auto bv = attr.find("bufferView");
									if (bv != attr.end())
									{
										std::int32_t idx = bv->get<std::int32_t>();
										this->markBufferView(model, idx);
									}

									auto sov = attr.find("stringOffsetBufferView");
									if (sov != attr.end())
									{
										std::int32_t idx = sov->get<std::int32_t>();
										this->markBufferView(model, idx);
									}

									auto aov = attr.find("arrayOffsetBufferView");
									if (aov != attr.end())
									{
										std::int32_t idx = aov->get<std::int32_t>();
										this->markBufferView(model, idx);
									}
								}
							}
						}
					}
				}
			}
		}
	}

	void GarbageCollector::markAllMaterials(const Model &model)
	{
		const fx::gltf::Document *doc = model.getDocument();
		if (doc)
		{
			for (std::size_t i = 0; i < doc->materials.size(); ++i)
			{
				this->markMaterial(model, i);
			}
		}
	}

	void GarbageCollector::markImage(const Model &model, std::int32_t index)
	{
		if (mImages.mark(index))
		{
			const fx::gltf::Image &image = model.getDocument()->images[index];
			this->markBufferView(model, image.bufferView);
		}
	}

	void GarbageCollector::markSampler(const Model &model, std::int32_t index)
	{
		mSamplers.mark(index);
	}

	void GarbageCollector::markTexture(const Model &model, std::int32_t index)
	{
		if (mTextures.mark(index))
		{
			const fx::gltf::Texture &texture = model.getDocument()->textures[index];
			this->markImage(model, texture.source);
			this->markSampler(model, texture.sampler);
		}
	}

	void GarbageCollector::markMaterial(const Model &model, std::int32_t index)
	{
		if (mMaterials.mark(index))
		{
			const fx::gltf::Material &material = model.getDocument()->materials[index];
				
			this->markTexture(model, material.emissiveTexture.index);
			this->markTexture(model, material.normalTexture.index);
			this->markTexture(model, material.occlusionTexture.index);
			this->markTexture(model, material.pbrMetallicRoughness.baseColorTexture.index);
			this->markTexture(model, material.pbrMetallicRoughness.metallicRoughnessTexture.index);
		}
	}

	void GarbageCollector::markCamera(const Model &model, std::int32_t index)
	{
		mCameras.mark(index);
	}

	void GarbageCollector::markSkin(const Model &model, std::int32_t index)
	{
		if (mSkins.mark(index))
		{
			const fx::gltf::Skin &skin = model.getDocument()->skins[index];
			this->markAccessor(model, skin.inverseBindMatrices);

			for (std::uint32_t joint : skin.joints)
			{
				this->markNode(model, joint);
			}

			this->markNode(model, skin.skeleton);
		}
	}

	void GarbageCollector::markAnimation(const Model &model, std::int32_t index)
	{
		if (mAnimations.mark(index))
		{
			const fx::gltf::Animation &anim = model.getDocument()->animations[index];
			for (const fx::gltf::Animation::Channel &channel : anim.channels)
			{
				this->markNode(model, channel.target.node);
			}

			for (const fx::gltf::Animation::Sampler &sampler : anim.samplers)
			{
				this->markAccessor(model, sampler.input);
				this->markAccessor(model, sampler.output);
			}
		}
	}

	void GarbageCollector::markAllAnimations(const Model &model)
	{
		std::size_t count = model.getDocument()->animations.size();
		for (std::size_t i = 0; i < count; ++i)
		{
			this->markAnimation(model, static_cast<std::int32_t>(i));
		}
	}

	bool GarbageCollector::sweep()
	{
		std::int32_t removals = 0;

		removals += mBuffers.sweep();
		removals += mBufferViews.sweep();
		removals += mAccessors.sweep();
		removals += mFeatureTables.sweep();
		removals += mImages.sweep();
		removals += mSamplers.sweep();
		removals += mTextures.sweep();
		removals += mMaterials.sweep();
		removals += mMeshes.sweep();
		removals += mCameras.sweep();
		removals += mSkins.sweep();
		removals += mNodes.sweep();
		removals += mScenes.sweep();
		removals += mAnimations.sweep();

		return removals > 0;
	}

	void GarbageCollector::collect(Model &model) const
	{
		this->collectBuffers(model);
		this->collectBufferViews(model);
		this->collectAccessors(model);
		this->collectFeatureTables(model);
		this->collectImages(model);
		this->collectSamplers(model);
		this->collectTextures(model);
		this->collectMaterials(model);
		this->collectMeshes(model);
		this->collectCameras(model);
		this->collectSkins(model);
		this->collectNodes(model);
		this->collectScenes(model);
		this->collectAnimations(model);
	}

	template <typename T>
	void GarbageCollector::applyRemovals(const GarbageMarker &marker, std::vector<T> &items)
	{
		// If all buffers were removed - this should probably not ever happen but for consistency we'll process it here
		if (marker.getOutputCount() == 0)
		{
			items.clear();
		}
		else
		{
			// If there were removals, go ahead and process them here
			if (marker.getRemovalCount() > 0)
			{
				std::vector<T> oldItems(std::move(items));
				items.resize(marker.getOutputCount());

				std::size_t oldIdx = 0;
				std::size_t newIdx = 0;
				for (std::size_t i = 0; i < marker.getRemovalCount(); ++i)
				{
					GarbageMarker::Removal removal = marker.getRemoval(i);
					for (std::size_t toMove = oldIdx; toMove < removal.start; ++toMove)
					{
						items[newIdx++] = std::move(oldItems[oldIdx++]);
					}

					oldIdx = removal.end;
				}
			}
		}
	}

	void GarbageCollector::collectBuffers(Model &model) const
	{
		std::vector<fx::gltf::Buffer> &buffers = model.getDocument()->buffers;
		GarbageCollector::applyRemovals(mBuffers, buffers);
	}

	void GarbageCollector::collectBufferViews(Model &model) const
	{
		std::vector<fx::gltf::BufferView> &bufferViews = model.getDocument()->bufferViews;
		GarbageCollector::applyRemovals(mBufferViews, bufferViews);

		if (mBuffers.needsRemapping())
		{
			for (fx::gltf::BufferView &view : bufferViews)
			{
				view.buffer = mBuffers.remap(view.buffer);
			}
		}
	}

	void GarbageCollector::collectAccessors(Model &model) const
	{
		std::vector<fx::gltf::Accessor> &accessors = model.getDocument()->accessors;
		GarbageCollector::applyRemovals(mAccessors, accessors);

		if (mBufferViews.needsRemapping())
		{
			for (fx::gltf::Accessor &accessor : accessors)
			{
				accessor.bufferView = mBufferViews.remap(accessor.bufferView);
				accessor.sparse.indices.bufferView = mBufferViews.remap(accessor.sparse.indices.bufferView);
				accessor.sparse.values.bufferView = mBufferViews.remap(accessor.sparse.values.bufferView);
			}
		}
	}

	void GarbageCollector::collectFeatureTables(Model &model) const
	{
		fx::gltf::Document *doc = model.getDocument();
		nlohmann::json::iterator metadata;
		nlohmann::json::iterator tables;
		bool haveJson = false;

		auto extlist = doc->extensionsAndExtras.find("extensions");
		if (extlist != doc->extensionsAndExtras.end())
		{
			metadata = extlist->find(model.getMetadataExtensionName());
			if (metadata != extlist->end())
			{
				// get the feature table list
				auto fts = metadata->find("featureTables");
				if (fts != metadata->end())
				{
					tables = fts;
					haveJson = true;
				}
			}
		}

		Collection *collection = model.getEntityCollection();
		if (collection)
		{
			geas::Dictionary<Node> &Nodes = collection->getNodes();

			// This one is trickier because it's not just a flat list
			// First process all removals
			if (mFeatureTables.getOutputCount() == 0)
			{
				// Easier case - all feature tables were removed so purge the collection of Nodes
				Nodes.clear();

				if (haveJson)
				{
					metadata->erase(tables);
				}
			}
			else
			{
				for (std::size_t i = 0; i < mFeatureTables.getRemovalCount(); ++i)
				{
					GarbageMarker::Removal removal = mFeatureTables.getRemoval(i);
					for (std::int32_t j = removal.start; j < removal.end; ++j)
					{
						// Feature tables start with 1 not 0, so we need to add 1 from the array
						auto ii = Nodes.find(j + 1);
						if (ii != Nodes.end())
						{
							if (haveJson)
							{
								tables->erase(ii->getLabel());
							}

							Nodes.erase(ii);
						}
					}
				}

				if (mFeatureTables.needsRemapping())
				{
					for (auto ii = Nodes.begin(); ii != Nodes.end(); ++ii)
					{
						// Feature tables start with 1 not 0, so we need to account for that
						std::int32_t oldIdx = ii->getIndex();
						std::int32_t newIdx = mFeatureTables.remap(oldIdx - 1) + 1;
						if (oldIdx != newIdx)
						{
							ii->setIndex(newIdx);
							Nodes.reindex(ii);
						}
					}
				}
			}

			if (mBufferViews.needsRemapping())
			{
				for (Node &node : model.getEntityCollection()->getNodes())
				{
					Table *values = nullptr;
					std::shared_ptr<Provider> entities = node.getProvider();

					if (haveJson)
					{
						auto ft = tables->find(node.getLabel());
						if (ft != tables->end())
						{
							// get count, we need this in some cases
							std::uint32_t count = 0;
							auto cc =  ft->find("count");
							if (cc != ft->end())
							{
								count = cc->get<std::uint32_t>();
							}

							// Get properties
							auto pp = ft->find("properties");
							if (pp != ft->end())
							{
								for (auto attr : *pp)
								{
									std::uint32_t attrIdx = values->findAttributeByLabel(0, attr.get<std::string>());
									std::shared_ptr<Provider> prov = values->getColumn(attrIdx);
									Column *column = dynamic_cast<Column *>(prov.get());
									Array &value = column->getArray();
									
									std::int32_t bufferIdx = -1;
									std::int32_t stringIdx = -1;
									std::int32_t arrayIdx = -1;

									auto bv = attr.find("bufferView");
									if (bv != attr.end())
									{
										std::int32_t idx = bv->get<std::int32_t>();
										std::int32_t mapped = mBufferViews.remap(idx);
										if (idx != mapped)
										{
											*bv = mapped;
										}
										bufferIdx = mapped;
									}

									auto sov = attr.find("stringOffsetBufferView");
									if (sov != attr.end())
									{
										std::int32_t idx = bv->get<std::int32_t>();
										std::int32_t mapped = mBufferViews.remap(idx);
										if (idx != mapped)
										{
											*sov = mapped;
										}
										stringIdx = mapped;
									}

									auto aov = attr.find("arrayOffsetBufferView");
									if (aov != attr.end())
									{
										std::int32_t idx = bv->get<std::int32_t>();
										std::int32_t mapped = mBufferViews.remap(idx);
										if (idx != mapped)
										{
											*aov = mapped;
										}
										arrayIdx = mapped;
									}

									if (stringIdx != -1)
									{
										if (arrayIdx != -1)
										{
											std::uint32_t stringCount = 0; // TODO how do we calculate this?
											value.setVariableBuffer(model.getBufferView(bufferIdx), model.getBufferView(stringIdx), stringCount, model.getBufferView(arrayIdx), count);
										}
										else
										{
											value.setVariableBuffer(model.getBufferView(bufferIdx), model.getBufferView(stringIdx), count);
										}
									}
									else if (arrayIdx != -1)
									{
										value.setVariableBuffer(model.getBufferView(bufferIdx), model.getBufferView(arrayIdx), count);
									}
									else
									{
										value.setFixedBuffer(model.getBufferView(bufferIdx));
									}
								}
							}
						}
					}
				}
			}
		}
	}

	void GarbageCollector::collectImages(Model &model) const
	{
		std::vector<fx::gltf::Image> &images = model.getDocument()->images;
		GarbageCollector::applyRemovals(mImages, images);

		if (mBufferViews.needsRemapping())
		{
			for (fx::gltf::Image &image : images)
			{
				image.bufferView = mBufferViews.remap(image.bufferView);
			}
		}
	}

	void GarbageCollector::collectSamplers(Model &model) const
	{
		std::vector<fx::gltf::Sampler> &samplers = model.getDocument()->samplers;
		GarbageCollector::applyRemovals(mSamplers, samplers);

		// Samplers don't reference any other data, so no furhter updates are needed
	}

	void GarbageCollector::collectTextures(Model &model) const
	{
		std::vector<fx::gltf::Texture> &textures = model.getDocument()->textures;
		GarbageCollector::applyRemovals(mTextures, textures);

		if (mImages.needsRemapping())
		{
			for (fx::gltf::Texture &texture : textures)
			{
				texture.source = mImages.remap(texture.source);
			}
		}

		if (mSamplers.needsRemapping())
		{
			for (fx::gltf::Texture &texture : textures)
			{
				texture.sampler = mSamplers.remap(texture.sampler);
			}
		}
	}

	void GarbageCollector::collectMaterials(Model &model) const
	{
		std::vector<fx::gltf::Material> &materials = model.getDocument()->materials;
		GarbageCollector::applyRemovals(mMaterials, materials);

		if (mTextures.needsRemapping())
		{
			for (fx::gltf::Material &material : materials)
			{
				material.emissiveTexture.index = mTextures.remap(material.emissiveTexture.index);
				material.normalTexture.index = mTextures.remap(material.normalTexture.index);
				material.occlusionTexture.index = mTextures.remap(material.occlusionTexture.index);
				material.pbrMetallicRoughness.baseColorTexture.index = mTextures.remap(material.pbrMetallicRoughness.baseColorTexture.index);
				material.pbrMetallicRoughness.metallicRoughnessTexture.index = mTextures.remap(material.pbrMetallicRoughness.metallicRoughnessTexture.index);
			}
		}
	}

	void GarbageCollector::collectMeshes(Model &model) const
	{
		std::vector<fx::gltf::Mesh> &meshes = model.getDocument()->meshes;
		GarbageCollector::applyRemovals(mMeshes, meshes);

		if (mMaterials.needsRemapping())
		{
			for (fx::gltf::Mesh &mesh : meshes)
			{
				for (fx::gltf::Primitive &prim : mesh.primitives)
				{
					prim.material = mMaterials.remap(prim.material);
				}
			}
		}

		if (mAccessors.needsRemapping())
		{
			for (fx::gltf::Mesh &mesh : meshes)
			{
				for (fx::gltf::Primitive &prim : mesh.primitives)
				{
					// update vertex attributes
					for (std::pair<const std::string, std::uint32_t> &item : prim.attributes)
					{
						item.second = mAccessors.remap(item.second);
					}
					
					// update index accessor
					prim.indices = mAccessors.remap(prim.indices);

					// remap morph target accessors
					for (fx::gltf::Attributes &target : prim.targets)
					{
						for (std::pair<const std::string, std::uint32_t> &item : target)
						{
							item.second = mAccessors.remap(item.second);
						}
					}
				}
			}
		}

		if (mTextures.needsRemapping())
		{
			for (fx::gltf::Mesh &mesh : meshes)
			{
				for (fx::gltf::Primitive &prim : mesh.primitives)
				{
					// process associated feature tables if present
					auto extlist = prim.extensionsAndExtras.find("extensions");
					if (extlist != prim.extensionsAndExtras.end())
					{
						auto ext = extlist->find(model.getMetadataExtensionName());
						if (ext != extlist->end())
						{
							auto texlist = ext->find("featureIdTextures");
							if (texlist != ext->end())
							{
								for (auto item : *texlist)
								{
									auto fid = item.find("featureIds");
									if (fid != item.end())
									{
										auto tx = fid->find("texture");
										if (tx != fid->end())
										{
											auto idx = tx->find("index");
											if (idx != tx->end())
											{
												std::int32_t value = idx->get<std::int32_t>();
												std::int32_t mapped = mTextures.remap(value);
												if (value != mapped)
												{
													*idx = mapped;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	void GarbageCollector::collectCameras(Model &model) const
	{
		std::vector<fx::gltf::Camera> &cameras = model.getDocument()->cameras;
		GarbageCollector::applyRemovals(mCameras, cameras);
	}

	void GarbageCollector::collectSkins(Model &model) const
	{
		std::vector<fx::gltf::Skin> &skins = model.getDocument()->skins;
		GarbageCollector::applyRemovals(mSkins, skins);

		if (mAccessors.needsRemapping())
		{
			for (fx::gltf::Skin &skin : skins)
			{
				skin.inverseBindMatrices = mAccessors.remap(skin.inverseBindMatrices);
			}
		}

		if (mNodes.needsRemapping())
		{
			for (fx::gltf::Skin &skin : skins)
			{
				skin.skeleton = mNodes.remap(skin.skeleton);
				for (std::uint32_t &joint : skin.joints)
				{
					joint = mNodes.remap(joint);
				}
			}
		}
	}

	void GarbageCollector::collectNodes(Model &model) const
	{
		std::vector<fx::gltf::Node> &nodes = model.getDocument()->nodes;
		GarbageCollector::applyRemovals(mNodes, nodes);

		// Remap child nodes if needed
		if (mNodes.needsRemapping())
		{
			for (fx::gltf::Node &node : nodes)
			{
				for (std::int32_t &child : node.children)
				{
					child = mNodes.remap(child);
				}
			}
		}

		if (mMeshes.needsRemapping())
		{
			for (fx::gltf::Node &node : nodes)
			{
				node.mesh = mMeshes.remap(node.mesh);
			}
		}

		if (mCameras.needsRemapping())
		{
			for (fx::gltf::Node &node : nodes)
			{
				node.camera = mCameras.remap(node.camera);
			}
		}
		
		if (mSkins.needsRemapping())
		{
			for (fx::gltf::Node &node : nodes)
			{
				node.skin = mSkins.remap(node.skin);
			}
		}
	}

	void GarbageCollector::collectScenes(Model &model) const
	{
		fx::gltf::Document *doc = model.getDocument();
		if (doc)
		{
			std::vector<fx::gltf::Scene> &scenes = doc->scenes;
			GarbageCollector::applyRemovals(mScenes, scenes);

			doc->scene = mScenes.remap(doc->scene);

			if (mNodes.needsRemapping())
			{
				for (fx::gltf::Scene &scene : scenes)
				{
					for (std::uint32_t &node : scene.nodes)
					{
						node = mNodes.remap(node);
					}
				}
			}
		}
	}

	void GarbageCollector::collectAnimations(Model &model) const
	{
		fx::gltf::Document *doc = model.getDocument();
		if (doc)
		{
			std::vector<fx::gltf::Animation> &animations = doc->animations;
			GarbageCollector::applyRemovals(mAnimations, animations);
		
			if (mNodes.needsRemapping())
			{
				for (fx::gltf::Animation &anim : animations)
				{
					for (fx::gltf::Animation::Channel &channel : anim.channels)
					{
						channel.target.node = mNodes.remap(channel.target.node);
					}
				}
			}

			if (mAccessors.needsRemapping())
			{
				for (fx::gltf::Animation &anim : animations)
				{
					for (fx::gltf::Animation::Sampler &sampler : anim.samplers)
					{
						sampler.input = mAccessors.remap(sampler.input);
						sampler.output = mAccessors.remap(sampler.output);
					}
				}
			}
		}
	}
}
