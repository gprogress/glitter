/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "Model.h"

#include <fx/gltf.h>

#include "Catalog.h"
#include "Column.h"
#include "ComponentId.h"
#include "EntityMapper.h"
#include "GarbageCollector.h"
#include "MetadataReader.h"
#include "MetadataWriter.h"
#include "Table.h"

#include <cio/Primitive.h>
#include <cio/Text.h>

#include <geas/Attribute.h>

#include <geas/json/OwtSchemaReader.h>
#include <geas/json/OwtSchemaWriter.h>

#include <cio/Status.h>

namespace glitter
{
	const char Model::sDefaultMetadataExtensionName[] = "EXT_feature_metadata";

	const char *Model::getDefaultMetadataExtensionName()
	{
		return sDefaultMetadataExtensionName;
	}

	Model::Model() noexcept :
		mProtocolFactory(nullptr),
		mPreserveCatalog(true),
		mAutoloadCatalog(true),
		mAutoloadCollection(true)
	{
		// nothing to do
	}

	Model::Model(Model &&in) noexcept :
		mDocument(std::move(in.mDocument)),
		mCatalog(std::move(in.mCatalog)),
		mEntityCollection(std::move(in.mEntityCollection)),
		mProtocolFactory(in.mProtocolFactory),
		mParentNode(std::move(in.mParentNode)),
		mPreserveCatalog(in.mPreserveCatalog),
		mAutoloadCatalog(in.mAutoloadCatalog),
		mAutoloadCollection(in.mAutoloadCollection)
	{
		in.mProtocolFactory = nullptr;
	}

	Model &Model::operator=(Model &&in) noexcept
	{
		if (this != &in)
		{
			mDocument = std::move(in.mDocument);
			mCatalog = std::move(in.mCatalog);
			mEntityCollection = std::move(in.mEntityCollection);

			mProtocolFactory = in.mProtocolFactory;
			in.mProtocolFactory = nullptr;

			mParentNode = std::move(in.mParentNode);

			mPreserveCatalog = in.mPreserveCatalog;
			mAutoloadCatalog = in.mAutoloadCatalog;
			mAutoloadCollection = in.mAutoloadCollection;
		}

		return *this;
	}

	Model::~Model() noexcept = default;

	void Model::clear() noexcept
	{
		mEntityCollection.reset();
		mDocument.reset();
		mCatalog.reset();

		mProtocolFactory = nullptr;
		mParentNode.clear();

		mPreserveCatalog = true;
		mAutoloadCatalog = true;
		mAutoloadCollection = true;
	}

	void Model::finalizeEntityCreation(const Entity &in)
	{
		this->ensureMetadata();

		// Get direct access to the value provider so we can inspect the attributes and update the buffer references
		Node *node = in.getNode();
		EntityMapper *mapper = dynamic_cast<EntityMapper *>(node->getProvider().get());
		Table *valueProvider = dynamic_cast<Table *>(mapper->getProvider().get());

		// We will collect all prior attribute data (which might be in different buffers) into a single new buffer
		std::size_t bufferIdx = mDocument->buffers.size();
		mDocument->buffers.emplace_back();
		fx::gltf::Buffer &newBuffer = mDocument->buffers.back();

		// The new entity has already been created, so use the current size to determine the memory use
		std::size_t entityCount = valueProvider->size();
		std::size_t newMemoryUse = valueProvider->estimateNewValueMemoryUse(entityCount);
		newBuffer.byteLength = newMemoryUse;
		newBuffer.data.resize(newMemoryUse);

		// If this is not the first entity created, we need to copy the old attribute data to the new buffer
		// The old attribute data might come from any number of existing buffers and buffer views
		std::size_t attrCount = valueProvider->getAttributeCount(0);
		std::size_t currentOffset = 0;
		for (std::size_t i = 0; i < attrCount; ++i)
		{
			std::shared_ptr<Provider> colprov = valueProvider->getColumn(i);
			Column *column = dynamic_cast<Column *>(colprov.get());
		
			const geas::Attribute &attr = column->getAttribute();
			std::size_t newAttrLength = column->estimateNewValueMemoryUse(entityCount);
			Array &value = column->getArray();

			// Copy old attribute data to current position in new buffer, then update current position to skip the new length
			if (entityCount > 1)
			{
				std::size_t oldAttrLength = column->estimateNewValueMemoryUse(entityCount - 1);
				std::memcpy(&newBuffer.data[currentOffset], value.getBuffer().data(), oldAttrLength);
			}

			// Allocate buffer view pointing to this new slice of the new buffer
			std::size_t bufferViewIdx = mDocument->bufferViews.size();
			mDocument->bufferViews.emplace_back();
			fx::gltf::BufferView &bufferView = mDocument->bufferViews.back();
			bufferView.buffer = bufferIdx;
			bufferView.byteOffset = currentOffset;
			bufferView.byteLength = newAttrLength;

			// TODO also propagate string and array views if present
			value.setFixedBuffer(this->getBufferView(bufferViewIdx));
			currentOffset += newAttrLength;
		}

		// update metadata extension JSON with new feature count
		if (mMetadataExtensionName.empty())
		{
			mMetadataExtensionName = getDefaultMetadataExtensionName();
		}

		MetadataWriter mdWriter;
		nlohmann::json &extlist = mDocument->extensionsAndExtras["extensions"];
		nlohmann::json &mdext = extlist[mMetadataExtensionName];
		nlohmann::json &ft = mdWriter.getOrCreateFeatureTable(mdext, node->getLabel());
		mdWriter.updateFeatureCount(ft, entityCount);
	}

	void Model::ensureMetadata()
	{
		if (!mCatalog)
		{
			mCatalog.reset(new Catalog());
		}

		if (!mEntityCollection)
		{
			mEntityCollection.reset(new Collection());
			mEntityCollection->setCatalog(mCatalog);
		}

		if (!mDocument)
		{
			mDocument.reset(new fx::gltf::Document());
		}

		nlohmann::json &extlist = mDocument->extensionsAndExtras["extensions"];
		if (mMetadataExtensionName.empty())
		{
			mMetadataExtensionName = Model::getDefaultMetadataExtensionName();
		}
		extlist[mMetadataExtensionName];
	}

	void Model::synchronizeCatalogToDocument()
	{
		geas::json::OwtSchemaWriter writer;
		writer.syncApplicationSchema(mDocument->extensionsAndExtras["extensions"][mMetadataExtensionName], *mCatalog->getApplicationSchema());
	}

	Node *Model::getOrCreateFeatureTable(const geas::Label &tableName)
	{
		this->ensureMetadata();

		geas::Dictionary<Node> &nodes = mEntityCollection->getNodes();
		Node *node = nodes.get(tableName);
		if (!node)
		{
			node = nodes.create(tableName);
			
			MetadataWriter writer;
			writer.getOrCreateFeatureTable(mDocument->extensionsAndExtras["extensions"][mMetadataExtensionName], tableName);
		}

		return node;
	}

	geas::Entity *Model::getOrCreateFeatureType(const geas::Label &entityTypeName)
	{
		this->ensureMetadata();

		geas::Dictionary<geas::Entity> &entities = mCatalog->getApplicationSchema()->getEntities();
		geas::Entity *entity = entities.get(entityTypeName);
		if (!entity)
		{
			entity = entities.create(entityTypeName);

			geas::json::OwtSchemaWriter writer;
			nlohmann::json &schema = writer.getOrCreateEmbeddedSchema(mDocument->extensionsAndExtras["extensions"][mMetadataExtensionName]);
			writer.syncEntity(schema, *entity);
		}

		return entity;
	}

	Entity Model::createFeature(const geas::Label &tableName, const geas::Label &entityTypeName)
	{
		this->ensureMetadata();

		Entity entityRef;

		Node *node = this->getOrCreateFeatureTable(tableName);
		geas::Entity* entity = this->getOrCreateFeatureType(entityTypeName);

		MetadataWriter writer;
		geas::json::OwtSchemaWriter gdxWriter;
		nlohmann::json &schema = writer.getOrCreateFeatureTable(mDocument->extensionsAndExtras["extensions"][mMetadataExtensionName], tableName);
		gdxWriter.syncEntity(schema, *entity);

		entityRef =	node->createEntity(entity);
		
		this->finalizeEntityCreation(entityRef);

		return entityRef;
	}

	Entity Model::createFeature(const cio::UniqueId &entityId, const geas::Label &entityTypeName)
	{
		this->ensureMetadata();

		Entity entityRef;

		// gets the information that was in unique id and puts it into unpacked
		ComponentId unpacked = { };
		std::memcpy(&unpacked, &entityId, sizeof(unpacked));

		// gets the node from the uniqueId passed in
		// if it doesn't exist, it can't be created since there's no feature table name associated
		Node *node = mEntityCollection->getNodes().get(geas::Index(unpacked.item));
		if (node)
		{
			// get or create an entity with the name entityTypeName
			geas::Entity *entity = this->getOrCreateFeatureType(entityTypeName);

			MetadataWriter writer;
			geas::json::OwtSchemaWriter gdxWriter;
			nlohmann::json &schema = writer.getOrCreateFeatureTable(mDocument->extensionsAndExtras["extensions"][mMetadataExtensionName], node->getLabel());
			gdxWriter.syncEntity(schema, *entity);

			// set feature type of entity
			entityRef = node->createEntity(entity);

			this->finalizeEntityCreation(entityRef);
		}
		
		return entityRef;
	}

	void Model::loadFromFile(const std::string &path)
	{
		this->loadFromPath(cio::Path::fromNativeFile(path));
	}

	void Model::loadFromPath(const cio::Path &path)
	{
		// check path extension for .gltf vs. .glb
		bool isBinary = path.matchesExtension("glb");
		bool isFile = path.isFileProtocol();
		if (!isFile)
		{
			throw std::runtime_error("Support for non-file inputs will be implemented soon");
		}

		mDocument.reset(new fx::gltf::Document());

		std::string filePath = path.toNativeFile();
		if (isBinary)
		{
			*mDocument = fx::gltf::LoadFromBinary(filePath);
		}
		else
		{
			*mDocument = fx::gltf::LoadFromText(filePath);
		}

		// Save these for later so we can implement external reference read
		mMainFile = path;
		mParentNode = path.getParent();

		this->loadFeatureMetadata();
	}

	void Model::storeToFile(const std::string &path)
	{
		this->storeToPath(cio::Path::fromUnixFile(path));
	}

	void Model::storeToPath(const cio::Path &path)
	{
		// check path extension for .gltf vs. .glb
		bool isBinary = path.matchesExtension("glb");

		bool isFile = path.isFileProtocol();
		if (!isFile)
		{
			throw std::runtime_error("Support for non-file inputs will be implemented soon");
		}

		// Create an empty document as a starting point
		if (!mDocument)
		{
			mDocument.reset(new fx::gltf::Document());
		}

		// TODO reprocess all Geodex data into extension data that fx-gltf understands

		std::string filePath = path.toNativeFile();
		fx::gltf::Save(*mDocument, filePath, isBinary);
	}

	bool Model::removeUnusedData()
	{
		GarbageCollector collector;
		collector.markAllAnimations(*this);
		collector.markScenes(*this);
		bool removals = collector.sweep();
		if (removals)
		{
			collector.collect(*this);
		}
		return removals;
	}

	void Model::save()
	{
		if (mMainFile.empty())
		{
			throw std::runtime_error("No main file to save Model to");
		}

		this->storeToPath(mMainFile);
		mParentNode = mMainFile.getParent();
	}

	const cio::Path &Model::getMainFile() const
	{
		return mMainFile;
	}

	void Model::setMainFile(cio::Path path)
	{
		mMainFile = std::move(path);
	}

	const cio::Path &Model::getParentNode() const
	{
		return mParentNode;
	}

	void Model::setParentNode(cio::Path path)
	{
		mParentNode = std::move(path);
	}

	const cio::ProtocolFactory *Model::getProtocolFactory() const
	{
		return mProtocolFactory;
	}

	void Model::setProtocolFactory(const cio::ProtocolFactory *protocols)
	{
		mProtocolFactory = protocols;
	}

	fx::gltf::Document *Model::getDocument()
	{
		return mDocument.get();
	}

	const fx::gltf::Document *Model::getDocument() const
	{
		return mDocument.get();
	}

	fx::gltf::Document *Model::getOrCreateDocument()
	{
		if (!mDocument)
		{
			mDocument.reset(new fx::gltf::Document());
		}
		return mDocument.get();
	}

	void Model::setDocument(std::unique_ptr<fx::gltf::Document> document)
	{
		mDocument = std::move(document);
	}

	std::unique_ptr<fx::gltf::Document> Model::takeDocument()
	{
		return std::move(mDocument);
	}

	std::shared_ptr<Catalog> Model::getCatalog()
	{
		return mCatalog;
	}

	std::shared_ptr<const Catalog> Model::getCatalog() const
	{
		return mCatalog;
	}

	std::shared_ptr<Catalog> Model::getOrCreateCatalog()
	{
		if (!mCatalog)
		{
			mCatalog.reset(new Catalog());
		}
		return mCatalog;
	}

	void Model::setCatalog(std::shared_ptr<Catalog> catalog)
	{
		mCatalog = std::move(catalog);
	}

	std::shared_ptr<Catalog> Model::takeCatalog()
	{
		return std::move(mCatalog);
	}

	void Model::setEntityCollection(std::unique_ptr<Collection> collection)
	{
		mEntityCollection = std::move(collection);
	}

	Collection *Model::getEntityCollection()
	{
		return mEntityCollection.get();
	}

	const Collection *Model::getEntityCollection() const
	{
		return mEntityCollection.get();
	}

	Collection *Model::getOrCreateEntityCollection()
	{
		if (!mEntityCollection)
		{
			mEntityCollection.reset(new Collection());
		}
		return mEntityCollection.get();
	}

	std::unique_ptr<Collection> Model::takeEntityCollection()
	{
		return std::move(mEntityCollection);
	}

	Model::operator bool() const
	{
		return mDocument != nullptr;
	}

	FeatureIdMapper Model::makeFeatureIdMapper(std::int32_t meshIdx, std::int32_t primIdx, std::uint32_t layer) const
	{
		const fx::gltf::Mesh &mesh = mDocument->meshes.at(meshIdx);
		const fx::gltf::Primitive &prim = mesh.primitives.at(primIdx);
		return makeFeatureIdMapper(prim, layer);
	}

	FeatureIdMapper Model::makeFeatureIdMapper(const fx::gltf::Primitive &prim, std::uint32_t layer) const
	{
		FeatureIdMapper mapper;

		mapper.bindSchema(mCatalog->getApplicationSchema());

		if (prim.indices != -1)
		{
			Array values = this->getDataView(prim.indices);
			mapper.bindVertexIndices(values.makeBinaryScalarAccessor<std::uint32_t>());
		}

		auto ext = prim.extensionsAndExtras.find("extensions");
		if (ext != prim.extensionsAndExtras.end())
		{
			auto ii = ext->find(mMetadataExtensionName);
			if (ii != ext->end())
			{
				MetadataReader reader;
				FeatureIdMap content = reader.loadSelectedFeatureIdMap(*mEntityCollection, prim, layer, *ii);
				mapper.setMap(content);

				if (content.accessor != -1)
				{
					Array values = this->getDataView(content.accessor);
					mapper.bindFeatureIds(values.makeBinaryScalarAccessor<std::uint32_t>());
				}

				if (content.table != -1)
				{
					Node *node = mEntityCollection->getNodes().get(geas::Index(content.table));
					mapper.bindNode(node);
				}
			}
		}

		return mapper;
	}

	cio::UniqueId Model::getVertexEntityId(std::int32_t meshIdx, std::int32_t primIdx, std::uint32_t vertexIdx, unsigned layer) const
	{
		cio::UniqueId uid;
		if (mDocument && meshIdx < mDocument->meshes.size())
		{
			const fx::gltf::Mesh &mesh = mDocument->meshes[meshIdx];
			if (primIdx < mesh.primitives.size())
			{
				const fx::gltf::Primitive &prim = mesh.primitives[primIdx];
				FeatureIdMapper mapper = this->makeFeatureIdMapper(prim, layer);
				if (mapper)
				{
					if (prim.indices == -1)
					{
						// no indices, so vertices and indices have the same definition
						std::uint32_t fid = mapper.getVertexFeatureId(vertexIdx);
					}
					else if (mapper.getMap().accessor != -1 && vertexIdx < mapper.getFeatureIds().size())
					{
						// Otherwise, vertex information only exists if there is a feature ID attribute
						// (procedural feature IDs are bound to index data, not vertex data) 
						std::uint32_t fid = mapper.getFeatureIds().get(vertexIdx);
						uid = this->makeEntityId(mapper.getMap().table, fid);
					}
				}
			}
		}

		return uid;
	}

	cio::UniqueId Model::getIndexEntityId(std::int32_t meshIdx, std::int32_t primIdx, std::uint32_t indexIdx, unsigned layer) const
	{
		cio::UniqueId uid;
		if (mDocument && meshIdx < mDocument->meshes.size())
		{
			const fx::gltf::Mesh &mesh = mDocument->meshes[meshIdx];
			if (primIdx < mesh.primitives.size())
			{
				const fx::gltf::Primitive &prim = mesh.primitives[primIdx];
				FeatureIdMapper mapper = this->makeFeatureIdMapper(prim, layer);
				std::uint32_t fid = mapper.getVertexFeatureId(indexIdx);
				uid = this->makeEntityId(mapper.getMap().table, fid);
			}
		}

		return uid;
	}

	FeatureIdWeight Model::getBestTriangleEntityId(std::int32_t meshIdx, std::int32_t primIdx, std::uint32_t triangleIdx, unsigned layer) const
	{
		std::int32_t dsid = -1;
		std::uint32_t fid = UINT32_MAX;
		std::size_t count = 0;

		if (mDocument && meshIdx < mDocument->meshes.size())
		{
			const fx::gltf::Mesh &mesh = mDocument->meshes[meshIdx];
			if (primIdx < mesh.primitives.size())
			{
				const fx::gltf::Primitive &prim = mesh.primitives[primIdx];
				FeatureIdMapper mapper = this->makeFeatureIdMapper(prim, layer);
				if (mapper)
				{
					dsid = mapper.getMap().table;
					std::array<std::uint32_t, 3> ids = mapper.getTriangleFeatureIds(triangleIdx);
					if (ids[0] != UINT32_MAX)
					{
						if (ids[0] == ids[1])
						{
							fid = ids[0];
							count = 2 + static_cast<std::size_t>(ids[0] == ids[2]);
						}
						else if (ids[0] == ids[2])
						{
							fid = ids[0];
							count = 2;
						}
						else if (ids[1] != UINT32_MAX && ids[1] == ids[2])
						{
							fid = ids[1];
							count = 2;
						}
						else
						{
							fid = std::min(std::min(ids[0], ids[1]), ids[2]);
							count = 1;
						}
					}
					else if (ids[1] != UINT32_MAX)
					{
						if (ids[1] == ids[2])
						{
							fid = ids[1];
							count = 1;
						}
						else
						{
							fid = std::min(ids[1], ids[2]);
							count = 1;
						}
					}
					else if (ids[2] != UINT32_MAX)
					{
						fid = ids[2];
						count = 1;
					}
				}
			}
		}

		FeatureIdWeight uid;
		if (fid != UINT32_MAX && dsid > 0)
		{
			uid.value = this->makeEntityId(dsid, fid);
		}

		uid.count = count;
		uid.total = 3;
		return uid;
	}

	FeatureIdWeight Model::getBestPrimitiveSetEntityId(std::int32_t meshIdx, std::int32_t primIdx, unsigned layer) const
	{
		std::int32_t dsid = -1;
		std::uint32_t fid = UINT32_MAX;
		std::size_t count = 0;
		std::size_t total = 0;

		if (mDocument && meshIdx < mDocument->meshes.size())
		{
			const fx::gltf::Mesh &mesh = mDocument->meshes[meshIdx];
			if (primIdx < mesh.primitives.size())
			{
				const fx::gltf::Primitive &prim = mesh.primitives[primIdx];
				FeatureIdMapper mapper = this->makeFeatureIdMapper(prim, layer);
				if (mapper)
				{
					total = mapper.size();
					dsid = mapper.getMap().table;
					if (mapper.getMap().isUniform())
					{
						fid = mapper.getMap().offset;
						count = mapper.size();
					}
					else if (mapper.getMap().isProcedural())
					{
						// In the procedural case, every vertex might a different feature ID
						// We'll just pick the lowest one
						fid = mapper.getMap().offset;
						count = 1;
					}
					else
					{
						std::map<std::uint32_t, std::uint32_t> counts;
						for (std::uint32_t v = 0; v < mapper.size(); ++v)
						{
							std::uint32_t vfid = mapper.getVertexFeatureId(v);
							if (vfid != UINT32_MAX)
							{
								++counts[vfid];
							}
						}

						for (const std::pair<std::uint32_t, std::uint32_t> &item : counts)
						{
							if (item.second > count)
							{
								fid = item.first;
								count = item.second;
							}
						}
					}
				}
			}
		}

		FeatureIdWeight uid;
		if (fid != UINT32_MAX && dsid > 0)
		{
			uid.value = this->makeEntityId(dsid, fid);
		}

		uid.count = count;
		uid.total = total;
		return uid;
	}

	FeatureIdWeight Model::getBestMeshEntityId(std::int32_t meshIdx, unsigned layer) const
	{
		FeatureIdWeight result = { cio::UniqueId(), 0, 0 };

		if (mDocument && meshIdx >= 0 && meshIdx < mDocument->meshes.size())
		{
			std::map<cio::UniqueId, Weight<void>> weights;
			this->updateFeatureWeightMap(mDocument->meshes[meshIdx], weights, layer);
			result = this->extractBestFeature(weights);
		}

		return result;
	}

	FeatureIdWeight Model::getBestNodeEntityId(std::int32_t nodeIdx, unsigned layer) const
	{
		FeatureIdWeight result = { cio::UniqueId(), 0, 0 };
		if (mDocument && nodeIdx >= 0 && nodeIdx < mDocument->nodes.size())
		{
			std::map<cio::UniqueId, Weight<void>> weights;
			this->updateFeatureWeightMap(mDocument->nodes[nodeIdx], weights, layer);
			result = this->extractBestFeature(weights);
		}
	
		return result;
	}

	FeatureIdWeight Model::getBestModelEntityId(std::uint32_t layer) const
	{
		std::map<cio::UniqueId, Weight<void>> weights;
		this->updateFeatureWeightMap(weights, layer);
		return this->extractBestFeature(weights);
	}

	void Model::updateFeatureWeightMap(std::map<cio::UniqueId, Weight<void>> &weights, unsigned layer) const
	{
		if (mDocument)
		{
			for (const fx::gltf::Scene &scene : mDocument->scenes)
			{
				this->updateFeatureWeightMap(scene, weights, layer);
			}
		}
	}

	void Model::updateFeatureWeightMap(const fx::gltf::Scene &scene, std::map<cio::UniqueId, Weight<void>> &weights, unsigned layer) const
	{
		if (mDocument)
		{
			for (std::uint32_t nodeIdx : scene.nodes)
			{
				if (nodeIdx < mDocument->nodes.size())
				{
					const fx::gltf::Node &node = mDocument->nodes[nodeIdx];
					this->updateFeatureWeightMap(node, weights, layer);
				}
			}
		}
	}

	void Model::updateFeatureWeightMap(const fx::gltf::Node &node, std::map<cio::UniqueId, Weight<void>> &weights, unsigned layer) const
	{
		if (mDocument)
		{
			std::vector<std::uint32_t> toProcess;

			if (node.mesh >= 0 && node.mesh < mDocument->meshes.size())
			{
				const fx::gltf::Mesh &mesh = mDocument->meshes[node.mesh];
				this->updateFeatureWeightMap(mesh, weights, layer);
			}

			for (std::uint32_t i : node.children)
			{
				if (i < mDocument->nodes.size())
				{
					toProcess.emplace_back(i);
				}
			}

			while (!toProcess.empty())
			{
				std::uint32_t nextIdx = toProcess.back();
				toProcess.pop_back();

				const fx::gltf::Node &nextNode = mDocument->nodes[nextIdx];

				if (nextNode.mesh >= 0 && nextNode.mesh < mDocument->meshes.size())
				{
					const fx::gltf::Mesh &mesh = mDocument->meshes[nextNode.mesh];
					this->updateFeatureWeightMap(mesh, weights, layer);
				}

				for (std::uint32_t i : nextNode.children)
				{
					if (i < mDocument->nodes.size())
					{
						toProcess.emplace_back(i);
					}
				}
			}
		}
	}

	void Model::updateFeatureWeightMap(const fx::gltf::Mesh &mesh, std::map<cio::UniqueId, Weight<void>> &weights, unsigned layer) const
	{
		for (const fx::gltf::Primitive &prim : mesh.primitives)
		{
			this->updateFeatureWeightMap(prim, weights, layer);
		}
	}

	void Model::updateFeatureWeightMap(const fx::gltf::Primitive &prim, std::map<cio::UniqueId, Weight<void>> &weights, unsigned layer) const
	{
		FeatureIdMapper mapper = this->makeFeatureIdMapper(prim, layer);
		if (mapper)
		{
			if (mapper.getMap().isUniform())
			{
				std::uint32_t fid = mapper.getMap().offset;
				Weight<void> weight = { 0, mapper.size() };
				cio::UniqueId uid;
				if (fid != UINT32_MAX)
				{
					weight.count = mapper.size();
					uid = this->makeEntityId(mapper.getMap().table, fid);
				}

				weights[uid] |= (weight);
			}
			else
			{
				const Weight<void> increment = { 1, 1 };
				for (std::size_t v = 0; v < mapper.size(); ++v)
				{
					cio::UniqueId uid;
					std::uint32_t fid = mapper.getVertexFeatureId(v);
					if (fid != UINT32_MAX)
					{
						uid = this->makeEntityId(mapper.getMap().table, fid);
					}
					weights[uid] |= increment;
				}
			}
		}
	}

	FeatureIdWeight Model::extractBestFeature(const std::map<cio::UniqueId, Weight<void>> &weights) const
	{
		FeatureIdWeight result = { cio::UniqueId(), 0, 0 };
		for (const std::pair<cio::UniqueId, Weight<void>> &item : weights)
		{
			if (item.second.count > result.count)
			{
				result.value = item.first;
				result.count = item.second.count;
			}
			result.total += item.second.count;
		}
		return result;
	}

	FeatureIdMapper Model::makeFeatureIdMapper(const cio::UniqueId &id, std::uint32_t layer) const
	{
		FeatureIdMapper mapper;
		ComponentId cid;
		std::memcpy(&cid, &id, sizeof(cid));
		if (cid.base == ComponentType::Mesh && cid.type == ComponentType::Vertex)
		{
			mapper = this->makeFeatureIdMapper(cid.item, cid.primitiveset, layer);
		}

		return mapper;
	}

	cio::State Model::addAttribute(Node *node, const geas::Attribute *type)
	{
		this->ensureMetadata();

		std::shared_ptr<Provider> valueProvider = node->getProvider();

		if (valueProvider->findAttributeByLabel(0, type->getLabel()) != UINT32_MAX)
		{
			return cio::succeed(cio::Action::Create, cio::Reason::Exists);
		}
		else
		{
			MetadataWriter writer;
			geas::json::OwtSchemaWriter gdxWriter;

			nlohmann::json &mdext = mDocument->extensionsAndExtras["extensions"][mMetadataExtensionName];

			// Look up schema definition, gltf requires us to use the schema information rather than the node information
			// TODO need to validate input attribute to set missing parameters and determine if this is a data type gltf can support
			geas::Dictionary<geas::Attribute> &schemaAttrs = mCatalog->getApplicationSchema()->getAttributes();
			const geas::Attribute *schemaAttr = nullptr;
			auto sa = schemaAttrs.find(type);
			if (sa == schemaAttrs.end())
			{
				schemaAttr = schemaAttrs.create(*type);				 
			}
			else
			{
				schemaAttr = &(*sa);
			}

			// Bind attribute to node's default feature type
			// TODO otherwise we'll need to create a placeholder feature type for the node
			const geas::Entity *entityType = node->getProvider()->getEntityType(0);
			if (entityType)
			{
				geas::Dictionary<geas::Entity> &schemaEntities = mCatalog->getApplicationSchema()->getEntities();
				auto et = schemaEntities.find(entityType);
				if (et != schemaEntities.end())
				{
					std::uint32_t attrCount = et->getAttributes().size();
					et->addAttribute(schemaAttr);
					if (et->getAttributes().size() > attrCount)
					{
						nlohmann::json &schemaRoot = gdxWriter.getOrCreateEmbeddedSchema(mDocument->extensionsAndExtras["extensions"][mMetadataExtensionName]);
						gdxWriter.syncEntityAttribute(schemaRoot, *schemaAttr);
					}
				}
			}

			std::uint32_t attrIdx = valueProvider->addAttribute(0, schemaAttr);
						
			EntityMapper *mapper = dynamic_cast<EntityMapper *>(valueProvider.get());
			
			Table *table = dynamic_cast<Table *>(mapper->getProvider().get());

			// bind new attribute value to new buffer
			Column *column = dynamic_cast<Column *>(table->getColumn(attrIdx).get());
			
			// Calculate how many bytes needed for the new attribute array based on the entity count multiplied by the size of each attribute value
			std::size_t newSize = column->estimateNewValueMemoryUse(node->getProvider()->size());

			// Add a new buffer of the new size
			std::uint32_t bufferIdx = mDocument->buffers.size() - 1;
			mDocument->buffers.emplace_back();
			fx::gltf::Buffer &newBuffer = mDocument->buffers.back();
			newBuffer.byteLength = newSize;
			newBuffer.data.resize(newSize);

			// Link a new buffer view to the new buffer
			std::uint32_t bufferViewIdx = mDocument->bufferViews.size();
			fx::gltf::BufferView bufferView;
			bufferView.buffer = bufferIdx;
			bufferView.byteOffset = 0;
			bufferView.byteLength = newSize;
			mDocument->bufferViews.emplace_back(bufferView);

			Array &value = column->getArray();
			value.setFixedBuffer(this->getBufferView(bufferViewIdx));

			nlohmann::json &ft = writer.getOrCreateFeatureTable(mdext, node->getLabel());
			writer.updateAttribute(ft, *table->getAttribute(0, attrIdx), bufferViewIdx);
		}

		return cio::succeed(cio::Action::Create);
	}
	
	cio::State Model::addAttribute(Node *node, const geas::Label &attributeName)
	{
		Provider* valueProvider = node->getProvider().get();
		geas::Attribute *newAttr = mCatalog->getApplicationSchema()->getAttributes().get(attributeName);
		cio::State result;
	
		if (newAttr)
		{
			result = this->addAttribute(node, newAttr);
		}
		else
		{
			geas::Attribute tmp;
			tmp.setLabel(attributeName);
			result = this->addAttribute(node, &tmp);
		}

		return result;
	}

	cio::State Model::addAttribute(const geas::Label &nodeName, const geas::Label &attributeName)
	{
		Node *test = mEntityCollection->getNodes().get(nodeName);
		
		return this->addAttribute(test, attributeName);
	}

	cio::State Model::addAttribute(const geas::Label &tableName, geas::Attribute *type)
	{
		Node *test = mEntityCollection->getNodes().get(tableName);
		
		return this->addAttribute(test, type);
	}

	cio::State Model::addAttribute(const cio::UniqueId& nodeId, geas::Attribute* type)
	{
		ComponentId unpacked = { };
		std::memcpy(&unpacked, &nodeId, sizeof(unpacked));
		Node* test = mEntityCollection->getNodes().get(geas::Index(unpacked.item));
		
		return this->addAttribute(test, type);
	}

	cio::State Model::addAttribute(const cio::UniqueId &nodeId, const geas::Label &attributeName)
	{
		ComponentId unpacked = { };
		std::memcpy(&unpacked, &nodeId, sizeof(unpacked));
		Node *test = mEntityCollection->getNodes().get(geas::Index(unpacked.item));
		
		return this->addAttribute(test, attributeName);
	}

	void Model::setEntity(const cio::UniqueId& objectId, const cio::UniqueId& entityId, std::uint32_t layerIndex /* = 0 */)
	{
		std::shared_ptr<geas::Application> schema = mEntityCollection->getCatalog()->getApplicationSchema();
		// gets the information that was in entityId and puts it into unpacked
		ComponentId unpackedEntity = { };
		std::memcpy(&unpackedEntity, &entityId, sizeof(unpackedEntity));

		std::uint32_t fid = 0;
		Node *featureTable = nullptr;

		// The method should first verify the given entityId references a valid entity,
		if (unpackedEntity.base == ComponentType::Node)
		{
			// if not entity or missing but is a potentially valid entity ID, create an empty node and create the entity
			if (unpackedEntity.type == ComponentType::Entity)
			{
				Node* node = mEntityCollection->getNodes().get(geas::Index(unpackedEntity.base));
				if (node)
				{
					featureTable = node;
					fid = unpackedEntity.item;
				}
			}
		}

		if (featureTable)
		{
			Entity entityRef(featureTable, fid);

			// Then it should examine the objectId to determine what kind of object it is and act accordingly.
			ComponentId unpackedObject = { };
			std::memcpy(&unpackedObject, &objectId, sizeof(unpackedObject));

			switch (unpackedObject.type)
			{
				case ComponentType::Vertex:
				{
					// then you will need to pull up the primitive set with that vertex
					fx::gltf::Primitive &primitiveSet = mDocument->meshes[geas::Index(unpackedObject.item)].primitives[unpackedObject.primitiveset];
					this->setEntityForVertex(primitiveSet, unpackedObject.element, entityRef, layerIndex);
				}
				break;

				case ComponentType::PrimitiveSet:
				{
					fx::gltf::Primitive &primitiveSet = mDocument->meshes[geas::Index(unpackedObject.item)].primitives[unpackedObject.primitiveset];
					this->setEntityForPrimitiveSet(primitiveSet, entityRef, layerIndex);
				}
				break;

				case ComponentType::Mesh:
				{
					fx::gltf::Mesh &mesh = mDocument->meshes[unpackedObject.item];
					this->setEntityForMesh(mesh, entityRef, layerIndex);
				}
				break;

				case ComponentType::Node:
				{
					fx::gltf::Node &node = mDocument->nodes[unpackedObject.item];
					this->setEntityForNode(node, entityRef, layerIndex);
				}
				break;

				default:
					// currently unhandled
					break;
			}
		}
	}

	void Model::setEntityForNode(fx::gltf::Node &node, const Entity &entity, std::uint32_t layerIndex /* = 0*/)
	{
		this->ensureMetadata();

		if (node.mesh != -1)
		{
			this->setEntityForMesh(mDocument->meshes[node.mesh], entity, layerIndex);
		}

		for (std::uint32_t child : node.children)
		{
			this->setEntityForNode(mDocument->nodes[child], entity, layerIndex);
		}
	}

	void Model::setEntityForMesh(fx::gltf::Mesh &mesh, const Entity &entity, std::uint32_t layerIndex /* = 0*/)
	{
		this->ensureMetadata();

		for (fx::gltf::Primitive &prim : mesh.primitives)
		{
			this->setEntityForPrimitiveSet(prim, entity, layerIndex);
		}
	}

	void Model::setEntityForPrimitiveSet(fx::gltf::Primitive &primitive, const Entity &entity, std::uint32_t layerIndex /*= 0*/)
	{
		this->ensureMetadata();

		FeatureIdMap map;
		map.accessor = -1;
		map.divisor = 0;
		map.offset = entity.getId();
		map.table = entity.getNode()->getIndex();

		MetadataWriter writer;
		nlohmann::json &extensionRoot = primitive.extensionsAndExtras["extensions"][mMetadataExtensionName];
		writer.updateFeatureMapper(extensionRoot, primitive, *mEntityCollection, map);
	}

	void Model::setEntityForVertex(fx::gltf::Primitive &primitive, std::uint32_t vertexIdx, const Entity &entity, std::uint32_t layerIndex /*= 0*/)
	{
		this->ensureMetadata();

		std::uint32_t fid = entity.getId();
		std::uint32_t tid = entity.getNode()->getIndex();

		FeatureIdMap current;

		// Get existing data without modifying anything
		// The data might not exist, in which case the default FeatureIdMap (offset = 0, divisor = 0, no feature able or 
		{
			auto extlist = primitive.extensionsAndExtras.find("extensions");
			if (extlist != primitive.extensionsAndExtras.end())
			{
				auto md = extlist->find(mMetadataExtensionName);
				if (md != extlist->end())
				{
					nlohmann::json &extensionRoot = *md;
					MetadataReader reader;
					current = reader.loadSelectedFeatureIdMap(*mEntityCollection, primitive, layerIndex, extensionRoot);
				}
			}
		}

		bool changes = false;

		// We're setting a feature table for the first time, which means the mapping probably didn't already exist
		if (current.table == -1)
		{
			current.table = tid;
			changes = true;
		}
		// If the current mapping references a table already, but the new feature is from a differen table,
		// then we have to changing which feature table is referenced
		// You can't mix tables in the same layer, so in this case we'll clear out the map first to start over
		else if (current.table != tid)
		{
			current.accessor = -1;
			current.divisor = 0;
			current.offset = 0;
			current.table = tid;
			changes = true;
		}

		// If no feature mapping exists currently, it shows up as a procedural mapping with offset = 0 and divisor = 0
		// Also, if the mapping exists and explicitly is a procedural mapping that will hit this case
		if (current.isProcedural())
		{
			// Check to see if the new FID is not the one calculated by the procedural algorithm
			// If it is, we don't need to do anything
			std::uint32_t oldFid = current.procedural(vertexIdx);
			if (oldFid != fid)
			{
				// We need to allocate a buffer, buffer view, and accessor to receive the explicit feature IDs				
				std::uint32_t vertexCount = this->getVertexCount(primitive);
				if (vertexCount > 0)
				{
					std::uint32_t bufferIdx = mDocument->buffers.size();
					mDocument->buffers.emplace_back();
					fx::gltf::Buffer &buffer = mDocument->buffers.back();

					buffer.byteLength = vertexCount * 4;
					buffer.data.resize(buffer.byteLength);

					std::uint32_t viewIdx = mDocument->bufferViews.size();
					mDocument->bufferViews.emplace_back();
					fx::gltf::BufferView &view = mDocument->bufferViews.back();
					view.buffer = bufferIdx;
					view.byteLength = buffer.byteLength;
					view.byteOffset = 0;
					view.byteStride = 0;

					std::uint32_t accIdx = mDocument->accessors.size();
					mDocument->accessors.emplace_back();
					fx::gltf::Accessor &accessor = mDocument->accessors.back();
					accessor.bufferView = viewIdx;
					accessor.byteOffset = 0;
					accessor.componentType = fx::gltf::Accessor::ComponentType::UnsignedInt;
					accessor.count = vertexCount;
					accessor.type = fx::gltf::Accessor::Type::Scalar;

					char attrName[64];
					std::snprintf(attrName, 64, "_FEATURE_ID_%u", layerIndex);

					primitive.attributes[attrName] = accIdx;

					Array fids = this->getDataView(accIdx);

					current.accessor = accIdx;
					current.divisor = 0;
					current.offset = 0;

					for (std::size_t i = 0; i < vertexCount; ++i)
					{
						fids.setBinary(current.procedural(i), i);
					}

					fids.setBinary(fid, vertexIdx);
				}
				// No vertices, we can just change the procedural offset
				else
				{
					current.offset = fid;
					current.divisor = 0;
				}

				changes = true;
			}
		}
		else
		{
			Array fids = this->getDataView(current.accessor);
			fids.setBinary(fid, vertexIdx);

			// This case modifies the underlying data buffer, but not the feature ID mapping itself
		}

		if (changes)
		{
			MetadataWriter writer;
			nlohmann::json &extensionRoot = primitive.extensionsAndExtras["extensions"][mMetadataExtensionName];
			writer.updateFeatureMapper(extensionRoot, primitive, *mEntityCollection, current);
		}
	}

	std::uint32_t Model::getVertexCount(const fx::gltf::Primitive &primitive) const
	{
		// Check for the accessor with the *lowest* count, since that's the highest index value that's usable from an index array
		std::uint32_t count = UINT32_MAX;
		for (const std::pair<std::string, std::uint32_t> &item : primitive.attributes)
		{
			fx::gltf::Accessor &accessor = mDocument->accessors[item.second];
			if (accessor.count < count)
			{
				count = accessor.count;
			}
		}

		// If no accessors are yet defined, but we have an index array, then check for the maximum index value
		if (count == UINT32_MAX)
		{
			count = 0;

			if (primitive.indices != -1)
			{
				Array indices = this->getDataView(primitive.indices);
				for (std::size_t i = 0; i < indices.size(); ++i)
				{
					std::uint32_t idx = indices.getBinary<std::uint32_t>(i);
					if (idx >= count)
					{
						count = idx + 1;
					}
				}
			}
		}

		return count;
	}

	FeatureIdWeight Model::getEntityId(const cio::UniqueId &id, unsigned layer) const
	{
		FeatureIdWeight entityId = { cio::UniqueId(), 0, 0 };

		ComponentId unpacked = { };
		std::memcpy(&unpacked, &id, sizeof(unpacked));

		if (unpacked.base == ComponentType::Node)
		{
			// By definition nodes and entities only have one layer
			if (layer == 0)
			{
				if (unpacked.type == ComponentType::Entity)
				{
					if (mEntityCollection)
					{
						const Node *ds = mEntityCollection->getNodes().get(geas::Index(unpacked.item));
						if (ds)
						{
							if (unpacked.primitiveset < ds->getProvider()->size())
							{
								entityId.value = id;
								entityId.count = 1;
								entityId.total = 1;
							}
						}
					}
				}
				else if (unpacked.type == ComponentType::Node)
				{
					// per the rules for this method, we pick the entity ID that has the highest frequency, breaking ties by preferring the lowest feature ID
					// Since each entity in a node by definition has a different ID, we just return the lowest one
					if (mEntityCollection)
					{
						const Node *ds = mEntityCollection->getNodes().get(geas::Index(unpacked.item));
						if (ds)
						{
							entityId.value = this->makeEntityId(unpacked.item, 0);
							entityId.count = 1;
							entityId.total = mEntityCollection->getNodes().get(unpacked.item)->getProvider()->size();
						}
					}
				}
			}
		}
		//  Mesh item, figure out the specifics
		else if (unpacked.base == ComponentType::Mesh)
		{
			if (unpacked.type == ComponentType::Index)
			{
				entityId.value = this->getEntityIdForPrimitiveIndex(unpacked.item, unpacked.primitiveset, unpacked.element, layer);
				if (entityId.value)
				{
					entityId.count = 1;
					entityId.total = 1;
				}
			}
			else if (unpacked.type == ComponentType::Vertex)
			{
				entityId.value = this->getVertexEntityId(unpacked.item, unpacked.primitiveset, unpacked.element, layer);
				if (entityId.value)
				{
					entityId.count = 1;
					entityId.total = 1;
				}
			}
			else if (unpacked.type == ComponentType::Primitive)
			{
				entityId = this->getBestTriangleEntityId(unpacked.item, unpacked.primitiveset, unpacked.element, layer);
			}
			else if (unpacked.type == ComponentType::PrimitiveSet)
			{
				entityId = this->getBestPrimitiveSetEntityId(unpacked.item, unpacked.element, layer);
			}
			else if (unpacked.type == ComponentType::Mesh)
			{
				entityId = this->getBestMeshEntityId(unpacked.item, layer);
			}
		}
		else if (unpacked.base == ComponentType::Node && unpacked.type == ComponentType::Node)
		{
			entityId = this->getBestNodeEntityId(unpacked.item, layer);
		}
		else if (unpacked.base == ComponentType::Document && unpacked.type == ComponentType::Document)
		{
			entityId = this->getBestModelEntityId(layer);
		}

		return entityId;
	}
	
	cio::UniqueId Model::getEntityIdForPrimitiveIndex(std::int32_t meshIdx, std::int32_t primitiveSetIdx, std::uint32_t idx, unsigned layer) const
	{
		cio::UniqueId entityId;

		// First step - get the actual mesh primitive and the feature ID accessor and such
		if (mDocument && mDocument->meshes.size() > meshIdx)
		{
			const fx::gltf::Mesh &mesh = mDocument->meshes[meshIdx];
			if (mesh.primitives.size() > primitiveSetIdx)
			{
				ComponentId locatedEntity = { };
				const fx::gltf::Primitive &primitiveSet = mesh.primitives[primitiveSetIdx];
				FeatureIdMapper mapper = this->makeFeatureIdMapper(primitiveSet, layer);
				std::uint32_t fid = mapper.getVertexFeatureId(idx);

				if (fid != UINT32_MAX)
				{
					entityId = this->makeEntityId(mapper.getMap().table, fid);
				}
			}
		}

		return entityId;
	}

	EntityWeight Model::getEntityData(const cio::UniqueId &id, unsigned layer) const
	{
		EntityWeight entity;
		FeatureIdWeight entityId = this->getEntityId(id, layer);

		ComponentId cid = { };
		std::memcpy(&cid, &entityId, sizeof(cid));

		geas::Index nodeId = cid.item;
		std::size_t featureId = cid.primitiveset;

		std::shared_ptr<const geas::Application> schema = mCatalog ? mCatalog->getApplicationSchema() : nullptr;

		if (mEntityCollection)
		{
			const Node *ds = mEntityCollection->getNodes().get(nodeId);
			if (ds)
			{
				// GDX-12 this is not yet const-correct, it is possible to modify the given entity through the data::Entity
				// Once we have full accessor support in place, we can parameterize data::Entity objects to enforce read-only access
				entity.value.bind(const_cast<Node *>(ds), featureId);
				entity.count = entityId.count;
			}
		}

		entity.total = entityId.total;

		return entity;
	}

	std::vector<EntityWeight> Model::getEntityData(const cio::UniqueId &id) 
	{
		std::size_t layers = this->getEntityLayerCount(id);
		std::vector<EntityWeight> weight(layers);
		for (std::size_t layer = 0; layer < layers; ++layer)
		{
			weight[layer] = this->getEntityData(id, layer);
		}
		return weight;
	}

	std::vector<EntityWeight> Model::getEntityData(const cio::UniqueId &id) const
	{
		std::size_t layers = this->getEntityLayerCount(id);
		std::vector<EntityWeight> weight(layers);
		for (std::size_t layer = 0; layer < layers; ++layer)
		{
			weight[layer] = this->getEntityData(id, layer);
		}
		return weight;
	}

	EntityWeight Model::getEntityData(const cio::UniqueId &id, unsigned layer)
	{
		EntityWeight entity = { Entity(), 0, 0 };
		FeatureIdWeight entityId = this->getEntityId(id, layer);

		ComponentId cid = { };
		std::memcpy(&cid, &entityId, sizeof(cid));

		geas::Index nodeId = cid.item;
		std::size_t featureId = cid.primitiveset;

		std::shared_ptr<const geas::Application> schema = mCatalog ? mCatalog->getApplicationSchema() : nullptr;

		if (mEntityCollection)
		{
			Node *ds = mEntityCollection->getNodes().get(nodeId);
			if (ds)
			{
				entity.value.bind(ds, featureId);
				entity.count = entityId.count;
			}
		}

		entity.total = entityId.total;
		return entity;
	}

	std::uint32_t Model::getEntityLayerCount() const
	{
		std::uint32_t count = 0;
		if (mDocument)
		{
			for (const fx::gltf::Scene &scene : mDocument->scenes)
			{
				std::uint32_t scenecount = this->getEntityLayerCount(scene);
				count = std::max(count, scenecount);
			}
		}

		return count;
	}

	std::uint32_t Model::getEntityLayerCount(const cio::UniqueId &id) const
	{
		ComponentId unpacked = { };
		std::memcpy(&unpacked, &id, sizeof(unpacked));

		std::uint32_t count = 0;

		// If the input ID is in fact an entity, then no need to do anything
		switch (unpacked.base)
		{
			case ComponentType::Table:
			{
				switch (unpacked.type)
				{
					case ComponentType::Entity:
					case ComponentType::Node:
						count = 1;
						break;

					default:
						break;
				}
			}
			break;

			case ComponentType::Mesh:
			{
				switch (unpacked.type)
				{
					case ComponentType::Index:
					case ComponentType::Vertex:
					case ComponentType::Primitive:
					case ComponentType::PrimitiveSet:
					{
						std::uint32_t meshIdx = unpacked.item;
						std::uint32_t primIdx = unpacked.primitiveset;
						if (mDocument && meshIdx < mDocument->meshes.size())
						{
							const fx::gltf::Mesh &mesh = mDocument->meshes[meshIdx];
							if (primIdx < mesh.primitives.size())
							{
								const fx::gltf::Primitive &prim = mesh.primitives[primIdx];
								count = this->getEntityLayerCount(prim);
							}
						}
					}
					break;

					case ComponentType::Mesh:
					{
						std::uint32_t meshIdx = unpacked.item;
						if (mDocument && meshIdx < mDocument->meshes.size())
						{
							const fx::gltf::Mesh &mesh = mDocument->meshes[meshIdx];
							count = this->getEntityLayerCount(mesh);
						}
					}
					break;

					default:
						break;
				}
			}
			break;

			case ComponentType::Node:
			{
				std::uint32_t nodeIdx = unpacked.item;
				if (unpacked.type == ComponentType::Node && mDocument && nodeIdx < mDocument->nodes.size())
				{
					count = this->getEntityLayerCount(mDocument->nodes[nodeIdx]);
				}
			}
			break;

			case ComponentType::Scene:
			{
				std::uint32_t sceneIdx = unpacked.item;
				if (unpacked.type == ComponentType::Scene && mDocument && sceneIdx < mDocument->scenes.size())
				{
					count = this->getEntityLayerCount(mDocument->scenes[sceneIdx]);
				}
			}
			break;

			case ComponentType::Document:
			{
				if (unpacked.type == ComponentType::Document && unpacked.item == 0 && mDocument)
				{
					count = this->getEntityLayerCount();
				}
			}
			break;

			default:
				break;
		}

		return count;
	}

	std::uint32_t Model::getEntityLayerCount(const fx::gltf::Primitive &prim) const
	{
		std::uint32_t count = 0;
		auto ee = prim.extensionsAndExtras.find("extensions");
		if (ee != prim.extensionsAndExtras.end())
		{
			auto md = ee->find(mMetadataExtensionName);
			if (md != ee->end())
			{
				MetadataReader reader;
				count = reader.findFeatureTableCount(*md);
			}
		}

		return count;
	}

	std::uint32_t Model::getEntityLayerCount(const fx::gltf::Mesh &mesh) const
	{
		std::uint32_t count = 0;

		for (const fx::gltf::Primitive &prim : mesh.primitives)
		{
			std::uint32_t primcount = this->getEntityLayerCount(prim);
			count = std::max(count, primcount);
		}

		return count;
	}

	std::uint32_t Model::getEntityLayerCount(const fx::gltf::Node &node) const
	{
		std::uint32_t count = 0;
		if (mDocument)
		{
			if (node.mesh >= 0 && node.mesh < mDocument->meshes.size())
			{
				count = this->getEntityLayerCount(mDocument->meshes[node.mesh]);
			}

			for (std::uint32_t i : node.children)
			{
				if (i < mDocument->nodes.size())
				{
					std::uint32_t childcount = this->getEntityLayerCount(mDocument->nodes[i]);
					count = std::max(count, childcount);
				}
			}
		}

		return count;
	}

	std::uint32_t Model::getEntityLayerCount(const fx::gltf::Scene &scene) const
	{
		std::uint32_t count = 0;
		if (mDocument)
		{
			for (std::uint32_t i : scene.nodes)
			{
				if (i < mDocument->nodes.size())
				{
					std::uint32_t childcount = this->getEntityLayerCount(mDocument->nodes[i]);
					count = std::max(count, childcount);
				}
			}
		}
		return count;
	}

	std::vector<std::vector<FeatureIdWeight>> Model::getAllEntityIds(const cio::UniqueId &id)
	{
		std::vector<std::vector<FeatureIdWeight>> layers;

		std::uint32_t layerCount = this->getEntityLayerCount(id);
		layers.resize(layerCount);

		for (std::uint32_t i = 0; i < layerCount; ++i)
		{
			layers[i] = this->getAllEntityIds(id, i);
		}

		return layers;
	}

	std::vector<FeatureIdWeight> Model::getAllEntityIds(const cio::UniqueId &id, unsigned layer) const
	{
		std::map<cio::UniqueId, Weight<void>> weights;
		ComponentId unpacked = { };
		std::memcpy(&unpacked, &id, sizeof(unpacked));

		std::uint32_t count = 0;

		// If the input ID is in fact an entity, then no need to do anything
		switch (unpacked.base)
		{
			case ComponentType::Table:
			{
				switch (unpacked.type)
				{
					case ComponentType::Entity:
					{
						// TODO validate it exists
						Weight<void> added = { 1, 1 };
						weights[id] |= added;
						break;
					}

					case ComponentType::Node:
					{
						const Node *ds = mEntityCollection->getNodes().get(geas::Index(unpacked.item));
						{
							// TODO implement
						}
					}

					default:
						break;
				}
			}
			break;

			case ComponentType::Mesh:
			{
				switch (unpacked.type)
				{
					case ComponentType::Index:
					{
						cio::UniqueId id = this->getEntityIdForPrimitiveIndex(unpacked.item, unpacked.primitiveset, unpacked.element, layer);
						if (id)
						{
							Weight<void> added = { 1, 1 };
							weights[id] |= added;
						}
					}
					break;

					case ComponentType::Vertex:
					{
						// TODO implement
					}
					break;

					case ComponentType::Primitive:
					{
						std::uint32_t vid = unpacked.element * 3;
						for (std::uint32_t n = 0; n < 3; ++n)
						{
							cio::UniqueId id = this->getEntityIdForPrimitiveIndex(unpacked.item, unpacked.primitiveset, unpacked.element, layer);
							if (id)
							{
								Weight<void> added = { 1, 1 };
								weights[id] |= added;
							}
						}
					}
					break;

					case ComponentType::PrimitiveSet:
					{
						std::uint32_t meshIdx = unpacked.item;
						std::uint32_t primIdx = unpacked.primitiveset;
						if (mDocument && meshIdx < mDocument->meshes.size())
						{
							const fx::gltf::Mesh &mesh = mDocument->meshes[meshIdx];
							if (primIdx < mesh.primitives.size())
							{
								this->updateFeatureWeightMap(mesh.primitives[primIdx], weights, layer);
							}
						}
					}
					break;

					case ComponentType::Mesh:
					{
						std::uint32_t meshIdx = unpacked.item;
						if (mDocument && meshIdx < mDocument->meshes.size())
						{
							const fx::gltf::Mesh &mesh = mDocument->meshes[meshIdx];
							this->updateFeatureWeightMap(mesh, weights, layer);
						}
					}
					break;

					default:
						break;
				}
			}
			break;

			case ComponentType::Node:
			{
				std::uint32_t nodeIdx = unpacked.item;
				if (unpacked.type == ComponentType::Node && mDocument && nodeIdx < mDocument->nodes.size())
				{
					this->updateFeatureWeightMap(mDocument->nodes[nodeIdx], weights, layer);
				}
			}
			break;

			case ComponentType::Scene:
			{
				std::uint32_t sceneIdx = unpacked.item;
				if (unpacked.type == ComponentType::Scene && mDocument && sceneIdx < mDocument->scenes.size())
				{
					this->updateFeatureWeightMap(mDocument->scenes[sceneIdx], weights, layer);
				}
			}
			break;

			case ComponentType::Document:
			{
				if (unpacked.type == ComponentType::Document && unpacked.item == 0 && mDocument)
				{
					this->updateFeatureWeightMap(weights, layer);
				}
			}
			break;

			default:
				break;
		}

		std::vector<FeatureIdWeight> result(weights.size());
		std::size_t total = 0;
		std::size_t i = 0;
		for (const std::pair<cio::UniqueId, Weight<void>> &item : weights)
		{
			result[i].value = item.first;
			result[i].count = item.second.count;
			total += item.second.count;
		}

		for (FeatureIdWeight &item : result)
		{
			item.total = total;
		}

		return result;
	}

	cio::Chunk<void> Model::getBuffer(std::int32_t bufferIdx)
	{
		cio::Chunk<void> chunk;

		if (mDocument && mDocument->buffers.size() > bufferIdx)
		{
			fx::gltf::Buffer &buffer = mDocument->buffers[bufferIdx];
			chunk.bind(buffer.data.data(), buffer.byteLength, nullptr);
		}

		return chunk;
	}

	cio::Chunk<const void> Model::getBuffer(std::int32_t bufferIdx) const
	{
		cio::Chunk<const void> chunk;

		if (mDocument && mDocument->buffers.size() > bufferIdx)
		{
			fx::gltf::Buffer &buffer = mDocument->buffers[bufferIdx];
			chunk.bind(buffer.data.data(), buffer.byteLength, nullptr);
		}

		return chunk;
	}

	cio::Chunk<void> Model::getBufferView(std::int32_t bufferViewIdx)
	{
		cio::Chunk<void> viewChunk;

		if (mDocument && bufferViewIdx >= 0 && bufferViewIdx < mDocument->bufferViews.size())
		{
			const fx::gltf::BufferView &view = mDocument->bufferViews[bufferViewIdx];
			cio::Chunk<void> chunk = this->getBuffer(view.buffer);
			if (chunk)
			{
				viewChunk = chunk.slice(view.byteOffset, view.byteLength);
			}
		}

		return viewChunk;
	}

	cio::Chunk<const void> Model::getBufferView(std::int32_t bufferViewIdx) const
	{
		// TODO GDX-12 This is not const correct, but we need the full accessor API in place to enforce read-only access
		cio::Chunk<const void> viewChunk;

		if (mDocument && bufferViewIdx >= 0 && bufferViewIdx < mDocument->bufferViews.size())
		{
			const fx::gltf::BufferView &view = mDocument->bufferViews[bufferViewIdx];
			cio::Chunk<const void> chunk = this->getBuffer(view.buffer);
			if (chunk)
			{
				viewChunk = chunk.slice(view.byteOffset, view.byteLength);
			}
		}

		return viewChunk;
	}

	cio::Type Model::getElementType(const fx::gltf::Accessor &accessor) const
	{
		cio::Type type = cio::Type::None;

		if (accessor.componentType == fx::gltf::Accessor::ComponentType::Float || accessor.normalized)
		{
			type = cio::Type::Real;
		}
		else
		{
			type = cio::Type::Integer;
		}

		return type;
	}

	std::pair<cio::Encoding, unsigned> Model::getElementLayout(const fx::gltf::Accessor &accessor) const
	{
		std::pair<cio::Encoding, unsigned> layout;

		switch (accessor.componentType)
		{
			case fx::gltf::Accessor::ComponentType::Byte:
				layout.first = accessor.normalized ? cio::Encoding::createNormalized<std::int8_t>() : cio::Encoding::create<std::int8_t>();
				break;

			case fx::gltf::Accessor::ComponentType::UnsignedByte:
				layout.first = accessor.normalized ? cio::Encoding::createNormalized<std::uint8_t>() : cio::Encoding::create<std::uint8_t>();
				break;

			case fx::gltf::Accessor::ComponentType::Short:
				layout.first = accessor.normalized ? cio::Encoding::createNormalized<std::int16_t>() : cio::Encoding::create<std::int16_t>();
				break;

			case fx::gltf::Accessor::ComponentType::UnsignedShort:
				layout.first = accessor.normalized ? cio::Encoding::createNormalized<std::uint16_t>() : cio::Encoding::create<std::uint16_t>();
				break;

			case fx::gltf::Accessor::ComponentType::UnsignedInt:
				layout.first = accessor.normalized ? cio::Encoding::createNormalized<std::uint32_t>() : cio::Encoding::create<std::uint32_t>();
				break;

			case fx::gltf::Accessor::ComponentType::Float:
				layout.first = cio::Encoding::create<float>();
				break;

			default:
				// unknown or None, don't set
				break;
		}

		switch (accessor.type)
		{
			case fx::gltf::Accessor::Type::Scalar:
				layout.second = 1;
				break;

			case fx::gltf::Accessor::Type::Vec2:
				layout.second = 2;
				break;

			case fx::gltf::Accessor::Type::Vec3:
				layout.second = 3;
				break;

			case fx::gltf::Accessor::Type::Vec4:
				layout.second = 4;
				break;

			case fx::gltf::Accessor::Type::Mat2:
				layout.second = 4;
				break;

			case fx::gltf::Accessor::Type::Mat3:
				layout.second = 9;
				break;

			case fx::gltf::Accessor::Type::Mat4:
				layout.second = 16;
				break;

			default:
				layout.second = 0;
				break;
		}

		return layout;
	}

	Array Model::getDataView(std::int32_t accessorIdx)
	{
		Array values;
		if (mDocument && accessorIdx >= 0 && accessorIdx < mDocument->accessors.size())
		{
			const fx::gltf::Accessor &accessor = mDocument->accessors[accessorIdx];
			values = this->getDataView(accessor);
		}
		
		return values;
	}

	Array Model::getDataView(std::int32_t accessorIdx) const
	{
		Array values;
		if (mDocument && accessorIdx >= 0 && accessorIdx < mDocument->accessors.size())
		{
			const fx::gltf::Accessor &accessor = mDocument->accessors[accessorIdx];
			values = this->getDataView(accessor);
		}
		return values;
	}

	Array Model::getDataView(fx::gltf::Accessor &accessor)
	{
		Array values;
		cio::Chunk<void> chunker = this->getBufferView(accessor.bufferView);
		if (chunker.size() > 0)
		{
			std::pair<cio::Encoding, unsigned> layout = this->getElementLayout(accessor);
			cio::Type type = this->getElementType(accessor);

			// Stride is on buffer view, inconveniently
			const fx::gltf::BufferView &bufferView = mDocument->bufferViews.at(accessor.bufferView);

			values.setFixedBuffer(std::move(chunker));
			values.setType(type);
			values.resize(accessor.count);
			values.setInitialOffset(accessor.byteOffset);
			values.setEncoding(layout.first);
			values.setElementCount(layout.second);
			values.setElementStride(layout.first.computeRequiredBytes());
			values.setStride(values.getElementStride() + bufferView.byteStride);
		}
		return values;
	}

	Array Model::getDataView(const fx::gltf::Accessor &accessor) const
	{
		// TODO GDX-12 this is not exactly const-correct yet, since the returned ValueArray can be used to modify the glTF buffers
		// Fixing this will require the full accessor API in place so Array is parameterized by constness and return type
		Array values;
		cio::Chunk<void> chunker = const_cast<Model *>(this)->getBufferView(accessor.bufferView);
		if (chunker.size() > 0)
		{
			std::pair<cio::Encoding, unsigned> layout = this->getElementLayout(accessor);
			cio::Type type = this->getElementType(accessor);

			// Stride is on buffer view, inconveniently
			const fx::gltf::BufferView &bufferView = mDocument->bufferViews.at(accessor.bufferView);
			values.setFixedBuffer(std::move(chunker));
			values.setType(type);
			values.resize(accessor.count);
			values.setInitialOffset(accessor.byteOffset);
			values.setEncoding(layout.first);
			values.setElementCount(layout.second);
			values.setElementStride(layout.first.computeRequiredBytes());
			values.setStride(values.getElementStride() * layout.second + bufferView.byteStride);
		}
		return values;
	}

	cio::UniqueId Model::makeId(const fx::gltf::Material &material) const
	{
		cio::UniqueId id;

		if (mDocument && !mDocument->materials.empty())
		{
			const fx::gltf::Material *begin = mDocument->materials.data();
			const fx::gltf::Material *end = mDocument->materials.data() + mDocument->materials.size();
			if (&material >= begin && &material < end)
			{
				std::uint32_t offset = static_cast<std::uint32_t>(&material - begin);
				id = this->makeMaterialId(offset);
			}
		}

		return id;
	}

	cio::UniqueId Model::makeId(const fx::gltf::Node &node) const
	{
		cio::UniqueId id;

		if (mDocument && !mDocument->nodes.empty())
		{
			const fx::gltf::Node *begin = mDocument->nodes.data();
			const fx::gltf::Node *end = mDocument->nodes.data() + mDocument->nodes.size();
			if (&node >= begin && &node < end)
			{
				std::uint32_t offset = static_cast<std::uint32_t>(&node - begin);
				id = this->makeNodeId(offset);
			}
		}

		return id;
	}

	cio::UniqueId Model::makeId(const fx::gltf::Mesh &mesh) const
	{
		cio::UniqueId id;

		if (mDocument && !mDocument->meshes.empty())
		{
			const fx::gltf::Mesh *begin = mDocument->meshes.data();
			const fx::gltf::Mesh *end = mDocument->meshes.data() + mDocument->meshes.size();
			if (&mesh >= begin && &mesh < end)
			{
				std::uint32_t offset = static_cast<std::uint32_t>(&mesh - begin);
				id = this->makeMeshId(offset);
			}
		}

		return id;
	}

	cio::UniqueId Model::makeId(const fx::gltf::Mesh &mesh, const fx::gltf::Primitive &prim) const
	{
		cio::UniqueId id;

		if (mDocument && !mDocument->meshes.empty() && !mesh.primitives.empty())
		{
			const fx::gltf::Mesh *begin = mDocument->meshes.data();
			const fx::gltf::Mesh *end = mDocument->meshes.data() + mDocument->meshes.size();
			if (&mesh >= begin && &mesh < end)
			{
				std::uint32_t offset = static_cast<std::uint32_t>(&mesh - begin);
				const fx::gltf::Primitive *pbegin = mesh.primitives.data();
				const fx::gltf::Primitive *pend = mesh.primitives.data() + mesh.primitives.size();
				if (&prim >= pbegin && &prim < pend)
				{
					std::uint32_t poffset = static_cast<std::uint32_t>(&prim - pbegin);
					id = this->makePrimitiveSetId(offset, poffset);
				}
			}
		}

		return id;
	}

	cio::UniqueId Model::makeVertexId(const fx::gltf::Mesh &mesh, const fx::gltf::Primitive &prim, std::uint32_t vtx) const
	{
		cio::UniqueId id;

		if (mDocument && !mDocument->meshes.empty() && !mesh.primitives.empty())
		{
			const fx::gltf::Mesh *begin = mDocument->meshes.data();
			const fx::gltf::Mesh *end = mDocument->meshes.data() + mDocument->meshes.size();
			if (&mesh >= begin && &mesh < end)
			{
				std::uint32_t offset = static_cast<std::uint32_t>(&mesh - begin);
				const fx::gltf::Primitive *pbegin = mesh.primitives.data();
				const fx::gltf::Primitive *pend = mesh.primitives.data() + mesh.primitives.size();
				if (&prim >= pbegin && &prim < pend)
				{
					std::uint32_t poffset = static_cast<std::uint32_t>(&prim - pbegin);
					// TODO validate that vertex index is okay
					id = this->makeVertexId(offset, poffset, vtx);
				}
			}
		}

		return id;
	}

	cio::UniqueId Model::makePrimitiveId(const fx::gltf::Mesh &mesh, const fx::gltf::Primitive &prim, std::uint32_t idx) const
	{
		cio::UniqueId id;

		if (mDocument && !mDocument->meshes.empty() && !mesh.primitives.empty())
		{
			const fx::gltf::Mesh *begin = mDocument->meshes.data();
			const fx::gltf::Mesh *end = mDocument->meshes.data() + mDocument->meshes.size();
			if (&mesh >= begin && &mesh < end)
			{
				std::uint32_t offset = static_cast<std::uint32_t>(&mesh - begin);
				const fx::gltf::Primitive *pbegin = mesh.primitives.data();
				const fx::gltf::Primitive *pend = mesh.primitives.data() + mesh.primitives.size();
				if (&prim >= pbegin && &prim < pend)
				{
					std::uint32_t poffset = static_cast<std::uint32_t>(&prim - pbegin);
					// TODO validate that primitive index is okay
					id = this->makePrimitiveId(offset, poffset, idx);
				}
			}
		}

		return id;
	}

	cio::UniqueId Model::makePrimitiveIndexId(const fx::gltf::Mesh &mesh, const fx::gltf::Primitive &prim, std::uint32_t idx) const
	{
		cio::UniqueId id;

		if (mDocument && !mDocument->meshes.empty() && !mesh.primitives.empty())
		{
			const fx::gltf::Mesh *begin = mDocument->meshes.data();
			const fx::gltf::Mesh *end = mDocument->meshes.data() + mDocument->meshes.size();
			if (&mesh >= begin && &mesh < end)
			{
				std::uint32_t offset = static_cast<std::uint32_t>(&mesh - begin);
				const fx::gltf::Primitive *pbegin = mesh.primitives.data();
				const fx::gltf::Primitive *pend = mesh.primitives.data() + mesh.primitives.size();
				if (&prim >= pbegin && &prim < pend)
				{
					std::uint32_t poffset = static_cast<std::uint32_t>(&prim - pbegin);
					// TODO validate that primitive index is okay
					id = this->makePrimitiveIndexId(offset, poffset, idx);
				}
			}
		}

		return id;
	}

	cio::UniqueId Model::makeMaterialId(std::uint32_t materialIdx) const
	{
		cio::UniqueId id;

		ComponentId fields;
		fields.base = ComponentType::Material;
		fields.type = ComponentType::Material;
		fields.attribute = 0;
		fields.item = materialIdx;
		fields.primitiveset = 0;
		fields.element = 0;

		std::memcpy(&id, &fields, sizeof(fields));

		return id;
	}

	cio::UniqueId Model::makeNodeId(std::uint32_t nodeIdx) const
	{
		cio::UniqueId id;

		ComponentId fields;
		fields.base = ComponentType::Node;
		fields.type = ComponentType::Node;
		fields.item = nodeIdx;
		fields.primitiveset = 0;
		fields.element = 0;

		std::memcpy(&id, &fields, sizeof(fields));
		return id;
	}

	cio::UniqueId Model::makeMeshId(std::uint32_t meshIdx) const
	{
		cio::UniqueId id;

		ComponentId fields;
		fields.base = ComponentType::Mesh;
		fields.type = ComponentType::Mesh;
		fields.attribute = 0;
		fields.item = meshIdx;
		fields.primitiveset = 0;
		fields.element = 0;

		std::memcpy(&id, &fields, sizeof(fields));
		return id;
	}

	cio::UniqueId Model::makePrimitiveSetId(std::uint32_t meshIdx, std::uint32_t primIdx) const
	{
		cio::UniqueId id;

		ComponentId fields;
		fields.base = ComponentType::Mesh;
		fields.type = ComponentType::PrimitiveSet;
		fields.attribute = 0;
		fields.item = meshIdx;
		fields.primitiveset = primIdx;
		fields.element = 0;

		std::memcpy(&id, &fields, sizeof(fields));
		return id;
	}

	cio::UniqueId Model::makePrimitiveId(std::uint32_t meshIdx, std::uint32_t primSetIdx, std::uint32_t primIdx) const
	{
		cio::UniqueId id;

		ComponentId fields;
		fields.base = ComponentType::Mesh;
		fields.type = ComponentType::Primitive;
		fields.attribute = 0;
		fields.item = meshIdx;
		fields.primitiveset = primSetIdx;
		fields.element = primIdx;

		std::memcpy(&id, &fields, sizeof(fields));

		return id;
	}

	cio::UniqueId Model::makeVertexId(std::uint32_t meshIdx, std::uint32_t primIdx, std::uint32_t vertexOffset) const
	{
		cio::UniqueId id;

		ComponentId fields;
		fields.base = ComponentType::Mesh;
		fields.type = ComponentType::Vertex;
		fields.attribute = 0;
		fields.item = meshIdx;
		fields.primitiveset = primIdx;
		fields.element = vertexOffset;

		std::memcpy(&id, &fields, sizeof(fields));
		return id;
	}

	cio::UniqueId Model::makePrimitiveIndexId(std::uint32_t meshIdx, std::uint32_t primIdx, std::uint32_t indexOffset) const
	{
		cio::UniqueId id;

		ComponentId fields;
		fields.base = ComponentType::Mesh;
		fields.type = ComponentType::Index;
		fields.attribute = 0;
		fields.item = meshIdx;
		fields.primitiveset = primIdx;
		fields.element = indexOffset;

		std::memcpy(&id, &fields, sizeof(fields));
		return id;
	}

	cio::UniqueId Model::makeEntityId(const geas::Index dsid, std::uint32_t fid) const
	{
		cio::UniqueId id;
		ComponentId fields;
		fields.base = ComponentType::Node;
		fields.type = ComponentType::Entity;
		fields.attribute = 0;
		fields.item = dsid;
		fields.primitiveset = fid;
		fields.element = 0;

		std::memcpy(&id, &fields, sizeof(fields));
		return id;
	}

	cio::UniqueId Model::makeEntityId(const geas::Label &node, std::uint32_t fid) const
	{
		cio::UniqueId id;
		if (mEntityCollection)
		{
			const Node *ds = mEntityCollection->getNodes().get(node);
			if (ds)
			{
				id = this->makeEntityId(ds->getIndex(), fid);
			}
		}
		return id;
	}

	cio::UniqueId Model::makeEntityId(const Node &node, std::uint32_t fid) const
	{
		return makeEntityId(node.getIndex(), fid);
	}

	void Model::loadFeatureMetadata()
	{
		if (!mPreserveCatalog)
		{
			mCatalog.reset();
			mEntityCollection.reset();
			mEntityCollection->setCatalog(mCatalog);
		}

		if (mDocument)
		{
			bool foundExtension = false;
			const char *targetExtensions = sDefaultMetadataExtensionName;
			if (!mMetadataExtensionName.empty())
			{
				targetExtensions = mMetadataExtensionName.c_str();
			}

			for (const std::string &ext : mDocument->extensionsUsed)
			{
				if (ext == targetExtensions)
				{
					mMetadataExtensionName = ext;
					foundExtension = true;
					break;
				}
			}

			if (foundExtension)
			{
				if (!mCatalog && mAutoloadCatalog)
				{
					mCatalog.reset(new Catalog());
				}

				if (!mEntityCollection && mAutoloadCollection)
				{
					mEntityCollection.reset(new Collection());
				}

				auto ee = mDocument->extensionsAndExtras.find("extensions");
				if (ee != mDocument->extensionsAndExtras.end())
				{
					auto ii = ee->find(mMetadataExtensionName);
					if (ii != ee->end())
					{
						MetadataReader reader;
						geas::json::OwtSchemaReader gdxReader;

						if (mAutoloadCatalog && mAutoloadCollection)
						{
							cio::Path filePath = mParentNode.getPath();
							reader.loadFeatureMetadata(*this, *ii, filePath);
						}
						else if (mAutoloadCatalog)
						{
							auto fc = ii->find("schema");
							if (fc != ii->end())
							{
								gdxReader.loadApplication(*mCatalog->getApplicationSchema(), *mCatalog->getProfiles(), *fc);
							}
						}
						else if (mAutoloadCollection)
						{
							auto ft = ii->find("featureTables");
							if (ft != ii->end())
							{
								reader.loadFeatureTables(*this, *ft);
							}
						}
					}
				}
			}
		}
	}

	void Model::synchronizeEntities()
	{
		// TODO implement using MetadataClassReader and/or MetadataClassWriter
	}

	const std::string &Model::getMetadataExtensionName() const
	{
		return mMetadataExtensionName;
	}

	void Model::setMetadataExtensionName(std::string name)
	{
		mMetadataExtensionName = std::move(name);
	}

	void Model::setPreserveCatalog(bool value)
	{
		mPreserveCatalog = value;
	}

	bool Model::getPreserveCatalog() const
	{
		return mPreserveCatalog;
	}

	void Model::setAutoloadCatalog(bool value)
	{
		mAutoloadCatalog = value;
	}

	bool Model::getAutoloadCatalog() const
	{
		return mAutoloadCatalog;
	}

	void Model::setAutoloadCollection(bool value)
	{
		mAutoloadCollection = value;
	}

	bool Model::getAutoloadCollection() const
	{
		return mAutoloadCollection;
	}
}
