/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_VARIABLEMATRIX_H
#define GLITTER_VARIABLEMATRIX_H

#include "VariableArray.h"
#include "FixedArray.h"

#include <cio/Chunk.h>
#include <cio/Types.h>

#include <vector>

namespace glitter
{
		template <typename S, typename T, typename U /*= void*/>
		class VariableMatrix
		{
		public:
			/**
			* Construct a new variable length matrix.  The matrix has variable number of rows and variable number of columns per row.
			* Each row/column element is of a variable length as well. 
			* [
			*  ["Hi", "there"]				//2 elements, Lengths of 2 and 5
			*  ["all", "you", "people"]		//3 elements, Lengths of 3, 3, 6
			*  ["out", "there"]				//2 elements, lengths of 3, 5
			* ]
			* row index offsets looks like this:      {0, 2, 5, 7}
			* column index offsets looks like this:   {0, 2, 7, 10, 13, 19, 22, 27}
			*/
			inline VariableMatrix() noexcept;

			/**
			* Construct a variable length matrix with the given data.  Since no offset data is given, we assume
			* the size of the matrix is 1 element, and the number of rows is 1, thus the number of columns is 1.
			* The offset is set to zero and the element size is initialized as the size of the matrix type (T).
			* 
			* @param data The data that the matrix is going to parse.
			*/
			inline explicit VariableMatrix(cio::Chunk<U> data);

			/**
			* Construct a variable length matrix with the given data, the given column offsets and the given row offsets.
			* The matrix has a set number of rows as determined by the number of row offsets given (minus 1 to account for
			* the end offset).  The number of columns in each row is variable.  The size of the matrix is the total number
			* of elemments in the matrix as determined by the number of column offsets given (minus 1 to account for the ending offset).
			* 
			* @param data The data that the matrix is going to parse and discern.
			* @param columnOffsets The offset of each column element in the matrix, also used to determine the size of the matrix.
			* @param rowOffsets  The offset of each row in the matix.  The matrix may have a variable number of columns per row.  
			*/
			inline explicit VariableMatrix(cio::Chunk<U> data, Scalar<S, U> columnOffsets, Scalar<S, U> rowOffsets);

			/**
			* Construct a variable length matrix with the given variable array.  This assumes the data is in one long row.
			*
			* @param columnData Variable array that contains data and an accesser into that data.
			*/
			inline VariableMatrix(VariableArray<S, T, U> columnData);

			/**
			* Construct a variable length matrix with the given variable array.  This assumes the data is in one long row.
			*
			* @param columnData Variable array that contains data and an accesser into that data.
			* @parma rowOffsets An accessor that contains the starting index of each row.
			*/
			inline VariableMatrix(VariableArray<S, T, U> columnData, Scalar<S, U> rowOffsets);


			/**
			* Construct a default, compiler generated, read-only copy of the matrix.
			*
			* @param in The read-only matrix to copy.
			*/
			inline VariableMatrix(const VariableMatrix<S, T, U>& in) = default;

			/**
			* Move the data and copy the attributes into a new instance of this class.
			*
			* @param in The matrix to copy.
			*/
			inline VariableMatrix(VariableMatrix<S, T, U>&& in) noexcept;

			/**
			* Copy the data and attributes into a new instance of this class.
			*
			* @param in The read-only matrix to copy.
			*/
			template <typename V, typename W, typename X>
			inline VariableMatrix(const VariableMatrix<V, W, X>& in);

			/**
			* Move the data and copy the attributes into a new instance of this class.
			*
			* @param in The matrix to copy.
			*/
			template <typename V, typename W, typename X>
			inline VariableMatrix(VariableMatrix<V, W, X>&& in) noexcept;

			/**
			* Create a read-only copy of the given matrix.
			*
			* @param in The given matrix to copy.
			*/
			inline VariableMatrix<S, T, U>& operator=(const VariableMatrix<S, T, U>& in) = default;

			/**
			* Constructs copy of the given matrix.
			*
			*@param in The given matrix of a templated type to copy.
			*/
			inline VariableMatrix<S, T, U>& operator=(VariableMatrix<S, T, U>&& in) noexcept;

			/**
			* Default destructor.
			*/
			inline ~VariableMatrix() noexcept = default;

			/**
			* Set the data in the matrix to the given chunk of data, along with column offsets
			* and row offsets that contain the values of each row index and column index.  The
			* number of elements in the matrix is the number of column offsets - 1 and the number
			* of rows is the number of row offsets -1.
			*
			* @param data The data for the matrix to parse.
			* @param columnOffsets The scalar data that contains the array of indicies used to access each column.
			* @param rowOffsets The scalar data that contains the array of indicies used to access each row.
			*/
			inline void bind(cio::Chunk<U> data);

			/**
			* Set the data in the matrix to the given chunk of data, along with column offsets
			* and row offsets that contain the values of each row index and column index.  The
			* number of elements in the matrix is the number of column offsets - 1 and the number
			* of rows is the number of row offsets -1.
			*
			* @param data The data for the matrix to parse.
			* @param columnOffsets The scalar data that contains the array of indicies used to access each column.
			* @param rowOffsets The scalar data that contains the array of indicies used to access each row.
			*/
			inline void bind(cio::Chunk<U> data, Scalar<S, U> columnOffsets, Scalar<S, U> rowOffsets);

			/**
			* Set the data in the matrix to the given chunk of data, along with column offsets
			* and row offsets that contain the values of each row index and column index.  The
			* number of elements in the matrix is the number of column offsets - 1 and the number
			* of rows is the number of row offsets -1.
			*
			* @param data The data for the matrix to parse.
			* @param columnOffsets The scalar data that contains the array of indicies used to access each column.
			* @param rowOffsets The scalar data that contains the array of indicies used to access each row.
			*/
			inline void bind(cio::Chunk<U> data, cio::Chunk<U> columnOffsets, std::size_t numColOffsets, cio::Chunk<U> rowOffsets, std::size_t numRowOffsets);

			/**
			* Set the data in the matrix to the given chunk of data, along with column offsets
			* and row offsets that contain the values of each row index and column index.  The
			* number of elements in the matrix is the number of column offsets - 1 and the number
			* of rows is the number of row offsets -1.
			*
			* @param data The data for the matrix to parse.
			* @param columnOffsets The data that contains the array of indicies used to access each column.
			* @param numColOffsets The number of column offsets in the data chunk.
			*/
			inline void bind(cio::Chunk<U> data, cio::Chunk<U> columnOffsets, std::size_t numColOffsets);

			/**
			* Set the data in the matrix to the given chunk of data, along with column offsets
			* and row offsets that contain the values of each row index and column index.  The
			* number of elements in the matrix is the number of column offsets - 1 and the number
			* of rows is the number of row offsets -1.
			*
			* @param data The data for the matrix to parse.
			* @param columnOffsets The scalar data that contains the array of indicies used to access each column.
			*/
			inline void bind(cio::Chunk<U> data, Scalar<S, U> columnOffsets);

			/**
			* Set the data in the matrix to the given variable array (data and data offsets)
			* and row offsets that contain the values of each row index and column index.  The
			* number of elements in the matrix is the number of column offsets - 1 and the number
			* of rows is the number of row offsets -1.
			*
			* @param columnData The data for the matrix to parse and the scalar data that contains the array of 
			* indicies used to access each column.
			* @param rowOffsets The scalar data that contains the array of indicies used to access each row.
			*/
			inline void bind(VariableArray<S, T, U> columnData, Scalar<S, U> rowOffsets);

			/**
			* Gets the total number of rows in the matrix.
			*
			* @return The number of rows.
			*/
			inline std::size_t getRowCount() const;

			/**
			* Sets the number of rows in the matrix.
			*
			* @param rows The number of rows.
			*/
			inline void setRowCount(std::size_t rows);

			/**
			* Gets the total number of columns in the given row of the matrix.
			*
			* @param rowIdx The row index in which you want the number of columns.
			* @return The number of columns in the given row.
			*/
			inline std::size_t getColumns(std::size_t rowIdx) const;

			/**
			* Sets the number of columns in the matrix at that particular
			* row.  NOTE - This will actually alter the row offsets and may
			* affect the number of columns for the subsequent rows.
			*
			* @param rowIdx The row index to set the column size for.
			* @param numColumns The number of columns to set at that row.
			*/
			inline void setColumns(std::size_t rowIdx, std::size_t numColumns);

			/**
			* Gets the size (the distance between column offsets) of the data at
			* the given column index and row index.
			*			
			* @param columnIdx The column index of the element.
			* @param rowIdx The row index of the element.
			* @return The size of the data element.
			*/
			inline std::size_t getElementSize(std::size_t columnIdx, std::size_t rowIdx) const;

			/**
			* Get the total number of elements in the data array.  This is the cumulative
			* number of columns for every row in the matrix.
			*
			* @return The number of matrix elements.
			*/
			inline std::size_t size() const;

			/**
			* Resize the matrix.
			*
			* @param length  The new size of the matrix.
			*/
			inline void resize(std::size_t length);

			/**
			* Clears the underlying data and resets the number of rows and size to zero,
			*/
			inline void clear();

			/**
			* Get the underlying chunk of data.
			*
			* @return The data the matrix is parsing.
			*/
			inline cio::Chunk<U>& getData();

			/**
			* Gets a read-only reference to the underlying chunk of data.
			*
			* @return the read-only reference to the underlying data.
			*/
			inline const cio::Chunk<U>& getData() const;

			/**
			* Sets the underlying data for the matrix.
			*
			* @param data The data to be moved to be owned by the matrix.
			*/
			inline void setData(cio::Chunk<U> data);

			/**
			* Get the variable array that contains the data and the data offsets.
			*
			* @return The variable array.
			*/
			inline VariableArray<S, T, U>& getColumnData();

			/**
			* Gets a read-only reference to the variable array that contains the data and the data offsets.
			*
			* @return the read-only reference to the offsets into the data.
			*/
			inline const VariableArray<S, T, U>& getColumnData() const;

			/**
			* Sets the underlying data and column offsets as a variable array.
			*
			* @param data The variable array to be owned by the matrix.
			*/
			inline void setColumnData(VariableArray<S, T, U>  data);

			/**
			* Get the data that represents the offsets into each row of the matrix.
			*
			* @return The offsets into each row of the matrix.
			*/
			inline Scalar<S, U>& getRowOffsets();

			/**
			* Gets a read-only reference to the offsets into each row of the matrix.
			*
			* @return the read-only reference to the offsets into each row of the matrix
			*/
			inline const Scalar<S, U>& getRowOffsets() const;

			/**
			* Sets the underlying row offsets.
			*
			* @param rowOffsets The row offsets to be moved to be owned by the matrix.
			*/
			inline void setRowOffsets(Scalar<S, U>  rowOffsets);

			/**
			* Gets a pointer to the underlying data.
			*
			* @return a pointer to the underlying data of templated type U.
			*/
			inline U* data();

			/**
			* Gets a const pointer to the underlying data.
			*
			* @return a const pointer to the data in the matrix.
			*/
			inline const U* data() const;

			/**
			* Gets a pointer to the underlying column offsets.
			*
			* @return a pointer to the underlying column offsets of templated type U.
			*/
			inline U* columnOffsets();

			/**
			* Gets a const pointer to the underlying column offsets.
			*
			* @return a const pointer to the column offsets in the matrix.
			*/
			inline const U* columnOffsets() const;

			/**
			* Gets a pointer to the underlying row offsets.
			*
			* @return a pointer to the underlying row offsets of templated type U.
			*/
			inline U* rowOffsets();

			/**
			* Gets a const pointer to the underlying row offsets.
			*
			* @return a const pointer to the row offsets in the matrix.
			*/
			inline const U* rowOffsets() const;

			/**
			* Gets the element at each column in the specified row as a vector of element components.  This will
			* create a vector for each element at the column/row location.  If a row contains 3 columns, the 
			* returned vector will be a vector of 3 vectors.  Each element is of variable length.  
			*
			* @param rowIdx The index to the row in the matrix.
			* @return The vector of column elements at the given row.
			*/
			inline std::vector<std::vector<cio::Value<T>>> get(std::size_t rowIdx) const;

			/**
			* Gets the element at the specified column and row index.  The element
			* is returned as a vector of element components.  
			*
			* @param columnIdx The index of the element within the array.
			* @param rowIdx The index to the array that contains the desired value.
			* @return The value at the given index and element index.
			*/
			inline std::vector<cio::Value<T>> get(std::size_t columnIdx, std::size_t rowIdx) const;

			/**
			* Sets all elements for a given row in the matrix.  Each vector contains an
			* element.  Each vector element is of variable size.
			*
			* @param rowIdx The row to set the elements for.
			* @param values The values to be set for the given row.
			*/
			inline void set(std::size_t rowIdx, std::vector<std::vector<cio::Value<T>>> values);

			/**
			* Sets the values at the given column and row index.  If the size of values is
			* greater than the current size, only the values within the current
			* size constraint will be set.
			*
			* @param columnIdx The column index of the matrix that you would like to replace values for.
			* @param rowIdx The row index of the matrix that you would like to replace values for.
			* @param values The values to be set at the given column and row index.
			*/
			inline void set(std::size_t columnIdx, std::size_t rowIdx, std::vector<cio::Value<T> > values);

		private:
			/*  The data and offsets into each column in the data.  */
			VariableArray<S, T, U>  mColumnData;

			/*  The offsets into each row of the matrix.  */
			Scalar<S, U>  mRowOffsets;

			/** The number of elements in the matrix.*/
			std::size_t mSize;

			/*  The number of rows in the matrix*/
			std::size_t mRows;
		};
}

/* Inline implementation */

namespace glitter
{
		template <typename S, typename T, typename U>
		inline VariableMatrix<S, T, U>::VariableMatrix() noexcept : 
			mSize(0),
			mRows(0)
		{
			// nothing more to do
		}

		template <typename S, typename T, typename U>
		inline VariableMatrix<S, T, U>::VariableMatrix(cio::Chunk<U> data) :
			mSize(1),
			mRows(1),
			mColumnData(VariableArray<S, T, U>(data))
		{
			// nothing more to do
		}

		template <typename S, typename T, typename U>
		inline VariableMatrix<S, T, U>::VariableMatrix(cio::Chunk<U> data, Scalar<S, U> columnOffsets, Scalar<S, U> rowOffsets) :
			mSize(columnOffsets.size() > 0 ? columnOffsets.size() - 1 : 0 ),
			mRows(rowOffsets.size() > 0 ? rowOffsets.size() - 1 : 0),
			mColumnData(VariableArray<S, T, U>(data, columnOffsets)),
			mRowOffsets(rowOffsets)
		{
			// nothing more to do
		}

		template <typename S, typename T, typename U>
		inline VariableMatrix<S, T, U>::VariableMatrix(VariableArray<S, T, U> columnData) :
			mSize(columnData.size()),
			mRows(1),
			mColumnData(columnData)
		{
			// nothing more to do
		}

		template <typename S, typename T, typename U>
		inline VariableMatrix<S, T, U>::VariableMatrix(VariableArray<S, T, U> columnData, Scalar<S, U> rowOffsets) :
			mColumnData(columnData),
			mSize(columnData.size()),
			mRowOffsets(rowOffsets),
			mRows(rowOffsets.size() > 0 ? rowOffsets.size() - 1 : 0)
		{
			// nothing more to do
		}

		template <typename S, typename T, typename U>
		inline VariableMatrix<S, T, U>::VariableMatrix(VariableMatrix<S, T, U>&& in) noexcept :
			mColumnData(in.mColumnData),
			mSize(in.mSize),
			mRowOffsets(in.mRowOffsets),
			mRows(in.mRows)
		{
			in.clear();
		}

		template <typename S, typename T, typename U>
		template <typename V, typename W, typename X>
		inline VariableMatrix<S, T, U>::VariableMatrix(const VariableMatrix<V, W, X>& in) :
			mColumnData(in.getColumnData()),
			mSize(in.size()),
			mRowOffsets(in.getRowOffsets()),
			mRows(in.getRowCount())
		{
			// nothing more to do
		}

		template <typename S, typename T, typename U>
		template <typename V, typename W, typename X>
		inline VariableMatrix<S, T, U>::VariableMatrix(VariableMatrix<V, W, X>&& in) noexcept :
			mColumnData(in.getColumnData()),
			mRowOffsets(in.getRowOffsets()),
			mRows(in.getRowCount()),
			mSize(in.size())
		{
			in.clear();
		}

		template <typename S, typename T, typename U>
		inline VariableMatrix<S, T, U>& VariableMatrix<S, T, U>::operator=(VariableMatrix<S, T, U>&& in) noexcept
		{
			if (this != &in)
			{
				mColumnData = in.mColumnData;
				mRowOffsets = in.mRowOffsets;
				mRows = in.mRows;
				mSize = in.mSize;
				in.clear();
			}
			return *this;
		}

		template <typename S, typename T, typename U>
		inline void VariableMatrix<S, T, U>::bind(cio::Chunk<U> data)
		{
			mColumnData.bind(std::move(data));
			mSize = mColumnData.size();
			mRows = 1;
		}

		template <typename S, typename T, typename U>
		inline void VariableMatrix<S, T, U>::bind(cio::Chunk<U> data, Scalar<S, U> columnOffsets)
		{
			mSize = columnOffsets.size() > 0 ? columnOffsets.size() - 1 : 1;
			mColumnData.bind(std::move(data), std::move(columnOffsets));
			mRows = 1;
		}
		template <typename S, typename T, typename U>
		inline void VariableMatrix<S, T, U>::bind(cio::Chunk<U> data, cio::Chunk<U> columnOffsets, std::size_t numColOffsets)
		{
			mColumnData.bind(std::move(data), std::move(columnOffsets));
			mSize = numColOffsets > 0 ? numColOffsets - 1 : 1;
			mRows = 1;
		}

		template <typename S, typename T, typename U>
		inline void VariableMatrix<S, T, U>::bind(cio::Chunk<U> data, cio::Chunk<U> columnOffsets, std::size_t numColOffsets, cio::Chunk<U> rowOffsets, std::size_t numRowOffsets)
		{
			mColumnData.bind(std::move(data), std::move(columnOffsets));
			mRowOffsets.bind(std::move(rowOffsets));
			mSize = numColOffsets > 0 ? numColOffsets - 1 : 1;
			mRows = numRowOffsets > 0 ? numRowOffsets - 1 : 1;
		}

		template <typename S, typename T, typename U>
		inline void VariableMatrix<S, T, U>::bind(cio::Chunk<U> data, Scalar<S, U> columnOffsets, Scalar<S, U> rowOffsets)
		{			
			mSize = columnOffsets.size() > 0 ? columnOffsets.size()-1 : 1;
			mColumnData.bind(std::move(data), std::move(columnOffsets));
			mRowOffsets = std::move(rowOffsets);
			mRows = mRowOffsets.size() > 0 ? mRowOffsets.size() - 1 : 1;
		}

		template <typename S, typename T, typename U>
		inline void VariableMatrix<S, T, U>::bind(VariableArray<S, T, U> columnData, Scalar<S, U> rowOffsets)
		{
			mColumnData = std::move(columnData);
			mRowOffsets = std::move(rowOffsets);
			mSize = mColumnData.getIndexData().size() > 0 ? mColumnData.getIndexData().size()-1: 1;
			mRows = mRowOffsets.size() > 0 ? mRowOffsets.size() - 1 : 1;
		}

		template <typename S, typename T, typename U>
		inline std::size_t VariableMatrix<S, T, U>::getRowCount() const
		{
			return mRows;
		}

		template <typename S, typename T, typename U>
		inline void VariableMatrix<S, T, U>::setRowCount(std::size_t rows)
		{
			mRows = rows;
		}


		template <typename S, typename T, typename U>
		inline std::size_t VariableMatrix<S, T, U>::getColumns(std::size_t rowIdx) const
		{
			std::size_t numColumns = 0;
			if (mRowOffsets.size() > rowIdx + 1)
			{
				std::size_t start = static_cast<unsigned int>(mRowOffsets.get(rowIdx));
				std::size_t end = static_cast<unsigned int>(mRowOffsets.get(rowIdx + 1));
				if (end > start)
				{
					numColumns = end - start;
				}
			}
			return numColumns;
		}

		template <typename S, typename T, typename U>
		inline void VariableMatrix<S, T, U>::setColumns(std::size_t rowIdx, std::size_t numColumns)
		{
			mRowOffsets.set(rowIdx, static_cast<S>(numColumns));
		}

		template <typename S, typename T, typename U>
		inline std::size_t VariableMatrix<S, T, U>::getElementSize(std::size_t columnIdx, std::size_t rowIdx) const
		{
			std::size_t numElements = 0;
			if (mRowOffsets.size() > rowIdx)
			{
				std::size_t loc = mRowOffsets.get(rowIdx) + columnIdx;
				numElements = mColumnData.getCount(loc);
			}
			return numElements;
		}

		template <typename S, typename T, typename U>
		inline std::size_t VariableMatrix<S, T, U>::size() const
		{
			return mSize;
		}

		template <typename S, typename T, typename U>
		inline void VariableMatrix<S, T, U>::resize(std::size_t length)
		{
			mSize = length;
		}

		template <typename S, typename T, typename U>
		inline void VariableMatrix<S, T, U>::clear()
		{
			mColumnData.clear();
			mRowOffsets.clear();
			mSize = 0;
			mRows = 0;
		}

		template <typename S, typename T, typename U>
		inline cio::Chunk<U>& VariableMatrix<S, T, U>::getData()
		{
			return mColumnData.getData();
		}

		template <typename S, typename T, typename U>
		inline const cio::Chunk<U>& VariableMatrix<S, T, U>::getData() const
		{
			return mColumnData.getData();
		}

		template <typename S, typename T, typename U>
		inline void VariableMatrix<S, T, U>::setData(cio::Chunk<U> data)
		{
			mColumnData.setData(std::move(data));
		}

		template <typename S, typename T, typename U>
		inline VariableArray<S, T, U>& VariableMatrix<S, T, U>::getColumnData()
		{
			return mColumnData;
		}

		template <typename S, typename T, typename U>
		inline const VariableArray<S, T, U>& VariableMatrix<S, T, U>::getColumnData() const
		{
			return mColumnData;
		}

		template <typename S, typename T, typename U>
		inline void VariableMatrix<S, T, U>::setColumnData(VariableArray<S, T, U> columnData)
		{
			mColumnData = std::move(columnData);
		}

		template <typename S, typename T, typename U>
		inline Scalar<S, U>& VariableMatrix<S, T, U>::getRowOffsets()
		{
			return mRowOffsets;
		}

		template <typename S, typename T, typename U>
		inline const Scalar<S, U>& VariableMatrix<S, T, U>::getRowOffsets() const
		{
			return mRowOffsets;
		}

		template <typename S, typename T, typename U>
		inline void VariableMatrix<S, T, U>::setRowOffsets(Scalar<S, U>  rowOffsets)
		{
			mRowOffsets = std::move(rowOffsets);
			mRows = mRowOffsets.size() > 0 ? mRowOffsets.size() - 1 : 0;
		}

		template <typename S, typename T, typename U>
		inline U* VariableMatrix<S, T, U>::data()
		{
			return mColumnData.data();
		}

		template <typename S, typename T, typename U>
		inline const U* VariableMatrix<S, T, U>::data() const
		{
			return mColumnData.data();
		}

		template <typename S, typename T, typename U>
		inline U* VariableMatrix<S, T, U>::columnOffsets()
		{
			return mColumnData.getIndexData();
		}

		template <typename S, typename T, typename U>
		inline const U* VariableMatrix<S, T, U>::columnOffsets() const
		{
			return mColumnData.getIndexData();
		}

		template <typename S, typename T, typename U>
		inline U* VariableMatrix<S, T, U>::rowOffsets()
		{
			return mRowOffsets.data();
		}

		template <typename S, typename T, typename U>
		inline const U* VariableMatrix<S, T, U>::rowOffsets() const
		{
			return mRowOffsets.data();
		}

		template <typename S, typename T, typename U>
		inline std::vector<std::vector<cio::Value<T>>> VariableMatrix<S, T, U>::get(std::size_t rowIdx) const
		{
			std::size_t firstColumn = mRowOffsets.get(rowIdx);
			std::size_t lastColumn = mRowOffsets.get(rowIdx + 1);
			std::size_t count = lastColumn - firstColumn;

			std::vector<std::vector<T>> vals(lastColumn - firstColumn);

			if (rowIdx + 1 < mRowOffsets.size())
			{
				for (std::size_t i = 0; i < count; ++i)
				{
					vals[i] = mColumnData.get(firstColumn + i);
				}
			}
			return vals;
		}

		template <typename S, typename T, typename U>
		inline std::vector<cio::Value<T>> VariableMatrix<S, T, U>::get(std::size_t columnIdx, std::size_t rowIdx) const
		{
			std::vector<cio::Value<T>> result;

			if (mRowOffsets.size() > rowIdx)
			{
				result = mColumnData.get(mRowOffsets.get(rowIdx) + columnIdx);
			}

			return result;
		}

		template <typename S, typename T, typename U>
		inline void VariableMatrix<S, T, U>::set(std::size_t columnIdx, std::size_t rowIdx, std::vector<cio::Value<T>> values)
		{
			if (mRowOffsets.size() > rowIdx)
			{
				mColumnData.set(mRowOffsets.get(rowIdx) + columnIdx, values);
			}
		}		
		
		template <typename S, typename T, typename U>
		inline void VariableMatrix<S, T, U>::set(std::size_t rowIdx, std::vector<std::vector<cio::Value<T>>> values)
		{
			std::size_t columns = this->getColumns(rowIdx);
			for (std::size_t i = 0; i < columns; ++i)
			{
				if (values.size() > i)
				{
					this->set(i, rowIdx, values.at(i));
				}

			}
		}
}


#endif
#pragma once
