/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_COMPONENTTYPE_H
#define GLITTER_COMPONENTTYPE_H

#include "Types.h"

#include <iosfwd>

namespace glitter
{
	/**
	 * An enumeration of the types of glTF and extended objects addressible by a Model.
	 */
	enum class ComponentType : std::uint8_t
	{
		/** Placeholder for no object type */
		None = 0,

		/** Top-level glTF document */
		Document,

		/** glTF scene */
		Scene,

		/** glTF camera */
		Camera,

		/** glTF buffer */
		Buffer,

		/** glTF buffer view*/
		BufferView,

		/** glTF accessor */
		Accessor,

		/** glTF image */
		Image,

		/** glTF texture */
		Texture,

		/** glTF sampler */
		Sampler,

		/** glTF material */
		Material,

		/** glTF node */
		Node,

		/** glTF mesh */
		Mesh,

		/** glTF primitive set contained on a mesh */
		PrimitiveSet,

		/** glTF primitive logically part of a primitive set */
		Primitive,

		/** glTF vertex logically part of a primitive set */
		Vertex,

		/** glTF single index component of a primitive on a primitive set*/
		Index,

		/** glTF animation */
		Animation,

		/** glTF skin */
		Skin,

		/** OWT/extended entity Node (glTF feature table) */
		Table,

		/** OWT/extended entity (glTF feature) */
		Entity,

		/** Definition of a glTF extension */
		Extension
	};

	/**
	 * Gets the text representation of a particular component type.
	 * 
	 * @param component the component type
	 * @return the text representation
	 */
	GLITTER_ABI const char *toString(ComponentType component);

	/**
	 * Prints the text representation of a particular component type to a C++ standard output stream.
	 *
	 * @param s The stream
	 * @param component the component type
	 * @return the stream after printing
	 */
	GLITTER_ABI std::ostream &operator<<(std::ostream &s, ComponentType component);
}

#endif
