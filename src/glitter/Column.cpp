/*=====================================================================================================================
 * Copyright 2022-2023, Geometric Progress LLC
 *
 * This file is part of the Geosemantic Application Schema Library (Geas).
 *
 * Geas is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Geas is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Geas.  If not, see <https://www.gnu.org/licenses/>
=====================================================================================================================*/
#include "Column.h"

#include <geas/Application.h>
#include <geas/Attribute.h>
#include <geas/Enumerant.h>
#include <geas/Enumeration.h>
#include <geas/Measures.h>
#include <geas/Unit.h>

#include <cio/Class.h>
#include <cio/Exception.h>
#include <cio/Type.h>

namespace glitter
{
	cio::Class<Column> Column::sMetaclass("glitter::Column");

	const cio::Class<Column> &Column::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}

	geas::Attribute Column::sSizeProperties;

	geas::Attribute Column::sColumnProperties;

	Column::Column() noexcept :
		mSize(0)
	{
		sSizeProperties.setValueType(cio::Type::Unsigned);

		sColumnProperties.setValueType(cio::Type::Unsigned);
		sColumnProperties.setRange(cio::VariantInterval(0u, 1u));
	}

	const cio::Metaclass &Column::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	Column::Column(const Column &in) = default;

	Column::Column(Column &&in) noexcept = default;

	Column &Column::operator=(const Column &in) = default;

	Column &Column::operator=(Column &&in) noexcept = default;

	Column::~Column() noexcept = default;

	void Column::clear() noexcept
	{
		Provider::clear();

		mSize = 0;
		mAttribute.clear();
		mValues.clear();
	}

	geas::Index Column::size() const
	{
		return static_cast<geas::Index>(mSize);
	}

	geas::Index Column::resize(geas::Index count)
	{
		mSize = count;
		mValues.resize(count);
		return count;
	}

	geas::Index Column::create(const geas::Entity *entity, const geas::Geometry *geometry)
	{
		return this->resize(mSize + 1);
	}

	const geas::Attribute *Column::getSizeProperties() const noexcept
	{
		return &sSizeProperties;
	}

	const geas::Attribute *Column::getAttributeCountProperties() const noexcept
	{
		return &sColumnProperties;
	}

	bool Column::isUniformFieldLayout() const noexcept
	{
		return true;
	}

	geas::Index Column::getAttributeCount(geas::Index object) const
	{
		return static_cast<bool>(mAttribute);
	}

	const geas::Attribute *Column::getAttribute(geas::Index object, geas::Index field) const
	{
		return &mAttribute;
	}

	void Column::setAttribute(geas::Index object, geas::Index field, const geas::Attribute *defn)
	{
		if (defn)
		{
			this->setAttribute(*defn);
		}
		else
		{
			this->clearAttribute();
		}
	}

	geas::Index Column::addAttribute(geas::Index object, const geas::Attribute *defn)
	{
		geas::Index idx = INT32_MIN;

		if (defn)
		{
			if (mAttribute)
			{
				throw cio::Exception(cio::Reason::Overflow);
			}

			idx = 0;
			this->setAttribute(*defn);
		}

		return idx;
	}

	void Column::removeAttribute(geas::Index object, geas::Index field)
	{
		mAttribute.clear();
		mValues.clear();
	}

	geas::Index Column::findAttributeByLabel(geas::Index object, const geas::Label &label) const
	{
		geas::Index fieldIdx = INT32_MIN;

		if (mAttribute.matches(label))
		{
			fieldIdx = 0;
		}

		return fieldIdx;
	}

	geas::Index Column::findAttributeByCode(geas::Index object, const geas::Code &code) const
	{
		geas::Index fieldIdx = INT32_MIN;

		if (mAttribute.matches(code))
		{
			fieldIdx = 0;
		}

		return fieldIdx;
	}

	geas::Index Column::findAttributeByIndex(geas::Index object, geas::Index idx) const
	{
		geas::Index fieldIdx = INT32_MIN;

		if (mAttribute.matches(idx))
		{
			fieldIdx = 0;
		}

		return fieldIdx;
	}

	geas::Index Column::findMatchingAttribute(geas::Index object, const geas::Attribute &attribute) const
	{
		geas::Index fieldIdx = INT32_MIN;

		if (mAttribute.matches(attribute))
		{
			fieldIdx = 0;
		}

		return fieldIdx;
	}

	Array Column::getValue(geas::Index object, geas::Index field) const
	{
		// TODO this is not yet const correct, but we need accessor support and a const value array to make that happen
		Array result;
		if (object < mSize)
		{
			const Array &base = mValues;
			if (base.isFixedByteAligned())
			{
				result = const_cast<Array &>(base).slice(object);
			}
			else
			{
				result = const_cast<Array &>(base);
			}
		}

		return result;
	}

	bool Column::setValue(geas::Index object, geas::Index field, const Array &value)
	{
		bool found = false;
		if (object < mSize)
		{
			Array &base = mValues;
			if (base.isFixedByteAligned())
			{
				geas::Index toSet = std::min(base.getElementCount(), value.getElementCount());
				for (unsigned i = 0; i < toSet; ++i)
				{
					base.setBinary(value.getBinary<std::uint64_t>(0, i), object, i);
				}
			}
			found = true;
		}

		return found;
	}

	geas::AttributeFault Column::validateCurrentValue(const geas::Attribute &constraint, geas::Index object, geas::Index field) const
	{
		geas::AttributeFault result;
		if (field > 0 || !mAttribute)
		{
			result.setOverallStatus(cio::fail(cio::Reason::Missing));
			result.setSeverity(cio::Severity::Error);
			result.setMessage("geas::Attribute not present");
		}
		else if (object < mSize)
		{
			const Array &base = mValues;
			if (base.isFixedByteAligned())
			{
				// TODO implement value array validation
				// result = constraint.validate(const_cast<Array &>(base).slice(object));
			}
			else
			{
				result = geas::AttributeFault(cio::Validation::NotSupported);
			}
		}
		else
		{
			result.setOverallStatus(cio::fail(cio::Reason::Missing));
			result.setSeverity(cio::Severity::Error);
		}

		return result;
	}


	std::size_t Column::estimateValueMemoryUse() const noexcept
	{
		return this->estimateNewValueMemoryUse(mSize);
	}

	std::size_t Column::estimateNewValueMemoryUse(std::size_t count) const noexcept
	{
		geas::Index total = 0;

		if (mAttribute)
		{
			std::size_t	added;
			if (mAttribute.getEnumeration())
			{
				added = mAttribute.getEnumeration()->getEnumerants().size() * cio::bits(mAttribute.getEnumeration()->getComponentType());
			}
			else
			{
				added = mAttribute.getRange().lower.bits();
			}
			total += added;
		}

		return count * total;
	}

	Array &Column::getArray() noexcept
	{
		return mValues;
	}

	const Array &Column::getArray() const noexcept
	{
		return mValues;
	}

	void Column::setArray(Array values) noexcept
	{
		mValues = std::move(values);
	}

	geas::Attribute &Column::getAttribute() noexcept
	{
		return mAttribute;
	}

	const geas::Attribute &Column::getAttribute() const noexcept
	{
		return mAttribute;
	}

	void Column::setAttribute(geas::Attribute attribute)
	{
		mAttribute = std::move(attribute);

		if (mAttribute)
		{
			mValues.setType(mAttribute.getValueType());
			if (mAttribute.getEnumeration())
			{
				mValues.setElementCount(mAttribute.getEnumeration()->getEnumerants().size());
			}
		}
		else
		{
			mValues.clear();
		}
	}

	void Column::clearAttribute() noexcept
	{
		mAttribute.clear();
		mValues.clear();
	}
}

