/*=====================================================================================================================
 * Copyright 2022-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_PRIMITIVE_H
#define GLITTER_PRIMITIVE_H

#include "Types.h"

namespace glitter
{
	/**
	 * The Glitter primitives enumerates the list of standard geometry modifiers for organizing
	 * geometry data values.
	 *
	 * These standard primitives are associated with term categories, and may be combined with geometry primitives
	 * to obtain standard geometry schemas.
	 *
	 * Non-standard primitives can still be used with the Primitive::Unknown type.
	 */
	enum class Primitive : std::uint8_t
	{
		/** No primitive defined. */
		None,

		/** The primitive is not one of the standard primitives on this list. */
		Unknown,

		/** The primitive is a single value of the geometry type. */
		Value,

		/** The primitive is a sequential list of values of the geometry type . */
		List,

		/** The primitive is a strip in which each primitive adds one new vertex and uses the preceding ones. */
		Strip,

		/** The primitive is a loop in which each primitive adds one new vertex and the first vertex is also the last vertex. */
		Loop,

		/** The primitive is a radial fan in which each primitive uses the first vertex and the preceding vertices. */
		Fan,

		/** The primitive is organized into a spatial partition tree such as a quadtree or octree. */
		Tree
	};

	/**
	 * Gets the default geometry schema used for the given geometry and primitive type.
	 * Not all combinations are sensible or defined.
	 *
	 * @param geometry The geometry type
	 * @param primitive The primmitive type
	 * @return the geometry schema or nullptr if none was found
	 */
	const geas::Geometry *getDefaultGeometry(GeometryType geometry, Primitive primitive) noexcept;
}

#endif
