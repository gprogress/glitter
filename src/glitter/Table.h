/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_TABLE_H
#define GLITTER_TABLE_H

#include "Types.h"

#include "Provider.h"

#include <geas/Entity.h>

#include <map>
#include <vector>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace glitter
{
	/**
	 * The Table implements the Provider interface to get and set attribute values
	 * using an array of attribute value arrays. Each attribute may have its own unique data buffer, or they
	 * may share the same data buffer either via interleaving or offsets.
	 *
	 * The Table may use its own custom physical data model for the attribute sequence, or it may reference a shared
	 * logical data model from another source.
	 *
	 * The main distinction between the Table and other approaches is that attribute values are grouped by type, not
	 * by entity or other parent object. So for example, one array has all the "Width" values, another array has all the "Material Type" values, and so on.
	 *
	 * The value arrays may be scalar arrays or vector arrays for multi-element attributes like position or color.
	 * For multi-valued and range-valued attributes, each item (list entry, lower bound, upper bound, and closure) currently must be represented
	 * as separate attributes.
	 *
	 * Logically, this class can be viewed as a database table where the rows are attributes and the columns are entities or other parent objects.
	 */
	class GLITTER_ABI Table : public Provider
	{
		public:
			/**
			 * Construct the empty Table.
			 */
			Table() noexcept;

			/**
			 * Construct a deep copy of a Table.
			 *
			 * @param in The table to copy
			 */
			Table(const Table &in);

			/**
			 * Construct a Table by moving in the contents of an existing one.
			 *
			 * @param in The table to move
			 */
			Table(Table &&in) noexcept;

			/**
			 * Assign a deep copy of a Table.
			 * The existing table contents are lost.
			 *
			 * @param in The table to copy
			 * @return this table
			 */
			Table &operator=(const Table &in);

			/**
			 * Transfer the content of a Table into this one.
			 * The existing table contents are lost.
			 *
			 * @param in The table to move
			 * @return this table
			 */
			Table &operator=(Table &&in) noexcept;

			/**
			 * Destructor.
			 */
			virtual ~Table() noexcept override;

			/**
			 * Clears all table content.
			 */
			virtual void clear() noexcept override;

			/**
			 * Gets the runtime metaclass for this object.
			 * This class overrides to return the metaclass for Table.
			 *
			 * @return the metaclass for this object
			 */
			virtual const cio::Metaclass &getMetaclass() const noexcept override;

			/**
			 * Gets the number of logical objects (entities or geometry primitives) that have values in this value provider.
			 * This is NOT the number of attributes.
			 *
			 * @return the number of logical objects
			 */
			virtual geas::Index size() const override;

			/**
			 * Sets the number of logical objects (entities or geometry primitives) that have values in this value provider.
			 * This implementation allows any size value to be set, and will resize every underlying attribute array accordingly.
			 *
			 * @param count The desired number of logical objects
			 * @return the number of logical objects actually set
			 */
			virtual geas::Index resize(geas::Index count) override;

			/**
			 * Add new record for an object.
			 *
			 * @todo See LTF-679: this method should be adding missing attributes and setting default values.
			 *
			 * @param entity Used to select the default values for attributes
			 * @param geometry Used to set up geometric representation if applicable
			 * @return geas::Index of new record that would become the entity index
			 */
			virtual geas::Index create(const geas::Entity *entity = nullptr, const geas::Geometry *geometry = nullptr) override;

			/**
			 * Gets a datatype describing the properties of this provider in terms of size() and resize() for object counts.
			 * This identifies the size range as 0 to INT32_MAX.
			 *
			 * @return the properties describing how attributes work with this provider
			 */
			virtual const geas::Attribute *getSizeProperties() const noexcept override;

			/**
			 * Gets a datatype describing the properties of this provider in terms of handling attribute counts.
			 * This identifes the count range as 0 to INT32_MAX, but not variable length (all objects must have same count).
			 *
			 * @return the properties describing how attributes work with this provider
			 */
			virtual const geas::Attribute *getAttributeCountProperties() const noexcept override;

			/**
			 * Reports whether this data provider has a uniform attribute field layout.
			 * If true, this means every object has the same attributes in the same sequence.
			 * If false, each object may have a different sequence of attribute fields.
			 *
			 * The Table implementation always returns true as this is an inherent property of this class.
			 *
			 * @return always true since Table always has a uniform field layout
			 */
			virtual bool isUniformFieldLayout() const noexcept override;

			/**
			 * Gets the number of attribute definitions in this table.
			 *
			 * It does not matter which object you specify: all objects share the same attribute field layout.
			 *
			 * @param object The object index
			 * @return the number of attribute definition
			 */
			virtual geas::Index getAttributeCount(geas::Index object) const override;

			/**
			 * Gets the definition of a specific attribute.
			 *
			 * It does not matter which object you specify: all objects share the same attribute field layout.
			 *
			 * @param object The object index
			 * @param field The field index
			 * @return the attribute definition
			 */
			virtual const geas::Attribute *getAttribute(geas::Index object, geas::Index field) const override;

			/**
			 * Changes the definition of a specific attribute.
			 * This does NOT modify the array value content for the attribute, so the content may no longer conform to the definition.
			 *
			 * It does not matter which object you specify: all objects share the same attribute field layout.
			 *
			 * @param object The object index
			 * @param field The attribute field index
			 * @param defn The new definition
			 */
			virtual void setAttribute(geas::Index object, geas::Index field, const geas::Attribute *defn) override;

			/**
			 * Adds a new attribute definition.
			 * This creates a new empty array for that attribute but does not actually set any content.
			 *
			 * It does not matter which object you specify: all objects share the same attribute field layout.
			 *
			 * @param object The object index
			 * @param defn The definition to add
			 * @return the attribute field index of the added definition
			 */
			virtual geas::Index addAttribute(geas::Index object, const geas::Attribute *defn) override;

			/**
			 * Removes a particular attribute definition.
			 * This also removes the value array for that definition.
			 * All attributes with a field number higher than the removed one have their index decremented by one.
			 *
			 * It does not matter which object you specify: all objects share the same attribute field layout.
			 *
			 * @param object The object index
			 * @param field The field index of the attribute to remove
			 */
			virtual void removeAttribute(geas::Index object, geas::Index field) override;

			/**
			 * Finds the field number of an attribute with the specified label.
			 *
			 * It does not matter which object you specify: all objects share the same attribute field layout.
			 *
			 * @param object The object index
			 * @param label The attribute label
			 * @return the attribute field number, or INT32_MIN if none was found
			 */
			virtual geas::Index findAttributeByLabel(geas::Index object, const geas::Label &label) const override;

			/**
			 * Finds the index of an attribute with the specified code.
			 *
			 * It does not matter which object you specify: all objects share the same attribute field layout.
			 *
			 * @param object The object index
			 * @param code The attribute code
			 * @return the attribute index, or INT32_MIN if none was found
			 */
			virtual geas::Index findAttributeByCode(geas::Index object, const geas::Code &code) const override;

			/**
			 * Finds the field number of an attribute with the specified index.
			 *
			 * It does not matter which object you specify: all objects share the same attribute field layout.
			 *
			 * @param object The object index
			 * @param idx The attribute index
			 * @return the attribute field number, or INT32_MIN if none was found
			 */
			virtual geas::Index findAttributeByIndex(geas::Index object, geas::Index idx) const override;

			/**
			 * Finds the field index of the first attribute that matches the given attribute.
			 * Any identifiers that are set on the attribute are used for the search.
			 *
			 * It does not matter which object you specify: all objects share the same attribute field layout.
			 *
			 * @param object The object index
			 * @param attribute The attribute to match with as many fields set as known
			 * @return the attribute field index, or INT32_MIN if not found
			 */
			virtual geas::Index findMatchingAttribute(geas::Index object, const geas::Attribute &attribute) const override;

			/**
			 * Gets a view of the value(s) of the given object's attribute.
			 * This will return a view of the underlying attribute array for the given attribute field number,
			 * with the offset set to identify the value of the given object.
			 *
			 * If the attribute value has multiple components (such as position), the value array will be configured to provide
			 * a view such that the first value is the first component, the second value is the second component, and so on.
			 *
			 * @param object The index of the object in the table
			 * @param attr the attribute field number
			 * @return the value array view of the values of the given attribute for the given object
			 */
			virtual Array getValue(geas::Index object, geas::Index attr) const override;

			/**
			 * Sets the value(s) of a particular attribute on a particular object from the given value array.
			 * If the actual attribute has fewer values than the given array, the excess input values are ignored.
			 * If the actual attribute has more values than the given array, the remaining values are left unchanged.
			 *
			 * @param object The index of the object in the table
			 * @param attr the attribute field number
			 * @param value The array specifying the new value(s) of the attribute
			 * @return whether the update was successful
			 */
			virtual bool setValue(geas::Index object, geas::Index attr, const Array &value) override;

			/**
			 * Validates the current value for the given object and attribute field to see if it matches the given datatype constraint.
			 *
			 * The Table implementation overrides the base implementation to directly inspect the value in situ for performance.
			 *
			 * @param constraint The datatype specifying the constraint
			 * @param object The object index
			 * @param attr index the attribute field index
			 * @return a fault describing validation failures, if any
			 */
			virtual geas::AttributeFault validateCurrentValue(const geas::Attribute &constraint, geas::Index object, geas::Index attr) const override;

			/**
			 * Estimates the total memory use of this provider for data values, excluding overhead and schema information.
			 *
			 * @return the estimated current memory use
			 */
			virtual std::size_t estimateValueMemoryUse() const noexcept override;

			/**
			* Calculate memory to be allocated.
			*
			* @param count The number of entities currently active
			* @return the memory size to be allocated in bytes
			*/
			virtual std::size_t estimateNewValueMemoryUse(std::size_t count) const noexcept override;

			/**
			 * Gets the application schema implementing the physical data model of this table.
			 * The physical data model is used for modeling the table-specific column information and may differ
			 * from the logical data model used at the Node and Collection level.
			 *
			 * @return the application schema or null if none is in use
			 */
			std::shared_ptr<geas::Application> getDataModel() noexcept;

			/**
			 * Gets the application schema implementing the physical data model of this table.
			 * The physical data model is used for modeling the table-specific column information and may differ
			 * from the logical data model used at the Node and Collection level.
			 *
			 * @return the application schema or null if none is in use
			 */
			std::shared_ptr<const geas::Application> getDataModel() const noexcept;

			/**
			 * Gets the application schema implementing the physical data model of this table, creating one if not present.
			 * The physical data model is used for modeling the table-specific column information and may differ
			 * from the logical data model used at the Node and Collection level.
			 *
			 * @return the application schema
			 * @throw std::bad_alloc If memory for a new schema could not be allocated
			 */
			std::shared_ptr<geas::Application> getOrCreateDataModel();

			/**
			 * Sets the current application schema implementing the physical data model of this table.
			 * This can be the same as the logical data model defined by a node or collection application schema,
			 * or a custom data model reflecting the actual content of a data provider.
			 *
			 * @param schema The application schema to set
			 */
			void setDataModel(std::shared_ptr<geas::Application> schema) noexcept;

			/**
			 * Removes the current application schema in use.
			 */
			void clearDataModel() noexcept;

			/**
			 * Adds a custom datatype to the data model for this table.
			 *
			 * @param datatype The configured datatype to add
			 * @return a pointer to the data type in the data model
			 */
			geas::Attribute *addCustomDatatype(geas::Attribute datatype);

			/**
			 * Adds a custom datatype to the data model for this table.
			 * The label and code are used for the datatype if set and it is initialized to use the given primitive type and bit pattern.
			 * You may additionally configure the returned datatype to set more specific constraints.
			 *
			 * @param label The label
			 * @param code The code
			 * @param type The primitive type
			 * @return a pointer to the data type in the data model
			 */
			geas::Attribute *addCustomDatatype(geas::Label label, geas::Code code, cio::Type type);

			/**
			 * Adds a custom datatype to the data model for this table with optionally a custom unit for applying scale and offset.
			 * The label and code are used for the datatype if set and it is initialized to use the given primitive type and bit pattern.
			 * You may additionally configure the returned datatype to set more specific constraints.
			 *
			 * @param label The label
			 * @param code The code
			 * @param type The primitive type
			 * @param quantity The custom quantity measure
			 * @return a pointer to the data type in the data model
			 */
			geas::Attribute *addCustomDatatype(geas::Label label, geas::Code code, cio::Type type, const geas::Quantity *quantity);

			/**
			 * Adds a custom attribute to this table that implements the given data type.
			 * The attribute will be appended to the end of the attribute list.
			 *
			 * The attribute label will be the same as the datatype if set, and will use the given datatype.
			 *
			 * @param label The label for the new field
			 * @param code The code for the new field
			 * @param datatype The datatype to add
			 * @return the custom attribute
			 */
			geas::Attribute *addCustomField(geas::Label label, geas::Code code, const geas::Attribute *datatype);

			/**
			 * Adds a custom unnamed unit to the table, typically used to implement a scale and offset.
			 *
			 * @param quantity The unit quantity type this custom unit relies on
			 * @return the custom unit definition
			 */
			geas::Unit *addCustomUnit(const geas::Quantity *quantity = nullptr);

			/**
			 * Gets direct access to the list of attribute arrays implementing the values for each attribute.
			 *
			 * @return the list of attribute arrays
			 */
			std::vector<std::shared_ptr<Provider>> &getColumns() noexcept;

			/**
			 * Gets direct access to the list of attribute arrays implementing the values for each attribute.
			 *
			 * @return the list of attribute arrays
			 */
			const std::vector<std::shared_ptr<Provider>> &getColumns() const noexcept;

			/**
			 * Directly replaces the list of attribute array values.
			 *
			 * @param values The new attribute array values
			 */
			void setColumns(std::vector<std::shared_ptr<Provider>> values) noexcept;

			/**
			 * Gets direct access to the value array for the given attribute.
			 *
			 * @param field The attribute field index
			 * @return the value array for that attribute
			 */
			std::shared_ptr<Provider> getColumn(geas::Index field);

			/**
			 * Gets direct access to the value array for the given attribute.
			 *
			 * @param field The attribute field number
			 * @return the value array for that attribute
			 */
			std::shared_ptr<const Provider> getColumn(geas::Index field) const;

			/**
			 * Directly replaces the value array for the given attribute.
			 *
			 * @param field The attribute field index
			 * @param values the value array for that attribute
			 */
			void setColumn(geas::Index field, std::shared_ptr<Provider> values);

			/**
			 * Checks whether there is a value array for the given attribute field index.
			 *
			 * @param field The attribute field index
			 * @return whether there is a value array for that attribute
			 */
			bool hasColumn(geas::Index field) const noexcept;

			/**
			 * Checks whether there might be values for the given entity or object.
			 *
			 * @param object The object index
			 * @return whether there might be values for that object
			 */
			bool hasObject(geas::Index object) const noexcept;

			/**
			 * Checks whether there is a value for the given attribute on the given object.
			 *
			 * @param object The object index
			 * @param field The attribute field index
			 * @return whether there is a value for that object's attribute
			 */
			bool hasObjectColumn(geas::Index object, geas::Index field) const noexcept;

		private:
			/** The metaclass for this class. */
			static cio::Class<Table> sMetaclass;

			/* Size properties for this class. */
			static geas::Attribute sSizeProperties;

			/** Attribute count properties for this class. */
			static geas::Attribute sColumnProperties;

			/** The number of logical entities or objects. */
			std::size_t mSize;

			/** The physical data model for this schema, if any. */
			std::shared_ptr<geas::Application> mDataModel;

			/** Placeholder entity for table fields. */
			geas::Entity mTableEntity;

			/** The list of attribute value arrays. */
			std::vector<std::shared_ptr<Provider>> mColumns;
	};
}

#ifndef _MSC_VER
#pragma warning (pop)
#endif

#endif
