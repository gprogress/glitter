/*=====================================================================================================================
 * Copyright 2022-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_DATA_H
#define GLITTER_DATA_H

#include "Types.h"

#include <type_traits>

namespace glitter
{
	/**
	 * The Data union is a data container for storing N bytes of aligned primitive data.
	 * 
	 * In the general case, the binary layout is created by rounding up N to the next multiple of 64 so that
	 * aligned storage can be used for 64-bit integers and double precision floats, as well as pointers on 64-bit systems.
	 * This will effectively create a fixed-length array of 8 * (N + 7 / 8) bytes or (N + 7 / 8) 64-bit values.
	 * 
	 * Specializations exist for the empty case (0), and narrower cases of 1, 2, and 4. Due to alignment, 3 is rounded up to 4.
	 * The narrower alignments do not normally support external pointers and in general the 8-byte instantiation should be preferred.
	 * 
	 * Data members are defined to provide easy access to all built-in C++ primitive types.
	 * It is generally expected this union will be wrapped in a higher level API that describes the type of data and any deallocator needed
	 * if it is stored as a pointer. Pointers are only stored for 8 byte aligned instantiations even for 32-bit systems to ensure compatibility.
	 * 
	 * @tparam <N> The requested number of bytes
	 */
	template <std::size_t N>
	union Data
	{
		/** The instantiated size of the union */
		using Bytes = std::integral_constant<unsigned, N>;
	
		/** The actual number of bytes used */
		using L = std::integral_constant<unsigned, 8 * ((N + 7) / 8)>;

		// Alignment - bits
		
		/** Array of packed bits, 8 per byte */
		std::uint8_t bits[L::value];

		// Alignment - bytes

		/** Array of C++ boolean, 1 per byte */
		bool boolean[L::value];
		
		/** Array of 8-bit unsigned integers, 1 per byte */ 
		std::uint8_t uint8[L::value];
		
		/** Array of 8-bit signed integers, 1 per byte */ 
		std::int8_t sint8[L::value];
		
		/** Array of 8-bit UTF-8 or ASCII characters, 1 per byte */ 
		char text[L::value];
		
		/** Array of 16-bit unsigned integers, 1 for each 2 bytes */
		std::uint16_t uint16[L::value / 2];
		
		/** Array of 16-bit signed integers, 1 for each 2 bytes */
		std::int16_t sint16[L::value / 2];
		
		/** Array of 16-bit UTF-16 characters, 1 for each 2 bytes */
		char16_t text16[L::value / 2];

		// Alignment - 4 bytes

		/** Array of 32-bit unsigned integers, 1 for each 4 bytes */
		std::uint32_t uint32[L::value / 4];
		
		/** Array of 32-bit signed integers, 1 for each 4 bytes */
		std::int32_t sint32[L::value / 4];
		
		/** Array of 32-bit single-precision floating point, 1 for each 4 bytes */
		float float32[L::value / 4];
		
		/** Array of 32-bit UCS-4 text, 1 for each 4 bytes */
		char32_t text32[L::value / 4];

		// Alignment - 8 bytes

		/** Array of 64-bit unsigned integers, 1 for each 8 bytes */
		std::uint64_t uint64[L::value / 8];
		
		/** Array of 64-bit signed integers, 1 for each 8 bytes */
		std::int64_t sint64[L::value / 8];
		
		/** Array of 64-bit double-precision floating point, 1 for each 4 bytes */
		double float64[L::value / 8];

		// Alignment - platform dependent

		/** Array of pointers to external Data<N> data */
		Data<N> *data[L::value / sizeof(void *)];
		
		/** Array of pointers to read-only external Data<N> data */
		const Data<N> *cdata[L::value / sizeof(void *)];
		
		/** Array of pointers to external Variant data - structured values */
		cio::Variant *variant[L::value / sizeof(void *)];
		
		/** Array of pointers to read-only external Variant data - structured values */
		const cio::Variant *cvariant[L::value / sizeof(void *)];
		
		/** Array of pointers to unaligned arbitrary data */
		void *ptr[L::value / sizeof(void *)];
		
		/** Array of pointers to read-only unaligned arbitrary data */
		const void *cptr[L::value / sizeof(void *)];
	};

	/**
	 * The specialization for Data<0> is an empty container with a fixed length of 0.
	 * Due to requirements of C++ it still ends up taking up 1 byte of memory unless
	 * used as an empty base class.
	 */
	template <>
	union Data<0>
	{
		/** The instantiated size of the union */
		using S = std::integral_constant<unsigned, 0>;
	
		/** The number of bytes of data available */
		using L = std::integral_constant<unsigned, 1>;
	};

	/**
	 * The specialization for Data<1> only includes 1-byte types with a 1-byte alignment.
	 */
	template <>
	union Data<1>
	{
		/** The instantiated size of the union */
		using S = std::integral_constant<unsigned, 1>;
	
		/** The number of bytes of data available */
		using L = std::integral_constant<unsigned, 1>;
		
		// Alignment - bits
		
		/** Array of packed bits, 8 per byte */
		std::uint8_t bits[1];
		
		// Alignment - bytes

		/** Array of C++ boolean, 1 per byte */
		bool boolean[1];
		
		/** Array of 8-bit unsigned integers, 1 per byte */ 
		std::uint8_t uint8[1];
		
		/** Array of 8-bit signed integers, 1 per byte */ 
		std::int8_t sint8[1];
		
		/** Array of 8-bit UTF-8 or ASCII characters, 1 per byte */ 
		char text[1];
	};
	
	/**
	 * The specialization for Data<1> only includes 1-byte and 2-byte types with a 2-byte alignment.
	 */
	template <>
	union Data<2>
	{
		/** The instantiated size of the union */
		using S = std::integral_constant<unsigned, 2>;
	
		/** The number of bytes of data available */
		using L = std::integral_constant<unsigned, 2>;
		
		// Alignment - bits
		
		/** Array of packed bits, 8 per byte */
		std::uint8_t bits[2];
		
		// Alignment - bytes

		/** Array of C++ boolean, 1 per byte */
		bool boolean[2];
		
		/** Array of 8-bit unsigned integers, 1 per byte */ 
		std::uint8_t uint8[2];
		
		/** Array of 8-bit signed integers, 1 per byte */ 
		std::int8_t sint8[2];
		
		/** Array of 8-bit UTF-8 or ASCII characters, 1 per byte */ 
		char text[2];
		
		// Alignment - 2 bytes

		/** Array of 16-bit unsigned integers, 1 for each 2 bytes */
		std::uint16_t uint16[1];
		
		/** Array of 16-bit signed integers, 1 for each 2 bytes */
		std::int16_t sint16[1];
		
		/** Array of 16-bit UTF-16 characters, 1 for each 2 bytes */
		char16_t text16[1];
	};
	
	/**
	 * The specialization for Data<3> uses 1, 2, and 4-byte types, with 4-byte alignment.
	 */
	template <>
	union Data<3>
	{
		/** The instantiated size of the union */
		using S = std::integral_constant<unsigned, 3>;
	
		/** The number of bytes of data available */
		using L = std::integral_constant<unsigned, 4>;
		
		// Alignment - bits
		
		/** Array of packed bits, 8 per byte */
		std::uint8_t bits[4];
		
		// Alignment - bytes
		
		/** Array of C++ boolean, 1 per byte */
		bool boolean[4];
		
		/** Array of 8-bit unsigned integers, 1 per byte */ 
		std::uint8_t uint8[4];
		
		/** Array of 8-bit signed integers, 1 per byte */ 
		std::int8_t sint8[4];
		
		/** Array of 8-bit UTF-8 or ASCII characters, 1 per byte */ 
		char text[4];

		// Alignment - 2 bytes

		/** Array of 16-bit unsigned integers, 1 for each 2 bytes */
		std::uint16_t uint16[2];
		
		/** Array of 16-bit signed integers, 1 for each 2 bytes */
		std::int16_t sint16[2];
		
		/** Array of 16-bit UTF-16 characters, 1 for each 2 bytes */
		char16_t text16[2];
		
		// Alignment - 4 bytes

		/** Array of 32-bit unsigned integers, 1 for each 4 bytes */
		std::uint32_t uint32[1];

		/** Array of 32-bit signed integers, 1 for each 4 bytes */		
		std::int32_t sint32[1];
		
		/** Array of 32-bit single-precision floating point, 1 for each 4 bytes */
		float float32[1];
		
		/** Array of 32-bit UCS-4 text, 1 for each 4 bytes */
		char32_t text32[1];
	};
	
	/**
	 * The specialization for Data<4> uses 1, 2, and 4-byte types, with 4-byte alignment.
	 */
	template <>
	union Data<4>
	{
		/** The instantiated size of the union */
		using S = std::integral_constant<unsigned, 4>;
	
		/** The number of bytes of data available */
		using L = std::integral_constant<unsigned, 4>;
		
		// Alignment - bits
		
		/** Array of packed bits, 8 per byte */
		std::uint8_t bits[4];
		
		// Alignment - bytes

		/** Array of C++ boolean, 1 per byte */
		bool boolean[4];
		
		/** Array of 8-bit unsigned integers, 1 per byte */ 
		std::uint8_t uint8[4];
		
		/** Array of 8-bit signed integers, 1 per byte */ 
		std::int8_t sint8[4];
		
		/** Array of 8-bit UTF-8 or ASCII characters, 1 per byte */ 
		char text[4];

		// Alignment - 2 bytes
		
		/** Array of 16-bit unsigned integers, 1 for each 2 bytes */
		std::uint16_t uint16[2];
		
		/** Array of 16-bit signed integers, 1 for each 2 bytes */
		std::int16_t sint16[2];
		
		/** Array of 16-bit UTF-16 characters, 1 for each 2 bytes */
		char16_t text16[2];		
		
		// Alignment - 4 bytes

		/** Array of 32-bit unsigned integers, 1 for each 4 bytes */
		std::uint32_t uint32[1];
		
		/** Array of 32-bit signed integers, 1 for each 4 bytes */
		std::int32_t sint32[1];
		
		/** Array of 32-bit single-precision floating point, 1 for each 4 bytes */
		float float32[1];
		
		/** Array of 32-bit UCS-4 text, 1 for each 4 bytes */
		char32_t text32[1];
	};
}

#endif
