/*=====================================================================================================================
 * Copyright 2022-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "KeyValueList.h"

#include <geas/Attribute.h>

#include <cio/Variant.h>

#include <limits>

namespace glitter
{
	const cio::Variant KeyValueList::sInvalidValue;

	KeyValueList::KeyValueList() noexcept
	{
		// do nothing
	}

	KeyValueList::KeyValueList(const KeyValueList &in) :
		Provider(in),
		mKeys(in.mKeys),
		mAttributes(in.mAttributes)
	{
		// do nothing 
	}

	KeyValueList::KeyValueList(KeyValueList &&in) noexcept :
		Provider(std::move(in)),
		mKeys(std::move(in.mKeys)),
		mAttributes(std::move(in.mAttributes))
	{
		// do nothing
	}

	KeyValueList::~KeyValueList() noexcept = default;

	KeyValueList &KeyValueList::operator=(const KeyValueList &rhs)
	{
		if (this != &rhs)
		{
			Provider::operator=(rhs);
			mKeys = rhs.mKeys;
			mAttributes = rhs.mAttributes;
		}
		return *this;
	}

	KeyValueList &KeyValueList::operator=(KeyValueList &&rhs) noexcept
	{
		if (this != &rhs)
		{
			Provider::operator=(std::move(rhs));
			mKeys = std::move(rhs.mKeys);
			mAttributes = std::move(rhs.mAttributes);
		}

		return *this;
	}

	cio::Variant &KeyValueList::defineAttribute(const geas::Attribute &attr)
	{
		std::size_t pos = getAttributePosition(attr.getIndex());

		if (pos >= mAttributes.size())
		{
			pos = getAttributeCount();
			mKeys.emplace_back(attr.getIndex());
			mAttributes.emplace_back(attr.getValueType());
		}

		return mAttributes[pos];
	}

	cio::Variant &KeyValueList::defineAttribute(geas::Index id)
	{
		std::size_t pos = getAttributePosition(id);

		if (pos >= mAttributes.size())
		{
			pos = getAttributeCount();
			mKeys.emplace_back(id);
			mAttributes.emplace_back();
		}

		return mAttributes[pos];
	}

	cio::Variant &KeyValueList::defineAttribute(geas::Index k, cio::Variant toCopy)
	{
		std::size_t pos = getAttributePosition(k);

		if (pos >= mAttributes.size())
		{
			mKeys.emplace_back(geas::Index(0)); // unknown
			mAttributes.emplace_back(std::move(toCopy));
			pos = mAttributes.size() - 1;
		}
		else
		{
			mAttributes[pos] = std::move(toCopy);
		}

		return mAttributes[pos];
	}

	bool KeyValueList::removeAttributeById(geas::Index id)
	{
		return removeAttributeByPosition(getAttributePosition(id));
	}

	bool KeyValueList::removeAttributeByPosition(std::size_t pos)
	{
		bool success = false;

		if (pos < mAttributes.size())
		{
			mKeys.erase(mKeys.begin() + pos);
			mAttributes.erase(mAttributes.begin() + pos);
			success = true;
		}

		return success;
	}

	std::size_t KeyValueList::getAttributeCount() const
	{
		return mAttributes.size();
	}

	const cio::Variant &KeyValueList::getAttributeByPosition(std::size_t idx) const
	{
		return (idx < getAttributeCount()) ? mAttributes[idx] : sInvalidValue;
	}

	const cio::Variant &KeyValueList::getAttributeById(geas::Index id) const
	{
		return getAttributeByPosition(getAttributePosition(id));
	}

	std::size_t KeyValueList::getAttributePosition(geas::Index id) const
	{
		std::size_t pos = SIZE_MAX;

		for (std::size_t i = 0; i < mKeys.size(); ++i)
		{
			if (mKeys[i] == id)
			{
				pos = i;
				break;
			}
		}

		return pos;
	}

	KeyValueList::iterator KeyValueList::begin()
	{
		return mAttributes.begin();
	}

	KeyValueList::iterator KeyValueList::end()
	{
		return mAttributes.end();
	}

	KeyValueList::const_iterator KeyValueList::begin() const
	{
		return mAttributes.begin();
	}

	KeyValueList::const_iterator KeyValueList::end() const
	{
		return mAttributes.end();
	}

	KeyValueList::iterator KeyValueList::erase(KeyValueList::iterator ii)
	{
		return mAttributes.erase(ii);
	}

	std::pair<KeyValueList::iterator, bool> KeyValueList::insert(geas::Index k, cio::Variant v)
	{
		std::pair<KeyValueList::iterator, bool> result;

		std::size_t pos = getAttributePosition(k);

		if (pos < mAttributes.size())
		{
			result.first = mAttributes.begin() + pos;
			result.second = false;
			(*result.first) = std::move(v);
		}
		else
		{
			mKeys.emplace_back(k); // unknown
			mAttributes.emplace_back(std::move(v));
			result.first = mAttributes.begin() + (mAttributes.size() - 1);
			result.second = true;
		}

		return result;
	}

	void KeyValueList::clear() noexcept
	{
		Provider::clear();
		mKeys.clear();
		mAttributes.clear();
	}

	geas::Index KeyValueList::size() const
	{
		return static_cast<geas::Index>(mAttributes.size());
	}

	bool KeyValueList::empty() const
	{
		return mAttributes.empty();
	}

	void KeyValueList::clearAllValues()
	{
		mKeys.clear();
		mAttributes.clear();
	}

	std::size_t KeyValueList::getMemoryUse() const
	{
		return (sizeof(KeyValueList) + ((sizeof(geas::Index) * mKeys.capacity()) + sizeof(cio::Variant) * mAttributes.capacity()));
	}

	bool KeyValueList::operator==(const KeyValueList &other) const
	{
		return mKeys == other.mKeys && mAttributes == other.mAttributes;
	}

	bool KeyValueList::operator!=(const KeyValueList &other) const
	{
		return !(*this == other);
	}
}
