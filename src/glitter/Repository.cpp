/*=====================================================================================================================
 * Copyright 2022-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "Repository.h"

#include "Collection.h"

#include <geas/Dictionary.cpp>

#include <cio/Class.h>

namespace glitter
{
	cio::Class<Repository> Repository::sMetaclass("Repository");

	Repository::Repository() noexcept
	{
		// nothing more to do
	}

	Repository::Repository(const Repository &in) = default;

	Repository::Repository(Repository &&in) noexcept = default;

	Repository &Repository::operator=(const Repository &in) = default;

	Repository &Repository::operator=(Repository &&in) noexcept = default;

	Repository::~Repository() noexcept = default;

	const cio::Metaclass &Repository::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	void Repository::clear() noexcept
	{
		geas::Definition::clear();

		mCollections.clear();
	}

	geas::Dictionary<Collection> &Repository::getCollections() noexcept
	{
		return mCollections;
	}

	const geas::Dictionary<Collection> &Repository::getCollections() const noexcept
	{
		return mCollections;
	}

	void Repository::addCollection(Collection resource)
	{
		mCollections.insert(std::move(resource));
	}

	void Repository::setCollections(geas::Dictionary<Collection> collections) noexcept
	{
		mCollections = std::move(collections);
	}

	void Repository::clearCollections() noexcept
	{
		mCollections.clear();
	}
}

/* Explicit instantiations */

#if defined _MSC_VER
#pragma warning(disable: 4251)
#endif

namespace geas
{
	template class GLITTER_TEMPLATE_ABI geas::Dictionary<glitter::Repository>;
}
