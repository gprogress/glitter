/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_ARRAY_H
#define GLITTER_ARRAY_H

#include "Types.h"

#include <cio/Primitive.h>
#include <cio/Encoding.h>
#include <cio/Type.h>

#include "Data.h"
#include "FixedArray.h"
#include "VariableArray.h"
#include "VariableMatrix.h"

#include <numeric>
#include <vector>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace glitter
{
	/**
	 * The Value Array class implements a view of a data buffer interpreted
	 * to define a logical number of data values where each data value has 1 or more primitive element.
	 */
	class GLITTER_ABI Array
	{
		public:
			/**
			 * Construct the empty value array.
			 */
			Array();

			/**
			 * Construct a deep copy of a value array.
			 *
			 * @param in The value array to copy
			 */
			Array(const Array &in);

			/**
			 * Construct a value array by transferring in the contents of an existing one.
			 *
			 * @param in The value array to move
			 */
			Array(Array &&in) noexcept;

			/**
			 * Assign a deep copy of a value array.
			 * The existing content is discarded.
			 *
			 * @param in The value array to copy
			 * @return this value array
			 */
			Array &operator=(const Array &in);

			/**
			 * Transfer the content of a value array into this one.
			 * The existing content is discarded.
			 *
			 * @param in The value array to move
			 * @return this value array
			 */
			Array &operator=(Array &&in) noexcept;

			/**
			 * Destructor.
			 */
			virtual ~Array() noexcept;

			/**
			 * Clears and discards all content of this value array.
			 */
			virtual void clear() noexcept;

			/**
			 * Gets the underlying data buffer for this value array.
			 *
			 * @return the data buffer
			 */
			inline const cio::Chunk<void> &getBuffer() const;

			/**
			 * Sets the underlying data buffer for this value array.
			 *
			 * @param data The data buffer to set
			 * @param numCols The number of columns per data element, defaults to 1 for scalar data
			 */
			void setFixedBuffer(cio::Chunk<void> data, std::size_t numCols = 1);

			/**
			 * Sets the underlying data buffer for this value array.
			 *
			 * @param data The data buffer to set
			 * @param columnOffsets The data that contains the column offsets
			 * @param numColOffsets The number of column offsets
			 */
			void setVariableBuffer(cio::Chunk<void> data, cio::Chunk<void> columnOffsets, std::size_t numColOffsets);

			/**
			 * Sets the underlying data buffer for this value array.
			 *
			 * @param data The data buffer to set
			 * @param columnOffsets The data that contains the column offsets
			 * @param numColOffsets The number of column offsets
			 * @param rowOffsets The data that contains the column offsets
			 * @param numRowOffsets The number of column offsets
			 */
			void setVariableBuffer(cio::Chunk<void> data, cio::Chunk<void> columnOffsets, std::size_t numColOffsets, cio::Chunk<void> rowOffsets, std::size_t numRowOffsets);

			/**
			 * Gets the bit pattern specification for individual values in this data array.
			 *
			 * @return the numeric bit pattern layout
			 */
			cio::Encoding getEncoding() const;

			/**
			 * Sets the bit pattern specification for individual values in this data array.
			 *
			 * @param layout The numeric bit pattern layout
			 */
			void setEncoding(cio::Encoding layout);

			/**
			 * Gets the initial memory offset of the first value relative to the data buffer start.
			 *
			 * @return the initial memory byte offset
			 */
			std::size_t getInitialOffset() const;

			/**
			 * Sets the initial memory offset of the first value relative to the data buffer start.
			 *
			 * @param bytes the initial memory byte offset
			 */
			void setInitialOffset(std::size_t bytes);

			/**
			 * Gets the number of bytes between each sequential value in the array.
			 * For multi-component values per row, this is the bytes between each corresponding row
			 * (i.e. the bytes between A(which is a1, a2... an) value #1 and B(which is b1, b2... bn) value #1.
			 *
			 * @return the byte stride between sequential values
			 */
			std::size_t getStride() const;

			/**
			 * Gets the number of bytes between the array value at the given index and the next array value.  Use
			 * this in the case of an array that contains a variable number of values per index.
			 * This is the bytes between each corresponding component
			 * (i.e. If row A has 3 values and row B has 2 values, at row index A, this is the bytes between
			 * A value #1 and B value #1 = 3 values * the size of each value in bytes).
			 *
			 * @param rowIdx The index to start reading the number of bytes
			 * @return the byte stride between sequential values in a variable array
			 */
			std::size_t getStride(std::size_t rowIdx) const;

			/**
			 * Sets the number of bytes between each sequential value in the array.
			 * For multi-component values such as position, this is the bytes between each corresponding component
			 * (i.e. the bytes between X value #1 and X value #2, not the bytes between X value #1 and Y value #1).
			 *
			 * @param rowIdx The index to start reading the number of bytes
			 * @return the byte stride between sequential values
			 */
			std::size_t getColumns(std::size_t rowIdx) const;

			/**
			 * Gets the number of bytes between each sequential value in the array.
			 * For multi-component values such as position, this is the bytes between each corresponding component
			 * (i.e. the bytes between X value #1 and X value #2, not the bytes between X value #1 and Y value #1).
			 *
			 * @return the byte stride between sequential values
			 */
			std::size_t getRows() const;

			/**
			 * Sets the number of bytes between each sequential value in the array.
			 * For multi-component values such as position, this is the bytes between each corresponding component
			 * (i.e. the bytes between X value #1 and X value #2, not the bytes between X value #1 and Y value #1).
			 *
			 * @param bytes the byte stride between sequential values
			 */
			void setStride(std::size_t bytes);

			/**
			 * Gets the number of bytes between each component of the same value in the array.
			 * For multi-component values such as position, this is the bytes between each component
			 * (i.e. the bytes between X value #1 and Y value #1, not the bytes between X value #1 and X value #2).
			 *
			 * For scalar values with only 1 component, this value is irrelevant.
			 *
			 * @return the byte stride between components of the same value
			 */
			std::size_t getElementStride() const;

			/**
			 * Sets the number of bytes between each component of the same value in the array.
			 * For multi-component values such as position, this is the bytes between each component
			 * (i.e. the bytes between X value #1 and Y value #1, not the bytes between X value #1 and X value #2).
			 *
			 * For scalar values with only 1 component, this value is irrelevant.
			 *
			 * @param bytes the byte stride between components of the same value
			 */
			void setElementStride(std::size_t bytes);

			/**
			 * Gets direct access to the binary vector accessor that represents how to interpret the underlying data buffer.
			 * Advanced users can use this accessor to efficiently get and set values.
			 *
			 * @return the vector accessor for this attribute
			 */
			inline FixedArray<Data<8>, void> &getDefaultBinaryAccessor();

			/**
			 * Configures this value array to use the values as defined by the given vector accessor.
			 * Advanced users can build the accessor however they wish and then attach it to this class.
			 *
			 * @param accessor The vector accessor to bind
			 */
			void bind(FixedArray<Data<8>, void> accessor);

			/**
			 * Configures this value array to use the values as defined by the given vector accessor.
			 * Advanced users can build the accessor however they wish and then attach it to this class.
			 *
			 * @param accessor The vector accessor to bind
			 */
			void bindVariable(VariableMatrix<Data<8>, Data<8>, void> accessor);

			/**
			* Binds value to the accessor.
			*
			* @tparam <S> The datatype of the offset
			* @tparam <T> The datatype of the value
			* @param value The value
			* @param offsets The offsets
			* @param offsetCount The offset count
			*/
			template <typename S, typename T>
			void bind(T *value, S *offsets, std::size_t offsetCount);

			/**
			* Binds value to the accessor.
			*
			* @tparam <S> The datatype of the offset
			* @tparam <T> The datatype of the value
			* @tparam M The offset count
			* @param value The value
			* @param offsets The offsets
			*/
			template <typename S, typename T, std::size_t M>
			void bind(T *value, S(&offsets)[M]);

			/**
			* Binds value to the accessor.
			*
			* @tparam <S> The datatype of the offset
			* @tparam <T> The datatype of the value
			* @param value The value
			* @param colOffsets The column offsets
			* @param colOffsetCount The column offset count
			* @param rowOffsets The row offsets
			* @param rowOffsetCount The row offset count
			*/
			template <typename S, typename T>
			void bind(T *value, S *colOffsets, std::size_t colOffsetCount, S *rowOffsets, std::size_t rowOffsetCount);

			/**
			* Binds value to the accessor.
			*
			* @tparam <S> The datatype of the offset
			* @tparam <T> The datatype of the value
			* @tparam M The column offset count
			* @tparam N The row offset count
			* @param value The value
			* @param colOffsets The column offsets
			* @param rowOffsets The row offsets
			*/
			template <typename S, typename T, std::size_t M, std::size_t N>
			void bind(T *value, S(&colOffsets)[M], S(&rowOffsets)[N]);

			/**
			 * Configures this value array to represent a contiguous array of count values each with the given number of elements.
			 * The accessor is configured with the standard number layout and type for the given value.
			 *
			 * @pre value must point to a valid location in memory of at least sizeof(T) * count * elements bytes
			 *
			 * @tparam <T> The data type of the value array
			 * @param value The value
			 * @param count The number of values in the array
			 * @param elements The number of components per value
			 */
			template <typename T>
			void bind(T *value, std::size_t count, std::size_t elements = 1);

			/**
			 * Configures this value array to represent a contiguous fixed-size array of scalar values.
			 * The accessor is configured with the standard number layout and type for the given value.
			 *
			 * @tparam <T> The data type of the value array
			 * @tparam N The number of values in the array
			 * @param value The value
			 */
			template <typename T, std::size_t N>
			void bind(T(&value)[N]);

			/**
			 * Configures this value array to represent a uniform value such that every access gets or sets the same value.
			 * This can be used as an optimization when all values are or must be the same for every element.
			 *
			 * In the normal case that sizeof(T) <= 8, the uniform will be stored without a separate memory allocation for efficiency.
			 *
			 * @tparam <T> The data type of the value to put into the data array as the uniform value
			 * @param value The value array
			 * @param elements The number of components per value, which will all be the same uniform value
			 */
			template <typename T>
			void bindToUniform(T value, std::size_t elements = 1);

			/**
			 * Checks whether this value array is configured as a uniform.
			 * This can be true if bindToUniform was called previously, or if the vector accessor was otherwise configured with a stride and element stride both set to 0.
			 *
			 * @return whether this value array is configured as a uniform
			 */
			inline bool isUniform() const;

			/**
			 * Gets a value array that represents the value at the given position index.
			 * The Array will have the same configuration as this one, except the size will be 1 and the initial offset will point to the referenced value.
			 *
			 * @param index The index of the value to get
			 * @return the value array representing that one value
			 */
			Array slice(unsigned index);

			/**
			 * Gets a value array that represents a subset of values starting at the given position index.
			 * The Array will have the same configuration as this one, except the size will be the given size and the initial offset will point to the first referenced value.
			 *
			 * @param index The index of the value to get
			 * @param count the number of values to get
			 * @return the value array representing that one value
			 */
			Array slice(unsigned index, unsigned count);

			/**
			 * Creates a binary vector accessor for the array data values, interpreting the data as a particular desired type.
			 * It is your responsibility to make sure the type T is actually compatible with the binary bit patterns in the data array,
			 * although the accessor is smart enough to only read the number of bits per value actually present.
			 *
			 * For example, you can call makeBinaryAccessor<float> to expose this data array as a set of float vector values.
			 * If the underlying array has 3 elements, then you would have an accessor for sequences of 3 floats.
			 *
			 * @tparam <T> The desired element value type for the accessor
			 * @return the vector binary accessor provided the requested view
			 */
			template <typename T>
			FixedArray<T, void> makeBinaryAccessor();

			/**
			 * Creates a read-only binary vector accessor for the array data values, interpreting the data as a particular desired type.
			 * It is your responsibility to make sure the type T is actually compatible with the binary bit patterns in the data array,
			 * although the accessor is smart enough to only read the number of bits per value actually present.
			 *
			 * For example, you can call makeBinaryAccessor<float> to expose this data array as a set of float vector values.
			 * If the underlying array has 3 elements, then you would have an accessor for sequences of 3 floats.
			 *
			 * @tparam <T> The desired element value type for the accessor
			 * @return the vector binary accessor provided the requested view
			 */
			template <typename T>
			FixedArray<const T, const void> makeBinaryAccessor() const;

			/**
			 * Creates a binary scalar accessor for a particular component of the array data values, interpreting the data as a particular desired type.
			 * It is your responsibility to make sure the type T is actually compatible with the binary bit patterns in the data array,
			 * although the accessor is smart enough to only read the number of bits per value actually present.
			 *
			 * For example, you can call makeBinaryScalarAccessor<float> to expose this data array as a set of float vector values.
			 * If the underlying array has 3 elements, then you would have an accessor for each value of the specified element.
			 * By default, the specified element is element 0, which is the only element present if the attribute itself is a scalar.
			 *
			 * @tparam <T> The desired element value type for the accessor
			 * @param element the index of the element to access by this view, by default 0
			 * @return the scalar binary accessor provided the requested view
			 */
			template <typename T>
			Scalar<T, void> makeBinaryScalarAccessor(std::uint32_t element = 0);

			/**
			 * Creates a read-only binary scalar accessor for a particular component of the array data values, interpreting the data as a particular desired type.
			 * It is your responsibility to make sure the type T is actually compatible with the binary bit patterns in the data array,
			 * although the accessor is smart enough to only read the number of bits per value actually present.
			 *
			 * For example, you can call makeBinaryScalarAccessor<float> to expose this data array as a set of float vector values.
			 * If the underlying array has 3 elements, then you would have an accessor for each value of the specified element.
			 * By default, the specified element is element 0, which is the only element present if the attribute itself is a scalar.
			 *
			 * @tparam <T> The desired element value type for the accessor
			 * @param element the index of the element to access by this view, by default 0
			 * @return the scalar binary accessor provided the requested view
			 */
			template <typename T>
			Scalar<const T, const void> makeBinaryScalarAccessor(std::uint32_t element = 0) const;

			/**
			 * Gets a single scalar value from the data array given its index and component.
			 * It is your responsibility to make sure the type T is actually compatible with the binary bit patterns in the data array,
			 * although the accessor is smart enough to only read the number of bits per value actually present.
			 *
			 * @tparam <T> The desired element value type for the accessor
			 * @param value The reference to the value to load the accessed data into
			 * @param idx The value index
			 * @param element The element index, by default 0
			 * @return whether this access succeeded
			 */
			template <typename T>
			bool getBinary(T &value, std::size_t idx = 0, std::size_t element = 0) const;

			/**
			 * Gets a single scalar value from the data array given its index and component.
			 * It is your responsibility to make sure the type T is actually compatible with the binary bit patterns in the data array,
			 * although the accessor is smart enough to only read the number of bits per value actually present.
			 *
			 * @tparam <T> The desired element value type for the accessor
			 * @param idx The value index
			 * @param element The element index, by default 0
			 * @return the accessed value
			 */
			template <typename T>
			T getBinary(std::size_t idx = 0, std::size_t element = 0) const;

			/**
			 * Sets a single scalar value from the data array given its index and component.
			 * It is your responsibility to make sure the type T is actually compatible with the binary bit patterns in the data array,
			 * although the accessor is smart enough to only write the number of bits per value actually present.
			 *
			 * @tparam <T> The desired element value type for the accessor
			 * @param value The value to store into the proper location in the data buffer
			 * @param idx The value index
			 * @param element The element index, by default 0
			 * @return whether this write succeeded
			 */
			template <typename T>
			bool setBinary(T value, std::size_t idx = 0, std::size_t element = 0);

			/**
			 * Gets a text representation of the given scalar value from the data array given its index and component.
			 * This can interpret a limited number of bit patterns to prepare a human readable text.
			 * If the primitive type is Primitive::Boolean, this will return "true" or "false" appropriately.
			 * If the primitive type is Primitive::Text, this will interpret the binary value as a character and return it as a text string.
			 * If the primitive type is Primitive::Integer and the value is a signed two's complement integer type, it will be unpacked into a 64-bit signed integer and printed.
			 * If the primitive type is Primitive::Real and it matches the layout for IEEE 754 float or double, that will be printed accordingly.
			 * Any unhandled values will be printed as a hexadecimal byte pattern.
			 *
			 * Future versions of this method will be able to handle any binary encoding and will also deal with variable-length and multi-element text queries.
			 *
			 * @param colIdx The index of the value to get
			 * @param element The element of the value to get
			 * @return the text view of the value
			 */
			std::string getText(std::size_t colIdx, std::size_t element) const;

			/**
			 * Gets a text representation of the given scalar value from the data array given its index and component.
			 * This can interpret a limited number of bit patterns to prepare a human readable text.
			 * If the primitive type is Primitive::Boolean, this will return "true" or "false" appropriately.
			 * If the primitive type is Primitive::Text, this will interpret the binary value as a character and return it as a text string.
			 * If the primitive type is Primitive::Integer and the value is a signed two's complement integer type, it will be unpacked into a 64-bit signed integer and printed.
			 * If the primitive type is Primitive::Real and it matches the layout for IEEE 754 float or double, that will be printed accordingly.
			 * Any unhandled values will be printed as a hexadecimal byte pattern.
			 *
			 * Future versions of this method will be able to handle any binary encoding and will also deal with variable-length and multi-element text queries.
			 *
			 * @param idx The index of the value to get
			 * @return the text view of the value
			 */
			std::string getText(std::size_t idx) const;

			/**
			 * Sets a text representation of the given scalar value from the data array given its index and component.
			 * This can interpret a limited number of bit patterns to parse human-readable text.
			 * If the primitive type is Primitive::Boolean, the values "true" and "false" will map directly, and any number will be parsed as such and then be "true" if non-zero.
			 * If the primitive type is Primitive::Text, this will interpret the binary value as a character and put the first string character into it, if any, or null terminator if the string is empty.
			 * If the primitive type is Primitive::Integer, the string will be parsed for an integer and then set as best as possible given the layout.
			 * If the primitive type is Primitive::Real and it matches the layout for IEEE 754 float or double, the string will be parsed using the conventional rules for such.
			 * As a special case, hexadecimal strings of the form "0x1234AF" and so on will be interpreted as a byte sequence and used to set the value regardless of the semantics.
			 *
			 * @param text The text value to use
			 * @param idx The index of the value to set
			 * @param element The element of the value to set
			 * @return whether the operation was successful
			 */
			bool setText(const std::string &text, std::size_t idx, std::size_t element);

			/**
			 * Sets a text representation of the given scalar value from the data array given its index and component.
			 * This can interpret a limited number of bit patterns to parse human-readable text.
			 * If the primitive type is Primitive::Boolean, the values "true" and "false" will map directly, and any number will be parsed as such and then be "true" if non-zero.
			 * If the primitive type is Primitive::Text, this will interpret the binary value as a character and put the first string character into it, if any, or null terminator if the string is empty.
			 * If the primitive type is Primitive::Integer, the string will be parsed for an integer and then set as best as possible given the layout.
			 * If the primitive type is Primitive::Real and it matches the layout for IEEE 754 float or double, the string will be parsed using the conventional rules for such.
			 * As a special case, hexadecimal strings of the form "0x1234AF" and so on will be interpreted as a byte sequence and used to set the value regardless of the semantics.
			 *
			 * @param text The text value to use
			 * @param idx The index of the value to set
			 * @return whether the operation was successful
			 */
			bool setText(const std::string &text, std::size_t idx);

			/**
			 * Sets a text representation of the given scalar value from the data array given its index and component.
			 * This can interpret a limited number of bit patterns to parse human-readable text.
			 * If the primitive type is Primitive::Boolean, the values "true" and "false" will map directly, and any number will be parsed as such and then be "true" if non-zero.
			 * If the primitive type is Primitive::Text, this will interpret the binary value as a character and put the first string character into it, if any, or null terminator if the string is empty.
			 * If the primitive type is Primitive::Integer, the string will be parsed for an integer and then set as best as possible given the layout.
			 * If the primitive type is Primitive::Real and it matches the layout for IEEE 754 float or double, the string will be parsed using the conventional rules for such.
			 * As a special case, hexadecimal strings of the form "0x1234AF" and so on will be interpreted as a byte sequence and used to set the value regardless of the semantics.
			 *
			 * @param text The text value to use
			 * @return whether the operation was successful
			 */
			bool setText(const std::string &text);

			/**
			 * Sets a text representation of the given scalar value from the data array given its index and component.
			 * This can interpret a limited number of bit patterns to parse human-readable text.
			 * If the primitive type is Primitive::Boolean, the values "true" and "false" will map directly, and any number will be parsed as such and then be "true" if non-zero.
			 * If the primitive type is Primitive::Text, this will interpret the binary value as a character and put the first string character into it, if any, or null terminator if the string is empty.
			 * If the primitive type is Primitive::Integer, the string will be parsed for an integer and then set as best as possible given the layout.
			 * If the primitive type is Primitive::Real and it matches the layout for IEEE 754 float or double, the string will be parsed using the conventional rules for such.
			 * As a special case, hexadecimal strings of the form "0x1234AF" and so on will be interpreted as a byte sequence and used to set the value regardless of the semantics.
			 *
			 * @param text The text value to use
			 * @param idx The index of the value to set
			 * @param element The element of the value to set
			 * @return whether the operation was successful
			 */
			bool setText(const char *text, std::size_t idx, std::size_t element);

			/**
			 * Gets the number of elements for each logical value.
			 *
			 * @return the number of elements
			 */
			inline std::size_t getElementCount() const;

			/**
			 * Sets the number of data elements in the buffer.
			 *
			 * @param count the number of elements
			 */
			void setElementCount(std::size_t count);

			/**
			 * Gets the number of logical values in the array.
			 *
			 * @return the number of logical values
			 */
			inline std::size_t size() const;

			/**
			 * Sets the number of logical values in the array.
			 * This does not modify the actual underlying array.
			 *
			 * @param count the number of logical values
			 */
			void resize(std::size_t count);

			/**
			 * Gets the semantic primitive type of the values in this array.
			 * It doesn't affect the layout or bit pattern, but is used for text conversions and transcoding.
			 *
			 * @return the semantic primitive type
			 */
			cio::Type getType() const;

			/**
			 * Sets the semantic primitive type of the values given.
			 * It doesn't affect the layout or bit pattern, but is used for text conversions and transcoding.
			 *
			 * @param type The semantic primitive type
			 */
			void setType(cio::Type type);

			/**
			* Determine whether the variable dataset is fixed byte aligned.
			*
			* @return whether the variable dataset is fixed byte aligned
			*/
			inline bool isFixedByteAligned() const;

			/**
			 * Converts a vector of strings to a vector of chars.
			 *
			 * @param strings The vector of strings
			 * @return a vector of char
			 */
			inline std::vector<char> toChar(std::vector<std::string> strings) const;

			/**
			* Convert the string to a vector of chars.
			*
			* @tparam N The string length
			* @param value The string
			* @return the vector of chars
			*/
			template< std::size_t N>
			inline std::vector<char> toChar(std::string(&value)[N]) const;

			/**
			 * Gets a vector of offsets between the start of strings.
			 *
			 * @param strings The strings
			 * @return a vector of offsets between the start of strings
			 */
			inline std::vector<std::uint32_t> getOffsets(std::vector<std::string> strings) const;

			/**
			 * Gets a vector of offsets between the start of strings.
			 *
			 * @tparam N The length of the string
			 * @param value The strings
			 * @return a vector of offsets between the start of strings
			 */
			template<std::size_t N>
			inline std::vector<std::uint32_t> getOffsets(std::string(&value)[N]) const;

		private:
			/** True if using a variable matrix, false if using a fixed array. */
			bool mVariableDataSet;

			/** The layout encoding. */
			cio::Encoding mEncoding;

			/** The uniform value array. */
			Data<8> mUniform;

			/** The accessor. */
			FixedArray<Data<8>, void> mAccessor;

			/** The matrix accessor. */
			VariableMatrix<std::uint32_t, Data<8>, void> mMatrixAccessor;

			/** The primitive type. */
			cio::Type mType;
	};
}

/* Inline implementation */

namespace glitter
{
	inline const cio::Chunk<void> &Array::getBuffer() const
	{
		if (mVariableDataSet)
			return mMatrixAccessor.getData();
		else
			return mAccessor.getData();
	}

	template <typename T>
	inline FixedArray<T, void> Array::makeBinaryAccessor()
	{
		return mAccessor.reference<T>();
	}

	template <typename T>
	inline FixedArray<const T, const void> Array::makeBinaryAccessor() const
	{
		return mAccessor.reference<const T, const void>();
	}

	template <typename T>
	Scalar<T, void> Array::makeBinaryScalarAccessor(std::uint32_t element)
	{
		auto selected = mAccessor.select(element);
		return Scalar<T, void>::reference(selected);
	}

	template <typename T>
	Scalar<const T, const void> Array::makeBinaryScalarAccessor(std::uint32_t element) const
	{
		return Scalar<const T, const void>::reference(mAccessor.select(element));
	}

	template <typename T>
	inline bool Array::getBinary(T &value, std::size_t idx, std::size_t element) const
	{
		bool success = false;
		value = T();
		if (this->isFixedByteAligned() && idx < mAccessor.size() && element < mAccessor.getColumnCount())
		{
			Data<8> tmp = mAccessor.get(idx, element);
			std::memcpy(&value, &tmp, std::min(sizeof(T), sizeof(tmp)));
			success = true;
		}
		return success;
	}

	template <typename T>
	inline T Array::getBinary(std::size_t idx, std::size_t element) const
	{
		T value = T();
		this->getBinary(value, idx, element);
		return value;
	}

	template <typename T>
	inline bool Array::setBinary(T value, std::size_t idx, std::size_t element)
	{
		bool success = false;
		if (this->isFixedByteAligned() && idx < mAccessor.size() && element < mAccessor.getColumnCount())
		{
			Data<8> tmp = { 0 };
			size_t elementSize = static_cast<size_t>(mEncoding.computeRequiredBytes());
			std::memcpy(&tmp, &value, std::min(sizeof(T), elementSize));

			mAccessor.set(idx, element, tmp, elementSize);
			success = true;
		}

		return success;
	}

	inline FixedArray<Data<8>, void> &Array::getDefaultBinaryAccessor()
	{
		return mAccessor;
	}

	inline std::size_t Array::size() const
	{
		std::size_t sz = 0;
		sz = mVariableDataSet ? mMatrixAccessor.size() : mAccessor.size();
		return sz;
	}

	inline std::size_t Array::getElementCount() const
	{
		std::size_t numElements = 0;
		numElements = mVariableDataSet ? mMatrixAccessor.size() : mAccessor.getColumnCount();
		return numElements;
	}

	template <typename T>
	void Array::bind(T *value, std::size_t count, std::size_t elements)
	{
		std::size_t width = sizeof(cio::Value<T>);
		mAccessor.bind(cio::Chunk<void>(value, count * elements * width, nullptr));
		mAccessor.resize(count * elements);
		mAccessor.setColumnCount(elements);
		mAccessor.setElementSize(width);
		mAccessor.setElementStride(width);
		mAccessor.setOffset(0);
		mAccessor.setStride(elements * width);

		mEncoding = cio::Encoding::create<T>();
		mType = cio::primitive<T>();
		mVariableDataSet = false;
	}

	template <typename T, std::size_t N>
	void Array::bind(T(&value)[N])
	{
		this->bind(value, N, 1);
	}

	template <typename S, typename T>
	inline void Array::bind(T *value, S *offsets, std::size_t offsetCount)
	{
		std::size_t width = sizeof(cio::Value<T>);
		std::size_t offWidth = sizeof(cio::Value<S>);
		std::size_t dataSize = offsetCount * width;
		if (offsetCount > 0)
			dataSize = offsets[offsetCount - 1];
		mMatrixAccessor.bind(cio::Chunk<void>(value, dataSize, nullptr), cio::Chunk<void>(offsets, offsetCount * offWidth, nullptr), offsetCount);
		mMatrixAccessor.getColumnData().setElementSize(width);
		mMatrixAccessor.getColumnData().setOffset(0);

		mType = cio::primitive<T>();
		mVariableDataSet = true;
	}

	template <typename S, typename T, std::size_t M>
	inline void  Array::bind(T *value, S(&offsets)[M])
	{
		this->bind(value, offsets, M);
	}

	template <typename S, typename T>
	inline void Array::bind(T *value, S *colOffsets, std::size_t colOffsetCount, S *rowOffsets, std::size_t rowOffsetCount)
	{
		std::size_t width = sizeof(cio::Value<T>);
		std::size_t offWidth = sizeof(cio::Value<S>);
		std::size_t dataSize = colOffsetCount * width;
		if (colOffsetCount > 0)
			dataSize = colOffsets[colOffsetCount - 1];
		mMatrixAccessor.bind(
			cio::Chunk<void>(value, dataSize, nullptr),
			cio::Chunk<void>(colOffsets, colOffsetCount * offWidth, nullptr), colOffsetCount,
			cio::Chunk<void>(rowOffsets, rowOffsetCount * offWidth, nullptr), rowOffsetCount);
		mMatrixAccessor.getColumnData().setElementSize(width);
		mMatrixAccessor.getColumnData().setOffset(0);

		mType = cio::primitive<T>();
		mVariableDataSet = true;
	}

	template <typename S, typename T, std::size_t M, std::size_t N>
	inline void  Array::bind(T *value, S(&colOffsets)[M], S(&rowOffsets)[N])
	{
		this->bind(value, colOffsets, M, rowOffsets, N);
	}

	template <typename T>
	inline void  Array::bindToUniform(T value, std::size_t elements)
	{
		if (sizeof(T) <= 8)
		{
			mUniform.uint64[0] = 0;
			std::memcpy(&mUniform, &value, sizeof(T));

			cio::Chunk<void> wrapper(&mUniform, sizeof(T), nullptr);
			mAccessor.bind(std::move(wrapper));
		}
		else
		{
			cio::Chunk<void> data(sizeof(T), nullptr);
			std::memcpy(data.data(), &value, sizeof(T));
			mAccessor.bind(std::move(data));
		}

		mAccessor.setOffset(0);
		mAccessor.setStride(0);
		mAccessor.setElementStride(0);
		mAccessor.setElementSize(sizeof(T));
		mAccessor.setColumnCount(elements);
		mVariableDataSet = false;
	}

	inline bool Array::isUniform() const
	{
		return mAccessor.getStride() == 0 && mAccessor.getColumnCount() <= 1 || mAccessor.getElementStride() == 0;
	}

	inline bool Array::isFixedByteAligned() const
	{
		return (mEncoding.computeRequiredBits() % 8 == 0) && mType != cio::Type::Text && mType != cio::Type::Blob && !mVariableDataSet;
	}

	template<std::size_t N>
	inline std::vector<char> Array::toChar(std::string(&value)[N]) const
	{
		std::vector<std::string> vec(value, value + (sizeof(value) / sizeof(std::string)));
		return this->toChar(vec);
	}

	inline std::vector<char> Array::toChar(std::vector<std::string> strings) const
	{
		auto append = [](std::vector<char> v, const std::string &s)
		{
			v.insert(v.end(), s.begin(), s.end()); return v;
		};
		return std::accumulate(strings.begin(), strings.end(), std::vector<char>(), append);
	}

	template<std::size_t N>
	inline std::vector<std::uint32_t> Array::getOffsets(std::string(&value)[N]) const
	{
		std::vector<std::string> vec(value, value + (sizeof(value) / sizeof(std::string)));
		return this->getOffsets(vec);
	}

	inline std::vector<std::uint32_t> Array::getOffsets(std::vector<std::string> strings) const
	{
		std::vector<std::uint32_t> offsets(strings.size() + 1, 0);
		for (std::size_t i = 0; i < strings.size(); ++i)
		{
			offsets[i + 1] = strings.at(i).length() + offsets[i];
		}
		return offsets;
	}
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
