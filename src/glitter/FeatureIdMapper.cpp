/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "FeatureIdMapper.h"

#include "Node.h"

namespace glitter
{
	std::uint32_t FeatureIdMapper::sConstantZero(0);

	FeatureIdMapper::FeatureIdMapper() :
		mSchema(nullptr),
		mNode(nullptr)
	{
		this->unbindVertexIndices();
		this->unbindFeatureIds();
	}

	void FeatureIdMapper::bindVertexIndices(Scalar<std::uint32_t, void> accessor)
	{
		mVertexIndices = std::move(accessor);
		mVertexMultiplier = 0;

		if (mFeatureIds.data() == &sConstantZero)
		{
			mFeatureIds.resize(mVertexIndices.size());
		}
	}

	void FeatureIdMapper::resize(std::uint32_t count)
	{
		mVertexIndices.resize(count);
		if (mFeatureIds.data() == &sConstantZero)
		{
			mFeatureIds.resize(count);
		}
	}

	void FeatureIdMapper::unbindVertexIndices()
	{
		mVertexIndices.bind(cio::Chunk<void>(&sConstantZero, 4, nullptr));
		mVertexIndices.setStride(0);
		mVertexIndices.setOffset(0);
		mVertexIndices.setElementSize(4);
		mVertexIndices.resize(mFeatureIds.size());
		mVertexMultiplier = 1;
	}

	void FeatureIdMapper::bindFeatureIds(Scalar<std::uint32_t, void> accessor)
	{
		mFeatureIds = std::move(accessor);
		if (mVertexIndices.data() == &sConstantZero)
		{
			mVertexIndices.resize(mFeatureIds.size());
		}
	}

	void FeatureIdMapper::unbindFeatureIds()
	{
		mFeatureIds.bind(cio::Chunk<void>(&sConstantZero, 4, nullptr));
		mFeatureIds.setStride(0);
		mFeatureIds.setOffset(0);
		mFeatureIds.setElementSize(4);
		mFeatureIds.resize(mVertexIndices.size());
	}

	std::vector<std::uint32_t> FeatureIdMapper::getPrimitiveSetFeatureIds() const
	{
		std::vector<std::uint32_t> fids;

		fids.resize(mVertexIndices.size());
		for (std::uint32_t i = 0; i < fids.size(); ++i)
		{
			fids[i] = this->getVertexFeatureId(i);
		}
		return fids;
	}

	std::uint32_t FeatureIdMapper::getTriangleFeatureId(std::uint32_t t) const
	{
		std::uint32_t value = UINT32_MAX;
		std::uint32_t v = t * 3;
		std::uint32_t result[3];
		result[0] = this->getVertexFeatureId(v);
		result[1] = this->getVertexFeatureId(v + 1);
		result[2] = this->getVertexFeatureId(v + 2);
		if (result[0] == result[1] && result[1] == result[2])
		{
			value = result[0];
		}
		return value;
	}

	std::array<std::uint32_t, 3> FeatureIdMapper::getTriangleFeatureIds(std::uint32_t t) const
	{
		std::uint32_t v = t * 3;
		std::array<std::uint32_t, 3> result;
		result[0] = this->getVertexFeatureId(v);
		result[1] = this->getVertexFeatureId(v + 1);
		result[2] = this->getVertexFeatureId(v + 2);
		return result;
	}

	std::uint32_t FeatureIdMapper::getPrimitiveSetFeatureId() const
	{
		std::uint32_t value = this->getVertexFeatureId(0);

		for (std::uint32_t i = 1; i < mVertexIndices.size(); ++i)
		{
			std::uint32_t next = this->getVertexFeatureId(i);
			if (next != value)
			{
				value = UINT32_MAX;
				break;
			}
		}

		return value;
	}

	Entity FeatureIdMapper::getVertexEntity(std::uint32_t v) const
	{
		Entity entity;
		std::uint32_t fid = this->getVertexFeatureId(v);
		entity.bind(mNode, fid);
		return entity;
	}

	Entity FeatureIdMapper::getTriangleEntity(std::uint32_t t) const
	{
		Entity entity;
		std::uint32_t fid = this->getTriangleFeatureId(t);
		if (fid != UINT32_MAX)
		{
			entity.bind(mNode, fid);
		}
		return entity;
	}

	std::array<Entity, 3> FeatureIdMapper::getTriangleEntities(std::uint32_t t) const
	{
		std::array<Entity, 3> entities;
		entities[0] = this->getVertexEntity(t * 3);
		entities[1] = this->getVertexEntity(t * 3 + 1);
		entities[2] = this->getVertexEntity(t * 3 + 2);
		return entities;
	}

	Entity FeatureIdMapper::getPrimitiveSetEntity() const
	{
		Entity entity;
		std::uint32_t fid = this->getPrimitiveSetFeatureId();
		if (fid != UINT32_MAX)
		{
			entity.bind(mNode, fid);
		}
		return entity;
	}

	std::vector<Entity> FeatureIdMapper::getPrimitiveSetEntities() const
	{
		std::vector<Entity> entities;
		entities.resize(mVertexIndices.size());
		for (std::uint32_t v = 0; v < mVertexIndices.size(); ++v)
		{
			entities[v] = this->getVertexEntity(v);
		}
		return entities;
	}

	FeatureIdMapper::operator bool() const
	{
		return mSchema && mNode;
	}
}