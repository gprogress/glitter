/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_FEATUREIDMAP_H
#define GLITTER_FEATUREIDMAP_H

#include "Types.h"

#include <iostream>

namespace glitter
{
	class FeatureIdMap
	{
		public:
			inline FeatureIdMap();

			std::int32_t table;
			std::int32_t accessor;
			std::int32_t offset;
			std::int32_t divisor;

			inline std::uint32_t procedural(std::uint32_t v) const;

			inline bool isProcedural() const;

			inline bool isUniform() const;
	};
}

/* Inline implementation */

namespace glitter
{
	inline FeatureIdMap::FeatureIdMap() :
		table(-1),
		accessor(-1),
		offset(0),
		divisor(0)
	{
		// nothing more to do
	}

	inline std::uint32_t FeatureIdMap::procedural(std::uint32_t v) const
	{
		return static_cast<std::uint32_t>(offset + (divisor ? (static_cast<std::int32_t>(v) / divisor) : 0));
	}

	inline bool FeatureIdMap::isProcedural() const
	{
		return accessor == -1;
	}

	inline bool FeatureIdMap::isUniform() const
	{
		return divisor == 0 && accessor == -1;
	}
}

#endif
