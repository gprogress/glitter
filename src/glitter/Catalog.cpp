/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "Catalog.h"

#include "KeyValueList.h"

#include <geas/Profiles.h>

#include <cio/Class.h>
#include <cio/Variant.h>

namespace glitter
{
		cio::Class<Catalog> Catalog::sMetaclass("glitter::Catalog");

		Catalog::Catalog() noexcept :
			mDefaultProfile(nullptr)
		{
			// nothign more to do
		}

		Catalog::Catalog(const Catalog &in) :
			geas::Definition(in)
		{
			this->copyCatalog(in);
		}

		Catalog::Catalog(Catalog &&in) noexcept :
			geas::Definition(std::move(in)),
			mVocabulary(std::move(in.mVocabulary)),
			mMeasures(std::move(in.mMeasures)),
			mApplicationSchema(std::move(in.mApplicationSchema)),
			mProfiles(std::move(in.mProfiles)),
			mDefaultProfile(in.mDefaultProfile),
			mCoverage(in.mCoverage)
		{
			// nothing more to do
		}

		Catalog &Catalog::operator=(const Catalog &in)
		{
			if (&in != this)
			{
				geas::Definition::operator=(in);
				this->copyCatalog(in);
			}

			return *this;
		}

		Catalog &Catalog::operator=(Catalog &&in) noexcept
		{
			if (this != &in)
			{
				geas::Definition::operator=(std::move(in));
		
				mVocabulary = std::move(in.mVocabulary);
				mMeasures = std::move(in.mMeasures);
				mApplicationSchema = std::move(in.mApplicationSchema);
				mProfiles = std::move(in.mProfiles);
				mDefaultProfile = in.mDefaultProfile;
				mCoverage = in.mCoverage;
			}

			return *this;
		}

		Catalog::~Catalog() noexcept = default;

		const cio::Metaclass &Catalog::getMetaclass() const noexcept
		{
			return sMetaclass;
		}

		void Catalog::clear() noexcept
		{
			geas::Definition::clear();

			mApplicationSchema.reset();
			mMeasures.reset();
			mVocabulary.reset();
			mCoverage.clear();
		}

		void Catalog::create()
		{
			mVocabulary.reset(new geas::Vocabulary());
			mMeasures.reset(new geas::Measures());
			mApplicationSchema.reset(new geas::Application());

			mApplicationSchema->setVocabulary(mVocabulary);
			mApplicationSchema->setMeasures(mMeasures);

			mCoverage.clear();
		}

		void Catalog::copyCatalog(const Catalog &in)
		{
			if (&in != this)
			{
				mApplicationSchema.reset();
				mVocabulary.reset();
				mMeasures.reset();

				// First duplicate application schema
				// If it has a vocabulary and measurement system those will be duplicated too
				if (in.mApplicationSchema)
				{
					mApplicationSchema.reset(new geas::Application(*in.mApplicationSchema));
				}

				// Duplicate vocabulary if application didn't
				if (in.mVocabulary && !mVocabulary)
				{
					mVocabulary.reset(new geas::Vocabulary(*in.mVocabulary));
				}
				else
				{
					mVocabulary = mApplicationSchema->getVocabulary();
				}

				// Duplicate measurement systme if application didn't
				if (in.mMeasures && !mMeasures)
				{
					mMeasures.reset(new geas::Measures(*in.mMeasures));
				}
				else
				{
					mMeasures = mApplicationSchema->getMeasures();
				}

				mCoverage = in.mCoverage;
			}
		}

		std::shared_ptr<geas::Application> Catalog::getApplicationSchema() noexcept
		{
			return mApplicationSchema;
		}

		std::shared_ptr<const geas::Application> Catalog::getApplicationSchema() const noexcept
		{
			return mApplicationSchema;
		}

		std::shared_ptr<geas::Application> Catalog::getOrCreateApplicationSchema()
		{
			if (!mApplicationSchema)
			{
				mApplicationSchema.reset(new geas::Application());
				mApplicationSchema->setVocabulary(mVocabulary);
				mApplicationSchema->setMeasures(mMeasures);
			}

			return mApplicationSchema;
		}

		void Catalog::setApplicationSchema(std::shared_ptr<geas::Application> schema) noexcept
		{
			mApplicationSchema = std::move(schema);
			if (mApplicationSchema)
			{
				mVocabulary = mApplicationSchema->getVocabulary();
				mMeasures = mApplicationSchema->getMeasures();
			}
		}

		void Catalog::clearApplicationSchema() noexcept
		{
			mApplicationSchema.reset();
		}

		std::shared_ptr<geas::Profiles> Catalog::getProfiles() noexcept
		{
			return mProfiles;
		}

		std::shared_ptr<const geas::Profiles> Catalog::getProfiles() const noexcept
		{
			return mProfiles;
		}

		std::shared_ptr<geas::Profiles> Catalog::getOrCreateProfiles()
		{
			if (!mProfiles)
			{
				mProfiles.reset(new geas::Profiles());
				mProfiles->setApplicationSchema(mApplicationSchema);
			}

			return mProfiles;
		}

		void Catalog::setProfiles(std::shared_ptr<geas::Profiles> profiles) noexcept
		{
			mProfiles = std::move(profiles);

			if (mProfiles)
			{
				if (mApplicationSchema)
				{
					mProfiles->setApplicationSchema(mApplicationSchema);
				}
			}
		}

		void Catalog::clearProfiles() noexcept
		{
			mProfiles.reset();
		}

		const geas::ApplicationProfile *Catalog::getDefaultProfile() const noexcept
		{
			return mDefaultProfile;
		}

		void Catalog::setDefaultProfile(const geas::ApplicationProfile *profile) noexcept
		{
			mDefaultProfile = profile;
		}

		std::shared_ptr<geas::Vocabulary> Catalog::getVocabulary() noexcept
		{
			return mVocabulary;
		}

		std::shared_ptr<const geas::Vocabulary> Catalog::getVocabulary() const noexcept
		{
			return mVocabulary;
		}

		std::shared_ptr<geas::Vocabulary> Catalog::getOrCreateVocabulary()
		{
			if (!mVocabulary)
			{
				mVocabulary.reset(new geas::Vocabulary());
				
				if (mApplicationSchema)
				{
					mApplicationSchema->setVocabulary(mVocabulary);
				}
			}

			return mVocabulary;
		}

		void Catalog::setVocabulary(std::shared_ptr<geas::Vocabulary> vocabulary) noexcept
		{
			mVocabulary = std::move(vocabulary);

			if (mApplicationSchema)
			{
				mApplicationSchema->setVocabulary(mVocabulary);
			}
		}

		void Catalog::clearVocabulary() noexcept
		{
			mVocabulary.reset();

			if (mApplicationSchema)
			{
				mApplicationSchema->clearVocabulary();
			}
		}

		std::shared_ptr<geas::Measures> Catalog::getMeasures() noexcept
		{
			return mMeasures;
		}

		std::shared_ptr<const geas::Measures> Catalog::getMeasures() const noexcept
		{
			return mMeasures;
		}

		std::shared_ptr<geas::Measures> Catalog::getOrCreateMeasures()
		{
			if (!mMeasures)
			{
				mMeasures.reset(new geas::Measures());

				if (mApplicationSchema)
				{
					mApplicationSchema->setMeasures(mMeasures);
				}
			}

			return mMeasures;
		}

		void Catalog::setMeasures(std::shared_ptr<geas::Measures> system) noexcept
		{
			mMeasures = std::move(system);

			if (mApplicationSchema)
			{
				mApplicationSchema->setMeasures(mMeasures);
			}
		}

		void Catalog::clearMeasures() noexcept
		{
			mMeasures.reset();
			
			if (mApplicationSchema)
			{
				mApplicationSchema->clearMeasures();
			}
		}

		const Region &Catalog::getCoverage() const noexcept
		{
			return mCoverage;
		}

		void Catalog::setCoverage(Region &coverage) noexcept
		{
			mCoverage = coverage;
		}

		void Catalog::clearCoverage() noexcept
		{
			mCoverage.clear();
		}
}

/* Explicit instantiations */

#include <geas/Dictionary.cpp>

#if defined _MSC_VER
#pragma warning(disable: 4251)
#endif

namespace geas
{
	template class GLITTER_TEMPLATE_ABI geas::Dictionary<glitter::Catalog>;
}

