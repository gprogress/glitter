/**
 * Copyright 2021 Geometric Progress LLC
 *
 * This file is part of Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
 */
#include <cio/Logger.h>
#include <cio/Path.h>
#include <cio/ProtocolFactory.h>

#include <geodex/schema/Application.h>
#include <geodex/data/Catalog.h>
#include <geodex/data/Provider.h>
#include <geodex/excel/GGDMReader.h>

#include <glitter/MetadataReader.h>
#include <glitter/Model.h>

#include <set>

cio::Severity validateEntities(const gdx::schema::Application &schema, const gdx::data::Provider &provider, cio::Logger &logger)
{
	cio::Severity severity = cio::Severity::None;

	const gdx::Entity *defaultEntity = provider.getDefaultEntityType();
	if (defaultEntity)
	{
		logger.info() << "Default dataset entity: " << defaultEntity->getLabel();
	}
	else
	{
		logger.info() << "Default dataset entity: none present";
	}

	std::set<const gdx::Entity *> entities;

	std::size_t count = provider.size();
	logger.info() << "Dataset entity count: " << count;

	const gdx::ValueProvider *values = provider.getValueProvider();
	if (values)
	{
		std::size_t attrcount = values->getDefinitionCount();
		logger.info() << "Dataset has " << attrcount << " attribute types";
		for (std::size_t i = 0; i < attrcount; ++i)
		{
			gdx::Attribute attr = values->getDefinition(i);
			logger.info() << "Attribute #" << i << " is " << attr.getLabel() << " with type " << attr.getConstraint().getPrimitive();
			logger.info() << "Attribute value encoding is " << attr.getConstraint().getLayout();
		}

		const std::vector<gdx::EntityProvider::TypeSource> &sources = provider.getEntityTypeSource();
		if (sources.empty())
		{
			logger.info() << "Dataset does not have attributes providing entity type, all entity types are defaulted";
		}
		else
		{ 
			for (const gdx::EntityProvider::TypeSource &source : sources)
			{
				gdx::Attribute attr = provider.getValueProvider()->getDefinition(source.attr);
				logger.info() << "Dataset entity type attribute: " << attr.getLabel() << " (resolved by " << source.identifier << ")";
			}
		}

		for (std::size_t e = 0; e < provider.size(); ++e)
		{
			const gdx::Entity *type = provider.getEntityType(&schema, e);
			if (type != defaultEntity && entities.insert(type).second)
			{
				if (type)
				{
					logger.info() << "Entity type discovered: " << type->getLabel();
				}
				else
				{
					logger.info() << "Dataset has entities without a type";
				}
			}
		}
	}
	else
	{
		logger.info() << " Dataset has no attributes, all entities are fully defaulted";
	}

	for (std::size_t i = 0; i < count; ++i)
	{

	}

	return severity;
}

cio::Severity validateEntities(const glitter::Model &model, cio::Logger &logger)
{
	cio::Severity severity = cio::Severity::None;

	auto catalog = model.getCatalog();
	if (catalog)
	{
		const gdx::ApplicationSchema *schema = catalog->getApplicationSchema();
		if (schema)
		{
			const gdx::Collection *collection = model.getEntityCollection();
			if (collection)
			{
				for (const gdx::Dataset &dataset : collection->getDatasets())
				{
					logger.info() << "Processing dataset " << dataset.getLabel();

					const gdx::EntityProvider *entities = dataset.getEntities();
					if (entities)
					{

					}
					else
					{
						logger.info() << "No entities in dataset";
					}
				}
			}
			else
			{
				logger.info() << "No entity collection available on input";
			}
		}
		else
		{
			logger.info() << "No application schema available on input";
		}
	}
	else
	{
		logger.info() << "No catalog available on input";
	}

	return severity;
}

int main(int argc, char **argv)
{
	int validation = 0;

	cio::Logger logger("glitter");

	glitter::Model model;

	cio::Path location;

	bool optimize = false;

	// Load each referenced file in order
	for (int i = 1; i < argc; ++i)
	{
		// First check for options
		if (cio::equalCaseInsensitive(argv[i], "--optimize"))
		{
			optimize = true;
		}
		else
		{
			cio::Path location(argv[i]);
			if (location.matchesExtension(".xlsx"))
			{
				logger.info() << "Loading GGDM catalog " << location;

				auto catalog = model.getOrCreateCatalog();
				gdx::excel::GGDMReader reader;
				reader.loadSchema(*catalog->getApplicationSchema(), location, *cio::ProtocolFactory::getDefault());
			}
			else if (location.matchesExtension(".gltf") || location.matchesExtension(".glb"))
			{
				logger.info() << "Loading glTF model " << location;
				model.loadFromPath(location);
			}
			else if (location.matchesExtension(".json"))
			{
				logger.info() << "Loading JSON catalog " << location;

				auto catalog = model.getOrCreateCatalog();
				glitter::MetadataReader reader;
				reader.loadFeatureSchemaFile(*catalog->getApplicationSchema(), location);
			}
		}
	}

	cio::Severity severity = validateEntities(model, logger);

	if (optimize)
	{
		if (model.removeUnusedData())
		{
			model.save();
		}
	}

	return severity > cio::Severity::Info ? static_cast<int>(severity) : 0;
}
