/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "Array.h"

#include <cio/Primitive.h>
#include <cio/Parse.h>
#include <cio/Print.h>
#include <cio/Text.h>
#include <cctype>

namespace glitter
{
	Array::Array() :
		mVariableDataSet(false),
		mType(cio::Type::None)
	{
		std::memset(&mUniform, 0, sizeof(mUniform));
	}

	Array::Array(const Array &in) :
		mEncoding(in.mEncoding),
		mAccessor(in.mAccessor),
		mVariableDataSet(in.mVariableDataSet),
		mMatrixAccessor(in.mMatrixAccessor),
		mType(in.mType)
	{
		std::memcpy(&mUniform, &in.mUniform, sizeof(mUniform));
		if (static_cast<void *>(mAccessor.getData().data()) == static_cast<const void *>(&in.mUniform))
		{
			mAccessor.getData().bind(&mUniform, mAccessor.getData().size(), nullptr);
		}
	}

	Array::Array(Array &&in) noexcept :
		mEncoding(in.mEncoding),
		mAccessor(in.mAccessor),
		mVariableDataSet(in.mVariableDataSet),
		mMatrixAccessor(in.mMatrixAccessor),
		mType(in.mType)
	{
		std::memcpy(&mUniform, &in.mUniform, sizeof(mUniform));
		std::memset(&in.mUniform, 0, sizeof(mUniform));

		if (static_cast<void *>(mAccessor.getData().data()) == static_cast<void *>(&in.mUniform))
		{
			mAccessor.getData().bind(&mUniform, mAccessor.getData().size(), nullptr);
		}
	}

	Array &Array::operator=(const Array &in)
	{
		if (this != &in)
		{
			mEncoding = in.mEncoding;
			mAccessor = in.mAccessor;
			mMatrixAccessor = in.mMatrixAccessor;
			mVariableDataSet = in.mVariableDataSet;
			mType = in.mType;

			std::memcpy(&mUniform, &in.mUniform, sizeof(mUniform));
			if (static_cast<void *>(mAccessor.getData().data()) == static_cast<const void *>(&in.mUniform))
			{
				mAccessor.getData().bind(&mUniform, mAccessor.getData().size(), nullptr);
			}
		}

		return *this;
	}

	Array &Array::operator=(Array &&in) noexcept
	{
		if (this != &in)
		{
			mEncoding = in.mEncoding;
			mAccessor = std::move(in.mAccessor);
			mMatrixAccessor = std::move(in.mMatrixAccessor);
			mVariableDataSet = in.mVariableDataSet;
			mType = in.mType;

			std::memcpy(&mUniform, &in.mUniform, sizeof(mUniform));
			std::memset(&in.mUniform, 0, sizeof(mUniform));

			if (static_cast<void *>(mAccessor.getData().data()) == static_cast<void *>(&in.mUniform))
			{
				mAccessor.getData().bind(&mUniform, mAccessor.getData().size(), nullptr);
			}
		}
		return *this;
	}

	Array::~Array() noexcept = default;

	void Array::clear() noexcept
	{
		mEncoding.clear();
		mAccessor.clear();
		mMatrixAccessor.clear();
		mVariableDataSet = false;
		mType = cio::Type::None;
	}

	void Array::setFixedBuffer(cio::Chunk<void> data, std::size_t columns)
	{
		mAccessor.bind(std::move(data));
		mAccessor.setColumnCount(columns);
		mVariableDataSet = false;
	}

	void Array::setVariableBuffer(cio::Chunk<void> data, cio::Chunk<void> columnOffsets, std::size_t numColOffsets)
	{
		mMatrixAccessor.bind(std::move(data), std::move(columnOffsets), numColOffsets);
		mVariableDataSet = true;
	}

	void Array::setVariableBuffer(cio::Chunk<void> data, cio::Chunk<void> columnOffsets, std::size_t numColOffsets, cio::Chunk<void> rowOffsets, std::size_t numRowOffsets)
	{
		mMatrixAccessor.bind(std::move(data), std::move(columnOffsets), numColOffsets, std::move(rowOffsets), numRowOffsets);
		mVariableDataSet = true;
	}

	void Array::resize(std::size_t count)
	{
		mVariableDataSet ? mMatrixAccessor.resize(count) : mAccessor.resize(count);
	}

	void Array::bind(FixedArray<Data<8>, void> accessor)
	{
		mAccessor = std::move(accessor);
		mVariableDataSet = false;
	}

	void Array::bindVariable(VariableMatrix< Data<8>, Data<8>, void> accessor)
	{
		mMatrixAccessor = std::move(accessor);
		mVariableDataSet = true;
	}

	void Array::setEncoding(cio::Encoding layout)
	{
		mEncoding = layout;
		mAccessor.setElementSize(layout.computeRequiredBytes());
		mMatrixAccessor.getColumnData().setElementSize(layout.computeRequiredBytes());
	}

	cio::Encoding Array::getEncoding() const
	{
		return mEncoding;
	}

	Array Array::slice(unsigned index)
	{
		Array result;
		result.mType = mType;
		result.mEncoding = mEncoding;
		result.mAccessor = FixedArray<Data<8>, void>::slice(mAccessor, index, 1);
		return result;
	}

	Array Array::slice(unsigned index, unsigned count)
	{
		Array result;
		result.mType = mType;
		result.mEncoding = mEncoding;
		result.mAccessor = FixedArray<Data<8>, void>::slice(mAccessor, index, count);
		return result;
	}

	std::string Array::getText(std::size_t idx) const
	{
		std::string text;
		if (mVariableDataSet)
		{
			if (mMatrixAccessor.size() > idx)
			{
				if (mMatrixAccessor.getRowCount() == 1)
				{
					std::vector<Data<8>> vec = mMatrixAccessor.getColumnData().get(idx);
					for (auto v : vec)
					{
						text.append(v.text);
					}
				}
				else
				{
					std::vector<std::vector<Data<8>>> vec = mMatrixAccessor.get(idx);
					for (auto v : vec)
					{
						for (auto vv : v)
						{
							text.append(vv.text);
						}
					}

				}

			}
		}
		return text;
	}

	std::string Array::getText(std::size_t idx, std::size_t element) const
	{
		// Gets the raw binary value
		std::string text;

		if (mVariableDataSet)
		{
			if (mMatrixAccessor.size() > idx)
			{
				//In the case of only one matrix row, as in a variable array, get a single value
				if (mMatrixAccessor.getRowCount() == 1)
				{
					Data<8> tmp = mMatrixAccessor.getColumnData().get(idx, element);
					text = tmp.text;
				}
				//If there are multiple rows, in the case of a true variable matrix, get the 
				//element at the idx/element location (column/row)
				else
				{
					std::vector<Data<8>> vec = mMatrixAccessor.get(idx, element);
					for (auto v : vec)
					{
						text.append(v.text);
					}
				}
			}
		}
		else if (this->isFixedByteAligned() && idx < mAccessor.size() && element < mAccessor.getColumnCount())
		{
			std::uint64_t bits = this->getBinary<std::uint64_t>(idx, element);
			switch (mType)
			{
				case cio::Type::Boolean:
					text = (bits != 0) ? "true" : "false";
					break;

				case cio::Type::Integer:
#if 0
					if (mEncoding.isSigned())
					{
						unsigned signBit = mEncoding.computeRequiredBits() - 1;
						std::int64_t value = bits;
						bool negative = (bits >> signBit) != 0;
						if (negative)
						{
							if (mEncoding.hasSignBit())
							{
								value = -static_cast<std::int64_t>(bits & ((1ull << signBit) - 1));
							}
							else // two's complement, need to sign extend and then we're good
							{
								std::int64_t extendMask = ~((1ull << signBit) - 1);
								value = static_cast<std::int64_t>(bits) | extendMask;
							}
						}

						text = std::to_string(value);
					}
					else
					{
						text = std::to_string(bits);
					}
#else
					{
						std::size_t numberOfBits = mEncoding.computeRequiredBits();
						if (numberOfBits == 8u)
						{
							if (mEncoding.isSigned())
							{
								int8_t value = getBinary<int8_t>(idx, element);
								text = std::to_string(value);
							}
							else
							{
								uint8_t value = getBinary<uint8_t>(idx, element);
								text = std::to_string(value);
							}
						}
						else if (numberOfBits == 16u)
						{
							if (mEncoding.isSigned())
							{
								int16_t value = getBinary<int16_t>(idx, element);
								text = std::to_string(value);
							}
							else
							{
								uint16_t value = getBinary<uint16_t>(idx, element);
								text = std::to_string(value);
							}
						}
						else if (numberOfBits == 32u)
						{
							if (mEncoding.isSigned())
							{
								int32_t value = getBinary<int32_t>(idx, element);
								text = std::to_string(value);
							}
							else
							{
								uint32_t value = getBinary<uint32_t>(idx, element);
								text = std::to_string(value);
							}
						}
						else if (numberOfBits == 64u)
						{
							if (mEncoding.isSigned())
							{
								int64_t value = getBinary<int64_t>(idx, element);
								text = std::to_string(value);
							}
							else
							{
								uint64_t value = getBinary<uint64_t>(idx, element);
								text = std::to_string(value);
							}
						}
					}
#endif
					break;

				case cio::Type::Real:
				{
					if (mEncoding == cio::Encoding::create<float>())
					{
						float value = 0.0f;
						std::memcpy(&value, &bits, 4);

						char tmp[16];
						std::snprintf(tmp, 16, "%.7g", value);
						text = tmp;
					}
					else if (mEncoding == cio::Encoding::create<double>())
					{
						double value = 0.0;
						std::memcpy(&value, &bits, 8);
						char tmp[32];
						std::snprintf(tmp, 32, "%.15lg", value);
						text = tmp;
					}
				}
				break;

				default:
					// do nothing
					break;
			}

			// Value was unhandled, so just print out hex bytes
			if (text.empty())
			{
				std::size_t length = mEncoding.computeRequiredBytes();
				const std::uint8_t *ptr = reinterpret_cast<const std::uint8_t *>(&bits);
				text.resize(length * 2 + 2);
				text[0] = '0';
				text[1] = 'x';

				for (unsigned i = 0; i < length; ++i)
				{
					std::uint8_t byte = ptr[i];
					text[2 + 2 * i] = cio::printHexDigit(byte >> 4);
					text[3 + 2 * i] = cio::printHexDigit(byte & 0xFF);
				}
			}
		}
		return text;
	}

	bool Array::setText(const std::string &text, std::size_t idx, std::size_t element)
	{
		return this->setText(text.c_str(), idx, element);
	}

	//TODO - sizing and memory data
	bool Array::setText(const std::string &stext, std::size_t idx)
	{
		bool success = false;
		if (mVariableDataSet)
		{
			if (mMatrixAccessor.size() > idx)
			{
				const char *text = stext.c_str();
				std::size_t len = stext.length();
				if (mMatrixAccessor.getRowCount() == 1)
				{
					std::vector<Data<8>> vec(len);
					for (std::size_t v = 0; v < len; ++v)
					{
						Data<8> tmp = {};
						std::memcpy(&tmp, text++, 1);
						vec[v] = tmp;
					}
					mMatrixAccessor.getColumnData().set(idx, vec);
					success = true;
				}
			}
		}
		return success;
	}

	bool Array::setText(const std::string &text)
	{
		return this->setText(text.c_str(), 0, 0);
	}

	bool Array::setText(const char *text, std::size_t idx, std::size_t element)
	{
		bool success = false;
		std::uint64_t bits = 0;
		std::size_t len = std::strlen(text);

		if (mVariableDataSet)
		{
			if (mMatrixAccessor.size() > idx)
			{
				if (mMatrixAccessor.getRowCount() == 1)
				{
					Data<8> tmp = {};
					std::memcpy(&tmp, text, sizeof(tmp));
					mMatrixAccessor.getColumnData().set(idx, element, tmp);
					success = true;
				}
				else
				{
					std::vector<Data<8>> vec(len);
					for (std::size_t v = 0; v < len; ++v)
					{
						Data<8> tmp = {};
						std::memcpy(&tmp, text++, 1);
						vec[v] = tmp;
					}
					mMatrixAccessor.set(idx, element, vec);
					success = true;
				}
			}
		}
		else if (this->isFixedByteAligned() && idx < mAccessor.size() && element < mAccessor.getColumnCount())
		{
			// Special case, binary array ftw
			if (text[0] && text[0] == '0' && text[1] && text[1] == 'x')
			{
				std::uint8_t *ptr = reinterpret_cast<std::uint8_t *>(&bits);
				success = true;
				for (unsigned i = 0; i < 8 && text[i * 2 + 2] && text[i * 2 + 3]; ++i)
				{
					char hi = text[i * 2 + 2];
					char lo = text[i * 2 + 3];
					if (std::isxdigit(hi) && std::isxdigit(lo))
					{
						ptr[i] = cio::parseByte(hi, lo);
					}
					else
					{
						success = false;
						break;
					}
				}
			}
			else
			{
				switch (mType)
				{
					case cio::Type::Boolean:
					{
						if (cio::compareTextCaseInsensitive(text, "true") == 0)
						{
							bits = true;
							success = true;
						}
						else if (cio::compareTextCaseInsensitive(text, "false") == 0)
						{
							bits = false;
							success = true;
						}
						else
						{
							char *end;
							unsigned long long parsed = std::strtoull(text, &end, 10);
							bits = parsed;
							success = (end - text) == len;
						}
					}
					break;

					case cio::Type::Integer:
					{
						char *end;
						if (mEncoding.isSigned())
						{
							long long parsed = std::strtoll(text, &end, 10);
							success = (end - text) == len;
							if (success)
							{
								if (mEncoding.hasSignBit() && parsed < 0)
								{
									bits = static_cast<std::uint64_t>(-parsed) | (1ull << (mEncoding.computeRequiredBits() - 1));
								}
								else
								{
									bits = parsed;
								}
							}
						}
						else
						{
							char *end;
							unsigned long long parsed = std::strtoull(text, &end, 10);
							bits = parsed;
							success = (end - text) == len;
						}
					}
					break;

					case cio::Type::Real:
					{
						char *end;
						if (mEncoding == cio::Encoding::create<float>())
						{
							float parsed = std::strtof(text, &end);
							success = (end - text) == len;
							std::memcpy(&bits, &parsed, 4);
						}
						else if (mEncoding == cio::Encoding::create<double>())
						{
							double parsed = std::strtod(text, &end);
							success = (end - text) == len;
							std::memcpy(&bits, &parsed, 8);
						}

						break;
					}
					break;

					default:
						// should not happen
						success = false;
						break;
				}
			}

			if (success)
			{
				success = this->setBinary(bits, idx, element);
			}
		}

		return success;
	}

	void Array::setInitialOffset(std::size_t bytes)
	{
		mVariableDataSet ? mMatrixAccessor.getColumnData().setOffset(bytes) : mAccessor.setOffset(bytes);
	}

	std::size_t Array::getInitialOffset() const
	{
		return mVariableDataSet ? mMatrixAccessor.getColumnData().getOffset() : mAccessor.getOffset();
	}

	void Array::setStride(std::size_t bytes)
	{
		if (!mVariableDataSet)
			mAccessor.setStride(bytes);
	}

	std::size_t Array::getStride() const
	{
		return mVariableDataSet ? 0 : mAccessor.getStride();
	}

	std::size_t Array::getStride(std::size_t rowIdx) const
	{
		return mVariableDataSet ? mMatrixAccessor.getColumnData().getStride(rowIdx) : 0;
	}

	std::size_t Array::getColumns(std::size_t rowIdx) const
	{
		std::size_t columns = 0;
		if (mVariableDataSet)
		{
			if (mMatrixAccessor.getRowOffsets().data())
			{
				columns = mMatrixAccessor.getColumns(rowIdx);
			}
			else
			{
				columns = mMatrixAccessor.getColumnData().getCount(rowIdx);
			}
		}
		else
		{
			columns = mAccessor.getColumnCount();
		}
		return columns;
	}

	std::size_t Array::getElementStride() const
	{
		return mVariableDataSet ? mMatrixAccessor.getColumnData().getElementSize() : mAccessor.getElementStride();
	}

	void Array::setElementStride(std::size_t bytes)
	{
		mVariableDataSet ? mMatrixAccessor.getColumnData().setElementSize(bytes) : mAccessor.setElementStride(bytes);
	}

	void Array::setElementCount(std::size_t count)
	{
		if (!mVariableDataSet)
			mAccessor.setColumnCount(count);
	}

	cio::Type Array::getType() const
	{
		return mType;
	}

	void Array::setType(cio::Type type)
	{
		mType = type;
	}
}
