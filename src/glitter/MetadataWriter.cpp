/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "MetadataWriter.h"

#include "Catalog.h"
#include "Collection.h"
#include "FeatureIdMap.h"

#include <fx/gltf.h>
#include <nlohmann/json.hpp>

namespace glitter
{
	nlohmann::json &MetadataWriter::getOrCreateFeatureTable(nlohmann::json &extensionRoot, const geas::Label &name) const
	{
		return extensionRoot["featureTables"][name];
	}

	void MetadataWriter::updateAttribute(nlohmann::json &table, const geas::Attribute &defn, std::uint32_t bufferViewIdx) const
	{
		nlohmann::json &properties = table["properties"];
		nlohmann::json &attr = properties[defn.getLabel()];
		attr["bufferView"] = bufferViewIdx;
	}

	void MetadataWriter::updateFeatureCount(nlohmann::json &table, std::size_t count) const
	{
		table["count"] = count;
	}

	void MetadataWriter::updateTableFeatureType(nlohmann::json &table, const geas::Label &featureType) const
	{
		table["class"] = featureType;
	}

	void MetadataWriter::updateFeatureMapper(nlohmann::json &extensionRoot, const fx::gltf::Primitive &primitive, const Collection &collection, const FeatureIdMap &map, std::uint32_t layerIndex) const
	{
		// First, ensure the relevant JSON node exists and has enough layers
		nlohmann::json &attrs = extensionRoot["featureIdAttributes"];
		if (attrs.empty())
		{
			attrs = nlohmann::json::array({});
		}

		while (layerIndex >= attrs.size())
		{
			attrs.push_back(nlohmann::json::object({}));
		}

		nlohmann::json &attrParent = attrs[layerIndex];
		
		if (map.table != -1)
		{
			const Node *ds = collection.getNodes().get(map.table);
			if (ds && !ds->getLabel().empty())
			{
				attrParent["featureTable"] = ds->getLabel();
			}
			else
			{
				attrParent.erase("featureTable");
			}
		}
		else
		{
			attrParent.erase("featureTable");
		}

		nlohmann::json &attrDef = attrParent["featureIds"];

		// If there's no accessor, we have a procedural mapping
		if (map.accessor == -1)
		{
			attrDef.erase("attribute");
			attrDef["constant"] = map.offset;
			attrDef["divisor"] = map.divisor;
		}
		else
		{
			// look up an attribute name for the accessor in the primitive set
			attrDef.erase("constant");
			attrDef.erase("divisor");

			bool found = false;
			for (const std::pair<std::string, std::uint32_t> &item : primitive.attributes)
			{
				if (item.second == map.accessor)
				{
					attrDef["attribute"] = item.first;
					found = true;
					break;
				}
			}

			if (!found)
			{
				attrDef.erase("attribute");
			}
		}
	}
}
