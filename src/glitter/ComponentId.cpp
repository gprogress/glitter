/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "ComponentId.h"

#include <sstream>

namespace glitter
{
	std::string toString(const ComponentId &id)
	{
		std::ostringstream ss;
		ss << id;
		return ss.str();
	}

	std::ostream &operator<<(std::ostream &s, const ComponentId &id)
	{
		if (id.base != ComponentType::None)
		{
			s << id.base << '/' << id.item;
		}

		if (id.type != id.base)
		{
			// TODO work on ID layout to handle vertex ID, attribute ID, and such more cleanly

			s << '/' << id.type << '/' << id.element;
		}

		return s;
	}
}