/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_SCALAR_H
#define GLITTER_SCALAR_H

#include <cio/Types.h>
#include <cio/Chunk.h>

#include <cstring>

namespace glitter
{
		template <typename T, typename U /*= void*/>
		class Scalar
		{
		public:
			/**
			* Constructs an accessor of a different type with the values in the given accessor and
			* uses the same referenced data of the given accessor.
			*
			* @param in The given accessor that contains data and values needed to construct new accessor.
			* @return An accessor that references the same data as the given accessor.
			*/
			template <typename V, typename W>
			inline static Scalar<T, U> reference(Scalar<V, W>& in);

			/**
			* Constructs a read-only accessor of a different type with the values in the given accessor and
			* the referenced data of the given accessor.
			*
			* @param in The given read-only accessor that contains data and values needed to construct new accessor.
			* @return A read-only accessor that references the same data as the given accessor.
			*/
			template <typename V, typename W>
			inline static Scalar<const T, const U> reference(const Scalar<V, W>& in);

			/**
			* Construct a new accessor with size and offset of zero, stride and element size as the
			* size of the data type.
			*/
			inline Scalar() noexcept;

			/**
			* Construct an accessor with the given data, size is the number of
			* logical data elements in the data, offset is set to zero.  Stride and element
			* size are the size of the data type of the accessor.
			*
			* @param data The data that the accessor is going to parse and discern.
			*/
			inline explicit Scalar(cio::Chunk<U> data);

			/**
			* Construct an accessor with the given data, size, offset, stride and element size.
			*
			* @param data The data that the accessor is going to parse.
			* @param count The number of elements.
			* @param offset The number of bytes from the beginning of the data to begin reading the data, default of zero.
			* @param stride The number of bytes between each element, default is the size of the data type.
			* @param elementSize The size is bytes of each element, default is the size of the data type.
			*/
			inline Scalar(cio::Chunk<U> data, std::size_t count, std::size_t offset = 0, std::size_t stride = sizeof(cio::Value<T>), std::size_t elementSize = sizeof(cio::Value<T>));

			/**
			* Construct a default, compiler generated, read-only copy of the accessor.
			*
			* @param in The read-only accessor to copy.
			*/
			inline Scalar(const Scalar<T, U>& in) = default;

			/**
			* Move the data and copy the attributes into a new instance of this class.
			*
			* @param in The accessor to copy.
			*/
			inline Scalar(Scalar<T, U>&& in) noexcept;

			/**
			* Copy the data and attributes into a new instance of this class.
			*
			* @param in The read-only accessor to copy.
			*/
			template <typename V, typename W>
			inline Scalar(const Scalar<V, W>& in);

			/**
			* Move the data and copy the attributes into a new instance of this class.
			*
			* @param in The accessor to copy.
			*/
			template <typename V, typename W>
			inline Scalar(Scalar<V, W>&& in) noexcept;

			/**
			* Create a read-only copy of the given accessor.
			*
			* @param in The given accessor to copy.
			*/
			inline Scalar<T, U>& operator=(const Scalar<T, U>& in) = default;

			/**
			* Constructs copy of the given accessor.
			*
			*@param in The given accessor of a templated type to copy.
			*/
			inline Scalar<T, U>& operator=(Scalar<T, U>&& in) noexcept;

			/**
			* Default destructor.
			*/
			inline ~Scalar() noexcept = default;

			/**
			* Set the data in the accessor to the data given.  Inizializes the attributes,
			* size is the number of logical data elements and offset is zero, stride and
			* element size are set to the size of the data type of the accessor.
			*
			* @param data The data for the accessor to parse.
			*/
			inline void bind(cio::Chunk<U> data);

			/**
			* Retrieve a single value from the given location index in
			* the underlying data.
			*
			* @param idx The location of the desired value.
			* @return The value at the desired location.
			*/
			inline cio::Value<T> get(std::size_t idx) const;

			/**
			* Insert the given value into the given data location in the
			* underlying data structure.
			*
			* @param idx The index at which to place the given value.
			* @param value The value to insert at the given location.
			*/
			inline void set(std::size_t idx, cio::Value<T> value);

			/**
			* Add a new data location, with the given value at the
			* given index location.  This will resize the data
			* to make room for the new value.
			*
			* @param idx The index at which to insert the new data.
			* @param value The value to insert at the given location.
			*/
			inline void add(std::size_t idx, cio::Value<T> value);

			/**
			* Remove the value at the given index and remove the index
			* completely.  This will resize the data to adjust for the
			* index that has been deleted.
			*
			* @param idx The index to remove
			*/
			inline void remove(std::size_t idx);

			/**
			* Adjust the size of the underlying data.  This will copy
			* the original data into a new buffer, resize and
			* assign the data in the new buffer to the old buffer.  
			* You may add multiple indices at once but remove only one at
			* a time.  
			*
			* @param idx The index that has been added or removed.
			* @param remove A flag that indicates whether this index should be 
			* added or removed.
			*/
			inline void sizeAdjust(std::size_t idx, bool remove = false);

			/**
			* Gets the size of the elements that comprise the underlying data.
			*
			* @return The size of the data types that comprise the data.
			*/
			inline std::size_t getElementSize() const;

			/**
			* Sets the size of the elements within the data.
			*
			* @param count The size of the data type within the underlying data.
			*/
			inline void setElementSize(std::size_t count);

			/**
			* Get the offset, the location to begin reading calculated from the beginning
			* of the underlying data.
			*
			* @return The offset in bytes
			*/
			inline std::size_t getOffset() const;

			/**
			* Sets the offset, or the location from the beginning of the
			* underlying data array to begin parsing data.
			*
			* @param offset The size, in bytes, of the offset.
			*/
			inline void setOffset(std::size_t offset);

			/**
			* Gets the number of bytes from one element to the next.
			*
			* @return The size, in bytes, between elements.
			*/
			inline std::size_t getStride() const;

			/**
			* Set the size, in bytes, from one data element to the next.
			*
			* @param stride The number of bytes between one element and the next.
			*/
			inline void setStride(std::size_t stride);

			/**
			* Get the number of elements in the data array.
			*
			* @return The number of elements.
			*/
			inline std::size_t size() const;

			/**
			* Resize the accessor.
			*
			* @param length  The desired size of the underlying data.
			*/
			inline void resize(std::size_t length);

			/**
			* Clears the underlying data, set the size and offset to zero and set the
			* size of the stride and element size to the size of the data type.
			*/
			inline void clear();

			/**
			* Get the underlying chunk of data.
			*
			* @return The data the accessor is parsing.
			*/
			inline cio::Chunk<U>& getData();

			/**
			* Gets a read-only reference to the underlying chunk of data.
			*
			* @return the read-only reference to the underlying data.
			*/
			inline const cio::Chunk<U>& getData() const;

			/**
			* Sets the underlying data for the accessor.
			*
			* @param data The data to be moved to be owned by the accessor.
			*/
			inline void setData(cio::Chunk<U> data);

			/**
			* Gets a pointer to the underlying data.
			*
			* @return a pointer to the underlying data of templated type U.
			*/
			inline U* data();

			/**
			* Gets a const pointer to the underlying data.
			*
			* @return a const pointer to the data in the accessor.
			*/
			inline const U* data() const;

		private:
			/*  The data that the accessor is interested in parsing.  */
			cio::Chunk<U> mData;

			/*  The number of logical elements that the data contains.  */
			std::size_t mSize;

			/*  The offset in bytes from the beginning of the data */
			std::size_t mOffset;

			/** 
			 * The distance between data.  The size of the type times the size of the component for instance
			 * size of float * size of scalar (which is 1)
			 */
			std::size_t mStride;

			/** The size in bytes of the type multiplied by the number of components in a scalar (which is always 1) */
			std::size_t mElementSize;
		};
}

	/* Inline implementation */

namespace glitter
{
		template <typename T, typename U>
		template <typename V, typename W>
		inline Scalar<T, U> Scalar<T, U>::reference(Scalar<V, W>& in)
		{
			return Scalar<T, U>(in.getData().reference(), in.size(), in.getOffset(), in.getStride(), in.getElementSize());
		}

		template <typename T, typename U>
		template <typename V, typename W>
		inline Scalar<const T, const U> Scalar<T, U>::reference(const Scalar<V, W>& in)
		{
			return Scalar<const T, const U>(in.getData().reference(), in.size(), in.getOffset(), in.getStride(), in.getElementSize());
		}

		template <typename T, typename U>
		inline Scalar<T, U>::Scalar() noexcept :
			mSize(0),
			mOffset(0),
			mStride(sizeof(cio::Value<T>)),
			mElementSize(sizeof(cio::Value<T>))
		{
			// nothing more to do
		}

		template <typename T, typename U>
		inline Scalar<T, U>::Scalar(cio::Chunk<U> data) :
			mData(std::move(data)),
			mSize(mData.size() / sizeof(cio::Value<T>)),
			mOffset(0),
			mStride(sizeof(cio::Value<T>)),
			mElementSize(sizeof(cio::Value<T>))
		{
			// nothing more to do
		}

		template <typename T, typename U>
		inline Scalar<T, U>::Scalar(cio::Chunk<U> data, std::size_t count, std::size_t offset /* = 0 */, std::size_t stride /* = sizeof(T) */, std::size_t elementSize /* = sizeof(T) */) :
			mData(std::move(data)),
			mSize(count),
			mOffset(offset),
			mStride(stride),
			mElementSize(elementSize)
		{
			// nothing more to do
		}

		template <typename T, typename U>
		inline Scalar<T, U>::Scalar(Scalar<T, U>&& in) noexcept :
			mData(std::move(in.mData)),
			mSize(in.mSize),
			mOffset(in.mOffset),
			mStride(in.mStride),
			mElementSize(in.mElementSize)
		{
			in.clear();
		}

		template <typename T, typename U>
		template <typename V, typename W>
		inline Scalar<T, U>::Scalar(Scalar<V, W>&& in) noexcept :
			mData(std::move(in.getData())),
			mSize(in.size()),
			mOffset(in.getOffset()),
			mStride(in.getStride()),
			mElementSize(in.getElementSize())
		{
			in.clear();
		}

		template <typename T, typename U>
		template <typename V, typename W>
		inline Scalar<T, U>::Scalar(const Scalar<V, W>& in) :
			mData(in.getData()),
			mSize(in.size()),
			mOffset(in.getOffset()),
			mStride(in.getStride()),
			mElementSize(in.getElementSize())
		{
			// nothing more to do
		}


		template <typename T, typename U>
		inline Scalar<T, U>& Scalar<T, U>::operator=(Scalar<T, U>&& in) noexcept
		{
			if (this != &in)
			{
				mData = std::move(in.mData);
				mSize = in.mSize;
				mOffset = in.mOffset;
				mStride = in.mStride;
				mElementSize = in.mElementSize;
				in.clear();
			}

			return *this;
		}

		template <typename T, typename U>
		inline void Scalar<T, U>::bind(cio::Chunk<U> data)
		{
			mData = std::move(data);
			mSize = mData.size() / sizeof(cio::Value<T>);
			mOffset = 0;
			mStride = sizeof(cio::Value<T>);
			mElementSize = sizeof(cio::Value<T>);
		}

		template <typename T, typename U>
		inline cio::Value<T> Scalar<T, U>::get(std::size_t idx) const
		{
			T result = T();
			std::size_t loc = mOffset + mStride * idx;
			std::size_t chunk = loc / sizeof(cio::Value<U>);
			std::size_t off = loc % sizeof(cio::Value<U>);
			const U *data = &mData[chunk];
			std::memcpy(&result, reinterpret_cast<const cio::Value<U> *>(data) + off, mElementSize);
			return result;
		}

		template <typename T, typename U>
		inline void Scalar<T, U>::set(std::size_t idx, cio::Value<T> value)
		{
			if (idx >= mSize)
			{
				sizeAdjust(idx);
			}

			std::size_t loc = mOffset + mStride * idx;
			std::size_t chunk = loc / sizeof(cio::Value<U>);
			std::size_t off = loc % sizeof(cio::Value<U>);
			U *data = &mData[chunk];
			std::memcpy(reinterpret_cast<cio::Value<U> *>(data) + off, &value, mElementSize);
		}

		template <typename T, typename U>
		inline void Scalar<T, U>::add(std::size_t idx, cio::Value<T> value)
		{
			this->set(idx, value);
		}

		template <typename T, typename U>
		inline void Scalar<T, U>::remove(std::size_t idx)
		{
			if (idx < mSize)
				sizeAdjust(idx, true);
		}

		template <typename T, typename U>
		inline void Scalar<T, U>::sizeAdjust(std::size_t idx, bool remove)
		{
			std::size_t moveSize = remove ? 0 : mStride;
			std::size_t oldSize = remove ? mStride : 0;
			std::size_t dataLoc = mOffset + idx * mStride;
			std::size_t newSize = remove ? mSize - 1 : mSize + 1;
			newSize = newSize * mStride;

			//Create temp data
			cio::Chunk<U> tmp(newSize, mData.allocator());
			std::memcpy(static_cast<cio::Value<U> *>(tmp.data()), static_cast<cio::Value<U> *>(mData.data()), dataLoc);
			std::memcpy(static_cast<cio::Value<U> *>(tmp.data()) + dataLoc + moveSize, static_cast<cio::Value<U> *>(mData.data()) + dataLoc + oldSize, mData.size() - (dataLoc + oldSize));
			mData = std::move(tmp);
			mSize = newSize / mStride;
		}

		template <typename T, typename U>
		inline std::size_t Scalar<T, U>::getOffset() const
		{
			return mOffset;
		}

		template <typename T, typename U>
		inline void Scalar<T, U>::setOffset(std::size_t offset)
		{
			mOffset = offset;
		}

		template <typename T, typename U>
		inline std::size_t Scalar<T, U>::getStride() const
		{
			return mStride;
		}

		template <typename T, typename U>
		inline std::size_t Scalar<T, U>::getElementSize() const
		{
			return mElementSize;
		}

		template <typename T, typename U>
		inline void Scalar<T, U>::setElementSize(std::size_t count)
		{
			mElementSize = count;
		}

		template <typename T, typename U>
		inline void Scalar<T, U>::setStride(std::size_t stride)
		{
			mStride = stride;
		}

		template <typename T, typename U>
		inline std::size_t Scalar<T, U>::size() const
		{
			return mSize;
		}

		template <typename T, typename U>
		inline void Scalar<T, U>::resize(std::size_t length)
		{
			mSize = length;
		}

		template <typename T, typename U>
		inline void Scalar<T, U>::clear()
		{
			mData.clear();
			mSize = 0;
			mOffset = 0;
			mStride = sizeof(cio::Value<T>);
			mElementSize = sizeof(cio::Value<T>);
		}

		template <typename T, typename U>
		inline cio::Chunk<U>& Scalar<T, U>::getData()
		{
			return mData;
		}

		template <typename T, typename U>
		inline const cio::Chunk<U>& Scalar<T, U>::getData() const
		{
			return mData;
		}

		template <typename T, typename U>
		inline void Scalar<T, U>::setData(cio::Chunk<U> data)
		{
			mData = std::move(data);
		}

		template <typename T, typename U>
		inline U* Scalar<T, U>::data()
		{
			return mData.data();
		}

		template <typename T, typename U>
		inline const U* Scalar<T, U>::data() const
		{
			return mData.data();
		}
}

#endif
