/*=====================================================================================================================
 * Copyright 2022-2023, Geometric Progress LLC
 *
 * This file is part of the Geosemantic Application Schema Library (Geas).
 *
 * Geas is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Geas is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Geas.  If not, see <https://www.gnu.org/licenses/>
=====================================================================================================================*/
#include <geas/model/LasReader.h>

#include <geas/data/Repository.h>

#include <cio/Path.h>

#include <iostream>
#include <vector>

int main(int argc, char **argv)
{
	std::vector<cio::Path> files;

	int failed = 0;

	for (int i = 1; i < argc; ++i)
	{
		files.emplace_back(argv[i]);
	}

	for (const cio::Path &path : files)
	{
		try
		{
			geas::data::Repository repository;
			if (path.matchesExtension("las"))
			{
				geas::model::LasReader reader;
				reader.loadRepository(repository, path);
			}
		}
		catch (const std::exception &e)
		{
			std::cout << "Failed to process " << path << ": " << e.what();
			++failed;
		}
	}

	return failed;
}