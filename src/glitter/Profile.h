/*=====================================================================================================================
 * Copyright 2022-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_PROFILE_H
#define GLITTER_PROFILE_H

#include "Types.h"

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace glitter
{
	/**
	 * The Profile is a class used to mediate between a general Geas Application Schema and specific 3D concepts such as geometry types,
	 * geometry primitives, and vertex attributes.
	 */
	class GLITTER_ABI Profile
	{
		public:
			/**
			 * Gets the metaclass for this class.
			 *
			 * @return the metaclass for this class
			 */
			static const cio::Class<Profile> &getDeclaredMetaclass() noexcept;

			/**
			 * Default constructor.
			 */
			Profile() noexcept;

			/**
			 * Copy constructor.
			 *
			 * @param in The profile to copy
			 */
			Profile(const Profile &in);

			/**
			 * Move constructor.
			 *
			 * @param in The profile to move
			 */
			Profile(Profile &&in) noexcept;

			/**
			 * Copy assignment.
			 *
			 * @param in The profile to copy
			 * @return this profile
			 */
			Profile &operator=(const Profile &in);

			/**
			 * Move assignment.
			 *
			 * @param in The profile to move
			 * @return this profile
			 */
			Profile &operator=(Profile &&in) noexcept;

			/**
			 * Destructor.
			 */
			virtual ~Profile() noexcept;

			/**
			 * Clears the profile state.
			 */
			virtual void clear() noexcept;

			/**
			 * Gets the runtime metaclass for this object instance.
			 *
			 * @return the runtime metaclass for this object
			 */
			virtual const cio::Metaclass &getMetaclass() const noexcept;

			// New methods introduced by Profile

			/**
			* Apply to the schema
			*
			* @param schema The schema
			*/
			virtual void applyToSchema(geas::Application &schema) const;

			/**
			* Match from the schema.
			*
			* @param schema The schema
			*/
			virtual void matchFromSchema(const geas::Application &schema);

			/**
			* Get a default attribute.
			*
			* @param attribute An attribute
			* @return a default attribute
			*/
			const geas::Attribute *getAttribute(AttributeType attribute) const;

			/**
			* Get a default geometry.
			*
			* @param geometry A geometry
			* @param primitive A primitive
			* @return the default geometry
			*/
			const geas::Geometry *getGeometry(GeometryType geometry, Primitive primitive) const;

		private:
			/** The metaclass for this class. */
			static cio::Class<Profile> sMetaclass;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
