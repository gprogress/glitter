/*=====================================================================================================================
 * Copyright 2022-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_READER_H
#define GLITTER_READER_H

#include "Types.h"

#include <cio/Types.h>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace glitter
{
	/**
	 * The Reader provides the base interface for loading data formats for catalogs, collections, and resources.
	 *
	 * When possible, readers should try to allow any resource type and protocol - relying on the associated protocol factory to mediate this -
	 * and primarily focus on the decoding of the data from generic cio::Input streams.
	 */
	class GLITTER_ABI Reader
	{
		public:
			/**
			 * Gets the metaclass for this class.
			 *
			 * @return the metaclass for this class
			 */
			static const cio::Class<Reader> &getDeclaredMetaclass() noexcept;

			/**
			 * Default constructor.
			 * Sets up to use the default protocol factory.
			 */
			Reader() noexcept;

			/**
			 * Copy constructor.
			 *
			 * @param in The reader to copy
			 */
			Reader(const Reader &in);

			/**
			 * Move constructor.
			 *
			 * @param in The reader to move
			 */
			Reader(Reader &&in) noexcept;

			/**
			 * Copy assignment.
			 *
			 * @param in The reader to copy
			 * @return this reader
			 */
			Reader &operator=(const Reader &in);

			/**
			 * Move assignment.
			 *
			 * @param in The reader to move
			 * @return this reader
			 */
			Reader &operator=(Reader &&in) noexcept;

			/**
			 * Destructor.
			 */
			virtual ~Reader() noexcept;

			/**
			 * Clears the state of this reader.
			 */
			virtual void clear() noexcept;

			/**
			 * Gets the runtime metaclass for this object instance.
			 *
			 * @return the runtime metaclass for this object
			 */
			virtual const cio::Metaclass &getMetaclass() const noexcept;

			/**
			 * Loads a repository from the given URI.
			 *
			 * The base implementation attempts to open the given path as an input and then
			 * calls loadRepositoryFromInput.
			 *
			 * @param repo The repository to update
			 * @param path The path to the main resource identifying the input
			 */
			virtual void loadRepository(Repository &repo, const cio::Path &path);

			/**
			 * Loads a repository from the given input stream.
			 *
			 * The base implementation attempts to load a collection from the given input,
			 * and then adds it to the given repository.
			 *
			 * @param repo The repository to update
			 * @param input The input to read
			 */
			virtual void loadRepositoryFromInput(Repository &repo, cio::Input &input);

			/**
			 * Loads a catalog from the given URI.
			 *
			 * The base implementation attempts to open the given path as an input and then
			 * calls loadCatalogFromInput.
			 *
			 * @param catalog the catalog to update
			 * @param path The path to the main resource identifying the input
			 */
			virtual void loadCatalog(Catalog &catalog, const cio::Path &path);

			/**
			 * Loads a catalog from the given input stream.
			 *
			 * The base implementation throws an exception indicating this operation is not supported.
			 *
			 * @param catalog the catalog to update
			 * @param input The input to read
			 */
			virtual void loadCatalogFromInput(Catalog &catalog, cio::Input &input);

			/**
			 * Loads a collection from the given URI.
			 *
			 * The base implementation attempts to open the given path as an input and then
			 * calls loadCollectionFromInput.
			 *
			 * @param collection The collection to update
			 * @param path The path to the main resource identifying the input
			 */
			virtual void loadCollection(Collection &collection, const cio::Path &path);

			/**
			 * Loads a collection from the given input stream.
			 *
			 * The base implementation attempts to load a Catalog from the input, then attempts
			 * to load a Node from the input.
			 *
			 * @param collection The collection to update
			 * @param input The input to read
			 */
			virtual void loadCollectionFromInput(Collection &collection, cio::Input &input);

			/**
			 * Loads a node from the given URI.
			 *
			 * The base implementation attempts to open the given path as an input and then
			 * calls loadCollectionFromInput.
			 *
			 * @param node the node to update
			 * @param path The path to the main resource identifying the input
			 */
			virtual void loadNode(Node &node, const cio::Path &path);

			/**
			 * Loads a node from the given input stream.
			 *
			 * The base implementation throws an exception indicating this operation is not supported.
			 *
			 * @param node the node to update
			 * @param input The input to read
			 */
			virtual void loadNodeFromInput(Node &node, cio::Input &input);

			/**
			 * Gets the protocol factory in use to work with URI protocols.
			 *
			 * @return the protocol factory in use
			 */
			const cio::ProtocolFactory *getProtocols() const noexcept;

			/**
			 * Sets the protocolf actory to use to work with URI protocols.
			 * If nullptr is provided, this disables the use of protocols.
			 *
			 * @param protocols The protocol factory to use
			 */
			void setProtocols(const cio::ProtocolFactory *protocols) noexcept;

		protected:
			/** The protocol factory to use when necessary. */
			const cio::ProtocolFactory *mProtocols;

		private:
			/** The metaclass for this class. */
			static cio::Class<Reader> sMetaclass;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
