/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "MetadataReader.h"

#include "Collection.h"
#include "Column.h"
#include "EntityMapper.h"
#include "FeatureIdMap.h"
#include "Model.h"
#include "Table.h"

#include <cio/Encoding.h>
#include <cio/Primitive.h>

#include <geas/Application.h>
#include <geas/Attribute.h>
#include <geas/Entity.h>
#include <geas/json/OwtSchemaReader.h>

#include <fx/gltf.h>
#include <nlohmann/json.hpp>

#include <sstream>
#include <fstream>

namespace glitter
{
	MetadataReader::MetadataReader()
	{
		// nothing more to do
	}

	MetadataReader::MetadataReader(const MetadataReader &in) = default;

	MetadataReader &MetadataReader::operator=(const MetadataReader &in) = default;

	MetadataReader::~MetadataReader() noexcept = default;

	void MetadataReader::loadFeatureTable(Model &model, const geas::Label &nodeName, const nlohmann::json &featureTable)
	{
		std::shared_ptr<geas::Application> schema = model.getOrCreateCatalog()->getApplicationSchema();
		Collection *collection = model.getOrCreateEntityCollection();

		geas::Index nextIndex = collection->getNodes().size() + 1; // geas::Index 0 is "invalid" (TODO: reconsider?)

		Node *node = collection->getNodes().getOrCreate(nextIndex, nodeName, geas::Code(), cio::Path());
		geas::Label entityTypeName;

		std::uint32_t objectCount = 0;
		
		auto nn = featureTable.find("class");
		if (nn != featureTable.end())
		{
			entityTypeName = nn->get<std::string>();
		}

		const geas::Entity *entity = nullptr;
		if (!entityTypeName.empty())
		{
			entity = schema->getEntities().get(entityTypeName);
		}

		auto cc = featureTable.find("count");
		if (cc != featureTable.end())
		{
			objectCount = cc->get<std::uint32_t>();
		}

		std::unique_ptr<EntityMapper> provider(new EntityMapper());
		provider->setDefaultEntityType(entity);

		std::unique_ptr<Table> table(new Table());
		table->resize(objectCount);

		std::vector<geas::Attribute> attributes;
		std::vector<std::shared_ptr<Provider>> values;

		auto aa = featureTable.find("properties");
		if (aa != featureTable.end() && aa->is_object())
		{
			// Each key is a field name reference the entity attribute
			for (auto ii = aa->begin(); ii != aa->end(); ++ii)
			{
				geas::Label attrName = ii.key();

				geas::Attribute tmp;
				tmp.setLabel(std::move(attrName));
				attributes.emplace_back(std::move(tmp));

				const geas::Attribute &attribute = attributes.back();
				std::shared_ptr<Column> column(new Column());
				Array value;
				
				// TODO update for string and array views and set up actual buffers
				auto bb = ii->find("bufferView");
				if (bb != ii->end())
				{
					std::uint32_t bufferView = bb->get<std::uint32_t>();

					//value.setEncoding(attribute.getEncoding());
					value.setType(attribute.getValueType());
					/*
					cio::VariantInterval elements = attribute.getRange();
					
					if (elements.lower < elements.upper || cardinality.lower < cardinality.upper)
					{
						if (attribute.getValueType() == cio::Type::Text)
						{
							auto sob = ii->find("stringOffsetBufferView");
							if (sob != ii->end())
							{
								std::uint32_t stringOffsetBufferView = sob->get<std::uint32_t>();
								if (cardinality.upper == 1)
								{
									value.setVariableBuffer(model.getBufferView(bufferView), model.getBufferView(stringOffsetBufferView), value.size());
								}
								else
								{
									auto aob = ii->find("arrayOffsetBufferView");
									if (aob != ii->end())
									{
										std::uint32_t arrayOffsetBufferView = aob->get<std::uint32_t>();
										std::uint32_t stringCount = 0; // TODO how do we calculate this?
										value.setVariableBuffer(model.getBufferView(bufferView), model.getBufferView(stringOffsetBufferView), stringCount, model.getBufferView(arrayOffsetBufferView), value.size());
									}
								}
							}
						}
						else
						{
							auto aob = ii->find("arrayOffsetBufferView");
							if (aob != ii->end())
							{
								std::uint32_t arrayOffsetBufferView = aob->get<std::uint32_t>();
								value.setVariableBuffer(model.getBufferView(bufferView), model.getBufferView(arrayOffsetBufferView), value.size());
							}
						}
					} 
					else
					{
						// we have a fixed access pattern and can work with that
						value.setFixedBuffer(model.getBufferView(bufferView), elements.lower * cardinality.lower);
					}

					// feature attributes don't support strided access yet I don't think, so always pack as tightly as possible
					std::size_t elementBytes = value.getEncoding().computeRequiredBytes();
					value.setElementStride(elementBytes);
					value.setStride(elementBytes * value.getElementCount());
					value.resize(objectCount);

					column->setArray(std::move(value));

					values.emplace_back(std::move(column));*/
				}
			}
		}

		table->setColumns(std::move(values));
		provider->setProvider(std::move(table));
		node->setProvider(std::move(provider));
	}

	void MetadataReader::loadFeatureTables(Model &model, const nlohmann::json &format)
	{
		for (auto ii = format.begin(); ii != format.end(); ++ii)
		{
			geas::Label nodeName = ii.key();
			this->loadFeatureTable(model, nodeName, *ii);
		}
	}
	
	void MetadataReader::loadFeatureMetadata(Model &model, const nlohmann::json &format, const cio::Path& baseDirectory)
	{
		// Ensure we have an catalog and applicaiton schema
		std::shared_ptr<Catalog> catalog = model.getOrCreateCatalog();
		std::shared_ptr<geas::Application> schema = catalog->getOrCreateApplicationSchema();
		std::shared_ptr<geas::Profiles> profiles = catalog->getOrCreateProfiles();

		// check for "schema" tag
		geas::json::OwtSchemaReader reader;
		reader.loadFeatureSchema(*schema, *profiles, format, baseDirectory);

		auto ff = format.find("featureTables");
		if (ff != format.end())
		{
			this->loadFeatureTables(model, *ff);
		}
	}

	void MetadataReader::loadFeatureMetadataFromText(Model &model, const std::string &text, const cio::Path& baseDirectory)
	{
		nlohmann::json document = nlohmann::json::parse(text);
		this->loadFeatureMetadata(model, document, baseDirectory);
	}

	void MetadataReader::loadFeatureMetadataFromFile(Model &model, const cio::Path &filePath)
	{
		nlohmann::json document;
		std::string nativePath = filePath.toNativeFile();
		std::ifstream stream(nativePath.c_str());
		if (!stream)
		{
			throw std::runtime_error("Could not open file for reading: " + filePath.get());
		}

		stream >> document;
		this->loadFeatureMetadata(model, document, filePath);
	}
	
	std::vector<FeatureIdMap> MetadataReader::loadFeatureIdMaps(const Collection &collection, const fx::gltf::Primitive &primitive, const nlohmann::json &format)
	{
		std::vector<FeatureIdMap> result;
		auto ii = format.find("featureIdAttributes");
		if (ii != format.end() && ii->is_array())
		{
			for (auto jj : *ii)
			{
				result.emplace_back(loadFeatureIdMap(collection, primitive, format));
			}
		}
		return result;
	}

	FeatureIdMap MetadataReader::loadSelectedFeatureIdMap(const Collection &collection, const fx::gltf::Primitive &primitive, std::uint32_t layer, const nlohmann::json &root)
	{
		FeatureIdMap result;
		auto ii = root.find("featureIdAttributes");
		if (ii != root.end() && ii->is_array())
		{
			const nlohmann::json &item = ii->at(layer);
			result = loadFeatureIdMap(collection, primitive, item);
		}
		return result;
	}
	
	FeatureIdMap MetadataReader::loadFeatureIdMap(const Collection &collection, const fx::gltf::Primitive &primitive, const nlohmann::json &format)
	{
		FeatureIdMap result;
		auto ft = format.find("featureTable");
		if (ft != format.end())
		{
			geas::Label tableName = ft->get<std::string>();
			auto ds = collection.getNodes().find(tableName);
			if (ds != collection.getNodes().end())
			{
				result.table = ds->getIndex();
			}
		}

		auto aa = format.find("featureIds");
		if (aa != format.end())
		{
			auto vv = aa->find("attribute");
			if (vv != aa->end())
			{
				std::string attribute = vv->get<std::string>();
				auto bb = primitive.attributes.find(attribute);
				if (bb != primitive.attributes.end())
				{
					result.accessor = bb->second;
				}
			}

			auto cc = aa->find("constant");
			if (cc != aa->end())
			{
				result.offset = cc->get<std::uint32_t>();
			}

			auto mm = aa->find("divisor");
			if (mm != aa->end())
			{
				result.divisor = mm->get<std::uint32_t>();
			}
		}

		return result;
	}

	std::uint32_t MetadataReader::findFeatureTableCount(const nlohmann::json &format) const
	{
		std::uint32_t count = 0;
		auto ii = format.find("featureIdAttributes");
		if (ii != format.end() && ii->is_array())
		{
			count = static_cast<std::uint32_t>(ii->size());
		}
		return count;
	}
}
