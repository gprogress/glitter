/*=====================================================================================================================
 * Copyright 2022-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_COLUMN_H
#define GLITTER_COLUMN_H

#include "Types.h"

#include "Provider.h"

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace glitter
{
	/**
	 * The Column implements the Provider interface to get and set attribute values
	 * using a single value array that provides access to a single attribute.
	 *
	 * Logically, this class can be viewed as a database column for a single attribute with each
	 * row representing a separate entity or geometry object.
	 */
	class GLITTER_ABI Column : public Provider
	{
		public:
			/**
			 * Gets the metaclass for this class.
			 *
			 * @return the metaclass
			 */
			static const cio::Class<Column> &getDeclaredMetaclass() noexcept;

			/**
			 * Construct the empty Column.
			 */
			Column() noexcept;

			/**
			 * Construct a deep copy of a Column.
			 *
			 * @param in The table to copy
			 */
			Column(const Column &in);

			/**
			 * Construct a Column by moving in the contents of an existing one.
			 *
			 * @param in The table to move
			 */
			Column(Column &&in) noexcept;

			/**
			 * Assign a deep copy of a Column.
			 * The existing table contents are lost.
			 *
			 * @param in The table to copy
			 * @return this table
			 */
			Column &operator=(const Column &in);

			/**
			 * Transfer the content of a Column into this one.
			 * The existing table contents are lost.
			 *
			 * @param in The table to move
			 * @return this table
			 */
			Column &operator=(Column &&in) noexcept;

			/**
			 * Destructor.
			 */
			virtual ~Column() noexcept override;

			/**
			 * Clears all table content.
			 */
			virtual void clear() noexcept override;

			/**
			 * Gets the runtime metaclass for this object.
			 * This class overrides to return the metaclass for Column.
			 *
			 * @return the metaclass for this object
			 */
			virtual const cio::Metaclass &getMetaclass() const noexcept override;

			/**
			 * Gets the number of logical objects (entities or geometry primitives) that have values in this value provider.
			 *
			 * @return the number of logical objects
			 */
			virtual geas::Index size() const override;

			/**
			 * Sets the number of logical objects (entities or geometry primitives) that have values in this value provider.
			 * This implementation allows any size value to be set, and will resize every underlying attribute array accordingly.
			 *
			 * @param count The desired number of logical objects
			 * @return the number of logical objects actually set
			 */
			virtual geas::Index resize(geas::Index count) override;

			/**
			 * Add new record for an object.
			 *
			 * @todo See LTF-679: this method should be adding missing attributes and setting default values.
			 *
			 * @param entity Used to select the default values for attributes
			 * @param geometry Used to set up geometric representation if applicable
			 * @return geas::Index of new record that would become the entity index
			 */
			virtual geas::Index create(const geas::Entity *entity = nullptr, const geas::Geometry *geometry = nullptr) override;

			/**
			 * Gets a datatype describing the properties of this provider in terms of size() and resize() for object counts.
			 * This identifies the size range as 0 to INT32_MAX.
			 *
			 * @return the properties describing how attributes work with this provider
			 */
			virtual const geas::Attribute *getSizeProperties() const noexcept override;

			/**
			 * Gets a datatype describing the properties of this provider in terms of handling attribute counts.
			 * This identifes the count range as [0, 1], but not variable length (all objects must have same count).
			 *
			 * @return the properties describing how attributes work with this provider
			 */
			virtual const geas::Attribute *getAttributeCountProperties() const noexcept override;

			/**
			 * Reports whether this data provider has a uniform attribute field layout.
			 * If true, this means every object has the same attributes in the same sequence.
			 * If false, each object may have a different sequence of attribute fields.
			 *
			 * The Column implementation always returns true as this is an inherent property of this class.
			 *
			 * @return always true since Column always has a uniform field layout
			 */
			virtual bool isUniformFieldLayout() const noexcept override;

			/**
			 * Gets the number of attribute definitions in this table.
			 *
			 * It does not matter which object you specify: all objects share the same attribute field layout.
			 *
			 * @param object The object index
			 * @return the number of attribute definition
			 */
			virtual geas::Index getAttributeCount(geas::Index object) const override;

			/**
			 * Gets the definition of a specific attribute.
			 *
			 * It does not matter which object or attribute you specify,
			 * this class always returns the same attribute.
			 *
			 * @param object The object index
			 * @param field The field index
			 * @return the attribute definition
			 */
			virtual const geas::Attribute *getAttribute(geas::Index object, geas::Index field) const override;

			/**
			 * Changes the definition of a specific attribute.
			 * This does NOT modify the array value content for the attribute, so the content may no longer conform to the definition.
			 *
			 * It does not matter which object or attribute you specify,
			 * this class will set the single managed attribute.
			 *
			 * @param object The object index
			 * @param field The attribute field index
			 * @param defn The new definition
			 */
			virtual void setAttribute(geas::Index object, geas::Index field, const geas::Attribute *defn) override;

			/**
			 * Adds a new attribute definition.
			 * This only succeeds if no attribute is currently set.
			 *
			 * @param object The object index
			 * @param defn The definition to add
			 * @return the attribute field index of the added definition
			 */
			virtual geas::Index addAttribute(geas::Index object, const geas::Attribute *defn) override;

			/**
			 * Removes a particular attribute definition.
			 * This also removes the value array for that definition.
			 * This will remove the single attribute on this class.
			 *
			 * @param object The object index
			 * @param field The field index of the attribute to remove
			 */
			virtual void removeAttribute(geas::Index object, geas::Index field) override;

			/**
			 * Finds the field number of an attribute with the specified label.
			 *
			 * It does not matter which object you specify: all objects share the same attribute.
			 *
			 * @param object The object index
			 * @param label The attribute label
			 * @return the attribute field number, or INT32_MIN if none was found
			 */
			virtual geas::Index findAttributeByLabel(geas::Index object, const geas::Label &label) const override;

			/**
			 * Finds the index of an attribute with the specified code.
			 *
			 * It does not matter which object you specify: all objects share the same attribute.
			 *
			 * @param object The object index
			 * @param code The attribute code
			 * @return the attribute index, or INT32_MIN if none was found
			 */
			virtual geas::Index findAttributeByCode(geas::Index object, const geas::Code &code) const override;

			/**
			 * Finds the field number of an attribute with the specified index.
			 *
			 * It does not matter which object you specify: all objects share the same attribute.
			 *
			 * @param object The object index
			 * @param idx The attribute index
			 * @return the attribute field number, or INT32_MIN if none was found
			 */
			virtual geas::Index findAttributeByIndex(geas::Index object, geas::Index idx) const override;

			/**
			 * Finds the field index of the first attribute that matches the given attribute.
			 * Any identifiers that are set on the attribute are used for the search.
			 *
			 * It does not matter which object you specify: all objects share the same attribute.
			 *
			 * @param object The object index
			 * @param attribute The attribute to match with as many fields set as known
			 * @return the attribute field index, or INT32_MIN if not found
			 */
			virtual geas::Index findMatchingAttribute(geas::Index object, const geas::Attribute &attribute) const override;

			/**
			 * Gets a view of the value(s) of the given object's attribute.
			 * This will return a view of the underlying attribute array for the given attribute field number,
			 * with the offset set to identify the value of the given object.
			 *
			 * If the attribute value has multiple components (such as position), the value array will be configured to provide
			 * a view such that the first value is the first component, the second value is the second component, and so on.
			 *
			 * @param object The index of the object in the table
			 * @param attr The attribute field number
			 * @return the value array view of the values of the given attribute for the given object
			 */
			virtual Array getValue(geas::Index object, geas::Index attr) const override;

			/**
			 * Sets the value(s) of a particular attribute on a particular object from the given value array.
			 * If the actual attribute has fewer values than the given array, the excess input values are ignored.
			 * If the actual attribute has more values than the given array, the remaining values are left unchanged.
			 *
			 * @param object The index of the object in the table
			 * @param attr The attribute field number
			 * @param value The array specifying the new value(s) of the attribute
			 * @return whether the update was successful
			 */
			virtual bool setValue(geas::Index object, geas::Index attr, const Array &value) override;

			/**
			 * Validates the current value for the given object and attribute field to see if it matches the given datatype constraint.
			 *
			 * The Column implementation overrides the base implementation to directly inspect the value in situ for performance.
			 *
			 * @param constraint The datatype specifying the constraint
			 * @param object The object index
			 * @param attr The attribute field index
			 * @return a fault describing validation failures, if any
			 */
			virtual  geas::AttributeFault validateCurrentValue(const geas::Attribute &constraint, geas::Index object, geas::Index attr) const override;

			/**
			 * Estimates the total memory use of this provider for data values, excluding overhead and schema information.
			 *
			 * @return the estimated current memory use
			 */
			virtual std::size_t estimateValueMemoryUse() const noexcept override;

			/**
			* Calculate memory to be allocated
			*
			* @param count The number of entities currently active
			* @return the memory size to be allocated in bytes
			*/
			virtual std::size_t estimateNewValueMemoryUse(std::size_t count) const noexcept override;

			/**
			 * Gets direct access to the attribute value array implementing the values.
			 *
			 * @return the attribute value array
			 */
			Array &getArray() noexcept;

			/**
			 * Gets direct access to the attribute value array implementing the values.
			 *
			 * @return the attribute value array
			 */
			const Array &getArray() const noexcept;

			/**
			 * Directly replaces the attribute value array implementing the values.
			 *
			 * @param values The new attribute array values
			 */
			void setArray(Array values) noexcept;

			/**
			 * Gets the attribute describing this column.
			 *
			 * @return the column attribute
			 */
			geas::Attribute &getAttribute() noexcept;

			/**
			 * Gets the attribute describing this column.
			 *
			 * @return the column attribute
			 */
			const geas::Attribute &getAttribute() const noexcept;

			/**
			 * Sets the attribute describing this column.
			 *
			 * @param attribute The attribute
			 */
			void setAttribute(geas::Attribute attribute);

			/**
			 * Clears the attribute without changing the object count.
			 */
			void clearAttribute() noexcept;

		private:
			/** The metaclass for this class. */
			static cio::Class<Column> sMetaclass;

			/* Size properties for this class. */
			static geas::Attribute sSizeProperties;

			/** Attribute count properties for this class. */
			static geas::Attribute sColumnProperties;

			/** The number of logical entities or objects. */
			std::size_t mSize;

			/** The attribute describing the column. */
			geas::Attribute mAttribute;

			/** The list of attribute value arrays. */
			Array mValues;
	};
}

#ifndef _MSC_VER
#pragma warning (pop)
#endif

#endif
