/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_VARIABLEARRAY_H
#define GLITTER_VARIABLEARRAY_H

#include "Scalar.h"
#include "FixedArray.h"

#include <cio/Chunk.h>
#include <cio/Class.h>
#include <cio/Types.h>

#include <map>
#include <string>
#include <vector>

namespace glitter
{
		template <typename S, typename T, typename U /*= void*/>
		class VariableArray
		{
		public:
			/**
			* Construct a new accessor with no data or scalar index data and size and offset of zero.
			*/
			inline VariableArray() noexcept;

			/**
			* Construct a variable accessor with the given data, is 1, offset is set to zero.  No
			* scalar index data is set.
			*
			* @param data The data that the accessor is going to parse and discern.
			*/
			inline explicit VariableArray(cio::Chunk<U> data);

			/**
			* Construct a variable accessor with the given data, indexOffsets and offset.
			*
			* @param data The data that the accessor is going to parse.
			* @param indexOffsets Scalar data that contains the offsets into the data.
			* @param dataOffset The number of bytes from the beginning of the data to begin reading the data, default of 0.
			*/
			inline VariableArray(cio::Chunk<U> data, Scalar<S, U> indexOffsets, std::size_t dataOffset = 0);

			/**
			* Construct a variable accessor with the given data, indexOffsets, offset and element size.
			*
			* @param data The data that the accessor is going to parse.
			* @param indexOffsets Scalar data that contains the offsets into the data.
			* @param dataOffset The number of bytes from the beginning of the data to begin reading the data.
			* @param elementSize  The size, in bytes, of each data element.  
			*/
			inline VariableArray(cio::Chunk<U> data, Scalar<S, U> indexOffsets, std::size_t dataOffset, std::size_t elementSize);

			/**
			* Construct a default, compiler generated, read-only copy of the accessor.
			*
			* @param in The read-only accessor to copy.
			*/
			inline VariableArray(const VariableArray<S, T, U>& in) = default;

			/**
			* Move the data and copy the attributes into a new instance of this class.
			*
			* @param in The accessor to copy.
			*/
			inline VariableArray(VariableArray<S, T, U>&& in) noexcept;

			/**
			* Copy the data and attributes into a new instance of this class.
			*
			* @param in The read-only accessor to copy.
			*/
			template <typename V, typename W, typename X>
			inline VariableArray(const VariableArray<V, W, X>& in);

			/**
			* Move the data and copy the attributes into a new instance of this class.
			*
			* @param in The accessor to copy.
			*/
			template <typename V, typename W, typename X>
			inline VariableArray(VariableArray<V, W, X>&& in) noexcept;

			/**
			* Create a read-only copy of the given accessor.
			*
			* @param in The given accessor to copy.
			*/
			inline VariableArray<S, T, U>& operator=(const VariableArray<S, T, U>& in) = default;

			/**
			* Constructs copy of the given accessor.
			*
			*@param in The given accessor of a templated type to copy.
			*/
			inline VariableArray<S, T, U>& operator=(VariableArray<S, T, U>&& in) noexcept;

			/**
			* Default destructor.
			*/
			inline ~VariableArray() noexcept = default;

			/**
			* Set the data in the accessor to the data given.  Inizializes the attributes,
			* size is the number of logical data elements and offset is zero, stride and
			* element size are set to the size of the data type of the accessor.
			*
			* @param data The data for the accessor to parse.
			*/
			inline void bind(cio::Chunk<U> data);

			/**
			* Set the data in the accessor to the data given.  Inizializes the attributes,
			* size is the number of logical data elements and offset is zero, stride and
			* element size are set to the size of the data type of the accessor.
			*
			* @param data The data for the accessor to parse.
			* @param indexData The index data that contains the offsets to the row indicies.
			*/
			inline void bind(cio::Chunk<U> data, cio::Chunk<U> indexData);

			/**
			* Set the data in the accessor to the data given and sets the offsets
			* to each column element in the data.  Size is the number of offsets
			* in the index array - 1, offset is zero, element size is set to the 
			* size of the data type of the accessor.
			*
			* @param data The data for the accessor to parse.
			* @param indexArray The scalar data that contains the array of indicies used to access the data.
			*/
			inline void bind(cio::Chunk<U> data, Scalar<S, U>  indexArray);

			/**
			* Gets the offset, or the location from the beginning of the
			* underlying data array to begin parsing data.
			*
			* @return The size, in bytes, of the offset.
			*/
			inline std::size_t getOffset() const;

			/**
			* Sets the offset, or the location from the beginning of the
			* underlying data array to begin parsing data.
			*
			* @param offset The size, in bytes, of the offset.
			*/
			inline void setOffset(std::size_t offset);

			/**
			* Gets the size, in bytes, of each element in the data.
			*
			* @return The size, in bytes, of each element.
			*/
			inline std::size_t getElementSize() const;

			/**
			* Sets the size in bytes of each data element.
			*
			* @param newSize The size, in bytes, of each element.
			*/
			inline void setElementSize(std::size_t newSize);

			/**
			* Gets the number of rows in the data
			*
			* @return The number of rows.
			*/
			inline std::size_t getRowCount() const;

			/**
			* Sets the number of rows.
			*
			* @param rows The number of rows in the variable array.
			*/
			inline void setRowCount(std::size_t rows);

			/**
			* Get the number of elements in the data array.
			*
			* @return The number of elements.
			*/
			inline std::size_t size() const;

			/**
			* Resize the accessor.
			*
			* @param length  The desired size of the underlying data.
			*/
			inline void resize(std::size_t length);

			/**
			* Clears the underlying data, resets the size, offset, element size, element
			* count, element stride and stride.
			*/
			inline void clear();

			/**
			* Get the underlying chunk of data.
			*
			* @return The data the accessor is parsing.
			*/
			inline cio::Chunk<U>& getData();

			/**
			* Gets a read-only reference to the underlying chunk of data.
			*
			* @return the read-only reference to the underlying data.
			*/
			inline const cio::Chunk<U>& getData() const;

			/**
			* Sets the underlying data for the accessor.
			*
			* @param data The data to be moved to be owned by the accessor.
			*/
			inline void setData(cio::Chunk<U> data);

			/**
			* Get the underlying chunk of data that represents the offsets.
			*
			* @return The offsets into each variable data array.
			*/
			inline Scalar<S, U>& getIndexData();

			/**
			* Gets a read-only reference to the underlying chunk of data that represents the offsets.
			*
			* @return the read-only reference to the offsets into the data.
			*/
			inline const Scalar<S, U>& getIndexData() const;

			/**
			* Sets the underlying data offsets.
			*
			* @param data The data offsets to be moved to be owned by the accessor.
			*/
			inline void setIndexData(Scalar<S, U>  data);

			/**
			* Gets a pointer to the underlying data.
			*
			* @return a pointer to the underlying data of templated type U.
			*/
			inline U* data();

			/**
			* Gets a const pointer to the underlying data.
			*
			* @return a const pointer to the data in the accessor.
			*/
			inline const U* data() const;

			/**
			* Gets a pointer to the underlying data.
			*
			* @return a pointer to the underlying data of templated type U.
			*/
			inline U* indexData();

			/**
			* Gets a const pointer to the underlying data.
			*
			* @return a const pointer to the data in the accessor.
			*/
			inline const U* indexData() const;

			/**
			* Gets the number of elements at the particular given array index.
			*
			* @param idx The index to the array in which the element count is desired.
			* @return The number of elements at a particular index.
			*/
			inline std::size_t getCount(std::size_t idx) const;

			/**
			* Gets the distance, it bytes, that the elements at the given array index
			* span.
			*
			* @param idx The row index to the array in which the span is desired.
			* @return The number of bytes from the given row index to the next row index.
			*/
			inline std::size_t getStride(std::size_t idx) const;

			/**
			* Gets the value of the element at the specified element index that is
			* within the particular array specified by array index.
			*
			* @param idx The index to the array that contains the desired value.
			* @param eleIdx The index of the element within the array.
			* @return The value at the given index and element index.
			*/
			inline cio::Value<T> get(std::size_t idx, std::size_t eleIdx) const;

			/**
			* Gets all elements at the specified element index.  This will
			* access the Scalar index values, calculate the number of indicies the 
			* elements span and return each element within that span as a vector.
			*
			* @param idx The index to the array that contains the desired value.
			* @return The values between the given index and the subsequent index.
			*/
			inline std::vector<cio::Value<T>> get(std::size_t idx) const;

			/**
			* Adds a new row with one element of the given value.  This will also add
			* a row to the offset array.  If the row already exists at that index,
			* the new row will be inserted and all subsequent rows will be moved by one.
			*
			* @param idx The index at which to insert or add the row.
			* @param val The value to be set at the first element of the new row.
			*/
			inline void add(std::size_t idx, cio::Value<T> val);

			/**
			* Adds a new row with one element of the given value.  This will also add
			* a row to the offset array.  If the row already exists at that index,
			* the new row will be inserted and all subsequent rows will be moved by one.
			*
			* @param idx The index to add to the element
			*/
			inline void remove(std::size_t idx);

			/**
			* Sets the value of the element at the specified index and element
			* index located within that index.
			*
			* @param idx The index to the array that contains the value to be changed.
			* @param eleIdx The index of the element within the array.
			* @param val The value to be set at the given index and element index.
			*/
			inline void set(std::size_t idx, std::size_t eleIdx, cio::Value<T> val);

			/**
			* Sets the values at the given index.  This will call the size adjust function
			* if the size of the values given are different than the size of the values
			* already at that row index.
			*
			* @param idx The index to the array that you would like to set values for.
			* @param values The values to be set at the given index.
			* @param numVals The number of values to be set.
			*/
			inline void set(std::size_t idx, cio::Value<const T*> values, std::size_t numVals);

			/**
			* Sets the values at the given index.  This will call the size adjust function
			* if the size of the values given are different than the size of the values
			* already at that row index.
			*
			* @param idx The index to the array that you would like to set values for.
			* @param values The values to be set at the given index.
			*/
			inline void set(std::size_t idx, const std::vector<cio::Value<T>>& values);

			/**
			* This replaces all values at a given row index with the values
			* given in the string.  
			*
			* @param idx The index into the row that values need to be set for.
			* @param text The new text in the given row.
			*/
			inline void setText(std::size_t idx, const std::string& text);

			/**
			* This handles the adjustment of the size of the data as values of 
			* different sizes replace old values in the data.
			*
			* @param idx The index into the row that needs to be re-sized.
			* @param newSize The new size at the given row.
			*/
			inline void sizeAdjust(std::size_t idx, std::size_t newSize);


		private:
			/*  The data that the accessor is interested in parsing.  */
			cio::Chunk<U> mData;

			/*  The offsets into the data.  */
			Scalar<S, U>  mIndexData;

			/** The total number of elements in the entire array */
			std::size_t mSize;

			/** The number of row, each row may contain a variable number of elements or columns*/  //was mSize
			std::size_t mRows;

			/*  The offset in bytes from the beginning of the data */
			std::size_t mOffset;

			/*  The offset in bytes of each element */
			std::size_t mElementSize;
		};
}

/* Inline implementation */

namespace glitter
{
		template <typename S, typename T, typename U>
		inline VariableArray<S, T, U>::VariableArray() noexcept :
			mSize(0),
			mRows(0),
			mOffset(0),
			mElementSize(sizeof(cio::Value<T>))
		{
			// nothing more to do
		}

		template <typename S, typename T, typename U>
		inline VariableArray<S, T, U>::VariableArray(cio::Chunk<U> data) :
			mData(std::move(data)),
			mSize(mData.size()),
			mRows(1),
			mOffset(0),
			mElementSize(sizeof(cio::Value<T>))
		{
			// nothing more to do
		}

		template <typename S, typename T, typename U>
		inline VariableArray<S, T, U>::VariableArray(cio::Chunk<U> data, Scalar<S, U> indexOffsets, std::size_t dataOffset) :
			mData(std::move(data)),
			mIndexData(std::move(indexOffsets)),
			mRows(mIndexData.size() > 0 ? mIndexData.size() - 1 : 1),
			mSize(mIndexData.size() > 0 ? mIndexData.get(mIndexData.size() - 1) : 1),
			mOffset(dataOffset),
			mElementSize(sizeof(cio::Value<T>))
		{
			// nothing more to do
		}

		template <typename S, typename T, typename U>
		inline VariableArray<S, T, U>::VariableArray(cio::Chunk<U> data, Scalar<S, U> indexOffsets, std::size_t dataOffset, std::size_t elementSize) :
			mData(std::move(data)),
			mIndexData(std::move(indexOffsets)),
			mRows(mIndexData.size() > 0 ? mIndexData.size() - 1 : 1),
			mSize(mIndexData.size() > 0 ? mIndexData.get(mIndexData.size() - 1) : 1),
			mOffset(dataOffset),
			mElementSize(elementSize)
		{
			// nothing more to do
		}

		template <typename S, typename T, typename U>
		inline VariableArray<S, T, U>::VariableArray(VariableArray<S, T, U>&& in) noexcept :
			mData(std::move(in.mData)),
			mIndexData(std::move(in.mIndexData)),
			mRows(in.mRows),
			mSize(in.mSize),
			mOffset(in.mOffset)
		{
			in.clear();
		}

		template <typename S, typename T, typename U>
		template <typename V, typename W, typename X>
		inline VariableArray<S, T, U>::VariableArray(const VariableArray<V, W, X>& in) :
			mData(in.getData()),
			mIndexData(in.getIndexData()),
			mRows(in.getRowCount()),
			mSize(in.size()),
			mOffset(in.getOffset()),
			mElementSize(in.getElementSize())

		{
			// nothing more to do
		}

		template <typename S, typename T, typename U>
		template <typename V, typename W, typename X>
		inline VariableArray<S, T, U>::VariableArray(VariableArray<V, W, X>&& in) noexcept :
			mData(std::move(in.getData())),
			mIndexData(std::move(in.getIndexData())),
			mRows(in.getRowCount()),
			mSize(in.size()),
			mOffset(in.getOffset()),
			mElementSize(in.getElementSize())
		{
			in.clear();
		}

		template <typename S, typename T, typename U>
		inline VariableArray<S, T, U>& VariableArray<S, T, U>::operator=(VariableArray<S, T, U>&& in) noexcept
		{
			if (this != &in)
			{
				mData = std::move(in.mData);
				mIndexData = in.mIndexData;
				mRows = in.mRows;
				mSize = in.mSize;
				mOffset = in.mOffset;
				mElementSize = in.mElementSize;
				in.clear();
			}
			return *this;
		}

		template <typename S, typename T, typename U>
		inline void VariableArray<S, T, U>::bind(cio::Chunk<U> data, Scalar<S, U> indexData)
		{
			mData = std::move(data);
			mIndexData = std::move(indexData);
			mRows = mIndexData.size() > 0 ? (mIndexData.size() - 1) : 1;
			mSize = mIndexData.size() > 0 ? mIndexData.get(mIndexData.size() - 1) : 1;
			mOffset = 0;
			mElementSize = sizeof(cio::Value<T>);
		}

		template <typename S, typename T, typename U>
		inline void VariableArray<S, T, U>::bind(cio::Chunk<U> data, cio::Chunk<U> indexData)
		{
			mData = std::move(data);
			mIndexData.bind(std::move(indexData));
			mRows = mIndexData.size() > 0 ? (mIndexData.size() - 1) : 1;
			mSize = mIndexData.size() > 0 ? mIndexData.get(mIndexData.size() - 1) : 1;
			mOffset = 0;
			mElementSize = sizeof(cio::Value<T>);
		}

		template <typename S, typename T, typename U>
		inline void VariableArray<S, T, U>::bind(cio::Chunk<U> data)
		{
			mData = std::move(data);
			mRows = 1;
			mSize = mData.size();
			mOffset = 0;
			mElementSize = sizeof(cio::Value<T>);
		}

		template <typename S, typename T, typename U>
		inline std::size_t VariableArray<S, T, U>::getOffset() const
		{
			return mOffset;
		}

		template <typename S, typename T, typename U>
		inline void VariableArray<S, T, U>::setOffset(std::size_t offset)
		{
			mOffset = offset;
		}

		template <typename S, typename T, typename U>
		inline std::size_t VariableArray<S, T, U>::getElementSize() const
		{
			return mElementSize;
		}

		template <typename S, typename T, typename U>
		inline void VariableArray<S, T, U>::setElementSize(std::size_t newSize)
		{
			mElementSize = newSize;
		}

		template <typename S, typename T, typename U>
		inline std::size_t VariableArray<S, T, U>::size() const
		{
			return mSize;
		}

		template <typename S, typename T, typename U>
		inline void VariableArray<S, T, U>::resize(std::size_t length)
		{
			mSize = length;
		}

		template <typename S, typename T, typename U>
		inline std::size_t VariableArray<S, T, U>::getRowCount() const
		{
			return mRows;
		}

		template <typename S, typename T, typename U>
		inline void VariableArray<S, T, U>::setRowCount(std::size_t rows)
		{
			mRows = rows;
		}

		template <typename S, typename T, typename U>
		inline void VariableArray<S, T, U>::clear()
		{
			mData.clear();
			mIndexData.clear();
			mRows = 0;
			mSize = 0;
			mOffset = 0;
			mElementSize = sizeof(cio::Value<T>);
		}

		template <typename S, typename T, typename U>
		inline cio::Chunk<U>& VariableArray<S, T, U>::getData()
		{
			return mData;
		}

		template <typename S, typename T, typename U>
		inline const cio::Chunk<U>& VariableArray<S, T, U>::getData() const
		{
			return mData;
		}

		template <typename S, typename T, typename U>
		inline void VariableArray<S, T, U>::setData(cio::Chunk<U> data)
		{
			mData = std::move(data);
		}

		template <typename S, typename T, typename U>
		inline Scalar<S, U>& VariableArray<S, T, U>::getIndexData()
		{
			return mIndexData;
		}

		template <typename S, typename T, typename U>
		inline const Scalar<S, U>& VariableArray<S, T, U>::getIndexData() const
		{
			return mIndexData;
		}

		template <typename S, typename T, typename U>
		inline void VariableArray<S, T, U>::setIndexData(Scalar<S, U> indicies)
		{
			mIndexData = std::move(indicies);
		}

		template <typename S, typename T, typename U>
		inline U* VariableArray<S, T, U>::data()
		{
			return mData.data();
		}

		template <typename S, typename T, typename U>
		inline const U* VariableArray<S, T, U>::data() const
		{
			return mData.data();
		}

		template <typename S, typename T, typename U>
		inline U* VariableArray<S, T, U>::indexData()
		{
			return mIndexData.data();
		}

		template <typename S, typename T, typename U>
		inline const U* VariableArray<S, T, U>::indexData() const
		{
			return mIndexData.data();
		}

		template <typename S, typename T, typename U>
		inline std::size_t VariableArray<S, T, U>::getCount(std::size_t idx) const
		{
			std::size_t eleCount = 0;
			if (mIndexData.data() && mIndexData.size() > idx + 1)
			{
				std::size_t start = static_cast<std::size_t>(mIndexData.get(idx));
				std::size_t end = static_cast<std::size_t>(mIndexData.get(idx + 1));
				if (start >= 0 && end >= 0 && end > start)
				{
					eleCount = end - start;
				}
			}
			return eleCount;
		}

		template <typename S, typename T, typename U>
		inline std::size_t VariableArray<S, T, U>::getStride(std::size_t idx) const
		{
			return this->getCount(idx) * sizeof(cio::Value<T>);
		}

		template <typename S, typename T, typename U>
		inline cio::Value<T> VariableArray<S, T, U>::get(std::size_t idx, std::size_t eleIdx) const
		{
			T elementValue = T();
            size_t count = this->getCount(idx);
			if (mData && mIndexData.data() && idx < mRows && count > eleIdx)
			{
				std::size_t startByte = static_cast<cio::Value<S>>(mIndexData.get(idx)) * mElementSize + eleIdx * mElementSize + mOffset;
				std::size_t adjustOff = startByte % sizeof(cio::Value<U>);
				startByte = (startByte + adjustOff) / sizeof(cio::Value<U>);
				const U *data = static_cast<const cio::Value<U> *>(mData.data()) + startByte;
                std::memcpy(&elementValue, reinterpret_cast<const cio::Value<U>*>(data), mElementSize);
			}
			return elementValue;
		}

		template <typename S, typename T, typename U>
		inline std::vector<cio::Value<T>> VariableArray<S, T, U>::get(std::size_t idx) const
		{
			std::size_t numVals = this->getCount(idx);
			std::vector<T> vals(numVals);
			for (std::size_t i = 0; i < numVals; ++i)
			{
				vals[i] = static_cast<cio::Value<T>>(this->get(idx, i));
			}
			return vals;
		}

		template <typename S, typename T, typename U>
		inline void VariableArray<S, T, U>::add(std::size_t idx, cio::Value<T> val)
		{
			//Adjust the size at the current index first or add a new index at the end.
			this->sizeAdjust(idx, this->getCount(idx) + 1);
			mIndexData.add(idx+1, 1);
			mRows = mIndexData.size() > 0 ? (mIndexData.size() - 1) : 1;
			mSize = mIndexData.size() > 0 ? mIndexData.get(mIndexData.size() - 1) : 1;
			this->set(idx, 0, val);
		}

		template <typename S, typename T, typename U>
		inline void VariableArray<S, T, U>::remove(std::size_t idx)
		{
			this->sizeAdjust(idx, 0);
			mIndexData.remove(idx+1);
			mRows = mIndexData.size() > 0 ? (mIndexData.size() - 1) : 1;
			mSize = mIndexData.size() > 0 ? mIndexData.get(mIndexData.size() - 1) : 1;
		}

		template <typename S, typename T, typename U>
		inline void VariableArray<S, T, U>::set(std::size_t idx, std::size_t eleIdx, cio::Value<T> val)
		{
			if (mData && mIndexData.data())
			{
				if (idx >= mIndexData.size()-1)
				{
					this->add(idx, val);
				}

				if (this->getCount(idx) <= eleIdx)
				{
					this->sizeAdjust(idx, eleIdx + 1);
				}

				std::size_t startByte = static_cast<cio::Value<S>>(mIndexData.get(idx)) * mElementSize + eleIdx * mElementSize + mOffset;
				std::size_t adjustOff = startByte % sizeof(cio::Value<U>);
				startByte = (startByte + adjustOff) / sizeof(cio::Value<U>);
				U* data = static_cast<cio::Value<U> *>(mData.data()) + startByte;
				std::memcpy(reinterpret_cast<cio::Value<U> *>(data), &val, mElementSize);
			}
		}

		template <typename S, typename T, typename U>
		inline void VariableArray<S, T, U>::set(std::size_t idx, cio::Value<const T*> vals, std::size_t valSize)
		{
			std::size_t numVals = this->getCount(idx);
			if (valSize != numVals)
			{
				this->sizeAdjust(idx, valSize);
			}

			for (std::size_t i = 0; i < valSize; i++)
			{
				this->set(idx, i, *vals++);
			}
		}

		template <typename S, typename T, typename U>
		inline void VariableArray<S, T, U>::set(std::size_t idx, const std::vector<cio::Value<T>>& vals)
		{
			this->set(idx, vals.data(), vals.size());
		}

		template <typename S, typename T, typename U>
		inline void VariableArray<S, T, U>::setText(std::size_t idx, const std::string& text)
		{
			std::vector<cio::Value<T>> charVec(text.begin(), text.end());
			this->set(idx, charVec);
		}

		template <typename S, typename T, typename U>
		inline void VariableArray<S, T, U>::sizeAdjust(std::size_t idx, std::size_t newSize)
		{			
			std::size_t dataLoc = 0; 

			newSize = newSize * mElementSize;
			std::size_t oldSize = this->getCount(idx) * mElementSize;

			std::size_t dataSz = mData.size() + (newSize - oldSize);
			cio::Chunk<U> tmp(dataSz, mData.allocator());

			if (idx >= mIndexData.size())
			{
				dataLoc = static_cast<cio::Value<S>>(mIndexData.get(mIndexData.size() - 1)) * mElementSize + mOffset;
				std::size_t readSize = dataLoc + oldSize;

				std::memcpy(static_cast<cio::Value<U> *>(tmp.data()), static_cast<cio::Value<U> *>(mData.data()), mData.size());
				mData = std::move(tmp);
				std::size_t ns = mIndexData.get(mIndexData.size() - 1);
				mIndexData.add(idx + 1, static_cast<cio::Value<S>>(mIndexData.get(mIndexData.size() - 1) + newSize));
			}
			else
			{
				std::size_t dataLoc = static_cast<cio::Value<S>>(mIndexData.get(idx)) * mElementSize + mOffset;
				std::size_t readSize = dataLoc + oldSize;

				//Copy temp data
				std::memcpy(static_cast<cio::Value<U> *>(tmp.data()), static_cast<cio::Value<U> *>(mData.data()), dataLoc + std::min(newSize, oldSize));
				std::memcpy(static_cast<cio::Value<U> *>(tmp.data()) + dataLoc + newSize, static_cast<cio::Value<U> *>(mData.data()) + readSize, mData.size()- readSize);
				mData = std::move(tmp);

				//Correct the offsets
				for (std::size_t i = idx + 1; i < mIndexData.size(); ++i)
				{
					mIndexData.set(i, static_cast<cio::Value<S>>(mIndexData.get(i) + (newSize - oldSize) / mElementSize));
				}
			}
			mRows = mIndexData.size() > 0 ? (mIndexData.size() - 1) : 1;
			mSize = mIndexData.size() > 0 ? mIndexData.get(mIndexData.size() - 1) : 1;
		}
}


#endif
