/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_COLLECTION_H
#define GLITTER_COLLECTION_H

#include "Types.h"

#include "Catalog.h"
#include "Node.h"
#include "Region.h"

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace glitter
{
	/**
	 * The Collection represents some grouping of nodes implementing a particular application schema.
	 * Typically this is either a single node, a flat list of nodes, or a hierarchical tree of nodes.
	 * Each node typically contains some number of logical entity instances described by one or more attribute instances.
	 */
	class GLITTER_ABI Collection : public geas::Definition
	{
		public:
			/**
			 * Construct an empty collection with a null catalog.
			 */
			Collection() noexcept;

			/**
			 * Construct a deep copy of a collection.
			 * All nodes will be copied fully.
			 * The two collections will point to the same catalog.
			 *
			 * @param in The collection to copy
			 */
			Collection(const Collection &in);

			/**
			 * Construct a collection by transferring the contents of an existing collection.
			 *
			 * @param in The collection to move
			 */
			Collection(Collection &&in) noexcept;

			/**
			 * Performs a deep copy of a collection.
			 * All nodes will be copied fully.
			 * The two collections will point to the same catalog.
			 *
			 * @param in The collection to copy
			 * @return this collection
			 */
			Collection &operator=(const Collection &in);

			/**
			 * Transfers the contents of an existing collection.
			 *
			 * @param in The collection to move
			 * @return this collection
			 */
			Collection &operator=(Collection &&in) noexcept;

			/**
			 * Destructor. Deallocates all contained nodes.
			 */
			virtual ~Collection() noexcept override;

			/**
			 * Gets the runtime metaclass for this object instance.
			 *
			 * @return the runtime metaclass for this object
			 */
			virtual const cio::Metaclass &getMetaclass() const noexcept override;

			/**
			 * Clears all contained nodes and sets the referenced catalog to null.
			 */
			virtual void clear() noexcept override;

			/**
			 * Gets the catalog currently in use.
			 *
			 * @return the catalog in use
			 */
			std::shared_ptr<Catalog> getCatalog() noexcept;

			/**
			 * Gets the catalog currently in use.
			 *
			 * @return the catalog in use
			 */
			std::shared_ptr<const Catalog> getCatalog() const noexcept;

			/**
			 * Gets the catalog currently in use, creating one if none is present.
			 * If a new catalog is created, it will also create a new underlying
			 * vocabulary, measurement system, and application schema.
			 *
			 * @return the catalog in use
			 * @throw std::bad_alloc If the memory for a new catalog cannot be allocated
			 */
			std::shared_ptr<Catalog> getOrCreateCatalog();

			/**
			 * Sets the catalog to use.
			 *
			 * @param catalog the catalog to use
			 */
			void setCatalog(std::shared_ptr<Catalog> catalog) noexcept;

			/**
			 * Clears the catalog in use.
			 */
			void clearCatalog() noexcept;

			/**
			 * Gets the active application profile in use for this collection, if any.
			 *
			 * @return the active profile
			 */
			const geas::ApplicationProfile *getActiveProfile() const noexcept;

			/**
			 * Sets the active application profile in use for this collection.
			 * Setting to null disables the use of profiles.
			 *
			 * @param profile The profile to use
			 */
			void setActiveProfile(const geas::ApplicationProfile *profile) noexcept;

			/**
			 * Gets the full list of nodes in this collection.
			 *
			 * @return the list of all nodes in this collection
			 */
			geas::Dictionary<Node> &getNodes() noexcept;

			/**
			 * Gets the full list of nodes in this collection.
			 *
			 * @return the list of all nodes in this collection
			 */
			const geas::Dictionary<Node> &getNodes() const noexcept;

			/**
			 * Sets the full list of nodes in this collection.
			 *
			 * @param nodes The full list of nodes to use
			 */
			void setNodes(geas::Dictionary<Node> nodes) noexcept;

			/**
			 * Adds a node to this collection.
			 *
			 * @param node The node to add
			 * @throw std::bad_alloc If memory is needed for the node but could not be allocated
			 */
			void addNode(Node node);

			/**
			 * Clears all nodes on this collection.
			 */
			void clearNodes() noexcept;

			/**
			 * Gets the map projection assigned to this collection, if any.
			 *
			 * @return the projection, or nullptr if none was set
			 */
			const geas::Projection *getProjection() const noexcept;

			/**
			 * Sets the map projection assigned to this collection, if any.
			 *
			 * @param projection the projection, or nullptr if none was set
			 */
			void setProjection(const geas::Projection *projection) noexcept;

			/**
			 * Gets the geographic coverage of the collection if known.
			 *
			 * @return the geographic coverage
			 */
			const Region &getCoverage() const noexcept;

			/**
			 * Sets the geographic coverage of the collection if known.
			 *
			 * @param coverage the geographic coverage
			 */
			void setCoverage(Region &coverage) noexcept;

			/**
			 * Clears the geographic coverage of the collection.
			 */
			void clearCoverage() noexcept;

		private:
			/** The metaclass for this class. */
			static cio::Class<Collection> sMetaclass;

			/** The catalog in use for all contained nodes. */
			std::shared_ptr<Catalog> mCatalog;

			/** Active profile for this collection. */
			const geas::ApplicationProfile *mProfile;

			/** The set of all nodes. */
			geas::Dictionary<Node> mNodes;

			/** The projection assigned to this collection. */
			const geas::Projection *mProjection;

			/** The geographic bounds of the collection if known. */
			Region mCoverage;
	};
}

namespace geas
{
	/** Instantiate geas::Dictionary<Collection> for use in other containers. */
	extern template class GLITTER_TEMPLATE geas::Dictionary<glitter::Collection>;
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
