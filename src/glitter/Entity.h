/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_ENTITY_H
#define GLITTER_ENTITY_H

#include "Types.h"

#include "Node.h"
#include "Provider.h"

#include <iosfwd>

namespace glitter
{
	/**
	 * The Entity provides the ability to reference a particular entity instance in a Node using its object index.
	 * It simplifies the access pattern to present a view of the single entity of interest regardless of the underlying storage
	 * to assess its type and attribute values.
	 */
	class GLITTER_ABI Entity
	{
		public:
			/**
			 * Construct an unbound entity.
			 */
			inline Entity();

			/**
			 * Construct an entity reference bound to the given node and object index.
			 * The node will not be owned by this object.
			 *
			 * @param node The entity's parent node
			 * @param objectId The entity index
			 */
			inline Entity(Node *node, geas::Index objectId);

			/**
			 * Construct a shallow copy of an entity.
			 * This entity will be bound to the same object as the given one.
			 *
			 * @param in The entity to copy
			 */
			inline Entity(const Entity &in) = default;

			/**
			 * Assign a shallow copy of an entity.
			 * This entity will be bound to the same object as the given one.
			 *
			 * @param in The entity to copy
			 * @return this entity
			 */
			inline Entity &operator=(const Entity &in) = default;

			/**
			 * Destructor. Does nothing since this is just a shallow reference.
			 */
			inline ~Entity() noexcept = default;

			/**
			 * Unbinds this reference from its current node and object index.
			 * This does not erase or modify the underlying object.
			 */
			inline void clear() noexcept;

			/**
			 * Binds this entity reference to the given node and object index.
			 * The node will not be owned by this object.
			 *
			 * @param node The entity's parent node
			 * @param objectId The entity's object index
			 */
			inline void bind(Node *node, geas::Index objectId);

			/**
			 * Gets the application schema in use for the bound entity.
			 * This uses the application schema from the parent Node if available.
			 *
			 * @return the application schema, or nullptr if unknown
			 */
			inline std::shared_ptr<const geas::Application> getSchema() const;

			/**
			 * Gets the currently bound entity's parent node.
			 *
			 * @return the entity's parent node
			 */
			inline Node *getNode() const;

			/**
			 * Gets the currently bound entity's object index.
			 *
			 * @return the object index
			 */
			inline std::size_t getId() const;

			/**
			 * Gets the Entity schema of the bound entity reference.
			 * Equivalent to this->getNode()->getEntityType(this->getId()).
			 *
			 * @return the entity type
			 */
			inline const geas::Entity *getType() const;

			/**
			 * Gets the Geometry schema of the bound entity reference.
			 * Equivalent to this->getNode()->getGeometryType(this->getId()).
			 *
			 * @return the geometry type
			 */
			inline const geas::Geometry *getGeometry() const;

			/**
			 * Sets the formal Entity type of the bound entity reference.
			 * Equivalent to this->getNode()->setEntityType(this->getId(), type, geometry).
			 *
			 * @param type The entity type to set
			 * @param geometry The geometry type to set
			 */
			inline void setType(const geas::Entity *type, const geas::Geometry *geometry = nullptr);

			/**
			 * Gets the number of attributes currently defined for the bound entity.
			 * Equivalent to this->getNode->getAttributeCount(this->getId()).
			 *
			 * @return the attribute count
			 */
			inline geas::Index getAttributeCount() const;

			/**
			 * Gets the size of this entity, which is the number of attributes currently defined.
			 * Equivalent to this>getNode->getAttributeCount(this->getId()).
			 *
			 * @return the attribute count
			 */
			inline geas::Index size() const;

			/**
			 * Gets the physical field index for the attribute with the given label.
			 *
			 * @param label The attribute label
			 * @return the field index, or INT32_MIN if not found
			 */
			geas::Index getFieldIndex(const geas::Label &label) const;

			/**
			 * Gets the physical field index for the attribute with the given code.
			 *
			 * @param code The attribute code
			 * @return the field index, or INT32_MIN if not found
			 */
			geas::Index getFieldIndex(const geas::Code &code) const;

			/**
			 * Gets the physical field index for the field matching the given definition.
			 *
			 * @param field The field definition
			 * @return the field index, or INT32_MIN if not found
			 */
			geas::Index getFieldIndex(const geas::Attribute &field) const;

			/**
			 * Gets the attribute schema for the attribute with the given label.
			 *
			 * @param label The attribute label
			 * @return the attribute schema, or nullptr if not found
			 */
			const geas::Attribute *getLogicalField(const geas::Label &label) const;

			/**
			 * Gets the attribute schema for the attribute with the given code.
			 *
			 * @param code The attribute code
			 * @return the attribute schema, or nullptr if not found
			 */
			const geas::Attribute *getLogicalField(const geas::Code &code) const;

			/**
			 * Gets the attribute schema for the attribute with the given field index.
			 *
			 * @param field The field index
			 * @return the attribute schema, or nullptr if not found
			 */
			const geas::Attribute *getLogicalField(geas::Index field) const;

			/**
			 * Gets the attribute schema for the attribute with the given label.
			 *
			 * @param label The attribute label
			 * @return the attribute schema, or nullptr if not found
			 */
			const geas::Attribute *getPhysicalField(const geas::Label &label) const;

			/**
			 * Gets the attribute schema for the attribute with the given code.
			 *
			 * @param code The attribute code
			 * @return the attribute schema, or nullptr if not found
			 */
			const geas::Attribute *getPhysicalField(const geas::Code &code) const;

			/**
			 * Gets the attribute schema for the attribute with the given field index.
			 *
			 * @param field The field index
			 * @return the attribute schema, or nullptr if not found
			 */
			const geas::Attribute *getPhysicalField(geas::Index field) const;

			/**
			 * Gets the attribute value for the entity's attribute identified by the given attribute label.
			 *
			 * @param label The attribute label
			 * @return the value for that attribute, or an empty value array if it is not present
			 */
			inline Array getValue(const geas::Label &label) const;

			/**
			 * Gets the attribute value for the entity's attribute identified by the given attribute code.
			 *
			 * @param code The attribute code
			 * @return the value for that attribute, or an empty value array if it is not present
			 */
			inline Array getValue(const geas::Code &code) const;

			/**
			 * Gets the attribute value for the entity's attribute identified by the given attribute field index.
			 * The valid position index values range from 0 up to getAttributeCount() - 1.
			 * Attribe field index values may change unpredictably if the underlying entity is edited.
			 *
			 * @param field The attribute field index
			 * @return the value for that attribute, or an empty value array if it is not present
			 */
			inline Array getValue(geas::Index field) const;

			/**
			 * Sets the attribute value for the entity's attribute identified by the given attribute label.
			 * This is done unconditionally, regardless of any validation issues with the attribution schema.
			 *
			 * @param label The attribute label
			 * @param value The new value for that attribute, or an empty value array if it is not present
			 * @return whether the value was set
			 */
			inline bool setValue(const geas::Label &label, const Array &value);

			/**
			 * Sets the attribute value for the entity's attribute identified by the given attribute code.
			 * This is done unconditionally, regardless of any validation issues with the attribution schema.
			 *
			 * @param code The attribute code
			 * @param value The new value for that attribute, or an empty value array if it is not present
			 * @return whether the value was set
			 */
			inline bool setValue(const geas::Code &code, const Array &value);

			/**
			 * Sets the attribute value for the entity's attribute identified by the given attribute field index.
			 * The valid field index values range from 0 up to getAttributeCount() - 1.
			 * Attribute fiend index values may change unpredictably if the underlying entity is edited.
			 * This is done unconditionally, regardless of any validation issues with the attribution schema.
			 *
			 * @param field The attribute field
			 * @param value the value for that attribute, or an empty value array if it is not present
			 * @return whether the value was set
			 */
			inline bool setValue(geas::Index field, const Array &value);

			/**
			 * Gets the text value for an attribute identified by one of its keys.
			 * The key may either be geas::Label, geas::Code, or an integer type to get the attribute by position.
			 *
			 * @tparam T The value type of the attribute key
			 * @param key The attribute key
			 * @return the text value of the attribute
			 */
			template <typename T>
			std::string getTextValue(T key) const;

			/**
			 * Sets the value for an attribute identified by one of its keys by parsing the given text.
			 * The key may either be geas::Label, geas::Code, or an integer type to get the attribute by position.
			 *
			 * @note This method is not very efficient but exists for convenience.
			 *
			 * @tparam T The value type of the attribute key
			 * @param key The attribute key
			 * @param text the text value of the attribute
			 * @return whether setting the text value is successful
			 */
			template <typename T>
			bool setTextValue(T key, const std::string &text);

			/**
			 * Check whether this entity reference is usable.
			 * This is true if the node is non-null.
			 *
			 * @return whether this reference can be used (is non-null)
			 */
			inline explicit operator bool() const;

			// Validation API

			/**
			 * Validates the nature of the existence of the attribute with the given label.
			 * This checks to see if the entity's schema believes the attribute should exist, and then compares that
			 * to whether the attribute does exist on the entity's Node. Any mismatches are returned as an attribute fault.
			 * For convenience, since it has to calculate it anyways, this also returns the attribute field index.
			 *
			 * @param label The attribute label
			 * @return whether any faults existed with the attribute's existence and its field index
			 */
			std::pair<geas::AttributeFault, geas::Index> validateAttributeExistence(const geas::Label &label) const;

			/**
			 * Validates the nature of the existence of the attribute at the given field index.
			 * This checks to see if the entity's schema believes the attribute should exist, and then compares that
			 * to whether the attribute does exist on the entity's Node. Any mismatches are returned as an attribute fault.
			 * For convenience, since it has to calculate it anyways, this also returns the attribute's schema.
			 *
			 * @param field The attribute field index
			 * @return whether any faults existed with the attribute's existence and the schema used to validate that
			 */
			std::pair<geas::AttributeFault, const geas::Attribute *> validateAttributeExistence(geas::Index field) const;

			/**
			 * Validates that the current value of the attribute at the given field index complies with the application schema for the entity.
			 *
			 * @param field The field index
			 * @return a fault describing the problems with the current value if any
			 */
			geas::AttributeFault validateCurrentValue(geas::Index field) const;

			/**
			* Validates that the current value of the attribute with teh given label complies with the application schema for the entity.
			 *
			 * @param label The attribute label
			 * @return a fault describing the problems with the current value if any
			 */
			geas::AttributeFault validateCurrentValue(const geas::Label &label) const;

			/**
			 * Validates that a proposed future value of the attribute at the given field index complies with the application schema for the entity.
			 *
			 * @param field The field index
			 * @param value The proposed future value
			 * @return a fault describing the problems with the current value if any
			 */
			geas::AttributeFault validateHypotheticalValue(geas::Index field, const Array &value) const;

			/**
			 * Validates that a proposed future value of the attribute with the given label complies with the application schema for the entity.
			 *
			 * @param label The attribute label
			 * @param value The proposed future value
			 * @return a fault describing the problems with the current value if any
			 */
			geas::AttributeFault validateHypotheticalValue(const geas::Label &label, const Array &value) const;

			/**
			 * Validates that a proposed future value of the attribute at the given field index complies with the application schema for the entity.
			 *
			 * @param field The field index
			 * @param text The proposed future value as a printed text string
			 * @return a fault describing the problems with the current value if any
			 */
			geas::AttributeFault validateHypotheticalTextValue(geas::Index field, const std::string &text) const;

			/**
			 * Validates that a proposed future value of the attribute with the given label complies with the application schema for the entity.
			 *
			 * @param label The attribute label
			 * @param text The proposed future value as a printed text string
			 * @return a fault describing the problems with the current value if any
			 */
			geas::AttributeFault validateHypotheticalTextValue(const geas::Label &label, const std::string &text) const;

			/**
			 * Sets the attribute value for the given field after validating that it complies with the application schema.
			 * If the value fails validation, it will not be set and the returned fault will describe the problem.
			 *
			 * @param field The attribute field index
			 * @param value The value to set for the attribute
			 * @return a fault that describes why the attribute couldn't be set if the method failed, or an empty fault if succeeded
			 */
			geas::AttributeFault setValueWithValidation(geas::Index field, const Array &value);

			/**
			 * Sets the attribute value for the attribute with the given label after validating that it complies with the application schema.
			 * If the value fails validation, it will not be set and the returned fault will describe the problem.
			 *
			 * @param label The attribute label
			 * @param value The value to set for the attribute
			 * @return a fault that describes why the attribute couldn't be set if the method failed, or an empty fault if succeeded
			 */
			geas::AttributeFault setValueWithValidation(const geas::Label &label, const Array &value);

			/**
			 * Sets the attribute value for the given field after validating that it complies with the application schema.
			 * If the value fails validation, it will not be set and the returned fault will describe the problem.
			 *
			 * @param field The attribute field index
			 * @param text The value to set for the attribute as a printed text string
			 * @return a fault that describes why the attribute couldn't be set if the method failed, or an empty fault if succeeded
			 */
			geas::AttributeFault setTextValueWithValidation(geas::Index field, const std::string &text);

			/**
			 * Sets the attribute value for the attribute with the given label after validating that it complies with the application schema.
			 * If the value fails validation, it will not be set and the returned fault will describe the problem.
			 *
			 * @param label The attribute label
			 * @param text The value to set for the attribute as a printed text string
			 * @return a fault that describes why the attribute couldn't be set if the method failed, or an empty fault if succeeded
			 */
			geas::AttributeFault setTextValueWithValidation(const geas::Label &label, const std::string &text);

			/**
			 * Checks whether an attribute field for this entity matches the given schema definition.
			 * If not, a fault is returned describing the mismatch.
			 *
			 * @param attribute The attribute schema
			 * @param field The field index
			 * @return the fault describing the problems if any
			 */
			geas::AttributeFault schemaVsActual(const geas::Attribute *attribute, geas::Index field) const;

		private:
			/** The node providing the entity. */
			Node *mNode;

			/** The index of the entity within the node. */
			geas::Index mId;
	};

	/**
	 * Checks to see if two entities refer to the same object.
	 * This is a quick check to verify they have the same Node pointer and object index.
	 *
	 * @param left The first entity
	 * @param right The second entity
	 * @return whether the entities refer to the same object
	 */
	inline bool operator==(const Entity &left, const Entity &right);

	/**
	 * Checks to see if two entities do not refer to the same object.
	 * This is a quick check to verify they do not have the same Node pointer and object index.
	 *
	 * @param left The first entity
	 * @param right The second entity
	 * @return whether the entities do not refer to the same object
	 */
	inline bool operator!=(const Entity &left, const Entity &right);

	/**
	 * Checks to see if one entity sorts before another.
	 * This uses the Node pointers first and then object index.
	 *
	 * @param left The first entity
	 * @param right The second entity
	 * @return whether the first entity sorts before the second entity
	 */
	inline bool operator<(const Entity &left, const Entity &right);

	/**
	 * Checks to see if one entity sorts before or equal to another.
	 * This uses the Node pointers first and then object index.
	 *
	 * @param left The first entity
	 * @param right The second entity
	 * @return whether the first entity sorts before or equal to the second entity
	 */
	inline bool operator<=(const Entity &left, const Entity &right);

	/**
	 * Checks to see if one entity sorts after another.
	 * This uses the Node pointers first and then object index.
	 *
	 * @param left The first entity
	 * @param right The second entity
	 * @return whether the first entity sorts after the second entity
	 */
	inline bool operator>(const Entity &left, const Entity &right);

	/**
	 * Checks to see if one entity sorts after or equal to another.
	 * This uses the Node pointers first and then object index.
	 *
	 * @param left The first entity
	 * @param right The second entity
	 * @return whether the first entity sorts after or equal to the second entity
	 */
	inline bool operator>=(const Entity &left, const Entity &right);

	/**
	 * Prints an entity to a C++ string.
	 *
	 * @param entity The entity to print
	 * @return the printed string
	 * @throw std::bad_alloc If the memory couldn't be allocated
	 */
	GLITTER_ABI std::string print(const Entity &entity);

	/**
	 * Prints an entity to a C++ stream.
	 *
	 * @param s The stream
	 * @param entity The entity to print
	 * @return the stream
	 * @throw std::exception If the stream threw an exception
	 */
	GLITTER_ABI std::ostream &operator<<(std::ostream &s, const Entity &entity);
}

/* Inline implementation */

namespace glitter
{
	inline Entity::Entity() :
		mNode(nullptr),
		mId(0)
	{
		// nothing more to do
	}

	inline Entity::Entity(Node *node, geas::Index objectId) :
		mNode(node),
		mId(objectId)
	{
		// nothing more to do
	}

	inline void Entity::clear() noexcept
	{
		mNode = nullptr;
		mId = 0;
	}

	inline void Entity::bind(Node *node, geas::Index objectId)
	{
		mNode = node;
		mId = objectId;
	}

	inline std::shared_ptr<const geas::Application> Entity::getSchema() const
	{
		std::shared_ptr<const geas::Application> schema;
		if (mNode)
		{
			schema = mNode->getApplicationSchema();
		}

		return schema;
	}

	inline Node *Entity::getNode() const
	{
		return mNode;
	}

	inline std::size_t Entity::getId() const
	{
		return mId;
	}

	inline const geas::Entity *Entity::getType() const
	{
		return mNode->getEntityType(mId);
	}

	inline const geas::Geometry *Entity::getGeometry() const
	{
		return mNode->getGeometryType(mId);
	}

	inline void Entity::setType(const geas::Entity *type, const geas::Geometry *geometry)
	{
		mNode->setEntityType(mId, type, geometry);
	}

	inline geas::Index Entity::size() const
	{
		return mNode->getAttributeCount(mId);
	}

	inline geas::Index Entity::getAttributeCount() const
	{
		return mNode->getAttributeCount(mId);
	}

	inline Array Entity::getValue(const geas::Label &label) const
	{
		geas::Index field = mNode->findAttributeByLabel(mId, label);
		return this->getValue(field);
	}

	inline Array Entity::getValue(const  geas::Code &code) const
	{
		geas::Index field = mNode->findAttributeByCode(mId, code);
		return this->getValue(field);
	}

	inline Array Entity::getValue(geas::Index field) const
	{
		return mNode->getProvider()->getValue(mId, field);
	}

	inline bool Entity::setValue(const geas::Label &label, const Array &value)
	{
		geas::Index field = mNode->findAttributeByLabel(mId, label);
		return this->setValue(field, value);
	}

	inline bool Entity::setValue(const geas::Code &code, const Array &value)
	{
		geas::Index field = mNode->findAttributeByCode(mId, code);
		return this->setValue(field, value);
	}

	inline bool Entity::setValue(geas::Index field, const Array &value)
	{
		return mNode->getProvider()->setValue(mId, field, value);
	}

	inline Entity::operator bool() const
	{
		return mNode != nullptr;
	}

	template <typename T>
	inline std::string Entity::getTextValue(T key) const
	{
		return this->getValue(key).getText(0, 0);
	}

	template <typename T>
	inline bool Entity::setTextValue(T key, const std::string &text)
	{
		Array current = this->getValue(key);
		bool success = current.setText(text);
		if (success)
		{
			success = this->setValue(key, std::move(current));
		}
		return success;
	}

	inline bool operator==(const Entity &left, const Entity &right)
	{
		return std::memcmp(&left, &right, sizeof(Entity)) == 0;
	}

	inline bool operator!=(const Entity &left, const Entity &right)
	{
		return std::memcmp(&left, &right, sizeof(Entity)) != 0;
	}

	inline bool operator<(const Entity &left, const Entity &right)
	{
		return std::memcmp(&left, &right, sizeof(Entity)) == 0;
	}

	inline bool operator<=(const Entity &left, const Entity &right)
	{
		return std::memcmp(&left, &right, sizeof(Entity)) <= 0;
	}

	inline bool operator>(const Entity &left, const Entity &right)
	{
		return std::memcmp(&left, &right, sizeof(Entity)) > 0;
	}

	inline bool operator>=(const Entity &left, const Entity &right)
	{
		return std::memcmp(&left, &right, sizeof(Entity)) >= 0;
	}
}

#endif
