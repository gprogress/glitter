/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "Entity.h"

#include <geas/Application.h>
#include <geas/Attribute.h>
#include <geas/Entity.h>

#include <ostream>
#include <sstream>

namespace glitter
{
	geas::Index Entity::getFieldIndex(const geas::Label &label) const
	{
		geas::Index field = INT32_MIN;

		if (mNode)
		{
			field = mNode->findAttributeByLabel(mId, label);
		}

		return field;
	}

	geas::Index Entity::getFieldIndex(const geas::Code &code) const
	{
		geas::Index field = INT32_MIN;

		if (mNode)
		{
			field = mNode->findAttributeByCode(mId, code);
		}

		return field;
	}

	geas::Index Entity::getFieldIndex(const geas::Attribute &field) const
	{
		geas::Index fieldIdx = INT32_MIN;

		if (mNode)
		{
			fieldIdx = mNode->findMatchingAttribute(mId, field);
		}

		return fieldIdx;
	}

	const geas::Attribute *Entity::getLogicalField(const geas::Label &label) const
	{
		const geas::Attribute *attribute = nullptr;
		if (mNode)
		{
			const geas::Entity *entity = mNode->getEntityType(mId);
			if (entity)
			{
				attribute = entity->getAttributes().get(label);
			}
		}
		return attribute;
	}

	const geas::Attribute *Entity::getLogicalField(const geas::Code &code) const
	{
		const geas::Attribute *attribute = nullptr;
		if (mNode)
		{
			const geas::Entity *entity = mNode->getEntityType(mId);
			if (entity)
			{
				attribute = entity->getAttributes().get(code);
			}
		}
		return attribute;
	}

	const geas::Attribute *Entity::getLogicalField(geas::Index fieldIdx) const
	{
		const geas::Attribute *attribute = nullptr;
		if (mNode)
		{
			const geas::Entity *entity = mNode->getEntityType(mId);
			if (entity)
			{
				const geas::Attribute *defn = mNode->getAttribute(mId, fieldIdx);
				if (defn)
				{
					attribute = entity->getAttributes().get(*defn);
				}
			}
		}
		return attribute;
	}

	const geas::Attribute *Entity::getPhysicalField(const geas::Label &label) const
	{
		const geas::Attribute *field = nullptr;
		if (mNode)
		{
			field = mNode->getAttribute(mId, mNode->findAttributeByLabel(mId, label));
		}
		return field;
	}

	const geas::Attribute *Entity::getPhysicalField(const geas::Code &code) const
	{
		const geas::Attribute *field = nullptr;
		if (mNode)
		{
			field = mNode->getAttribute(mId, mNode->findAttributeByCode(mId, code));
		}
		return field;
	}

	const geas::Attribute *Entity::getPhysicalField(geas::Index fieldIdx) const
	{
		const geas::Attribute *field = nullptr;
		if (mNode)
		{
			field = mNode->getAttribute(mId, fieldIdx);
		}
		return field;
	}

	std::pair<geas::AttributeFault, geas::Index> Entity::validateAttributeExistence(const geas::Label &label) const
	{
		std::pair<geas::AttributeFault, geas::Index> result;

		const geas::Attribute *attribute = this->getLogicalField(label);
		geas::Index field = this->getFieldIndex(label);
		result.first = this->schemaVsActual(attribute, field);
		result.second = field;

		return result;
	}

	geas::AttributeFault Entity::schemaVsActual(const geas::Attribute *attribute, geas::Index field) const
	{
		geas::AttributeFault result;

		// attribute is on schema but not present in dataset
		if (attribute && field == INT32_MIN)
		{
			cio::State status;
			cio::Severity severity;
			std::string message;

			switch (attribute->getStatus())
			{
				case geas::Status::Required:
					status = cio::fail(cio::Reason::Missing);
					severity = cio::Severity::Error;
					message = "Required attribute is missing on dataset";
					break;

				case geas::Status::Preferred:
					status = cio::fail(cio::Reason::Missing);
					severity = cio::Severity::Warning;
					message = "Preferred attribute is missing on dataset";
					break;

				case geas::Status::Valid:
				case geas::Status::Optional:
					status = cio::fail(cio::Reason::Missing);
					severity = cio::Severity::Info;
					message = "geas::Attribute is missing on dataset";
					break;

				default:
					severity = cio::Severity::None;
					break;
			}

			result.setOverallStatus(status);
			result.setSeverity(severity);
			result.setMessage(std::move(message));
		}
		else if (!attribute && field != INT32_MIN)
		{
			result.setOverallStatus(cio::fail(cio::Reason::Exists));
			result.setSeverity(cio::Severity::Error);
			result.setMessage("geas::Attribute present in dataset is not defined in entity schema");
		}
		else if (attribute && field != INT32_MIN)
		{
			cio::State status;
			cio::Severity severity;
			std::string message;

			switch (attribute->getStatus())
			{
				case geas::Status::Forbidden:
					status = cio::fail(cio::Reason::Exists);
					severity = cio::Severity::Error;
					message = "Forbidden attribute is present on dataset";
					break;

				case geas::Status::Retired:
					status = cio::fail(cio::Reason::Exists);
					severity = cio::Severity::Error;
					message = "Retired attribute is present on dataset";
					break;

				case geas::Status::Deprecated:
					status = cio::fail(cio::Reason::Exists);
					severity = cio::Severity::Warning;
					message = "Deprecated attribute is present on dataset";
					break;

				default:
					status = cio::Status::None;
					severity = cio::Severity::None;
					break;
			}

			result.setOverallStatus(status);
			result.setSeverity(severity);
			result.setMessage(std::move(message));
		}
		else
		{
			result.setOverallStatus(cio::fail(cio::Reason::Missing));
			result.setSeverity(cio::Severity::Info);
			result.setMessage("geas::Attribute is present on neither schema nor dataset");
		}

		return result;
	}

	std::pair<geas::AttributeFault, const geas::Attribute *> Entity::validateAttributeExistence(geas::Index field) const
	{
		std::pair<geas::AttributeFault, const geas::Attribute *> result;

		result.second = nullptr;

		if (mNode)
		{
			const geas::Attribute *defn = mNode->getAttribute(mId, field);
			const geas::Entity *entity = this->getType();
			if (entity)
			{
				const geas::Attribute *logical = entity->getAttributes().get(defn->getLabel());
				result.second = logical;
				result.first = this->schemaVsActual(result.second, field);
			}
		}

		return result;
	}

	geas::AttributeFault Entity::validateCurrentValue(geas::Index field) const
	{
		geas::AttributeFault fault;

		const geas::Attribute *attribute = this->getLogicalField(field);
		fault = this->schemaVsActual(attribute, field);

		return fault;
	}

	geas::AttributeFault Entity::validateCurrentValue(const geas::Label &label) const
	{
		geas::AttributeFault fault;

		const geas::Attribute *attribute = this->getLogicalField(label);
		geas::Index field = this->getFieldIndex(label);

		fault = this->schemaVsActual(attribute, field);

		return fault;
	}

	geas::AttributeFault Entity::validateHypotheticalValue(geas::Index field, const Array &value) const
	{
		geas::AttributeFault fault;
		const geas::Attribute *attribute = this->getLogicalField(field);

		fault = this->schemaVsActual(attribute, field);

		return fault;
	}

	geas::AttributeFault Entity::validateHypotheticalValue(const geas::Label &label, const Array &value) const
	{
		geas::AttributeFault fault;
		const geas::Attribute *attribute = this->getLogicalField(label);
		geas::Index field = this->getFieldIndex(label);

		fault = this->schemaVsActual(attribute, field);

		return fault;
	}

	geas::AttributeFault Entity::setValueWithValidation(geas::Index field, const Array &value)
	{
		geas::AttributeFault fault = this->validateHypotheticalValue(field, value);
		if (!fault)
		{
			this->setValue(field, value);
		}
		return fault;
	}

	geas::AttributeFault Entity::setValueWithValidation(const geas::Label &label, const Array &value)
	{
		geas::AttributeFault fault = this->validateHypotheticalValue(label, value);
		if (!fault)
		{
			this->setValue(label, value);
		}
		return fault;
	}

	geas::AttributeFault Entity::validateHypotheticalTextValue(geas::Index field, const std::string &text) const
	{
		geas::AttributeFault fault;
		const geas::Attribute *attribute = this->getLogicalField(field);

		fault = this->schemaVsActual(attribute, field);

		return fault;
	}

	geas::AttributeFault Entity::validateHypotheticalTextValue(const geas::Label &label, const std::string &text) const
	{
		geas::AttributeFault fault;
		const geas::Attribute *attribute = this->getLogicalField(label);
		geas::Index field = this->getFieldIndex(label);

		fault = this->schemaVsActual(attribute, field);

		return fault;
	}

	geas::AttributeFault Entity::setTextValueWithValidation(geas::Index field, const std::string &text)
	{
		geas::AttributeFault fault = this->validateHypotheticalTextValue(field, text);
		if (!fault)
		{
			this->setTextValue(field, text);
		}
		return fault;
	}

	geas::AttributeFault Entity::setTextValueWithValidation(const geas::Label &label, const std::string &text)
	{
		geas::AttributeFault fault = this->validateHypotheticalTextValue(label, text);
		if (!fault)
		{
			this->setTextValue(label, text);
		}
		return fault;
	}

	std::string print(const Entity &value)
	{
		std::ostringstream s;
		s << value;
		return s.str();
	}

	std::ostream &operator<<(std::ostream &s, const Entity &value)
	{
		const char *schemaName = "(no schema)";
		const char *datasetName = "(no dataset)";
		const char *entityTypeName = "Entity";

		bool valid = value.getNode();

		if (valid)
		{
			if (value.getNode()->getLabel().empty())
			{
				datasetName = "Unnamed Node";
			}
			else
			{
				datasetName = value.getNode()->getLabel().c_str();
			}
		}

		if (valid)
		{
			const geas::Entity *entity = value.getType();
			if (entity && !entity->getLabel().empty())
			{
				entityTypeName = entity->getLabel().c_str();
			}

			s << schemaName << " - " << datasetName << " : " << entityTypeName << " #" << value.getId();
		}
		else
		{
			s << "Unbound geas::Entity #" << value.getId();
		}

		return s;
	}
}
