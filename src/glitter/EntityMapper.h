/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_ENTITYMAPPER_H
#define GLITTER_ENTITYMAPPER_H

#include "Types.h"

#include "Delegate.h"

#include <geas/Identifier.h>

#include <memory>
#include <utility>
#include <vector>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace glitter
{
	/**
	 * The Entity Mapper is a Delegate that defines the interface to obtain the types and attribute values of entities
	 * given a more general data Provider supplying unstructured attribute values.
	 *
	 * It consists of a Provider that provides the interface to
	 * get and set particular attribute values for particular entities, and a set of type sources to identify
	 * which attributes provide the value(s) necessary to decide the actual entity type.
	 */
	class GLITTER_ABI EntityMapper : public Delegate
	{
		public:
			/**
			 * Structure describing a particular attribute used to determine entity type.
			 */
			struct TypeSource
			{
				/** Attribute schema definition - general lookup. */
				const geas::Attribute *attribute;

				/** Attribute field index - optimized lookup, INT32_MIN if not possible to use. */
				geas::Index field;

				/** The type of identifier for an entity that the attribute values provide. */
				geas::Identifier identifier;
			};

			/**
			 * Construct the empty entity provider.
			 */
			EntityMapper() noexcept;

			/**
			 * Construct a deep copy of an entity provider.
			 *
			 * @param in The entity provider to copy
			 */
			EntityMapper(const EntityMapper &in);

			/**
			 * Construct an entity provider by transferring the contents of an existing one.
			 *
			 * @param in The entity provider to move
			 */
			EntityMapper(EntityMapper &&in) noexcept;

			/**
			 * Assign a deep copy of an entity provider.
			 *
			 * @param in The entity provider to copy
			 * @return this entity provider
			 */
			EntityMapper &operator=(const EntityMapper &in);

			/**
			 * Transfer the contents of an entity provider into this one.
			 *
			 * @param in The entity provider to move
			 * @return this entity provider
			 */
			EntityMapper &operator=(EntityMapper &&in) noexcept;

			/**
			 * Destructor.
			 */
			virtual ~EntityMapper() noexcept override;

			/**
			 * Clears all contents of this entity provider.
			 */
			virtual void clear() noexcept override;

			/**
			* Creates an object with the given entity type and geometry.
			* The entity mapper will be used to set the entity type and geometry type attribute value(s).
			* If entity or geometry type is null, the default types set on this mapper will be used instead.
			*
			* @param entity The entity type to create
			* @param geometry The geometry type to create
			* @return The index of the created object
			*/
			virtual geas::Index create(const geas::Entity *entity = nullptr, const geas::Geometry *geometry = nullptr) override;

			/**
			 * Gets the definition of the entity type for the given object.
			 * If entity type attributes have been defined, they are examined in the order defined to identify the first entity type that
			 * is valid according to the application schema.
			 *
			 * If no valid entity type was found, or if no entity type attributes are defined, the default entity type set on this mapper is returned.
			 *
			 * @param object The index of the object to examine
			 * @return the entity type definition for the requested object
			 */
			virtual const geas::Entity *getEntityType(geas::Index object) const override;

			/**
			 * Gets the definition of the geometry type for the given object.
			 * If geometry type attributes have been defined, they are examined in the order defined to identify the first geometry type that
			 * is valid according to the provided application schema.
			 *
			 * If no valid geometry type was found, or if no geometry type attributes are defined, the default geometry type set on this mapper is returned.
			 *
			 * @param object The index of the object to examine
			 * @return the geometry type definition for the requested object
			 */
			virtual const geas::Geometry *getGeometryType(geas::Index object) const override;

			/**
			 * Sets the definition of the entity type for the given object.
			 * If entity type attributes have been defined, they are examined in the order defined to set the corresponding value from the given entity definition.
			 *
			 * If no entity type attributes are defined, this method will throw an exception that there is no way to set the entity type individually for each object.
			 *
			 * @param object The index of the object to modify
			 * @param entity the entity type definition to set for the requested object
			 * @param geometry The geometry type definition to set for the requested object
			 * @throw std::invalid_argument If no entity type attributes have been defined so the entity type cannot be set individually per object
			 */
			virtual void setEntityType(geas::Index object, const geas::Entity *entity, const geas::Geometry *geometry = nullptr) override;

			/**
			 * Refresh the bound entity and geometry cached field index values if the underlying provider has changed.
			 * This will reset all cached field index values to the invalid state, then if the new provider is fixed field layout
			 * will recalculate them.
			 */
			virtual void onProviderChanged() override;

			/**
			 * Gets the application schema used by this mapper if one is set.
			 *
			 * @return the application schema or null if none is in use
			 */
			std::shared_ptr<const geas::Application> getApplicationSchema() const noexcept;

			/**
			 * Sets the current application schema.
			 *
			 * @param schema The application schema to set
			 */
			void setApplicationSchema(std::shared_ptr<const geas::Application> schema) noexcept;

			/**
			 * Removes the current application schema in use.
			 */
			void clearApplicationSchema() noexcept;

			/**
			 * Creates a list of the full definitions of all of the entity type attributes, obtained from the value provider.
			 *
			 * @return the entity type attributes
			 */
			std::vector<geas::Attribute> getEntityTypeAttributes() const;

			/**
			 * Gets direct access to the list of entity type attribute source definitions.
			 *
			 * @return the entity type sources list
			 */
			std::vector<EntityMapper::TypeSource> &getEntityTypeSources() noexcept;

			/**
			 * Gets read-only direct access to the list of entity type attribute source definitions.
			 *
			 * @return the entity type sources list
			 */
			const std::vector<EntityMapper::TypeSource> &getEntityTypeSources() const noexcept;

			/**
			 * Specifies that the given attribute provides an entity type definition using the specified type of identifier.
			 * If the current provider provides a fixed-attribute layout (all objects have the same attributes in the same order)
			 * it will also cache the field index.
			 *
			 * @param attribute The attribute to add
			 * @param idType The type of identifier that this attribute value provides such as geas::Label, geas::Code, etc.
			 */
			void bindEntityTypeSource(const geas::Attribute *attribute, geas::Identifier idType);

			/**
			 * Removes the given attribute from the list of attributes that can provide the entity type.
			 *
			 * @param attribute The attribute to remove
			 */
			void unbindEntityTypeSource(const geas::Attribute *attribute);

			/**
			 * Removes all definitions of which attributes might provide entity type information.
			 */
			void clearEntityTypeSource() noexcept;

			/**
			 * Creates a list of the full definitions of all of the entity type attributes, obtained from the value provider.
			 *
			 * @return the entity type attributes
			 */
			std::vector<geas::Attribute> getGeometryTypeAttributes() const;

			/**
			 * Gets direct access to the list of geometry type attribute source definitions.
			 *
			 * @return the geometry type sources list
			 */
			std::vector<EntityMapper::TypeSource> &getGeometryTypeSources() noexcept;

			/**
			 * Gets read-only direct access to the list of geometry type attribute source definitions.
			 *
			 * @return the geometry type sources list
			 */
			const std::vector<EntityMapper::TypeSource> &getGeometryTypeSources() const noexcept;

			/**
			 * Specifies that the given attribute field number provides a geometry type definition using the specified type of identifier.
			 *
			 * @param attribute The attribute to add
			 * @param idType The type of identifier that this attribute value provides such as geas::Label, geas::Code, etc.
			 */
			void bindGeometryTypeSource(const geas::Attribute *attribute, geas::Identifier idType);

			/**
			 * Removes the given attribute field number from the list of attributes that can provide the geometry type.
			 *
			 * @param attribute The attribute to remove
			 */
			void unbindGeometryTypeSource(const geas::Attribute *attribute);

			/**
			 * Removes all definitions of which attributes might provide geometry type information.
			 */
			void clearGeometryTypeSource() noexcept;

		private:
			/**
			 * Encodes a reference to the given definition as defined by the given type source.
			 *
			 * @param object The object index
			 * @param source The type source description
			 * @param target The definition to set
			 */
			void setTypeReference(geas::Index object, const std::vector<EntityMapper::TypeSource> &source, const geas::Definition *target);

			/**
			 * Decodes a reference to a definition on the given object as defined by the given type sources.
			 *
			 * @param object The object index
			 * @param source The type sources
			 * @return the definition found, if any
			 */
			geas::Definition getTypeReference(geas::Index object, const std::vector<EntityMapper::TypeSource> &source) const;

			/** The application schema used to locate entities and attributes. */
			std::shared_ptr<const geas::Application> mSchema;

			/** The list of attributes and identifier types that provide entity type. */
			std::vector<EntityMapper::TypeSource> mEntityTypeAttributes;

			/** The list of attributes and identifier types that provide geometry type. */
			std::vector<EntityMapper::TypeSource> mGeometryTypeAttributes;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
