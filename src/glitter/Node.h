/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_NODE_H
#define GLITTER_NODE_H

#include "Types.h"

#include "Delegate.h"

#include <geas/Attribute.h>

#include <memory>
#include <vector>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace glitter
{
	/**
	 * The Node represents a collection or subset of a collection with a unique identity.
	 * It also implements the Provider interface by delegating all queries to some other Provider
	 * to provide entity, geometry, and attribute schemas and data values.
	 *
	 * For simple data formats, the entire collection or file might just be one node. However, for more complex
	 * nodes each distinct part of the node may be a separate node. For example, each table in an SQL database
	 * could be realized as a separate node, or each major element in a 3D model such as glTF document.
	 *
	 * Where relevant, the Node uses a read-only Application Schema provided by the Catalog or some other source.
	 */
	class GLITTER_ABI Node : public geas::Definition, public Delegate
	{
		public:
			/**
			 * Default constructor.
			 */
			Node() noexcept;

			/**
			 * Copy constructor.
			 *
			 * @param in The node to copy
			 */
			Node(const Node &in);

			/**
			 * Move constructor.
			 *
			 * @param in The node to move
			 */
			Node(Node &&in) noexcept;

			/**
			 * Copy assignment.
			 *
			 * @param in The node to copy
			 * @return this node
			 */
			Node &operator=(const Node &in);

			/**
			 * Move assignment.
			 *
			 * @param in The node to move
			 * @return this node
			 */
			Node &operator=(Node &&in) noexcept;

			/**
			 * Destructor.
			 */
			virtual ~Node() noexcept override;

			/**
			 * Gets the runtime metaclass for this object instance.
			 *
			 * @return the runtime metaclass for this object
			 */
			virtual const cio::Metaclass &getMetaclass() const noexcept override;

			/**
			 * Clears the node state.
			 */
			virtual void clear() noexcept override;

			/**
			 * Gets the application schema used by this node if one is set.
			 *
			 * @return the application schema or null if none is in use
			 */
			std::shared_ptr<const geas::Application> getApplicationSchema() const noexcept;

			/**
			 * Sets the current application schema.
			 *
			 * @param schema The application schema to set
			 */
			void setApplicationSchema(std::shared_ptr<const geas::Application> schema) noexcept;

			/**
			 * Removes the current application schema in use.
			 */
			void clearApplicationSchema() noexcept;

			/**
			 * Sets the parent node that contains this node.
			 *
			 * @param parent the parent node
			 */
			void setContainer(Node *parent) noexcept;

			/**
			 * Gets the parent node that contains this node.
			 *
			 * @return the parent node
			 */
			Node *getContainer() noexcept;

			/**
			 * Gets the parent node that contains this node.
			 *
			 * @return the parent node
			 */
			const Node *getContainer() const noexcept;

			/**
			 * Gets the list of child nodes contained by this one.
			 *
			 * @return the child nodes
			 */
			const std::vector<const Node *> &getContainedNodes() const noexcept;

			/**
			 * Gets the list of child nodes contained by this one.
			 *
			 * @return the child nodes
			 */
			std::vector<Node *> &getContainedNodes() noexcept;

			/**
			 * Sets the list of contained child nodes.
			 *
			 * @param nodes The list of child nodes
			 */
			void setContainedNodes(std::vector<Node *> nodes) noexcept;

			/**
			 * Adds a node to the list of contained child nodes.
			 *
			 * @param node The node to add
			 */
			void addContainedNode(Node *node);

			/**
			 * Removes a node from the list of contained child nodes.
			 *
			 * @param node The node to remove
			 */
			void removeContainedNode(const Node *node);

			/**
			 * Clears the list of contained child ndoes.
			 */
			void clearContainedNodes() noexcept;

			/**
			 * Gets a reference to an entity with the given index.
			 * This does not necessarily guarantee the entity exists.
			 *
			 * @param idx The entity index
			 * @return the entity index
			 */
			Entity getEntity(geas::Index idx) noexcept;

			/**
			* Create a new entity using the given entity template.
			* If no provider currently exists, a default one (currently Table) is created.
			*
			* @param entity The entity to be bound to the reference
			* @return The entity that was created
			*/
			Entity createEntity(const geas::Entity *entity);

			/**
			 * Gets the map projection assigned to this node, if any.
			 *
			 * @return the projection, or nullptr if none was set
			 */
			const geas::Projection *getProjection() const noexcept;

			/**
			 * Sets the map projection assigned to this node, if any.
			 *
			 * @param projection the projection, or nullptr if none was set
			 */
			void setProjection(const geas::Projection *projection) noexcept;

		private:
			/** The metaclass for this class. */
			static cio::Class<Node> sMetaclass;

			/** Application schema in use. */
			std::shared_ptr<const geas::Application> mApplicationSchema;

			/** Parent node. */
			Node *mContainer;

			/** Child nodes. */
			std::vector<Node *> mChildren;

			/** Projection assigned to this node, if any. */
			const geas::Projection *mProjection;

			/** Data provider. */
			std::shared_ptr<Provider> mProvider;
	};
}

namespace geas
{
	/** Instantiate geas::Dictionary<Node> for use in other containers. */
	extern template class GLITTER_TEMPLATE geas::Dictionary<glitter::Node>;
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
