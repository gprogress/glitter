/*=====================================================================================================================
 * Copyright 2022-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "LasReader.h"

#include "TiffReader.h"

#include <cio/Buffer.h>
#include <cio/Class.h>
#include <cio/Input.h>
#include <cio/UniqueId.h>

#include <geas/Attribute.h>
#include <geas/Geometry.h>
#include <geas/Projection.h>

#include "Catalog.h"
#include "Collection.h"
#include "Node.h"

namespace glitter
{
	cio::Class<LasReader> LasReader::sMetaclass("glitter::LasReader");

	const cio::FixedText<16> LasReader::VLR_PROJECTION_USER("LASF_Projection");

	const std::uint16_t LasReader::VLR_PROJECTION_WKT_TYPE = 2111;

	const std::uint16_t LasReader::VLR_PROJECTION_TIFF_TYPE = 34735;

	const cio::Class<LasReader> &LasReader::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}

	LasReader::LasReader() noexcept :
		mLasFormat(0),
		mNode(nullptr),
		mProjection(nullptr),
		mCurrentOffset(0),
		mPointOffset(0),
		mPointCount(0),
		mPointLength(0)
	{
		// nothing more to do
	}

	LasReader::LasReader(std::shared_ptr<Profile> profile) noexcept :
		ModelReader(std::move(profile)),
		mLasFormat(0),
		mNode(nullptr),
		mProjection(nullptr),
		mCurrentOffset(0),
		mPointOffset(0),
		mPointCount(0),
		mPointLength(0)
	{
		// nothing more to do
	}

	LasReader::LasReader(const LasReader &in) = default;

	LasReader::LasReader(LasReader &&in) noexcept = default;

	LasReader &LasReader::operator=(const LasReader &in) = default;

	LasReader &LasReader::operator=(LasReader &&in) noexcept = default;

	LasReader::~LasReader() noexcept = default;

	void LasReader::clear() noexcept
	{
		ModelReader::clear();

		mCatalog.reset();
		mTable.reset();

		mLasFormat = 0;
		mNode = nullptr;
		mProjection = nullptr;
		mCurrentOffset = 0;
		mPointCount = 0;
		mPointLength = 0;
		mPointOffset = 0;
	}

	const cio::Metaclass &LasReader::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	void LasReader::loadCatalogFromInput(Catalog &catalog, cio::Input &input)
	{
		std::shared_ptr<Profile> profile = this->getOrCreateProfile();
		std::shared_ptr<geas::Application> app = catalog.getOrCreateApplicationSchema();

		// Buffer to inspect data
		cio::Buffer buffer(4096);

		// Baseline LAS 1.0 format has minimum of 235 bytes for header
		buffer.limit(227);
		input.fill(buffer);
		buffer.flip();

		cio::FixedText<4> signature = buffer.getFixedText<4>();
		if (signature != "LASF")
		{
			throw cio::Exception(cio::Reason::Unparsable, "LASF signature missing");
		}

		std::uint16_t sourceId = buffer.getLittle<std::uint16_t>();
		std::uint16_t globalEncoding = buffer.getLittle<std::uint16_t>();
		bool hasWkt = (globalEncoding & 0x04) != 0;

		cio::UniqueId uuid = buffer.getLittle<cio::UniqueId>(); // might not be correct due to byte layout
		std::uint16_t major = buffer.getLittle<std::uint8_t>();
		std::uint16_t minor = buffer.getLittle<std::uint8_t>();

		cio::FixedText<32> systemId = buffer.getFixedText<32>();
		cio::FixedText<32> generator = buffer.getFixedText<32>();;

		std::uint16_t creationDayOfYear = buffer.getLittle<std::uint16_t>();
		std::uint16_t creationYear = buffer.getLittle<std::uint16_t>();

		std::uint16_t headerSize = buffer.getLittle<std::uint16_t>();

		std::uint32_t offsetToPoints = buffer.getLittle<std::uint32_t>();

		std::uint32_t varLengthRecords = buffer.getLittle<std::uint32_t>();

		char format = buffer.get();

		std::uint32_t legacyPointCount = buffer.getLittle<std::uint32_t>(); // read legacy field first
		std::uint64_t pointCount = legacyPointCount;
		std::uint16_t pointLength = buffer.getLittle<std::uint16_t>();

		std::uint32_t legacyPointsByReturn[5] = {  };
		buffer.getLittleArray(legacyPointsByReturn, 5);

		std::uint64_t pointsByReturn[15] = { };
		for (std::size_t i = 0; i < 5; ++i)
		{
			pointsByReturn[i] = legacyPointsByReturn[i];
		}

		double scale[3] = { };
		buffer.getLittleArray<double>(scale, 3);

		double offset[3] = { };
		buffer.getLittleArray<double>(offset, 3);

		cio::RealInterval range[3];
		range[0].upper = buffer.getLittle<double>();
		range[0].lower = buffer.getLittle<double>();

		range[1].upper = buffer.getLittle<double>();
		range[1].lower = buffer.getLittle<double>();

		range[2].upper = buffer.getLittle<double>();
		range[2].lower = buffer.getLittle<double>();

		// Last record of original 1.0 format

		std::uint64_t waveformPacketOffset = 0;
		std::uint64_t extendedRecordOffset = 0;
		std::uint32_t extendedRecordCount = 0;

		// Track bytes read so we can seek to point record without needing to actually seek
		mCurrentOffset = buffer.position();

		// 1.3 specific header field
		if (headerSize > 227u)
		{
			// extend limit to header size
			std::size_t mark = buffer.position();
			buffer.limit(headerSize);
			input.fill(buffer);
			buffer.position(mark);

			mCurrentOffset = headerSize;

			if (headerSize >= 235u)
			{
				std::uint64_t waveformPacketOffset = buffer.getLittle<std::uint64_t>();

				// 1.4 specific header fields - validate these exist before reading

				if (headerSize >= 375u)
				{
					extendedRecordOffset = buffer.getLittle<std::uint64_t>();
					extendedRecordCount = buffer.getLittle<std::uint32_t>();

					pointCount = buffer.getLittle<std::uint64_t>();
					buffer.getLittleArray<std::uint64_t>(pointsByReturn, 15);
				}
			}
		}

		for (std::uint32_t i = 0; i < varLengthRecords; ++i)
		{
			buffer.reset();
			buffer.limit(54);
			input.fill(buffer);
			buffer.flip();

			mCurrentOffset += 54;

			std::uint16_t reserved = buffer.getLittle<std::uint16_t>();
			cio::FixedText<16> user = buffer.getFixedText<16>();
			std::uint16_t type = buffer.getLittle<std::uint16_t>();
			std::uint16_t length = buffer.getLittle<std::uint16_t>();
			cio::FixedText<32> description = buffer.getFixedText<32>();

			if (user == VLR_PROJECTION_USER)
			{
				buffer.reset();
				buffer.limit(length);
				input.fill(buffer);
				buffer.flip();

				if (type == VLR_PROJECTION_WKT_TYPE)
				{
					cio::Text text = buffer.getText();
					if (text)
					{
						geas::Projection *projection = app->getProjections().create();
						projection->getOrCreateDescription(nullptr)->setDefinition(text.duplicate());
						mProjection = projection;
					}
				}
				else if (type == VLR_PROJECTION_TIFF_TYPE)
				{
					TiffReader tiff;

					geas::Projection *projection = tiff.loadGeokeyDirectory(*app, buffer);
					mProjection = projection;
				}
			}
			else
			{
				// Skip unhandled record
				input.discard(length);
			}

			mCurrentOffset += length;
		}

		if (mCurrentOffset < offsetToPoints)
		{
			input.discard(offsetToPoints - mCurrentOffset);
			mCurrentOffset = offsetToPoints;
		}

		// Set fields 
		mLasFormat = format;
		mPointCount = pointCount;
		mPointLength = pointLength;

		// TODO use Profile to align definitions

		// Position XYZ is always valid
		geas::Geometry *geometry = app->getGeometries().getOrCreate(geas::Label("Point"));
		geas::Attribute *position = app->getAttributes().getOrCreate(geas::Label("Position"));
		geometry->addAttribute(position);

		if (mLasFormat == 3)
		{
			geas::Attribute *color = app->getAttributes().getOrCreate(geas::Label("Colour"));
			geometry->addAttribute(color);
		}
	}

	void LasReader::loadCollectionFromInput(Collection &collection, cio::Input &input)
	{
		std::shared_ptr<Profile> profile = this->getOrCreateProfile();

		if (!mCatalog)
		{
			mCatalog.reset(new Catalog());
			this->loadCatalogFromInput(*mCatalog, input);
		}

		Node node;
		node.setProjection(mProjection);

		this->loadNodeFromInput(node, input);

		collection.addNode(std::move(node));
	}

	void LasReader::loadNodeFromInput(Node &node, cio::Input &input)
	{
		std::shared_ptr<Profile> profile = this->getOrCreateProfile();

		if (!mCatalog)
		{
			mCatalog.reset(new Catalog());
			this->loadCatalogFromInput(*mCatalog, input);
		}

		if (mCurrentOffset < mPointOffset)
		{
			input.discard(mPointOffset - mCurrentOffset);
			mCurrentOffset = mPointOffset;
		}

		std::shared_ptr<Provider> provider = node.getOrCreateProvider();
		std::shared_ptr<const geas::Application> readSchema = mCatalog->getApplicationSchema();
		std::shared_ptr<const geas::Application> nodeSchema = node.getApplicationSchema();

		const geas::Geometry *geometry = mCatalog->getApplicationSchema()->getGeometries().get(geas::Label("Point"));

		// Node has different schema than reader, we'll have to do data model mapping
		if (nodeSchema && nodeSchema != readSchema)
		{
			// TODO implement
		}
		else if (!nodeSchema)
		{
			nodeSchema = readSchema;
			node.setApplicationSchema(nodeSchema);
		}

		// TODO match up provider's attributes with desired semantics
		// For now we just clear it and do our own thing
		provider->clear();

		provider->resize(mPointCount);

		// Create fields for relevant geometry

		// Position XYZ is always valid
		// We also support reading color
		const geas::Attribute *positionField = geometry->getAttributes().get(geas::Label("Position"));
		const geas::Attribute *colorField = geometry->getAttributes().get(geas::Label("Colour"));

		// TODO optimize this loop based no testing
		// TODO will need to batch these as geas does not support more than INT32_MAX points per provider
		cio::Buffer points(mPointLength);

		// TODO set this up appropriately
		Array xyz;

		for (std::uint64_t p = 0; p < mPointCount; ++p)
		{
			points.reset();
			input.fill(points);
			points.flip();

			std::uint32_t position[3] = { };
			points.getLittleArray<std::uint32_t>(position, 3);

			if (positionField)
			{
				provider->setAttribute(p, 0, positionField);
			}
		}
	}
}
