/*=====================================================================================================================
 * Copyright 2022-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_TIFFREADER_H
#define GLITTER_TIFFREADER_H

#include "Types.h"

#include "ModelReader.h"

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace glitter
{
	/**
	 * The Geodex Model TIFF Reader refines the Geodex Model Reader to load content stored in TIFF and GeoTIFF formats.
	 *
	 * @note Currently this is just a placeholder to load TIFF projection content for legacy LAS files
	 */
	class GLITTER_ABI TiffReader : public ModelReader
	{
		public:
			/**
			 * Gets the metaclass for this class.
			 *
			 * @return the metaclass for this class
			 */
			static const cio::Class<TiffReader> &getDeclaredMetaclass() noexcept;

			/** Type code for GeoTIFF tag for geo-key directory. */
			static const std::uint16_t GEOKEY_DIRECTORY_TAG;

			/** Type code for GeoTIFF tag for geo-key double parameters. */
			static const std::uint16_t GEO_DOUBLE_PARAM_TAG;

			/** Type code for GeoTIFF tag for geo-key text parameters. */
			static const std::uint16_t GEO_ASCII_PARAM_TAG;

			/** Key code for GeoTIFF directory for a projected coordniate system. */
			static const std::uint16_t GEO_PROJECTED_CS_KEY;

			/**
			 * Default constructor.
			 */
			TiffReader() noexcept;

			/**
			 * Constructs a reader with the given profile.
			 * This does NOT call onProfileChanged() since virtual methods cannot be used in a constructor.
			 *
			 * @param profile The profile to use
			 */
			explicit TiffReader(std::shared_ptr<Profile> profile) noexcept;

			/**
			 * Copy constructor.
			 *
			 * @param in The reader to copy
			 */
			TiffReader(const TiffReader &in);

			/**
			 * Move constructor.
			 *
			 * @param in The reader to move
			 */
			TiffReader(TiffReader &&in) noexcept;

			/**
			 * Copy assignment.
			 * This does NOT call onProfileChanged() since subclasses are expected to override it.
			 *
			 * @param in The reader to copy
			 * @return this reader
			 */
			TiffReader &operator=(const TiffReader &in);

			/**
			 * Move assignment.
			 * This does NOT call onProfileChanged() since subclasses are expected to override it.
			 *
			 * @param in The reader to move
			 * @return this reader
			 */
			TiffReader &operator=(TiffReader &&in) noexcept;

			/**
			 * Destructor.
			 */
			virtual ~TiffReader() noexcept override;

			/**
			 * Clears the reader state.
			 * This does NOT call onProfileChanged() since subclasses are expected to override it.
			 */
			virtual void clear() noexcept override;

			/**
			 * Gets the runtime metaclass for this object instance.
			 *
			 * @return the runtime metaclass for this object
			 */
			virtual const cio::Metaclass &getMetaclass() const noexcept override;

			/**
			 * Loads a projection from a geo-key directory buffer.
			 * This will process the immediate information to set the geas::Code and add fields to the projection as needed.
			 * The projection and any support information like world model will be registered with the given application schema.
			 *
			 * @param schema The application schema to update
			 * @param buffer The geo-key encoding to read
			 * @return the loaded projection
			 */
			geas::Projection *loadGeokeyDirectory(geas::Application &schema, cio::Buffer &buffer);

		private:
			/** The metaclass for this class. */
			static cio::Class<TiffReader> sMetaclass;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
