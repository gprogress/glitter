/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_METADATAREADER_H
#define GLITTER_METADATAREADER_H

#include "Types.h"

#include <nlohmann/json_fwd.hpp>

#include <cio/Path.h>
#include <cio/Primitive.h>
#include <cio/Text.h>
#include <map>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace glitter
{
	/**
	 * The MetadataClassReader is responsible for reading glTF feature metadata (EXT_feature_metadata) content from the appropriate JSON representation.
	 * It can either process a standalone JSON resource without a glTF model, or process a loaded glTF document to find and load the proper extension information.
	 * The reader builds out the class information as a Geodex Application Schema and one Geodex Node for each feature table.
	 */
	class GLITTER_ABI MetadataReader
	{
		public:
			/**
			* Initializes mPrimitives map variable
			*/
			MetadataReader();

			/**
			 * Construct a copy of MetadataReader.
			 *
			 * @param in The reader to copy
			 */
			MetadataReader(const MetadataReader &in);

			/**
			 * Construct a copy of MetadataReader.
			 *
			 * @param in The reader to copy
			 * @return This reader
			 */
			MetadataReader &operator=(const MetadataReader &in);

			/**
			 * Destructor. Any unsaved changes are lost.
			 */
			~MetadataReader() noexcept;

			/**
			* Reads and stores the information from the feature table passed in.
			*
			* @param model The model to process
			* @param NodeName Name of the Node to find the Node from the collection and set the entities
			* @param featureTable The feature table to parse
			*/
			void loadFeatureTable(Model &model, const geas::Label &NodeName, const nlohmann::json &featureTable);

			/**
			* Calls loadFeatureTable for each feature table in the file.
			*
			* @param model The model to process
			* @param collection The collection to get information for the Node
			* @param format Part of the JSON file to continue reading from for information about the feature table
			*/
			void loadFeatureTables(Model &model, const nlohmann::json &format);

			/**
			* Reads the gltf file and searches for key tags to call the appropriate function to save the needed data. There are three major tags: "schema",
			* "schemaUri", and "featureTable". The "schema" tag is broken down into the "classes" tag which call loadFeatureClasses and the "enums" tag which
			* calls load Enums. The "schemaUri" tag searches for the "format" tag before collecting information for a new schema file to read and calls
			* loadFeatureMetadataFromFile to read the new file. The "featureTable" tag calls loadFeatureTables to read and store data from the feature tables
			* in the file.
			*
			* @param schema The schema to pass to another function to read from 
			* @param collection The collection to send to the next function when referencing
			* @param format The JSON document to read from
			* @param baseDirectory The file path to the current gltf file being read
			*/
			void loadFeatureMetadata(Model &model, const nlohmann::json &format, const cio::Path &baseDirectory);

			/**
			* Converts the given text into a parsable JSON document and calls loadFeaturemetadata to read and store the information
			*
			* @param schema The schema to pass to another function to read from 
			* @param collection The collection to pass to another function to read from
			* @param text The text to convert into a JSON to read and store information from
			* @param baseDirectory The file path to the current gltf file being read
			*/
			void loadFeatureMetadataFromText(Model &model, const std::string &text, const cio::Path &baseDirectory);

			/**
			* Retrieves the data from the file found in the given file path, stores the data into a parsable JSON document, then calls loadFeatureMetadata to
			* appropriately store the data read in
			*
			* @param schema The schema to pass to another function to read from 
			* @param collection The collection to pass to another function to read from
			* @param filePath The file path of the gltf document that will be read
			*/
			void loadFeatureMetadataFromFile(Model &model, const cio::Path &filePath);

			/**
			* Checks for the "featureIdAttributes" and calls loadFeatureIdMap for each feature atribute map entry in the file.
			*
			* @param collection The collection to pass to another function to read from
			* @param primitive Passed to loadFeatureIdMap and used in that function to find if an attribute already exists
			* @param format The JSON document to read from
			* @return A vector of all the feature maps information in the file
			*/
			std::vector<FeatureIdMap> loadFeatureIdMaps(const Collection &collection, const fx::gltf::Primitive &primitive, const nlohmann::json & format);

			/**
			* Loads a specific feature attribute map
			*
			* @param collection The collection to pass to another function to read from
			* @param primitive Passed to loadFeatureIdMap and used in that function to find if an attribute already exists
			* @param layer The layer used to find where an attriibute is in the JSON
			* @param root The JSON document to read from
			* @return A featureIdMap containing data of the given feature ID attributes
			*/
			FeatureIdMap loadSelectedFeatureIdMap(const Collection &collection, const fx::gltf::Primitive &primitive, std::uint32_t layer, const nlohmann::json &root);

			/**
			* Reads and stores the feature table's name, attribute, constant, and divisor.
			*
			* @param collection The collection to pass to another function to read from
			* @param primitive Used to find if an attribute already exists
			* @param format The JSON document to read from
			* @return A featureIdMap containing the feature table's name, attribute, constant, and divisor
			*/
			FeatureIdMap loadFeatureIdMap(const Collection &collection, const fx::gltf::Primitive &primitive, const nlohmann::json &format);

			/**
			* Counts the number of feature id attributes that are in the feature table
			*
			* @param format The JSON document to read from
			* @return The number of feature id attributes that are in the feature table
			*/
			std::uint32_t findFeatureTableCount(const nlohmann::json &format) const;

		private:
			/** A map to allow adding enumeration constraints */
			std::multimap<geas::Enumeration *, geas::Label> enumMap;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
