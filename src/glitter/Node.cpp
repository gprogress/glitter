/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "Node.h"

#include "Entity.h"
#include "Provider.h"
#include "Table.h"

#include <geas/Dictionary.cpp>
#include <geas/Entity.h>

#include <cio/Class.h>

namespace glitter
{
	cio::Class<Node> Node::sMetaclass("glitter::Node");

	Node::Node() noexcept :
		mContainer(nullptr),
		mProjection(nullptr)
	{
		// nothing more to do
	}

	Node::Node(const Node &in) = default;

	Node::Node(Node &&in) noexcept :
		geas::Definition(std::move(in)),
		mContainer(in.mContainer),
		mChildren(in.mChildren),
		mProvider(std::move(in.mProvider)),
		mProjection(in.mProjection)
	{
		in.mContainer = nullptr;
	}

	Node &Node::operator=(const Node &in) = default;

	Node &Node::operator=(Node &&in) noexcept
	{
		if (this != &in)
		{
			geas::Definition::operator=(std::move(in));

			mContainer = in.mContainer;
			in.mContainer = nullptr;

			mChildren = std::move(in.mChildren);
			mProvider = std::move(in.mProvider);
			mProjection = in.mProjection;
		}

		return *this;
	}

	Node::~Node() noexcept = default;

	const cio::Metaclass &Node::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	void Node::clear() noexcept
	{
		geas::Definition::clear();

		mApplicationSchema.reset();
		mContainer = nullptr;
		mChildren.clear();
		mProvider.reset();
		mProjection = nullptr;
	}

	std::shared_ptr<const geas::Application> Node::getApplicationSchema() const noexcept
	{
		return mApplicationSchema;
	}

	void Node::setApplicationSchema(std::shared_ptr<const geas::Application> schema) noexcept
	{
		mApplicationSchema = std::move(schema);
	}

	void Node::clearApplicationSchema() noexcept
	{
		mApplicationSchema.reset();
	}

	void Node::setContainer(Node *parent) noexcept
	{
		mContainer = parent;
	}

	Node *Node::getContainer() noexcept
	{
		return mContainer;
	}

	const Node *Node::getContainer() const noexcept
	{
		return mContainer;
	}

	const std::vector<const Node *> &Node::getContainedNodes() const noexcept
	{
		return reinterpret_cast<const std::vector<const Node *> &>(mChildren);
	}

	std::vector<Node *> &Node::getContainedNodes() noexcept
	{
		return mChildren;
	}

	void Node::setContainedNodes(std::vector<Node *> datasets) noexcept
	{
		mChildren = std::move(datasets);
	}

	void Node::addContainedNode(Node *dataset)
	{
		mChildren.emplace_back(dataset);
	}

	void Node::removeContainedNode(const Node *dataset)
	{
		auto ii = std::find(mChildren.begin(), mChildren.end(), dataset);
		if (ii != mChildren.end())
		{
			mChildren.erase(ii);
		}
	}

	void Node::clearContainedNodes() noexcept
	{
		mChildren.clear();
	}

	Entity Node::getEntity(geas::Index idx) noexcept
	{
		return Entity(this, idx);
	}

	Entity Node::createEntity(const geas::Entity *entity)
	{
		Entity entityRef;

		if (!mProvider)
		{
			mProvider.reset(new Table());
		}

		entityRef.bind(this, mProvider->create(entity));

		return entityRef;
	}

	const geas::Projection *Node::getProjection() const noexcept
	{
		return mProjection;
	}

	void Node::setProjection(const geas::Projection *projection) noexcept
	{
		mProjection = projection;
	}
}

/* Explicit instantiations */

#if defined _MSC_VER
#pragma warning(disable: 4251)
#endif

namespace geas
{
	template class GLITTER_TEMPLATE_ABI geas::Dictionary<glitter::Node>;
}
