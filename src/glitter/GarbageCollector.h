/*=====================================================================================================================
 * Copyright 2021-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_GARBAGECOLLECTOR_H
#define GLITTER_GARBAGECOLLECTOR_H

#include "Types.h"

#include "GarbageMarker.h"

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace glitter
{
	/**
	 * The Garbage Collector implements a mark-and-sweep algorithm to detect and remove unused glTF model objects.
	 * It can be set up to detect objects that are reachable from the default scene or from any scene.
	 * It can also process whether to find objects reachable from the feature table extensions.
	 * 
	 * After all used objects have been marked, then a sweep pass is performed to determine which objects were unused
	 * and how to remap index values to remove them such that the new set of index values are still a contiguous range.
	 */
	class GLITTER_ABI GarbageCollector
	{
		public:
			/**
			 * Construct a new garbage collector.
			 */
			GarbageCollector() noexcept;

			/**
			 * Destructor.
			 */
			~GarbageCollector() noexcept;

			/**
			 * Clears the garbage collector's state to start over.
			 */
			void clear() noexcept;

			/**
			 * Marks the default scene as being in use, and then recursively marks all objects reachable from it.
			 * 
			 * @param model The model to use as input
			 */
			void markDefaultScene(const Model &model);

			/**
			 * Marks all scenes as being in use, and then recursively marks all objects reachable from each of them.
			 * 
			 * @param model The model to use as input
			 */
			void markScenes(const Model &model);

			/**
			 * Marks the given scene index as being in use, and then recursively marks all objects reachable from that scene.
			 * In particular this marks all node children of the scene and then recursively marks their contents.
			 *
			 * @param model The model to use as input
			 * @param index The scene index
			 */
			void markScene(const Model &model, std::int32_t index);

			/**
			 * Marks the given node index as being in use, and then recursively marks all objects reachable from that node.
			 * In particular this marks the mesh if one is present and its contents such as vertex attributes and index arrays, as well as any child nodes.
			 * 
			 * @param model The model to use as input
			 * @param index The node index
			 */
			void markNode(const Model &model, std::int32_t index);

			/**
			 * Marks the given mesh index as being in use, and then recursively marks all objects reachable from that mesh.
			 * In particular this marks for each primitive set the vertex attributes, index arrays, and feature tables they might use.
			 * 
			 * @param model The model to use as input
			 * @param index The mesh index
			 */
			void markMesh(const Model &model, std::int32_t index);

			/**
			 * Marks the given accessor index as being in use, and then recursively marks the associated buffer view and buffer.
			 * 
			 * @param model The model to use as input
			 * @param index The accessor index
			 */
			void markAccessor(const Model &model, std::int32_t index);

			/**
			 * Marks the given buffer view index as being in use, and then recursively marks the associated buffer.
			 *
			 * @param model The model to use as input
			 * @param index The buffer view index
			 */
			void markBufferView(const Model &model, std::int32_t index);

			/**
			 * Marks the given buffer as being in use.
			 * 
			 * @param model The model to use as input
			 * @param index The buffer index
			 */
			void markBuffer(const Model &model, std::int32_t index);

			/**
			 * Marks all feature tables as being in use.
			 * This will also check for buffer views associated with attributes in the feature table.
			 * This can be used to preserve feature tables even if they are not yet referenced by meshes.
			 *
			 * @param model The model to use as input
			 * @param index The feature table index
			 */
			void markAllFeatureTables(const Model &model);

			/**
			 * Marks the given feature table as being in use.
			 * This will also check for buffer views associated with attributes in the feature table.
			 *
			 * @param model The model to use as input
			 * @param index The feature table index
			 */
			void markFeatureTable(const Model &model, std::int32_t index);

			/**
			 * Marks the given image as being in use.
			 * This will also check for referenced buffer views.
			 *
			 * @param model The model to use as input
			 * @param index The image index
			 */
			void markImage(const Model &model, std::int32_t index);

			/**
			 * Marks the given sampler as being in use.
			 *
			 * @param model The model to use as input
			 * @param index The sampler index
			 */
			void markSampler(const Model &model, std::int32_t index);

			/**
			 * Marks the given texture as being in use.
			 * This will also check for all referenced samplers and associated buffer views.
			 *
			 * @param model The model to use as input
			 * @param index The texture index
			 */
			void markTexture(const Model &model, std::int32_t index);

			/**
			 * Marks all materials as being in use.
			 * This will also check for all referenced textures, samplers, and associated buffer views.
			 * 
			 * @param model The model to use as input
			 */
			void markAllMaterials(const Model &model);

			/**
			 * Marks the given material as being in use.
			 * This will also check for all referenced textures, samplers, and associated buffer views.
			 *
			 * @param model The model to use as input
			 * @param index The material index
			 */
			void markMaterial(const Model &model, std::int32_t index);

			/**
			 * Marks the given camera as being in use.
			 * 
			 * @param model The model to use as input
			 * @param index The camera index
			 */
			void markCamera(const Model &model, std::int32_t index);

			/**
			 * Marks the given skin as being in use.
			 * This will also mark referenced inverse-bind-matrix accessors and joint nodes.
			 *
			 * @param model The model to use as input
			 * @param index The skin index
			 */
			void markSkin(const Model &model, std::int32_t index);

			/**
			 * Marks the given animation as being in use.
			 * This will also mark referenced accessors and nodes.
			 * 
			 * @note Nothing within the glTF document uses animations, so it is up to the user to decide if they are used or not.
			 * 
			 * @param model The model to use as input
			 * @param index The animation index
			 */
			void markAnimation(const Model& model, std::int32_t index);

			/**
			 * Marks all animations as being in use.
			 * This will also mark referenced accessors and nodes.
			 *
			 * @note Nothing within the glTF document uses animations, so it is up to the user to decide if they are used or not.
			 * This method considers all animations as possibly being used and preserves all data they require.
			 *
			 * @param model The model to use as input
			 */
			void markAllAnimations(const Model &model);

			/**
			 * Finalizes the marking phase and conducts the initial sweep on all objects to calculate which objects are unused
			 * and how to remap indices.
			 * 
			 * @return whether any objects were found to be unused
			 */
			bool sweep();

			/**
			 * Performs garbage collection on the given model based on the mark and sweep analysis.
			 * All unused objects are removed and remaining objects are compacted into a contiguous sequence, remapping indices as needed.
			 * 
			 * @param model The model to update with the garbage collection results
			 */
			void collect(Model &model) const;

			/**
			 * Performs garbage collection on the given model to remove all unused buffers.
			 * Since buffers do not reference other objects, no other updates are performed.
			 * 
			 * @param model The model to update to remove unused buffers
			 */
			void collectBuffers(Model &model) const;

			/**
			 * Performs garbage collection on the given model to remove all unused buffer views.
			 * For buffer views that remain after collection, their references to buffer indices are updated if necessary.
			 *
			 * @param model The model to update to remove unused buffer views
			 */
			void collectBufferViews(Model &model) const;

			/**
			 * Performs garbage collection on the given model to remove all unused accessors.
			 * For accessors that remain after collection, their references to buffer view indices are updated if necessary.
			 *
			 * @param model The model to update to remove unused buffer views
			 */
			void collectAccessors(Model &model) const;

			/**
			 * Performs garbage collection on the given model to remove all unused scenes.
			 * For scenes that remain after collection, their references to nodes are updated if necessary.
			 * The default scene index is also updated if necessary.
			 *
			 * @param model The model to update to remove unused scenes
			 */
			void collectScenes(Model &model) const;

			/**
			 * Performs garbage collection on the given model to remove all unused nodes.
			 * For nodes that remain after collection, their references to meshes and child nodes are updated if necessary.
			 *
			 * @param model The model to update to remove unused nodes
			 */
			void collectNodes(Model &model) const;

			/**
			 * Performs garbage collection on the given model to remove all unused cameras.
			 * Cameras don't reference any other data so no futher updates needed to be made.
			 *
			 * @param model The model to update to remove unused skins
			 */
			void collectCameras(Model &model) const;

			/**
			 * Performs garbage collection on the given model to remove all unused skins.
			 * For skins that remain after collection, their references to inverse bind matrix accessors and joint nodes are updated if necessary.
			 *
			 * @param model The model to update to remove unused skins
			 */
			void collectSkins(Model &model) const;

			/**
			 * Performs garbage collection on the given model to remove all unused meshes.
			 * For meshes that remain after collection, their references to accessors and materials are updated if necessary.
			 * Note that feature tables are referenced by name rather than index so those will not need to be updated.
			 *
			 * @param model The model to update to remove unused meshes
			 */
			void collectMeshes(Model &model) const;

			/**
			 * Performs garbage collection on the given model to remove all unused images.
			 * For images that remain after collection, their references to buffer views are updated if necessary.
			 *
			 * @param model The model to update to remove unused images
			 */
			void collectImages(Model &model) const;

			/**
			 * Performs garbage collection on the given model to remove all unused samplers.
			 * Samplers don't reference any other data so no further updates are needed after that.
			 *
			 * @param model The model to update to remove unused samplers
			 */
			void collectSamplers(Model &model) const;

			/**
			 * Performs garbage collection on the given model to remove all unused textures.
			 * For textures that remain after collection, their references to images and samplers are updated if necessary.
			 *
			 * @param model The model to update to remove unused textures
			 */
			void collectTextures(Model &model) const;

			/**
			 * Performs garbage collection on the given model to remove all unused materials.
			 * For materials that remain after collection, their references to texture are updated if necessary.
			 *
			 * @param model The model to update to remove unused textures
			 */
			void collectMaterials(Model &model) const;

			/**
			 * Performs garbage collection on the given model to remove all unused feature tables.
			 * For feature tables that remain after collection, their references to buffer views for attributes are updated if necessary.
			 * The JSON extension text will also be updated.
			 * 
			 * @pre This method will only work correctly if collectBufferViews has already been called.
			 *
			 * @param model The model to update to remove unused feature tables
			 */
			void collectFeatureTables(Model &model) const;

			/**
			 * Performs garbage collection on the given model to remove all unused animations.
			 * Note that unlike all other data types in glTF, animations are not directly used within the document
			 * so it is up to the user to decide which animations are considered used.
			 * 
			 * For animations that remain after collection, their references to accessors and nodes are updated if necessary.
			 *
			 * @param model The model to update to remove unused animations
			 */
			void collectAnimations(Model &model) const;

		private:
			/** The garbage marker state for the buffers */
			GarbageMarker mBuffers;

			/** The garbage marker state for the buffer views */
			GarbageMarker mBufferViews;

			/** The garbage marker state for the accessors */
			GarbageMarker mAccessors;

			/** The garbage marker state for the feature tables */
			GarbageMarker mFeatureTables;

			/** The garbage marker state for images */
			GarbageMarker mImages;

			/** The garbage marker state for samplers */
			GarbageMarker mSamplers;

			/** The garbage marker state for textures */
			GarbageMarker mTextures;

			/** The garbage marker state for materials */
			GarbageMarker mMaterials;

			/** The garbage marker state for meshes */
			GarbageMarker mMeshes;

			/** The garbage marker state for cameras */
			GarbageMarker mCameras;

			/** The garbage marker state for skins */
			GarbageMarker mSkins;

			/** The garbage marker state for nodes */
			GarbageMarker mNodes;

			/** The garbage marker state for scenes */
			GarbageMarker mScenes;

			/** The garbage marker state for animations */
			GarbageMarker mAnimations;

			template <typename T>
			static void applyRemovals(const GarbageMarker &marker, std::vector<T> &items);
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif