/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_TYPES_H
#define GLITTER_TYPES_H

#if defined _WIN32
#if defined GLITTER_BUILD
#if defined GLITTER_BUILD_SHARED
#define GLITTER_ABI __declspec(dllexport)
#if defined _MSC_VER
#define GLITTER_TEMPLATE
#define GLITTER_TEMPLATE_ABI GLITTER_ABI
#else
#define GLITTER_TEMPLATE GLITTER_ABI
#define GLITTER_TEMPLATE_ABI
#endif
#else
#define GLITTER_ABI
#define GLITTER_TEMPLATE
#define GLITTER_TEMPLATE_ABI
#endif
#else
#define GLITTER_ABI __declspec(dllimport)
#define GLITTER_TEMPLATE GLITTER_ABI
#endif
#elif defined __ELF__
#define GLITTER_ABI __attribute__ ((visibility ("default")))
#define GLITTER_TEMPLATE
#define GLITTER_TEMPLATE_ABI
#endif

#include <cstdint>
#include <string>

namespace cio
{
	class Buffer;
	template <typename> class Class;
	class Encoding;
	class Input;
	class Metaclass;
	class Path;
	class Output;
	enum class Primitive : std::uint8_t;
	class Protocol;
	class ProtocolFactory;
	class State;
	class Text;
	enum class Type : std::uint8_t;
	class UniqueId;
}

namespace geas
{
	using Index = std::int32_t;
	using Label = cio::Text;

	class Application;
	class ApplicationProfile;
	class Attribute;
	class Enumeration;
	class Geometry;
	class Profiles;
	class Projection;
}

namespace fx
{
	namespace gltf
	{
		struct Accessor;
		struct Buffer;
		struct BufferView;
		struct Document;
		struct Material;
		struct Mesh;
		struct Node;
		struct Primitive;
		struct Scene;
	}
}

namespace glitter
{
	class Array;
	enum class AttributeType : std::uint8_t;
	class Catalog;
	class Collection;
	class Column;
	enum class ComponentType : std::uint8_t;
	struct ComponentId;
	template <typename> class Degree;
	class Delegate;
	class Entity;
	class EntityMapper;
	class FeatureIdMap;
	class FeatureIdMapper;
	class GarbageMarker;
	class GarbageCollector;
	enum class GeometryType : std::uint8_t;
	class Location;
	class KeyValueList;
	class LasReader;
	class MetadataReader;
	class MetadataWriter;
	class Model;
	class ModelReader;
	class Node;
	enum class Primitive : std::uint8_t;
	class Profile;
	class Provider;
	template <typename> class Radian;
	class Region;
	class Repository;
	class Table;
	class TiffReader;
	template <typename T, typename C = std::size_t, typename L = C> class Weight;

	using FeatureIdWeight = Weight<cio::UniqueId>;
	using EntityWeight = Weight<Entity>;
}

#endif
