/*=====================================================================================================================
 * Copyright 2022-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "Reader.h"

#include "Catalog.h"
#include "Collection.h"
#include "Repository.h"
#include "Node.h"

#include <cio/Class.h>
#include <cio/Exception.h>
#include <cio/Input.h>
#include <cio/ProtocolFactory.h>

namespace glitter
{
	cio::Class<Reader> Reader::sMetaclass("glitter::Reader");

	const cio::Class<Reader> &Reader::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}

	Reader::Reader() noexcept :
		mProtocols(cio::ProtocolFactory::getDefault())
	{
		// nothing more to do
	}

	Reader::Reader(const Reader &in) = default;

	Reader::Reader(Reader &&in) noexcept = default;

	Reader &Reader::operator=(const Reader &in) = default;

	Reader &Reader::operator=(Reader &&in) noexcept = default;

	Reader::~Reader() noexcept = default;

	void Reader::clear() noexcept
	{
		mProtocols = nullptr;
	}

	const cio::Metaclass &Reader::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	void Reader::loadRepository(Repository &repo, const cio::Path &path)
	{
		if (!mProtocols)
		{
			throw cio::Exception(cio::Reason::Refused, "Cannot load repository because protocol factory was disabled");
		}

		std::unique_ptr<cio::Input> input = mProtocols->openInput(path);
		if (input)
		{
			this->loadRepositoryFromInput(repo, *input);
		}
	}

	void Reader::loadRepositoryFromInput(Repository &repo, cio::Input &input)
	{
		Collection collection;
		this->loadCollectionFromInput(collection, input);
		if (collection)
		{
			repo.addCollection(std::move(collection));
		}
	}

	void Reader::loadCatalog(Catalog &catalog, const cio::Path &path)
	{
		if (!mProtocols)
		{
			throw cio::Exception(cio::Reason::Refused, "Cannot load repository because protocol factory was disabled");
		}

		std::unique_ptr<cio::Input> input = mProtocols->openInput(path);
		if (input)
		{
			this->loadCatalogFromInput(catalog, *input);
		}
	}

	void Reader::loadCatalogFromInput(Catalog &catalog, cio::Input &input)
	{
		throw cio::Exception(cio::Reason::Unsupported, "Cannot load catalog because reader does not support doing so");
	}

	void Reader::loadCollection(Collection &collection, const cio::Path &path)
	{
		if (!mProtocols)
		{
			throw cio::Exception(cio::Reason::Refused, "Cannot load repository because protocol factory was disabled");
		}

		std::unique_ptr<cio::Input> input = mProtocols->openInput(path);
		if (input)
		{
			this->loadCollectionFromInput(collection, *input);
		}
	}

	void Reader::loadCollectionFromInput(Collection &collection, cio::Input &input)
	{
		std::shared_ptr<Catalog> catalog(new Catalog());
		this->loadCatalogFromInput(*catalog, input);
		collection.setCatalog(catalog);

		Node node;
		node.setApplicationSchema(catalog->getApplicationSchema());
		this->loadNodeFromInput(node, input);
		collection.addNode(std::move(node));
	}

	void Reader::loadNode(Node &node, const cio::Path &path)
	{
		if (!mProtocols)
		{
			throw cio::Exception(cio::Reason::Refused, "Cannot load node because protocol factory was disabled");
		}

		std::unique_ptr<cio::Input> input = mProtocols->openInput(path);
		if (input)
		{
			this->loadNodeFromInput(node, *input);
		}
	}

	void Reader::loadNodeFromInput(Node &node, cio::Input &input)
	{
		throw cio::Exception(cio::Reason::Unsupported, "Cannot load node because reader does not support doing so");
	}

	const cio::ProtocolFactory *Reader::getProtocols() const noexcept
	{
		return mProtocols;
	}

	void Reader::setProtocols(const cio::ProtocolFactory *protocols) noexcept
	{
		mProtocols = protocols;
	}
}