/*=====================================================================================================================
 * Copyright 2022-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_GEOMETRYTYPE_H
#define GLITTER_GEOMETRYTYPE_H

#include "Types.h"

namespace glitter
{
	/**
	 * The Geometry Type enumerates the list of standard geometry type classes commonly used
	 * in 3D modeling to make it convenient to read, process, and write conventional model formats.
	 * These geometry types are also modified by the Primitive enumeration that further elaborates how
	 * the geometry vertex sequence is interpreted.
	 *
	 * These standard geometries can be linked to more general schema definitions, and also can suggest
	 * their own default definitions if you need one.
	 *
	 * Non-standard vertex geometries can still be used with the Geometry::Unknown type.
	 */
	enum class GeometryType : std::uint8_t
	{
		/** No geometry type is defined. */
		None,

		/** Geometry type is not one of the standard geometries on this list. */
		Unknown,

		/** Geometry type consists of individual points. */
		Point,

		/** Geometry type consists of line segments. */
		Line,

		/** Geometry type consists of general polygon surfaces. */
		Polygon,

		/** Geometry type consists of triangle surfaces. */
		Triangle,

		/** Geometry type consists of a gridded N-dimensional raster product. */
		Raster,

		/** Geometry type consists of a 1-dimension gradient raster. */
		Gradient,

		/** Geometry type consists of a 2-dimensional grid raster. */
		Grid,

		/** Geometry type consists of a 3-dimensional voxel raster. */
		Voxel
	};

	/**
	 * Gets the default geometry schema for the given geometry type.
	 * This should correspond to the geometry schema used for the Primitive::List primitive for this type.
	 *
	 * @param type The geometry type
	 * @return the default geometry schema
	 */
	const geas::Geometry *getDefaultGeometry(GeometryType type) noexcept;
}

#endif
