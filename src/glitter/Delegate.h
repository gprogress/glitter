/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_DELEGATE_H
#define GLITTER_DELEGATE_H

#include "Types.h"

#include "Provider.h"

#include <memory>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace glitter
{
	/**
	 * The Delegate is a Provider that delegates all requests
	 * to another Provider, optionally with a default entity and geometry type.
	 *
	 * This is primarily useful as a base class for other data providers that need
	 * to customize some but not all methods on top of an existing provider.
	 */
	class GLITTER_ABI Delegate : public Provider
	{
		public:
			/**
			 * Gets the metaclass for this class.
			 *
			 * @return the metaclass for this class.
			 */
			static const cio::Class<Delegate> &getDeclaredMetaclass() noexcept;

			/**
			 * Default constructor.
			 */
			Delegate() noexcept;

			/**
			 * Copy constructor.
			 *
			 * This method does NOT call onProviderChanged() since virtual methods cannot be called from constructors.
			 *
			 * @param in The delegate to copy
			 */
			Delegate(const Delegate &in);

			/**
			 * Move constructor.
			 *
			 * This method does NOT call onProviderChanged() since virtual methods cannot be called from constructors.
			 *
			 * @param in The delegate to move
			 */
			Delegate(Delegate &&in) noexcept;

			/**
			 * Copy assignment.
			 *
			 * This method does NOT call onProviderChanged() since subclasses should override this method.
			 *
			 * @param in The delegate to copy
			 * @return this delegate
			 */
			Delegate &operator=(const Delegate &in);

			/**
			 * Move assignment.
			 *
			 * This method does NOT call onProviderChanged() since subclasses should override this method.
			 *
			 * @param in The delegate to move
			 * @return this delegate
			 */
			Delegate &operator=(Delegate &&in) noexcept;

			/**
			 * Destructor.
			 */
			virtual ~Delegate() noexcept override;

			/**
			 * Gets the runtime metaclass for this object instance.
			 *
			 * @return the runtime metaclass for this object
			 */
			virtual const cio::Metaclass &getMetaclass() const noexcept override;

			/**
			 * Clears the delegate state.
			 *
			 * This method does NOT call onProviderChanged() since subclasses should override this method.
			 */
			virtual void clear() noexcept override;

			/**
			 * Sets the definition of the default entity type to use if no entity type is otherwise specified for objects.
			 * This can also be used to set a uniform entity type if all entities are the same type.
			 *
			 * @param entity The default entity type to use
			 */
			void setDefaultEntityType(const geas::Entity *entity) noexcept;

			/**
			 * Gets the definition of the default entity type to use if no entity type is otherwise specified for objects.
			 * If all entities are the same type, it should be this type.
			 *
			 * @return The default entity type to use
			 */
			const geas::Entity *getDefaultEntityType() const noexcept;

			/**
			 * Sets the definition of the default geometry type to use if no geometry type is otherwise specified.
			 * This can also be used to set a uniform geometry type if all entities are the same type.
			 *
			 * @param geometry The default geometry type to use
			 */
			void setDefaultGeometryType(const geas::Geometry *geometry) noexcept;

			/**
			 * Gets the definition of the default geometry type to use if no geometry type can be determined from attribute values.
			 * If all entities are the same type, it should be this type.
			 *
			 * @return The default geometry type to use
			 */
			const geas::Geometry *getDefaultGeometryType() const noexcept;

			/**
			 * Gets the current delegated data provider.
			 *
			 * @return the delegated data provider
			 */
			std::shared_ptr<Provider> getProvider() noexcept;

			/**
			 * Gets the current delegated data provider.
			 *
			 * @return the delegated data provider
			 */
			std::shared_ptr<const Provider> getProvider() const noexcept;

			/**
			 * Gets the current delegated data provider, creating a default one if none exists.
			 * Currently the default type is Table.
			 *
			 * This method calls onProviderChanged() if a new provider is created.
			 *
			 * @return the delegated data provider
			 */
			std::shared_ptr<Provider> getOrCreateProvider();

			/**
			 * Sets the delegated data provider to use.
			 *
			 * This method calls onProviderChanged().
			 *
			 * @param provider The data provider
			 */
			void setProvider(std::shared_ptr<Provider> provider) noexcept;

			/**
			 * Clears the delegated data provider.
			 *
			 * This method calls onProviderChanged() if the current provider is non-null.
			 */
			void clearProvider() noexcept;

			/**
			 * Removes and returns the delegated data provider without destroying it.
			 *
			 * This method calls onProviderChanged() if the current provider is non-null.
			 *
			 * @return the data provider
			 */
			std::shared_ptr<Provider> takeProvider() noexcept;

			// Provider-specific overrides -

			/**
			 * Gets the number of logical objects (entities or geometry primitives) that have values in this value provider.
			 * This delegates to the provider if set, and returns 0 otherwise.
			 *
			 * @return the number of logical objects
			 */
			virtual geas::Index size() const override;

			/**
			 * Sets the number of logical objects (entities or geometry primitives) that have values in this value provider.
			 *
			 * This delegates to the provider if set, otherwise if the requested size is non-zero this creates a new default provider
			 * (currently Table).
			 *
			 * @param count The desired number of logical objects
			 * @return the number of logical objects actually set
			 */
			virtual geas::Index resize(geas::Index count) override;

			/**
			 * Add new record for an object. The entity and geometry schemas may be specified, although it is up to the
			 * delegated provider whether to honor them.
			 *
			 * This delegates to the provider, creating a new default provider (currently Table) if none xists.
			 *
			 * @param entity Used to select the default values for attributes
			 * @param geometry Used to set up geometric representation if applicable
			 * @return index of the new object
			 */
			virtual geas::Index create(const geas::Entity *entity = nullptr, const geas::Geometry *geometry = nullptr) override;

			// Schema Management

			/**
			 * Gets a datatype describing the properties of this provider in terms of size() and resize() for object counts.
			 * The datatype should always be of type Integer. The number layout specifies the actual underlying integer used.
			 * The length constraint describes the minimum and maximum size.
			 * The variable length status indicates whether the size can be modified after construction.
			 *
			 * This delegates to the provider if set, and returns nullptr otherwise.
			 *
			 * Other fields such as cardinality, enumeration, and measure have no specific meaning.
			 *
			 * @return the properties describing how attributes work with this provider
			 */
			virtual const geas::Attribute *getSizeProperties() const noexcept override;

			/**
			 * Gets a datatype describing the properties of this provider in terms of handling attribute counts.
			 * The datatype should always be of type Integer. The number layout specifies the actual underlying integer used.
			 * The length constraint describes the minimum and maximum attribute count.
			 * The variable length status indicates whether different objects may have different attribute counts.
			 *
			 * This delegates to the provider if set, and returns nullptr otherwise.
			 *
			 * Other fields such as cardinality, enumeration, and measure have no specific meaning.
			 *
			 * @return the properties describing how attributes work with this provider
			 */
			virtual const geas::Attribute *getAttributeCountProperties() const noexcept override;

			/**
			 * Reports whether this data provider has a uniform attribute field layout.
			 * If true, this means every object has the same attributes in the same sequence.
			 * If false, each object may have a different sequence of attribute fields.
			 *
			 * This delegates to the provider if set, and returns false otherwise.
			 *
			 * @return whether this data provider uses a uniform attribute field layout
			 */
			virtual bool isUniformFieldLayout() const noexcept override;

			/**
			 * Gets the number of attributes available for the given object.
			 * This delegates to the provider, and returns 0 otherwise.
			 *
			 * @param object The object index
			 * @return the number of attributes available
			 */
			virtual geas::Index getAttributeCount(geas::Index object) const override;

			/**
			 * Gets the entity type for the given object.
			 * This delegates to the provider, and returns the default entity type otherwise.
			 *
			 * @param object The object index
			 * @return the entity type, or null if unknown
			 */
			virtual const geas::Entity *getEntityType(geas::Index object) const override;

			/**
			 * Gets the geometry type for the given object.
			 * This delegates to the provider, and returns the default geometry type otherwise.
			 *
			 * @param object The object index
			 * @return the geometry type, or null if unknown
			 */
			virtual const geas::Geometry *getGeometryType(geas::Index object) const override;

			/**
			 * Sets the entity type and/or geometry type for the given object.
			 * A nullptr parameter indicates to use the default.
			 * This delegates to the provider if set, and throw an exception otherwise.
			 *
			 * @param object The object index
			 * @param entity The entity type, or nullptr to use the default entity type
			 * @param geometry The geometry type, or nullptr to use the default geometry type
			 */
			virtual void setEntityType(geas::Index object, const geas::Entity *entity, const geas::Geometry *geometry = nullptr) override;

			/**
			 * Gets the attribute definition for a given object and attribute field.
			 * The field must be less than the attribute count for the object.
			 * This delegates to the provider if set, or returns nullptr otherwise.
			 *
			 * @param object The object index
			 * @param field The attribute field index
			 * @return the attribute schema
			 */
			virtual const geas::Attribute *getAttribute(geas::Index object, geas::Index field) const override;

			/**
			 * Sets the attribute definition for a given object and attribute field.
			 * This delegates to the provider if set, and throws an exception otherwise.
			 *
			 * @param object The object index
			 * @param field The attribute field index
			 * @param defn the attribute schema
			 */
			virtual void setAttribute(geas::Index object, geas::Index field, const geas::Attribute *defn) override;

			/**
			 * Adds an attribute to the given object, obtaining the new field index.
			 * This delegates to the provider if set, and throws an exception otherwise.
			 *
			 * @param object The object index
			 * @param defn The attribute schema
			 * @return the field index of the added attribute
			 */
			virtual geas::Index addAttribute(geas::Index object, const geas::Attribute *defn) override;

			/**
			 * Removes an attribute from the given object.
			 * This delegates to the provider if set, and does nothing otherwise
			 *
			 * @param object The object index
			 * @param field The attribute field index
			 */
			virtual void removeAttribute(geas::Index object, geas::Index field) override;

			/**
			 * Finds the field index of an attribute given its label.
			 *
			 * @param object The object index
			 * @param label The attribute label
			 * @return the attribute field index, or UINT32_MAX if not found
			 */
			virtual geas::Index findAttributeByLabel(geas::Index object, const geas::Label &label) const override;

			/**
			 * Finds the field index of an attribute given its code.
			 *
			 * @param object The object index
			 * @param code The attribute code
			 * @return the attribute field index, or UINT32_MAX if not found
			 */
			virtual geas::Index findAttributeByCode(geas::Index object, const geas::Code &code) const override;

			/**
			 * Finds the field index of an attribute given its schema index.
			 *
			 * @param object The object index
			 * @param idx The attribute schema index
			 * @return the attribute field index, or UINT32_MAX if not found
			 */
			virtual geas::Index findAttributeByIndex(geas::Index object, geas::Index idx) const override;

			/**
			 * Finds the field index of the first attribute that matches the given attribute.
			 * Any identifiers that are set on the attribute are used for the search.
			 *
			 * @param object The object index
			 * @param attribute The attribute to match with as many fields set as known
			 * @return the attribute field index, or UINT32_MAX if not found
			 */
			virtual geas::Index findMatchingAttribute(geas::Index object, const geas::Attribute &attribute) const override;

			// Value and memory management

			/**
			 * Gets an attribute value for the given object and attribute field.
			 *
			 * This delegates to the provider if set, and returns an empty value otherwise.
			 *
			 * @param object The object index
			 * @param field The attribute field index
			 * @return the value
			 */
			virtual Array getValue(geas::Index object, geas::Index field) const override;

			/**
			 * Sets an attribute value for the given object and attribute field.
			 *
			 * This delegates the the provider if set, or throws an exception otherwise.
			 *
			 * @param object The object index
			 * @param field The attribute field index
			 * @param value The value to set
			 * @return whether the value was set
			 */
			virtual bool setValue(geas::Index object, geas::Index field, const Array &value) override;

			/**
			 * Validates the current value for the given object and attribute field to see if it matches the given datatype constraint.
			 *
			 * If the provider is set, this delegates the validation to the provider. Otherwise it always fails with a Missing fault.
			 *
			 * @param constraint The datatype specifying the constraint
			 * @param object The object index
			 * @param field index the attribute field index
			 * @return a fault describing validation failures, if any
			 */
			virtual geas::AttributeFault validateCurrentValue(const geas::Attribute &constraint, geas::Index object, geas::Index field) const override;

			/**
			 * Estimates the total memory use of this provider.
			 * This delegates to the provider if available, and returns 0 otherwise.
			 *
			 * @return the estimated current memory use
			 */
			virtual std::size_t estimateValueMemoryUse() const noexcept override;

			/**
			 * Estimates the new memory use needed to create the given number of objects.
			 * This delegates to the provider if available, and returns 0 otherwise.
			 *
			 * @param count The number of objects to create
			 * @return the memory size to be allocated
			 */
			virtual std::size_t estimateNewValueMemoryUse(std::size_t count) const noexcept override;

			/**
			 * Method called when the underlying provider has been changed in any way.
			 * This can be used to recalculate any cached state.
			 *
			 * The base implementation does nothing.
			 */
			virtual void onProviderChanged();

		protected:
			/** Data provider. */
			std::shared_ptr<Provider> mProvider;

			/** The default entity type to return if no specific one is found for an object. */
			const geas::Entity *mDefaultEntity;

			/** The default geometry type to return if no specific one is found for an object. */
			const geas::Geometry *mDefaultGeometry;

			private:
			/** The metaclass for this class. */
			static cio::Class<Delegate> sMetaclass;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
