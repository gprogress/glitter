/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "EntityMapper.h"

#include <geas/Application.h>
#include <geas/Attribute.h>
#include <geas/Entity.h>

#include <geas/Identifier.h>

#include "Entity.h"
#include "Table.h"
#include "Provider.h"

namespace glitter
{
	EntityMapper::EntityMapper() noexcept
	{
		// nothing more to do
	}

	EntityMapper::EntityMapper(const EntityMapper &in) = default;

	EntityMapper::EntityMapper(EntityMapper &&in) noexcept :
		Delegate(std::move(in)),
		mSchema(std::move(in.mSchema)),
		mEntityTypeAttributes(std::move(in.mEntityTypeAttributes)),
		mGeometryTypeAttributes(std::move(in.mGeometryTypeAttributes))
	{
		// nothing more to do
	}

	EntityMapper &EntityMapper::operator=(const EntityMapper &in) = default;

	EntityMapper &EntityMapper::operator=(EntityMapper &&in) noexcept
	{
		if (this != &in)
		{
			Delegate::operator=(std::move(in));
			mSchema = std::move(in.mSchema);
			mEntityTypeAttributes = std::move(in.mEntityTypeAttributes);
			mGeometryTypeAttributes = std::move(in.mGeometryTypeAttributes);
		}
		return *this;
	}

	EntityMapper::~EntityMapper() noexcept = default;

	void EntityMapper::clear() noexcept
	{
		Delegate::clear();
		mSchema.reset();
		mEntityTypeAttributes.clear();
		mGeometryTypeAttributes.clear();
	}

	std::shared_ptr<const geas::Application> EntityMapper::getApplicationSchema() const noexcept
	{
		return mSchema;
	}

	void EntityMapper::setApplicationSchema(std::shared_ptr<const geas::Application> schema) noexcept
	{
		mSchema = std::move(schema);
	}

	void EntityMapper::clearApplicationSchema() noexcept
	{
		mSchema.reset();
	}

	geas::Index EntityMapper::create(const geas::Entity *entity, const geas::Geometry *geometry)
	{
		std::shared_ptr<Provider> provider = this->getOrCreateProvider();
		geas::Index idx = mProvider->create(entity, geometry);
		this->setEntityType(idx, entity, geometry);
		return idx;
	}

	void EntityMapper::setEntityType(geas::Index id, const geas::Entity *entity, const geas::Geometry *geometry)
	{
		if (entity)
		{
			this->setTypeReference(id, mEntityTypeAttributes, entity);
		}
		else if (mDefaultEntity)
		{
			this->setTypeReference(id, mEntityTypeAttributes, mDefaultEntity);
		}

		if (geometry)
		{
			this->setTypeReference(id, mGeometryTypeAttributes, entity);
		}
		else if (mDefaultGeometry)
		{
			this->setTypeReference(id, mGeometryTypeAttributes, mDefaultGeometry);
		}
	}

	void EntityMapper::onProviderChanged()
	{
		Delegate::onProviderChanged();

		for (TypeSource &source : mEntityTypeAttributes)
		{
			source.field = INT32_MIN;
		}


		for (TypeSource &source : mGeometryTypeAttributes)
		{
			source.field = INT32_MIN;
		}

		if (mProvider && mProvider->isUniformFieldLayout())
		{
			for (TypeSource &source : mEntityTypeAttributes)
			{
				source.field = mProvider->findMatchingAttribute(0, *source.attribute);
			}


			for (TypeSource &source : mGeometryTypeAttributes)
			{
				source.field = mProvider->findMatchingAttribute(0, *source.attribute);
			}
		}
	}

	void EntityMapper::setTypeReference(geas::Index object, const std::vector<EntityMapper::TypeSource> &source, const geas::Definition *target)
	{
		for (const TypeSource &source : source)
		{
			// Step one - figure out field index
			geas::Index field = source.field;

			if (field == INT32_MIN) // invalid field, do the per-object search
			{
				field = mProvider->findMatchingAttribute(object, *source.attribute);
			}

			switch (source.identifier)
			{
				case geas::Identifier::Label:
				{
					const geas::Label &label = target->getLabel();
					Array value; // TODO figure out how text attributes work
					mProvider->setValue(object, field, value);
					break;
				}

				case geas::Identifier::Code:
				{
					geas::Code code = target->getCode();

					Array value; // TODO figure out how code / fixed-text attributes work
					mProvider->setValue(object, field, value);
					break;
				}

				case geas::Identifier::Index:
				{
					geas::Index index = target->getIndex();

					Array value;
					value.bindToUniform(index);
					mProvider->setValue(object, field, value);
					break;
				}

				default:
				{
					// don't know how to set this type yet
				}
			}
		}
	}

	geas::Definition EntityMapper::getTypeReference(geas::Index object, const std::vector<EntityMapper::TypeSource> &source) const
	{
		geas::Definition defn;

		for (const TypeSource &source : mEntityTypeAttributes)
		{
			// Step one - figure out field index
			geas::Index field = source.field;

			if (field == INT32_MIN) // invalid field, do the per-object search
			{
				field = mProvider->findMatchingAttribute(object, *source.attribute);
			}

			switch (source.identifier)
			{
				case geas::Identifier::Label:
				{
					Array value = mProvider->getValue(object, field);
					// TODO figure out how to get text from this
					geas::Label label;
					defn.setLabel(label);
					break;
				}

				case geas::Identifier::Code:
				{
					Array value = mProvider->getValue(object, field);
					// TODO figure out how code / fixed-text attributes work
					geas::Code code;
					defn.setCode(code);
					break;
				}

				case geas::Identifier::Index:
				{
					geas::Index index = 0;
					Array value = mProvider->getValue(object, field);
					if (value.getBinary(index))
					{
						defn.setIndex(index);
					}
					break;
				}

				case geas::Identifier::Resource:
				{
					Array value = mProvider->getValue(object, field);
					// TODO figure out how to get text from this
					cio::Path path;
					defn.setCanonicalResource(path);
					break;
				}

				default:
				{
					// don't know how to get this type yet
					break;
				}
			}
		}

		return defn;
	}

	const geas::Entity *EntityMapper::getEntityType(geas::Index id) const
	{
		const geas::Entity *entity = mDefaultEntity;

		if (mSchema)
		{
			geas::Definition search = this->getTypeReference(id, mEntityTypeAttributes);
			if (search)
			{
				const geas::Entity *candidate = mSchema->getEntities().get(
					search.getIndex(),
					search.getLabel(),
					search.getCode(),
					search.getCanonicalResource()
				);

				if (candidate)
				{
					entity = candidate;
				}
			}
		}

		return entity;
	}

	std::vector<geas::Attribute> EntityMapper::getEntityTypeAttributes() const
	{
		std::vector<geas::Attribute> attrs;

		for (const TypeSource &source : mEntityTypeAttributes)
		{
			attrs.emplace_back(*source.attribute);
		}

		return attrs;
	}

	std::vector<EntityMapper::TypeSource> &EntityMapper::getEntityTypeSources() noexcept
	{
		return mEntityTypeAttributes;
	}

	const std::vector<EntityMapper::TypeSource> &EntityMapper::getEntityTypeSources() const noexcept
	{
		return mEntityTypeAttributes;
	}

	void EntityMapper::bindEntityTypeSource(const geas::Attribute *attribute, geas::Identifier idType)
	{
		TypeSource source = { attribute, INT32_MIN, idType };
		if (mProvider && mProvider->isUniformFieldLayout())
		{
			source.field = mProvider->findMatchingAttribute(0, *attribute);
		}

		mEntityTypeAttributes.emplace_back(source);
	}

	void EntityMapper::unbindEntityTypeSource(const geas::Attribute *attribute)
	{
		auto ii = mEntityTypeAttributes.begin();
		while (ii != mEntityTypeAttributes.end())
		{
			if (ii->attribute == attribute)
			{
				ii = mEntityTypeAttributes.erase(ii);
			}
			else
			{
				++ii;
			}
		}
	}

	void EntityMapper::clearEntityTypeSource() noexcept
	{
		mEntityTypeAttributes.clear();
	}

	const geas::Geometry *EntityMapper::getGeometryType(geas::Index id) const
	{
		const geas::Geometry *entity = mDefaultGeometry;

		if (mSchema)
		{
			geas::Definition search = this->getTypeReference(id, mGeometryTypeAttributes);
			if (search)
			{
				const geas::Geometry *candidate = mSchema->getGeometries().get(
					search.getIndex(),
					search.getLabel(),
					search.getCode(),
					search.getCanonicalResource()
				);

				if (candidate)
				{
					entity = candidate;
				}
			}
		}

		return entity;
	}

	std::vector<geas::Attribute> EntityMapper::getGeometryTypeAttributes() const
	{
		std::vector<geas::Attribute> attrs;

		for (const TypeSource &source : mGeometryTypeAttributes)
		{
			attrs.emplace_back(*source.attribute);
		}

		return attrs;
	}

	std::vector<EntityMapper::TypeSource> &EntityMapper::getGeometryTypeSources() noexcept
	{
		return mGeometryTypeAttributes;
	}

	const std::vector<EntityMapper::TypeSource> &EntityMapper::getGeometryTypeSources() const noexcept
	{
		return mGeometryTypeAttributes;
	}

	void EntityMapper::bindGeometryTypeSource(const geas::Attribute *attribute, geas::Identifier idType)
	{
		if (attribute)
		{
			TypeSource source = { attribute, INT32_MIN, idType };
			if (mProvider && mProvider->isUniformFieldLayout())
			{
				source.field = mProvider->findMatchingAttribute(0, *attribute);
			}

			mGeometryTypeAttributes.emplace_back(source);
		}
	}

	void EntityMapper::unbindGeometryTypeSource(const geas::Attribute *attribute)
	{
		auto ii = mGeometryTypeAttributes.begin();
		while (ii != mEntityTypeAttributes.end())
		{
			if (ii->attribute == attribute)
			{
				ii = mGeometryTypeAttributes.erase(ii);
			}
			else
			{
				++ii;
			}
		}
	}

	void EntityMapper::clearGeometryTypeSource() noexcept
	{
		mGeometryTypeAttributes.clear();
	}
}
