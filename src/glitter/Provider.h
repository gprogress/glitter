/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_PROVIDER_H
#define GLITTER_PROVIDER_H

#include "Types.h"

#include "Array.h"

#include <geas/Attribute.h>
#include <geas/AttributeFault.h>

#include <cio/Metaclass.h>
#include <cio/Validation.h>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace glitter
{
	/**
	 * The Geodex Data Provider defines the base interface for retrieving attribute values by some index, typically an entity index or
	 * a geometry primitive index such as a vertex or triangle index. The interface is logically exposed as a table where the columns
	 * are specified identified field values (attributes) and the rows are specific objects (entity instances or geometry primitives).
	 *
	 * Subclasses can implement this logical model in any physical way, such as a single unified data buffer with values either interleaved
	 * or segmented, a separate data buffer for each attribute value, a separate data buffer for each entity/primitive object, or key/value maps
	 * or polymorphic objects (such as from code generators from an entity schema).
	 *
	 * Currently the only subclass is Table which has one or more data buffers with a Value for each value type overlaid on the buffers.
	 */
	class GLITTER_ABI Provider
	{
		public:
			/**
			 * Construct an empty provider.
			 */
			Provider() noexcept;

			/**
			 * Copy constructor.
			 *
			 * @param in The provider to copy
			 */
			Provider(const Provider &in);

			/**
			 * Move constructor.
			 *
			 * @param in The provider to move
			 */
			Provider(Provider &&in) noexcept;

			/**
			 * Copy assignment.
			 *
			 * @param in The provider to copy
			 * @return this provider
			 */
			Provider &operator=(const Provider &in);

			/**
			 * Move assignment.
			 *
			 * @param in The provider to move
			 * @return this provider
			 */
			Provider &operator=(Provider &&in) noexcept;

			/**
			 * Destructor.
			 */
			virtual ~Provider() noexcept;

			/**
			 * Gets the metaclass for this object.
			 *
			 * @return the metaclass for this object
			 */
			virtual const cio::Metaclass &getMetaclass() const noexcept;

			/**
			 * Clears the provider state.
			 */
			virtual void clear() noexcept;

			/**
			 * Gets the number of logical objects (entities or geometry primitives) that have values in this value provider.
			 *
			 * The base implementation always returns 0. Subclasses must override to implement providing data.
			 *
			 * @return the number of logical objects
			 */
			virtual geas::Index size() const;

			/**
			 * Sets the number of logical objects (entities or geometry primitives) that have values in this value provider.
			 * The implementation must decide whether to allow this, how to allocate the values, and what the default values (if any)
			 * should be.
			 *
			 * The subclass may choose to allocate more or fewer objects based on constraints or memory limits.
			 * The actual number of objects available will be returned.
			 *
			 * The base implementation does nothing and returns the current size. Subclasses must override to implement resizing.
			 *
			 * @param count The desired number of logical objects
			 * @return the number of logical objects actually set
			 */
			virtual geas::Index resize(geas::Index count);

			/**
			 * Add new record for an object. The entity and geometry schemas may be specified, although it is up to the
			 * provider whether to honor them.
			 *
			 * The base implementation resizes the provider to increase it by 1, and if that succeeds, attempts
			 * to set the entity and geometry type on the new object if they were specified.
			 * Subclasses may override for performance.
			 *
			 * @param entity Used to select the default values for attributes
			 * @param geometry Used to set up geometric representation if applicable
			 * @return geas::Index of new record that would become the entity ID
			 */
			virtual geas::Index create(const geas::Entity *entity = nullptr, const geas::Geometry *geometry = nullptr);

			// Schema management

			/**
			 * Gets a datatype describing the properties of this provider in terms of size() and resize() for object counts.
			 * The datatype should always be of type Integer. The number layout specifies the actual underlying integer used.
			 * The length constraint describes the minimum and maximum size.
			 *
			 * In most cases this should be an intrinsic property of the provider class and this method may return
			 * a pointer to a global constant object.
			 *
			 * Other fields such as cardinality, enumeration, and measure have no specific meaning.
			 *
			 * @return the properties describing how attributes work with this provider
			 */
			virtual const geas::Attribute *getSizeProperties() const noexcept;

			/**
			 * Gets a datatype describing the properties of this provider in terms of handling attribute counts.
			 * The datatype should always be of type Integer. The number layout specifies the actual underlying integer used.
			 * The length constraint describes the minimum and maximum attribute count.
			 * The variable length status indicates whether different objects may have different attribute counts.
			 *
			 * In most cases this should be an intrinsic property of the provider class and this method may return
			 * a pointer to a global constant object.
			 *
			 * Other fields such as cardinality, enumeration, and measure have no specific meaning.
			 *
			 * @return the properties describing how attributes work with this provider
			 */
			virtual const geas::Attribute *getAttributeCountProperties() const noexcept;

			/**
			 * Reports whether this data provider has a uniform attribute field layout.
			 * If true, this means every object has the same attributes in the same sequence.
			 * If false, each object may have a different sequence of attribute fields.
			 *
			 * The base implementation checks the attribute count properties to see if the variable length field is set.
			 * Subclasses may override for performance or more specialized checks.
			 *
			 * Algorithms that are written for non-uniform field layouts should still work with uniform field layouts,
			 * but not vice-versa. Some algorithms may also be optimized knowing the field layout is the same for every object.
			 *
			 * @return whether this data provider uses a uniform attribute field layout
			 */
			virtual bool isUniformFieldLayout() const noexcept;

			/**
			 * Gets the entity type for the given object.
			 * The base implementation returns the nullptr. Subclasses should override to implement.
			 *
			 * @param object The object ID
			 * @return the entity type, or null if unknown
			 */
			virtual const geas::Entity *getEntityType(geas::Index object) const;

			/**
			 * Gets the geometry type for the given object.
			 * The base implementation returns the nullptr. Subclasses should override to implement.
			 *
			 * @param object The object ID
			 * @return the geometry type, or null if unknown
			 */
			virtual const geas::Geometry *getGeometryType(geas::Index object) const;

			/**
			 * Sets the entity type and/or geometry type for the given object.
			 * A nullptr parameter indicates not to change it.
			 *
			 * The base implementation throws an exception indicating the operation is unsupported.
			 * Subclasses should override to implement changing one or both types in any way desired.
			 *
			 * @param object The object ID
			 * @param entity The entity type, or nullptr to leave unchanged
			 * @param geometry The geometry type, or nullptr to leave unchanged
			 */
			virtual void setEntityType(geas::Index object, const geas::Entity *entity, const geas::Geometry *geometry = nullptr);

			/**
			 * Gets the number of attributes known to be present for the given object index.
			 *
			 * @param object The object index
			 * @return the number of attributes on that object
			 */
			virtual geas::Index getAttributeCount(geas::Index object) const;

			/**
			 * Gets the attribute definition for a given object and attribute field.
			 * The field must be less than the attribute count for the object.
			 *
			 * The base implementation always returns nullptr. Subclasses should override if they can provide this information.
			 *
			 * @param object The object index
			 * @param field The attribute field ID
			 * @return the attribute schema
			 */
			virtual const geas::Attribute *getAttribute(geas::Index object, geas::Index field) const;

			/**
			 * Sets the attribute definition for a given object and attribute field.
			 *
			 * The base implementation always throws an exception indicating this is unsupported.
			 * Subclasses should override if they support this operation.
			 *
			 * @param object The object index
			 * @param field The attribute field index
			 * @param defn the attribute schema
			 * @throw geas::AttributeFault if the attribute could not be set
			 */
			virtual void setAttribute(geas::Index object, geas::Index field, const geas::Attribute *defn);

			/**
			 * Adds an attribute to the given object, obtaining the new field ID.
			 *
			 * The base implementation always throws an exception indicating this is unsupported.
			 * Subclasses should override if they support this operation.
			 *
			 * @param object The object index
			 * @param defn The attribute schema
			 * @return the field index of the added attribute
			 * @throw geas::AttributeFault if the attribute could not be set
			 */
			virtual geas::Index addAttribute(geas::Index object, const geas::Attribute *defn);

			/**
			 * Removes an attribute from the given object.
			 *
			 * The base implementation always throws an exception indicating this is unsupported.
			 * Subclasses should override if they support this operation.
			 *
			 * @param object The object index
			 * @param field The attribute field index, or UINT32_MAX if not found
			 */
			virtual void removeAttribute(geas::Index object, geas::Index field);

			/**
			 * Finds the field index of an attribute given its label.
			 *
			 * The base implementation iterates through the attributes for the object
			 * until it finds the first matching one. Subclasses may override for performance.
			 *
			 * @param object The object index
			 * @param label The attribute label
			 * @return the attribute field index, or UINT32_MAX if not found
			 */
			virtual geas::Index findAttributeByLabel(geas::Index object, const geas::Label &label) const;

			/**
			 * Finds the field index of an attribute given its code.
			 *
			 * The base implementation iterates through the attributes for the object
			 * until it finds the first matching one. Subclasses may override for performance.
			 *
			 * @param id The object ID
			 * @param code The attribute code
			 * @return the attribute field index, or UINT32_MAX if not found
			 */
			virtual geas::Index findAttributeByCode(geas::Index id, const geas::Code &code) const;

			/**
			 * Finds the field index of an attribute given its schema index.
			 *
			 * The base implementation iterates through the attributes for the object
			 * until it finds the first matching one. Subclasses may override for performance.
			 *
			 * @param object The object index
			 * @param idx The attribute schema index
			 * @return the attribute field index, or UINT32_MAX if not found
			 */
			virtual geas::Index findAttributeByIndex(geas::Index object, geas::Index idx) const;

			/**
			 * Finds the field index of the first attribute that matches the given attribute.
			 * Any identifiers that are set on the attribute are used for the search.
			 *
			 * @param object The object index
			 * @param attribute The attribute to match with as many fields set as known
			 * @return the attribute field index, or INT32_MIN if not found
			 */
			virtual geas::Index findMatchingAttribute(geas::Index object, const geas::Attribute &attribute) const;

			// Value and memory management

			/**
			 * Gets an attribute value for the given object and attribute field.
			 *
			 * The base implementation returns an empty value. Subclasses must override.
			 *
			 * @param object The object ID
			 * @param field The attribute field ID
			 * @return the value
			 */
			virtual Array getValue(geas::Index object, geas::Index field) const;

			/**
			 * Sets an attribute value for the given object and attribute field.
			 *
			 * The base implementation throws an exception to indicate this operation is unsupported.
			 * Subclasses must override if they support this operation.
			 *
			 * @param object The object ID
			 * @param field The attribute field ID
			 * @param value The value
			 * @return whether the set was successful
			 */
			virtual bool setValue(geas::Index object, geas::Index field, const Array &value);

			/**
			 * Validates the current value for the given object and attribute field to see if it matches the given datatype constraint.
			 *
			 * The base implmentation gets the value and then validates it.
			 * Subclasses may override for performance.
			 *
			 * @param constraint The datatype specifying the constraint
			 * @param object The object ID
			 * @param attr ID the attribute field ID
			 * @return a fault describing validation failures, if any
			 */
			virtual geas::AttributeFault validateCurrentValue(const geas::Attribute &constraint, geas::Index object, geas::Index attr) const;

			/**
			 * Estimates the total memory use of this provider for data values, excluding overhead and schema information.
			 * If an exact answer is not possible to determine, an upper bound is acceptable.
			 * The base class returns SIZE_MAX indicating this is unknown.
			 *
			 * @return the estimated current memory use
			 */
			virtual std::size_t estimateValueMemoryUse() const noexcept;

			/**
			 * Estimates the new memory use needed to create the values for the given number of objects.
			 * The base class returns SIZE_MAX indicating this is unknown.
			 *
			 * @param count The number of objects to create
			 * @return the memory size to be allocated
			 */
			virtual std::size_t estimateNewValueMemoryUse(std::size_t count) const noexcept;

		private:
			/** The metaclass for this class. */
			static cio::Class<Provider> sMetaclass;
	};
}

#ifndef _MSC_VER
#pragma warning (pop)
#endif

#endif
