/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_WEIGHT_H
#define GLITTER_WEIGHT_H

#include "Types.h"

#include <cio/Metatypes.h>

#include <iosfwd>

namespace glitter
{
	template <typename T, typename C, typename L>
	struct Weight
	{
		T value;
		C count;
		L total;

		inline cio::RealQuotient<C, L> fraction() const;

		template <typename U, typename D, typename M>
		inline Weight<std::common_type_t<T, U>, std::common_type_t<C, D>, std::common_type_t<L, M>> max(const Weight<U, D, M> &in) const;
	};

	template <typename C, typename L>
	struct Weight<void, C, L>
	{
		C count;
		L total;

		inline cio::RealQuotient<C, L> fraction() const;

		template <typename U, typename D, typename M>
		inline Weight<void, std::common_type_t<C, D>, std::common_type_t<L, M>> max(const Weight<U, D, M> &in) const;
	};

	template <typename C, typename L, typename U, typename D, typename M>
	inline Weight<void, C, L> &operator|=(Weight<void, C, L> &left, const Weight<U, D, M> &right);

	template <typename T, typename C, typename L, typename U, typename D, typename M>
	inline bool operator==(const Weight<T, C, L> &left, const Weight<U, D, M> &right);

	template <typename T, typename C, typename L, typename U, typename D, typename M>
	inline bool operator!=(const Weight<T, C, L> &left, const Weight<U, D, M> &right);

	template <typename C, typename L, typename D, typename M>
	inline bool operator==(const Weight<void, C, L> &left, const Weight<void, D, M> &right);

	template <typename C, typename L, typename D, typename M>
	inline bool operator!=(const Weight<void, C, L> &left, const Weight<void, D, M> &right);

	template <typename T, typename C, typename L>
	inline std::ostream &operator<<(std::ostream &s, const Weight<T, C, L> &value);

	template <typename C, typename L>
	inline std::ostream &operator<<(std::ostream &s, const Weight<void, C, L> &value);
}

/* Inline implementation */

namespace glitter
{
	template <typename T, typename C, typename L>
	inline cio::RealQuotient<C, L> Weight<T, C, L>::fraction() const
	{
		return cio::RealQuotient<C, L>(this->count) / cio::RealQuotient<C, L>(this->total);
	}

	template <typename T, typename C, typename L>
	template <typename U, typename D, typename M>
	inline Weight<std::common_type_t<T, U>, std::common_type_t<C, D>, std::common_type_t<L, M>> Weight<T, C, L>::max(const Weight <U, D, M> &in) const
	{
		Weight<std::common_type_t<T, U>, std::common_type_t<C, D>, std::common_type_t<L, M>> result;
		if (this->value != in.value)
		{
			if (this->count == in.count)
			{
				if (this->value <= in.value)
				{
					result.value = this->value;
					result.count = this->in.count;
				}
				else
				{
					result.value = in.value;
					result.count = in.count;
				}
			}
			else if (this->count < in.count)
			{	
				result.value = in.value;
				result.count = in.count;
			}
			else
			{
				result.value = this->value;
				result.count = this->count;
			}
		}
		else
		{
			result.value = this->value;
			result.count = this->count + in.count;
		}

		result.total = this->total + in.total;
		return result;
	}

	template <typename C, typename L>
	inline cio::RealQuotient<C, L> Weight<void, C, L>::fraction() const
	{
		return cio::RealQuotient<C, L>(this->count) / cio::RealQuotient<C, L>(this->total);
	}

	template <typename C, typename L>
	template <typename U, typename D, typename M>
	inline Weight<void, std::common_type_t<C, D>, std::common_type_t<L, M>> Weight<void, C, L>::max(const Weight<U, D, M> &in) const
	{
		Weight<void, std::common_type_t<C, D>, std::common_type_t<L, M>> result;
		result.count = this->count + in.count;
		result.total = this->total + in.total;
		return result;
	}

	template <typename C, typename L, typename U, typename D, typename M>
	inline Weight<void, C, L> &operator|=(Weight<void, C, L> &left, const Weight<U, D, M> &right)
	{
		left.count += right.count;
		left.total += right.total;
		return left;
	}

	template <typename T, typename C, typename L, typename U, typename D, typename M>
	bool operator==(const Weight<T, C, L> &left, const Weight<U, D, M> &right)
	{
		return left.value == right.value && left.count == right.count && left.total == right.total;
	}

	template <typename T, typename C, typename L, typename U, typename D, typename M>
	inline bool operator!=(const Weight<T, C, L> &left, const Weight<U, D, M> &right)
	{
		return left.value != right.value && left.count != right.count && left.total != right.total;
	}

	template <typename C, typename L, typename D, typename M>
	inline bool operator==(const Weight<void, C, L> &left, const Weight<void, D, M> &right)
	{
		return left.count == right.count && left.total == right.total;
	}

	template <typename C, typename L, typename D, typename M>
	inline bool operator!=(const Weight<void, C, L> &left, const Weight<void, D, M> &right)
	{
		return left.count != right.count && left.total != right.total;
	}

	template <typename T, typename C, typename L>
	inline std::ostream &operator<<(std::ostream &s, const Weight<T, C, L> &value)
	{
		return s << value.value << ' ' << value.count << '/' << value.total;
	}

	template <typename C, typename L>
	inline std::ostream &operator<<(std::ostream &s, const Weight<void, C, L> &value)
	{
		return s << value.count << '/' << value.total;
	}
}

#endif
