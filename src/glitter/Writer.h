/*=====================================================================================================================
 * Copyright 2022-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_WRITER_H
#define GLITTER_WRITER_H

#include "Types.h"

#include <cio/Types.h>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

namespace glitter
{
	/**
	 * The Writer provides the base interface for writing data formats for catalogs, collections, and resources.
	 *
	 * When possible, writers should try to allow any resource type and protocol - relying on a protocol factory to mediate this -
	 * and primarily focus on the encoding of the data to generic streams.
	 */
	class GLITTER_ABI Writer
	{
		public:
			/**
			 * Default constructor.
			 */
			Writer() noexcept;

			/**
			 * Destructor.
			 */
			virtual ~Writer() noexcept;

			/**
			 * Gets the runtime metaclass for this object instance.
			 *
			 * @return the runtime metaclass for this object
			 */
			virtual const cio::Metaclass &getMetaclass() const noexcept;

			/**
			 * Stores a repository to the given URI.
			 *
			 * @param repo The repository to store
			 * @param path The path to the main resource identifying the output
			 * @param protocols The protocol factory that may be used to obtain necessary protocols to create readers or traverse directories
			 */
			virtual void storeRepository(const Repository &repo, const cio::Path &path, const cio::ProtocolFactory &protocols);

			/**
			 * Stores a catalog from the given URI.
			 *
			 * @param catalog the catalog to store
			 * @param path The path to the main resource identifying the output
			 * @param protocols The protocol factory that may be used to obtain necessary protocols to create readers or traverse directories
			 */
			virtual void storeCatalog(const Catalog &catalog, const cio::Path &path, const cio::ProtocolFactory &protocols);

			/**
			 * Stores a collection from the given URI.
			 *
			 * @param collection The collection to store
			 * @param path The path to the main resource identifying the output
			 * @param protocols The protocol factory that may be used to obtain necessary protocols to create readers or traverse directories
			 */
			virtual void storeCollection(const Collection &collection, const cio::Path &path, const cio::ProtocolFactory &protocols);

			/**
			 * Stores a node to the given URI.
			 *
			 * @param res The node to store
			 * @param path The path to the main resource identifying the output
			 * @param protocols The protocol factory that may be used to obtain necessary protocols to create readers or traverse directories
			 */
			virtual void storeNode(const Node &res, const cio::Path &path, const cio::ProtocolFactory &protocols);

		private:
			/** The metaclass for this class. */
			static cio::Class<Writer> sMetaclass;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
