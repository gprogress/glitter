/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "Delegate.h"

#include "Entity.h"
#include "Provider.h"
#include "Table.h"

#include <geas/Dictionary.cpp>
#include <geas/Entity.h>

#include <cio/Class.h>

namespace glitter
{
	cio::Class<Delegate> Delegate::sMetaclass("glitter::Delegate");

	const cio::Class<Delegate> &Delegate::getDeclaredMetaclass() noexcept
	{
		return sMetaclass;
	}

	Delegate::Delegate() noexcept :
		mDefaultEntity(nullptr),
		mDefaultGeometry(nullptr)
	{
		// nothing more to do
	}

	Delegate::Delegate(const Delegate &in) :
		Provider(in),
		mProvider(in.mProvider),
		mDefaultEntity(in.mDefaultEntity),
		mDefaultGeometry(in.mDefaultGeometry)
	{
		// nothing more to do
	}

	Delegate::Delegate(Delegate &&in) noexcept :
		Provider(std::move(in)),
		mProvider(std::move(in.mProvider)),
		mDefaultEntity(in.mDefaultEntity),
		mDefaultGeometry(in.mDefaultGeometry)
	{
		// nothing more to do
	}

	Delegate &Delegate::operator=(const Delegate &in)
	{
		if (this != &in)
		{
			Provider::operator=(in);
			mProvider = in.mProvider;
			mDefaultEntity = in.mDefaultEntity;
			mDefaultGeometry = in.mDefaultGeometry;
		}

		return *this;
	}

	Delegate &Delegate::operator=(Delegate &&in) noexcept
	{
		if (this != &in)
		{
			Provider::operator=(in);
			mProvider = std::move(in.mProvider);
			mDefaultEntity = in.mDefaultEntity;
			mDefaultGeometry = in.mDefaultGeometry;
		}

		return *this;
	}

	Delegate::~Delegate() noexcept = default;

	const cio::Metaclass &Delegate::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	void Delegate::clear() noexcept
	{
		Provider::clear();
		mProvider.reset();
		mDefaultEntity = nullptr;
		mDefaultGeometry = nullptr;
	}

	void Delegate::setDefaultEntityType(const geas::Entity *entity) noexcept
	{
		mDefaultEntity = entity;
	}

	const geas::Entity *Delegate::getDefaultEntityType() const noexcept
	{
		return mDefaultEntity;
	}

	void Delegate::setDefaultGeometryType(const geas::Geometry *geometry) noexcept
	{
		mDefaultGeometry = geometry;
	}

	const geas::Geometry *Delegate::getDefaultGeometryType() const noexcept
	{
		return mDefaultGeometry;
	}

	std::shared_ptr<Provider> Delegate::getProvider() noexcept
	{
		return mProvider;
	}

	std::shared_ptr<const Provider> Delegate::getProvider() const noexcept
	{
		return mProvider;
	}

	std::shared_ptr<Provider> Delegate::getOrCreateProvider()
	{
		if (!mProvider)
		{
			mProvider.reset(new Table());
			this->onProviderChanged();
		}

		return mProvider;
	}

	void Delegate::setProvider(std::shared_ptr<Provider> provider) noexcept
	{
		if (mProvider || provider)
		{
			mProvider = std::move(provider);
			this->onProviderChanged();
		}
	}

	void Delegate::clearProvider() noexcept
	{
		if (mProvider)
		{
			mProvider.reset();
			this->onProviderChanged();
		}
	}

	std::shared_ptr<Provider> Delegate::takeProvider() noexcept
	{
		std::shared_ptr<Provider> provider = std::move(mProvider);

		if (provider)
		{
			this->onProviderChanged();
		}

		return provider;
	}

	geas::Index Delegate::size() const
	{
		geas::Index count = 0;
		if (mProvider)
		{
			count = mProvider->size();
		}
		return count;
	}

	geas::Index Delegate::resize(geas::Index count)
	{
		geas::Index actual = 0;

		if (mProvider)
		{
			actual = mProvider->resize(count);
		}
		else if (count > 0)
		{
			actual = this->getOrCreateProvider()->resize(count);
		}

		return actual;
	}

	const geas::Attribute *Delegate::getSizeProperties() const noexcept
	{
		const geas::Attribute *type = nullptr;

		if (mProvider)
		{
			type = mProvider->getSizeProperties();
		}

		return type;
	}

	const geas::Attribute *Delegate::getAttributeCountProperties() const noexcept
	{
		const geas::Attribute *type = nullptr;

		if (mProvider)
		{
			type = mProvider->getAttributeCountProperties();
		}

		return type;
	}

	bool Delegate::isUniformFieldLayout() const noexcept
	{
		bool uniform = false;

		if (mProvider)
		{
			uniform = mProvider->isUniformFieldLayout();
		}

		return uniform;
	}

	const geas::Entity *Delegate::getEntityType(geas::Index object) const
	{
		const geas::Entity *type = mDefaultEntity;

		if (mProvider)
		{
			const geas::Entity *provided = mProvider->getEntityType(object);
			if (provided)
			{
				type = provided;
			}
		}

		return type;
	}

	const geas::Geometry *Delegate::getGeometryType(geas::Index object) const
	{
		const geas::Geometry *type = nullptr;
		if (mProvider)
		{
			const geas::Geometry *provided = mProvider->getGeometryType(object);
			if (provided)
			{
				type = provided;
			}
		}
		return type;
	}

	void Delegate::setEntityType(geas::Index object, const geas::Entity *entity, const geas::Geometry *geometry)
	{
		this->getOrCreateProvider()->setEntityType(object, entity ? entity : mDefaultEntity, geometry ? geometry : mDefaultGeometry);
	}

	geas::Index Delegate::create(const geas::Entity *entity, const geas::Geometry *geometry)
	{
		return this->getOrCreateProvider()->create(entity ? entity : mDefaultEntity, geometry ? geometry : mDefaultGeometry);
	}

	geas::Index Delegate::getAttributeCount(geas::Index object) const
	{
		geas::Index count = 0;

		if (mProvider)
		{
			count = mProvider->getAttributeCount(object);
		}

		return count;
	}

	const geas::Attribute *Delegate::getAttribute(geas::Index object, geas::Index field) const
	{
		const geas::Attribute *attr = nullptr;
		if (mProvider)
		{
			mProvider->getAttribute(object, field);
		}
		return attr;
	}

	void Delegate::setAttribute(geas::Index object, geas::Index field, const geas::Attribute *defn)
	{
		this->getOrCreateProvider()->setAttribute(object, field, defn);
	}

	geas::Index Delegate::addAttribute(geas::Index object, const geas::Attribute *defn)
	{
		return this->getOrCreateProvider()->addAttribute(object, defn);
	}

	void Delegate::removeAttribute(geas::Index object, geas::Index field)
	{
		if (mProvider)
		{
			mProvider->removeAttribute(object, field);
		}
	}

	geas::Index Delegate::findAttributeByLabel(geas::Index object, const geas::Label &label) const
	{
		geas::Index field = UINT32_MAX;
		if (mProvider)
		{
			field = mProvider->findAttributeByLabel(object, label);
		}
		return field;
	}

	geas::Index Delegate::findAttributeByCode(geas::Index object, const geas::Code &code) const
	{
		geas::Index field = UINT32_MAX;
		if (mProvider)
		{
			field = mProvider->findAttributeByCode(object, code);
		}
		return field;
	}

	geas::Index Delegate::findAttributeByIndex(geas::Index object, geas::Index idx) const
	{
		geas::Index field = UINT32_MAX;
		if (mProvider)
		{
			field = mProvider->findAttributeByIndex(object, idx);
		}
		return field;
	}

	geas::Index Delegate::findMatchingAttribute(geas::Index object, const geas::Attribute &attribute) const
	{
		geas::Index aid = UINT32_MAX;
		if (mProvider)
		{
			aid = mProvider->findMatchingAttribute(object, attribute);
		}
		return aid;
	}

	Array Delegate::getValue(geas::Index object, geas::Index field) const
	{
		Array value;
		if (mProvider)
		{
			value = mProvider->getValue(object, field);
		}
		return value;
	}

	bool Delegate::setValue(geas::Index object, geas::Index field, const Array &value)
	{
		bool done = false;
		if (mProvider)
		{
			mProvider->setValue(object, field, value);
		}
		return done;
	}

	geas::AttributeFault Delegate::validateCurrentValue(const geas::Attribute &constraint, geas::Index object, geas::Index field) const
	{
		geas::AttributeFault fault(cio::Validation::Missing);

		if (mProvider)
		{
			fault = mProvider->validateCurrentValue(constraint, object, field);
		}

		return fault;
	}

	std::size_t Delegate::estimateValueMemoryUse() const noexcept
	{
		std::size_t memory = 0;
		if (mProvider)
		{
			memory = mProvider->estimateValueMemoryUse();
		}
		return memory;
	}

	std::size_t Delegate::estimateNewValueMemoryUse(std::size_t count) const noexcept
	{
		std::size_t memory = 0;
		if (mProvider)
		{
			memory = mProvider->estimateNewValueMemoryUse(count);
		}
		return memory;
	}

	void Delegate::onProviderChanged()
	{
		// nothing to do in base class
	}
}

