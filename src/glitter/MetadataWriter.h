/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_METADATAWRITER_H
#define GLITTER_METADATAWRITER_H

#include "Types.h"

#include <string>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable:4251)
#endif

#include <nlohmann/json_fwd.hpp>

namespace glitter
{
	/**
	 * The MetadataClassWriter is responsible for writing glTF feature metadata (EXT_feature_metadata) content to the appropriate JSON representation.
	 * It can either write a standalone JSON resource without a glTF model, or update a loaded glTF document to update or create the proper extension information.
	 * The writer builds out glTF class definitions from the Geodex Entity definitions in a Geodex Application Schema, and builds out one feature table for each
	 * Geodex Node.
	 */
	class GLITTER_ABI MetadataWriter
	{
		public:
			/**
			 * Gets or creates the JSON node for the given named feature table.
			 * 
			 * @param extensionRoot The EXT_feature_metadata root node
			 * @param name The name of the feature table
			 * @return the feature table JSON node
			 */
			nlohmann::json &getOrCreateFeatureTable(nlohmann::json &extensionRoot, const geas::Label &name) const;

			/**
			 * Updates the given feature table to reflect the presence of a particular attribute and associates it with a buffer view index.
			 * 
			 * @param table The feature table JSON node
			 * @param defn The attribute definition
			 * @param bufferViewIdx The buffer view index for the attribute
			 */
			void updateAttribute(nlohmann::json &table, const geas::Attribute &defn, std::uint32_t bufferViewIdx) const;

			/**
			 * Updates the given feature table to reflect a modified feature count.
			 *
			 * @param table The feature table JSON node
			 * @param count The new feature count
			 */
			void updateFeatureCount(nlohmann::json &table, std::size_t count) const;

			/**
			 * Updates the given feature table to reflect a change in the feature class associated with the table.
			 *
			 * @param table The feature table JSON node
			 * @param featureType The feature type
			 */
			void updateTableFeatureType(nlohmann::json &table, const geas::Label &featureType) const;

			/**
			 * Updates the JSON to match the given feature ID mapping configuration.
			 * 
			 * @param extensionRoot The JSON node for the feature metadata extension attached to a mesh primitive (typically "EXT_feature_metadata")
			 * @param primitive The gltf primitive being updated
			 * @param collection The entity collection with the Node being referenced
			 * @param map The feature ID map describing the current mapping state
			 * @param layerIndex The layer index this mapping applies to
			 */
			void updateFeatureMapper(nlohmann::json &extensionRoot, const fx::gltf::Primitive &primitive, const Collection &collection, const FeatureIdMap &map, std::uint32_t layerIndex = 0) const;
};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif
