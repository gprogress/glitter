/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "Print.h"

#include <ostream>

namespace fx
{
	namespace gltf
	{
		namespace
		{
			// Component types are not sequential so we have to map them
			fx::gltf::Accessor::ComponentType sComponentTypes[] = {
				fx::gltf::Accessor::ComponentType::None,
				fx::gltf::Accessor::ComponentType::Byte,
				fx::gltf::Accessor::ComponentType::UnsignedByte,
				fx::gltf::Accessor::ComponentType::Short,
				fx::gltf::Accessor::ComponentType::UnsignedShort,
				fx::gltf::Accessor::ComponentType::UnsignedInt,
				fx::gltf::Accessor::ComponentType::Float
			};

			const char *sComponentTypeText[] = {
				"None",
				"Byte",
				"UnsignedByte",
				"Short",
				"UnsignedShort",
				"UnsignedInt",
				"Float"
			};

			// Accessor types are sequential
			const char *sAccessorTypeText[] = {
				"None",
				"Scalar",
				"Vec2",
				"Vec3",
				"Vec4",
				"Mat2",
				"Mat3",
				"Mat4"
			};

			// Target types are not sequential, need to map them
			const fx::gltf::BufferView::TargetType sTargetTypes[] = {
				fx::gltf::BufferView::TargetType::None,
				fx::gltf::BufferView::TargetType::ArrayBuffer,
				fx::gltf::BufferView::TargetType::ElementArrayBuffer
			};

			const char *sTargetTypeText[] = {
				"None",
				"ArrayBuffer",
				"ElementArrayBuffer"
			};

			// Primitive modes are sequential
			const char *sPrimitiveModeText[] = {
				"Points",
				"Lines",
				"LineLoop",
				"LineStrip",
				"Triangles",
				"TriangleStrip",
				"TriangleFan"
			};
		}
		
		const char *toString(fx::gltf::Accessor::ComponentType type)
		{
			const char *text = "Unknown"; // not actually a valid type, but a good error condition
			for (std::size_t i = 0; i < sizeof(sComponentTypeText) / sizeof(char *); ++i)
			{
				if (sComponentTypes[i] == type)
				{
					text = sComponentTypeText[i];
					break;
				}
			}
			return text;
		}

		std::ostream &operator<<(std::ostream &s, fx::gltf::Accessor::ComponentType type)
		{
			return s << toString(type);
		}

		const char *toString(fx::gltf::Accessor::Type type)
		{
			const char *text = "Unknown"; // not actually a valid type, but a good error condition for a bad binary read
			if (static_cast<std::size_t>(type) < sizeof(sAccessorTypeText) / sizeof(char *))
			{
				text = sAccessorTypeText[static_cast<std::size_t>(type)];
			}
			return text;
		}

		std::ostream &operator<<(std::ostream &s, fx::gltf::Accessor::Type type)
		{
			return s << toString(type);
		}

		const char *toString(fx::gltf::BufferView::TargetType type)
		{
			const char *text = "Unknown"; // not actually a valid type, but a good error condition
			for (std::size_t i = 0; i < sizeof(sTargetTypeText) / sizeof(char *); ++i)
			{
				if (sTargetTypes[i] == type)
				{
					text = sTargetTypeText[i];
					break;
				}
			}
			return text;

		}

		std::ostream &operator<<(std::ostream &s, fx::gltf::BufferView::TargetType type)
		{
			return s << toString(type);
		}

		const char *toString(fx::gltf::Primitive::Mode type)
		{
			const char *text = "Unknown"; // not actually a valid type, but a good error condition for a bad binary read
			if (static_cast<std::size_t>(type) < sizeof(sPrimitiveModeText) / sizeof(char *))
			{
				text = sPrimitiveModeText[static_cast<std::size_t>(type)];
			}
			return text;

		}

		std::ostream &operator<<(std::ostream &s, fx::gltf::Primitive::Mode type)
		{
			return s << toString(type);
		}
	}
}