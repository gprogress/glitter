/*=====================================================================================================================
 * Copyright 2021-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_GARBAGEMARKER_H
#define GLITTER_GARBAGEMARKER_H

#include "Types.h"

#include <vector>

#if defined _MSC_VER
#pragma warning(push, 1)
#pragma warning(disable: 4251)
#endif

namespace glitter
{
	/**
	 * The Garbage Marker is a helper class to perform garbage collection of a particular type of object in a glTF document represented by index values.
	 * It implements two phases: mark and sweep. The mark phase is used to note which object indices have been found to be in use from some external traversal.
	 * After all marking is done, the sweep phase calculates a set of removals for ranges of unused indices which can be used to both conduct these removals of the actual objects
	 * and to remap the remaining used indices to be contiguous.
	 */
	class GLITTER_ABI GarbageMarker
	{
		public:
			/**
			 * The Removal is a helper structure to track which indices were garbage collected and removed.
			 */
			struct Removal
			{
				/** The first index that was removed in this operation */
				std::int32_t start;

				/** The first index that was NOT removed in this operation (the end index) */
				std::int32_t end;

				/** The total cumulative number of indices that have been removed as of the end index */
				std::int32_t total;
			};

			/**
			 * Constructs a new garbage marker to track indices for a particular data type.
			 */
			GarbageMarker() noexcept;

			/**
			 * Destructor.
			 */
			~GarbageMarker() noexcept;

			/**
			 * Clears object state and deallocates all memory.
			 */
			void clear() noexcept;

			/**
			 * Sets the highest known index that might be marked as in use.
			 * This also allocates the necessary bitset memory to track the marking, avoiding the need for incremental resizing.
			 * 
			 * @param count The highest input index that might be marked
			 * @throw std::bad_alloc If a memory allocation was needed but failed
			 */
			void setInputCount(std::int32_t count);
			
			/**
			 * Gets the highest known index that might be marked as in use.
			 *
			 * @return The highest input index that might be marked
			 */
			std::int32_t getInputCount() const noexcept;

			/**
			 * Marks the given index as being in use.
			 * This will allocate additional memory if necessary and update the input count if the given index is higher.
			 * If this is the first time this index is marked, then this method returns true and updates the mark count.
			 * Otherwise it returns false.
			 * 
			 * @param idx The index to mark as in use
			 * @throw std::bad_alloc If a memory allocation was needed but failed
			 */
			bool mark(std::int32_t idx);

			/**
			 * Gets whether the given index is marked as in use.
			 * 
			 * @param idx The index of interest
			 * @return whether the index has been marked as in use
			 */
			bool isMarked(std::int32_t idx) const;

			/**
			 * Gets the total number of unique indices that have been marked.
			 * If 0, then no indices have been marked.
			 * If equal to the input count, then all indices have been marked.
			 * 
			 * @return the unique marked index count
			 */
			std::int32_t getMarkedCount() const noexcept;

			/**
			 * Sweeps all known indices and creates removal ranges for indices that were not marked as in use.
			 * 
			 * @return the total number of indices that were removed, 0 if all were in use
			 * @throw std::bad_alloc If removals were found but the memory to track them could not be allocated
			 */
			std::int32_t sweep();

			/**
			 * Gets the total number of output indices after the sweep phase has been calculated.
			 * If the same as the input count, all indices were in use and none were removed.
			 * 
			 * If called before the sweep phase has been run, this simply returns the input count.
			 * 
			 * @return the total number of output indices
			 */
			std::int32_t getOutputCount() const noexcept;

			/**
			 * Calculates what an index should be after garbage collection removes all unused indices.
			 * This takes the input index and looks for the nearest removal below it, then subtracts the total removed indices at that point.
			 * If there are no removals before the given index, it is returned unmodified.
			 * If the index itself was removed or is after the input index count, -1 is returned to indicate the index does not exist in the output.
			 * 
			 * If called before the sweep phase is performed, no removals exist so the index is returned unmodified unless it is higher than the input count.
			 * 
			 * @param index The input index
			 * @return the remapped output index after removals
			 */
			std::int32_t remap(std::int32_t index) const noexcept;

			/**
			 * Gets the number of Removal range entries.
			 * If 0, no removals were done and all indices are in use.
			 * 
			 * @return the number of distinct contiguous removal ranges
			 */
			std::int32_t getRemovalCount() const noexcept;

			/**
			 * Finds the nearest Removal entry before or containing the given index.
			 * The Removal entry will specify the start and end index of the removal range and the total number of removed indices up to the end index.
			 * If there are no removals before the given index, the returned Removal structure will have start equal to -1, end equal to 0, and total equal to 0.
			 * 
			 * @param idx The index of interest
			 * @return the nearest removal range before or containing the index, or the placeholder indicating no such range exists
			 */
			GarbageMarker::Removal findNearestRemoval(std::int32_t idx) const noexcept;

			/**
			 * Gets a given removal range given its position in the removal range sequence.
			 * The position is not tied to the index values, but rather is a value from 0 to getRemovalCount().
			 * 
			 * @param position The position of the removal range of interest
			 * @return the removal range of interest
			 */
			GarbageMarker::Removal getRemoval(std::int32_t position) const noexcept;

			/**
			 * Checks whether any users of the index type for this garbage marker need to remap their index values based on the results.
			 * This is true if some but not all of the indexes were removed as unused, and if there were valid used indices after removed indices.
			 * Conversely, if there were no removals, or if all removals were at the end, then no remapping is necessary and it can be skipped.
			 * 
			 * @return whether users of this type of index need to remap index values
			 */
			bool needsRemapping() const noexcept;

		private:
			/** Mapping structure to track which indices were removed and how to remap indices after them */
			std::vector<GarbageMarker::Removal> mRemovals;
			
			/** Bitset of index values that were marked for this traversal pass */
			std::vector<bool> mMarkedIndices;

			/** The total number of input indices that were processed, typically set to the total index count from the gltf document */
			std::int32_t mInputCount;

			/** The total number of unique indices that have been marked. */
			std::int32_t mMarkedCount;

			/** The total number of output indices that were preserved after garbage collection */
			std::int32_t mOutputCount;
	};
}

#if defined _MSC_VER
#pragma warning(pop)
#endif

#endif