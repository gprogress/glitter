#######################################################################################################################
# Copyright 2020-2023 Geometric Progress LLC
#
# This file is part of the Glitter project.
#
# Glitter is free software : you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Glitter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
#######################################################################################################################
include_directories("fx-gltf")

function(GLITTER_INCLUDE_GROUP BASE PREFIX FILES)
	set(_HEADERS )
	foreach(_FILE ${FILES} ${ARGN})
		list(APPEND _HEADERS ${BASE}/${PREFIX}/${_FILE})
	endforeach()
	install(FILES ${_HEADERS} DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/${PREFIX}")
endfunction()

GLITTER_INCLUDE_GROUP("fx-gltf" "fx" "gltf.h")
GLITTER_INCLUDE_GROUP("fx-gltf" "nlohmann" 
	"adl_serializer.hpp"
	"json.hpp"
	"json_fwd.hpp"
)
	
GLITTER_INCLUDE_GROUP("fx-gltf" "nlohmann/detail"
	"exceptions.hpp"
	"json_pointer.hpp"
	"json_ref.hpp"
	"macro_scope.hpp"
	"macro_unscope.hpp"
	"value_t.hpp"
)

GLITTER_INCLUDE_GROUP("fx-gltf" "nlohmann/detail/conversions"
	"from_json.hpp"
	"to_chars.hpp"
	"to_json.hpp"
)

GLITTER_INCLUDE_GROUP("fx-gltf" "nlohmann/detail/input"
	"binary_reader.hpp"
	"input_adapters.hpp"
	"json_sax.hpp"
	"lexer.hpp"
	"parser.hpp"
	"position_t.hpp"
)

GLITTER_INCLUDE_GROUP("fx-gltf" "nlohmann/detail/iterators"
	"internal_iterator.hpp"
	"iter_impl.hpp"
	"iteration_proxy.hpp"
	"iterator_traits.hpp"
	"json_reverse_iterator.hpp"
	"primitive_iterator.hpp"
)

GLITTER_INCLUDE_GROUP("fx-gltf" "nlohmann/detail/meta"
	"cpp_future.hpp"
	"detected.hpp"
	"is_sax.hpp"
	"type_traits.hpp"
	"void_t.hpp"
)

GLITTER_INCLUDE_GROUP("fx-gltf" "nlohmann/detail/output"
	"binary_writer.hpp"
	"output_adapters.hpp"
	"serializer.hpp"
)

GLITTER_INCLUDE_GROUP("fx-gltf" "nlohmann/thirdparty/hedley"
	"hedley.hpp"
	"hedley_undef.hpp"
)

add_subdirectory(glitter)
