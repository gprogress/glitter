/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_MODELTEST_H
#define GLITTER_MODELTEST_H

#include <glitter/Types.h>

#include <cio/Path.h>
#include <cio/test/Suite.h>

namespace glitter
{
	class ModelTest : public cio::test::Suite
	{
		public:
			static void createSampleBuffers(Model &model);

			static void createSampleViews(Model &model);

			static void createSampleAccessors(Model &model);

			ModelTest();

			explicit ModelTest(cio::Path dataDir);

			virtual ~ModelTest() noexcept override;

			virtual const cio::Metaclass &getMetaclass() const noexcept override;

			virtual cio::Severity setUp() override;

			void testLoadFromFile();

			void testGetBuffer();

			void testGetBufferView();

			void testGetDataView();

			void testGetEntityData();

			void assertSampleModel(const Model &model) const;

			void assertDocumentContent(const fx::gltf::Document &document) const;

		private:
			static cio::Class<ModelTest> sMetaclass;
			
			void bindTestMethods();

			cio::Path mDataDir;
			std::string mSampleModelFile;
			std::string mSampleModelBinFile;
	};
}

#endif
