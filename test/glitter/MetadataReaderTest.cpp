/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "MetadataReaderTest.h"

#include "SampleMetadata.h"

#include <glitter/Collection.h>
#include <glitter/MetadataReader.h>
#include <glitter/Model.h>
#include <glitter/Provider.h>
#include <glitter/Table.h>

#include <cio/Primitive.h>
#include <cio/Protocol.h>
#include <cio/File.h>
#include <cio/Filesystem.h>
#include <cio/Class.h>
#include <cio/test/Assert.h>
#include <cio/Text.h>

#include <geas/Application.h>
#include <geas/Attribute.h>
#include <geas/json/OwtSchemaReader.h>

#include <nlohmann/json.hpp>

#include <fstream>
#include <iostream>
#include <set>

namespace glitter
{
	cio::Class<MetadataReaderTest> MetadataReaderTest::sMetaclass("glitter::MetadataReaderTest");

	MetadataReaderTest::MetadataReaderTest() :
		cio::test::Suite("glitter/MetadataReader")
	{
		this->bindTestMethods();
	}

	MetadataReaderTest::MetadataReaderTest(cio::Path dataDir) :
		cio::test::Suite("glitter/MetadataReader"),
		mDataDir(std::move(dataDir))
	{
		this->bindTestMethods();
	}

	MetadataReaderTest::~MetadataReaderTest() noexcept = default;

	void MetadataReaderTest::bindTestMethods()
	{
		this->addTestCase("loadFeatureTables", &MetadataReaderTest::testLoadFeatureTables, 0);
		this->addTestCase("loadFeatureMetadata", &MetadataReaderTest::testLoadFeatureMetadata, 1);
		this->addTestCase("loadFeatureMetadataFromFile", &MetadataReaderTest::testLoadFeatureMetadataFromFile, 2);
		this->addTestCase("loadFeatureMetadataFromText", &MetadataReaderTest::testLoadFeatureMetadataFromText, 2);
	}

	const cio::Metaclass &MetadataReaderTest::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	cio::Severity MetadataReaderTest::setUp()
	{
		cio::Filesystem fs;
		cio::Path dataDir = mDataDir;
		if (!dataDir)
		{
			cio::Path appDir = fs.getApplicationDirectory();
			dataDir = appDir / "../share/test/glitter";
		}

		cio::Path sampleClasses = dataDir / "SampleClasses.json";
		mSampleClassesFile = sampleClasses.toNativeFile();

		CIO_ASSERT_EQUAL(cio::Resource::File, fs.getNativeType(mSampleClassesFile.c_str()));
		return cio::Severity::None;
	}

	void MetadataReaderTest::testLoadFeatureTables()
	{
		std::ifstream stream(mSampleClassesFile.c_str());
		CIO_ASSERT(stream.is_open());
		nlohmann::json document;
		stream >> document;

		CIO_ASSERT_EQUAL(1u, document.count("featureTables"));

		Model model;
		SampleMetadata::generateMinimalSchema(*model.getOrCreateCatalog()->getApplicationSchema());
		MetadataReader reader;
		reader.loadFeatureTables(model, document["featureTables"]);
		this->assertFeatureTablesLoaded(*model.getEntityCollection());
	}

	void MetadataReaderTest::testLoadFeatureMetadata()
	{
		std::ifstream stream(mSampleClassesFile.c_str());
		CIO_ASSERT(stream.is_open());
		nlohmann::json document;
		stream >> document;

		CIO_ASSERT_EQUAL(1u, document.count("schema"));

		nlohmann::json schemaRoot = document["schema"];
		CIO_ASSERT_EQUAL(1u, schemaRoot.count("classes"));

		CIO_ASSERT_EQUAL(1u, document.count("featureTables"));

		Model model;
		MetadataReader reader;
		reader.loadFeatureMetadata(model, document, mSampleClassesFile);
		this->assertFeatureClassesLoaded(*model.getCatalog()->getApplicationSchema());
		this->assertFeatureTablesLoaded(*model.getEntityCollection());
	}

	void MetadataReaderTest::testLoadFeatureMetadataFromFile()
	{
		Model model;
		MetadataReader reader;

		reader.loadFeatureMetadataFromFile(model, mSampleClassesFile);
		this->assertFeatureClassesLoaded(*model.getCatalog()->getApplicationSchema());
		this->assertFeatureTablesLoaded(*model.getEntityCollection());
	}

	void MetadataReaderTest::testLoadFeatureMetadataFromText()
	{
		std::string text;
		cio::File file(mSampleClassesFile, cio::ModeSet::readonly());
		cio::Length length = file.size();
		text.resize(length.size());
		file.read(&text[0], text.size());

		Model model;
		MetadataReader reader;

		reader.loadFeatureMetadataFromText(model, text, mSampleClassesFile);
		this->assertFeatureClassesLoaded(*model.getCatalog()->getApplicationSchema());
		this->assertFeatureTablesLoaded(*model.getEntityCollection());
	}

	void MetadataReaderTest::assertFeatureClassesLoaded(const geas::Application &schema) const
	{
		// Traverse schema and validate it
		{
			// JSON parsing may not preserve order
			std::set<cio::Text> expected = { "car", "building", "elevation" };
			std::set<cio::Text> found;

			const geas::Dictionary<geas::Entity> &entities = schema.getEntities();
			CIO_ASSERT_EQUAL(3u, entities.size());
			for (const geas::Entity &entity : entities)
			{
				bool isExpected = expected.count(entity.getLabel()) > 0;
				bool isDuplicate = !found.insert(geas::Label(entity.getLabel())).second;

				if (!isExpected)
				{
					throw cio::test::Fault(static_cast<std::string>("Unexpected entity " + entity.getLabel()), CIO_CURRENT_SOURCE(), cio::Severity::Error);
				}

				if (isDuplicate)
				{
					throw cio::test::Fault(std::string("Duplicated entity " + entity.getLabel()), CIO_CURRENT_SOURCE(), cio::Severity::Error);
				}
			}

			// Now we've validated that proper entities are defined, let's check to see that each entity has the expected attributes
			// Test #1: car
			{
				const geas::Entity *entity = entities.get(geas::Label("car"));

				// Validate that it has a name and definition
				CIO_ASSERT_EQUAL(1u, entity->getDescriptions().size());
				{
					const geas::Description &desc = entity->getDescriptions().front();
					CIO_ASSERT_EQUAL("Car", desc.getName());
					CIO_ASSERT_EQUAL("Cars on the road", desc.getDefinition());
				}

				std::set<geas::Label> expectedAttrs = { "color", "driver", "passengers", "fuelLevel", "isElectric", "seating"};
				std::set<geas::Label> foundAttrs;
				CIO_ASSERT_EQUAL(6u, entity->getAttributes().size());
				for (const geas::Attribute *attribute : entity->getAttributes())
				{
					bool isExpected = expectedAttrs.count(attribute->getLabel()) > 0;
					bool isDuplicate = !foundAttrs.insert(geas::Label(attribute->getLabel())).second;

					if (!isExpected)
					{
						throw cio::test::Fault(static_cast<std::string>("Unexpected attribute on car:" + attribute->getLabel()), CIO_CURRENT_SOURCE(), cio::Severity::Error);
					}

					if (isDuplicate)
					{
						throw cio::test::Fault(static_cast<std::string>("Duplicated attribute on car:" + attribute->getLabel()), CIO_CURRENT_SOURCE(), cio::Severity::Error);
					}
				}

				// Attribute count and presence is validated, let's inspect details
				// Color attribute - string
				{
					const geas::Attribute *attr = entity->getAttributes().get(geas::Label("color"));
					CIO_ASSERT_EQUAL(1u, attr->getDescriptions().size());
					{
						const geas::Description &desc = attr->getDescriptions().front();
						CIO_ASSERT_EQUAL("Color", desc.getName());
						CIO_ASSERT_EQUAL("The body color of the car", desc.getDefinition());
					}

					CIO_ASSERT_EQUAL(cio::Type::Text, attr->getValueType());
					CIO_ASSERT_EQUAL(1u, attr->getRange().lower);
					CIO_ASSERT_EQUAL(1u, attr->getRange().upper);
				}

				// Driver attribute - string
				{
					const geas::Attribute *attr = entity->getAttributes().get(geas::Label("driver"));
					CIO_ASSERT_EQUAL(1u, attr->getDescriptions().size());
					{
						const geas::Description &desc = attr->getDescriptions().front();
						CIO_ASSERT_EQUAL("Driver Name", desc.getName());
						CIO_ASSERT_EQUAL("The driver of the car", desc.getDefinition());
					}

					CIO_ASSERT_EQUAL(cio::Type::Text, attr->getValueType());
					CIO_ASSERT_EQUAL(1u, attr->getRange().lower);
					CIO_ASSERT_EQUAL(1u, attr->getRange().upper);
				}

				// Passenger attribute - array of strings
				{
					const geas::Attribute *attr = entity->getAttributes().get(geas::Label("passengers"));
					CIO_ASSERT_EQUAL(1u, attr->getDescriptions().size());
					{
						const geas::Description &desc = attr->getDescriptions().front();
						CIO_ASSERT_EQUAL("Passenger Names", desc.getName());
						CIO_ASSERT_EQUAL("Other passengers in the car", desc.getDefinition());
					}

					CIO_ASSERT_EQUAL(cio::Type::Text, attr->getValueType());
					CIO_ASSERT_EQUAL(0u, attr->getRange().lower);
					CIO_ASSERT_EQUAL(UINT32_MAX, attr->getRange().upper);
				}

				// Fuel level - float32
				{
					const geas::Attribute *attr = entity->getAttributes().get(geas::Label("fuelLevel"));
					CIO_ASSERT_EQUAL(1u, attr->getDescriptions().size());
					{
						const geas::Description &desc = attr->getDescriptions().front();
						CIO_ASSERT_EQUAL("Fuel Level", desc.getName());
						CIO_ASSERT_EQUAL("Fuel level from 0.0 (empty) to 100.0 (full tank/battery)", desc.getDefinition());
					}

					CIO_ASSERT_EQUAL(cio::Type::Text, attr->getValueType());
					CIO_ASSERT_EQUAL(1u, attr->getRange().lower);
					CIO_ASSERT_EQUAL(1u, attr->getRange().upper);
				}
				
				// isElectric - Boolean
				{
					const geas::Attribute *attr = entity->getAttributes().get(geas::Label("isElectric"));
					CIO_ASSERT_EQUAL(1u, attr->getDescriptions().size());
					{
						const geas::Description &desc = attr->getDescriptions().front();
						CIO_ASSERT_EQUAL("Is Electric", desc.getName());
						CIO_ASSERT_EQUAL("True if car is a fully electric vehicle", desc.getDefinition());
					}

					CIO_ASSERT_EQUAL(cio::Type::Text, attr->getValueType());
					CIO_ASSERT_EQUAL(1u, attr->getRange().lower);
					CIO_ASSERT_EQUAL(1u, attr->getRange().upper);
				}
			}

			// Test #2: building
			{
				const geas::Entity *entity = entities.get(geas::Label("building"));

				// Validate that it has a name but not a definition
				CIO_ASSERT_EQUAL(1u, entity->getDescriptions().size());
				{
					const geas::Description &desc = entity->getDescriptions().front();
					CIO_ASSERT_EQUAL("Building", desc.getName());
					CIO_ASSERT_EQUAL("", desc.getDefinition());
				}

				std::set<geas::Label> expectedAttrs = { "height", "address", "coordinates", "occupants" };
				std::set<geas::Label> foundAttrs;
				CIO_ASSERT_EQUAL(4u, entity->getAttributes().size());
				for (const geas::Attribute *attribute : entity->getAttributes())
				{
					bool isExpected = expectedAttrs.count(attribute->getLabel()) > 0;
					bool isDuplicate = !foundAttrs.insert(geas::Label(attribute->getLabel())).second;

					if (!isExpected)
					{
						throw cio::test::Fault(static_cast<std::string>("Unexpected attribute on building:" + attribute->getLabel()), CIO_CURRENT_SOURCE(), cio::Severity::Error);
					}

					if (isDuplicate)
					{
						throw cio::test::Fault(static_cast<std::string>("Duplicated attribute on building:" + attribute->getLabel()), CIO_CURRENT_SOURCE(), cio::Severity::Error);
					}
				}

				// Validate height
				{
					const geas::Attribute *attr = entity->getAttributes().get(geas::Label("height"));
					CIO_ASSERT_EQUAL("height", attr->getLabel());
					CIO_ASSERT_EQUAL(1u, attr->getDescriptions().size());
					{
						const geas::Description &desc = attr->getDescriptions().front();
						CIO_ASSERT_EQUAL("Height (m)", desc.getName());
						CIO_ASSERT_EQUAL("", desc.getDefinition());
					}

					CIO_ASSERT_EQUAL(cio::Type::Real, attr->getValueType());
					CIO_ASSERT_EQUAL(1u, attr->getRange().lower);
					CIO_ASSERT_EQUAL(1u, attr->getRange().upper);
				}

				// Validate address
				{
					const geas::Attribute *attr = entity->getAttributes().get(geas::Label("address"));
					CIO_ASSERT_EQUAL(1u, attr->getDescriptions().size());
					{
						const geas::Description &desc = attr->getDescriptions().front();
						CIO_ASSERT_EQUAL("Street Address", desc.getName());
						CIO_ASSERT_EQUAL("", desc.getDefinition());
					}

					CIO_ASSERT_EQUAL(cio::Type::Text, attr->getValueType());
					CIO_ASSERT_EQUAL(1u, attr->getRange().lower);
					CIO_ASSERT_EQUAL(1u, attr->getRange().upper);
				}

				// Validate coordinates
				{
					const geas::Attribute *attr = entity->getAttributes().get(geas::Label("coordinates"));
					CIO_ASSERT_EQUAL(1u, attr->getDescriptions().size());
					{
						const geas::Description &desc = attr->getDescriptions().front();
						CIO_ASSERT_EQUAL("Longitude/Latitude (degrees)", desc.getName());
						CIO_ASSERT_EQUAL("Geographic Coordinates", desc.getDefinition());
					}

					CIO_ASSERT_EQUAL(cio::Type::Real, attr->getValueType());
					CIO_ASSERT_EQUAL(1u, attr->getRange().lower);
					CIO_ASSERT_EQUAL(1u, attr->getRange().upper);
				}

				// Validate number occupants
				{
					const geas::Attribute *attr = entity->getAttributes().get(geas::Label("occupants"));
					CIO_ASSERT_EQUAL(1u, attr->getDescriptions().size());
					{
						const geas::Description &desc = attr->getDescriptions().front();
						CIO_ASSERT_EQUAL("Number of Occupants", desc.getName());
						CIO_ASSERT_EQUAL("", desc.getDefinition());
					}

					CIO_ASSERT_EQUAL(cio::Type::Integer, attr->getValueType());
					CIO_ASSERT_EQUAL(1u, attr->getRange().lower);
					CIO_ASSERT_EQUAL(1u, attr->getRange().upper);
				}
			}

			// Test #2: elevation
			{
				const geas::Entity *entity = entities.get(geas::Label("elevation"));

				// Validate that it has a name but not a definition
				CIO_ASSERT_EQUAL(1u, entity->getDescriptions().size());
				{
					const geas::Description &desc = entity->getDescriptions().front();
					CIO_ASSERT_EQUAL("Elevation", desc.getName());
					CIO_ASSERT_EQUAL("", desc.getDefinition());
				}

				// Validate that it has two attributes
				CIO_ASSERT_EQUAL(2u, entity->getAttributes().size());
				{
					const geas::Attribute *attr = entity->getAttributes().get(0);
					CIO_ASSERT_EQUAL("elevation", attr->getLabel());
					CIO_ASSERT_EQUAL(1u, attr->getDescriptions().size());
					{
						const geas::Description &desc = attr->getDescriptions().front();
						CIO_ASSERT_EQUAL("Elevation (m)", desc.getName());
						CIO_ASSERT_EQUAL("Elevation of the terrain in the scene (per-texel)", desc.getDefinition());
					}

					CIO_ASSERT_EQUAL(cio::Type::Integer, attr->getValueType());
					CIO_ASSERT_EQUAL(1u, attr->getRange().lower);
					CIO_ASSERT_EQUAL(1u, attr->getRange().upper);
				}
			}
		}
	}

	void MetadataReaderTest::assertFeatureTablesLoaded(const Collection &collection) const
	{
		const geas::Dictionary<Node> &nodes = collection.getNodes();
		CIO_ASSERT_EQUAL(3u, nodes.size());

		// node #0: buildings
		{
			const Node *node = nodes.get(geas::Label("buildings"));
			CIO_ASSERT(node);
			CIO_ASSERT_EQUAL("buildings", node->getLabel());
			CIO_ASSERT_EQUAL(1, node->getIndex());

			std::shared_ptr<const Provider> entityProvider = node->getProvider();
			CIO_ASSERT(entityProvider);

			const geas::Entity *entityType = entityProvider->getEntityType(0);
			CIO_ASSERT(entityType);
			CIO_ASSERT_EQUAL("building", entityType->getLabel());

			CIO_ASSERT_EQUAL(8u, entityProvider->size());

			CIO_ASSERT_EQUAL(8u, entityProvider->size());
			CIO_ASSERT_EQUAL(4u, entityProvider->getAttributeCount(0));

			// Validate "height" definition
			{
				std::uint32_t attrId = entityProvider->findAttributeByLabel(0, geas::Label("height"));
				const geas::Attribute defn = *entityProvider->getAttribute(0, attrId);
				CIO_ASSERT_EQUAL("height", defn.getLabel());

				CIO_ASSERT_EQUAL(cio::Type::Real, defn.getValueType());
				CIO_ASSERT_EQUAL(1u, defn.getRange().lower);
				CIO_ASSERT_EQUAL(1u, defn.getRange().upper);
			}

			// Validate "address" definition
			{
				std::uint32_t attrId = entityProvider->findAttributeByLabel(0, geas::Label("address"));
				const geas::Attribute defn = *entityProvider->getAttribute(0, attrId);
				CIO_ASSERT_EQUAL("address", defn.getLabel());

				CIO_ASSERT_EQUAL(cio::Type::Text, defn.getValueType());
				CIO_ASSERT_EQUAL(1u, defn.getRange().lower);
				CIO_ASSERT_EQUAL(1u, defn.getRange().upper);

				// TODO offset table is not yet being represented
			}

			// Validate "coordinates" definition
			{
				std::uint32_t attrId = entityProvider->findAttributeByLabel(0, geas::Label("coordinates"));
				const geas::Attribute defn = *entityProvider->getAttribute(0, attrId);
				CIO_ASSERT_EQUAL("coordinates", defn.getLabel());

				CIO_ASSERT_EQUAL(cio::Type::Real, defn.getValueType());
				CIO_ASSERT_EQUAL(1u, defn.getRange().lower);
				CIO_ASSERT_EQUAL(1u, defn.getRange().upper);
			}

			// Validate "occupants" definition
			{
				std::uint32_t attrId = entityProvider->findAttributeByLabel(0, geas::Label("occupants"));
				const geas::Attribute defn = *entityProvider->getAttribute(0, attrId);
				CIO_ASSERT_EQUAL("occupants", defn.getLabel());

				CIO_ASSERT_EQUAL(cio::Type::Integer, defn.getValueType());
				CIO_ASSERT_EQUAL(1u, defn.getRange().lower);
				CIO_ASSERT_EQUAL(1u, defn.getRange().upper);
			}
		}

		// node #1: Cars
		{
			const Node *node = nodes.get(geas::Label("cars"));
			CIO_ASSERT(node);
			CIO_ASSERT_EQUAL("cars", node->getLabel());
			CIO_ASSERT_EQUAL(2, node->getIndex());

			std::shared_ptr<const Provider> entityProvider = node->getProvider();
			CIO_ASSERT(entityProvider);

			const geas::Entity *entityType = entityProvider->getEntityType(1);
			CIO_ASSERT(entityType);
			CIO_ASSERT_EQUAL("car", entityType->getLabel());

			CIO_ASSERT_EQUAL(3u, entityProvider->size());

			CIO_ASSERT_EQUAL(3u, entityProvider->size());
			CIO_ASSERT_EQUAL(2u, entityProvider->getAttributeCount(0));

			// Validate "passengers" definition
			{
				std::uint32_t attrId = entityProvider->findAttributeByLabel(0, geas::Label("passengers"));
				const geas::Attribute defn = *entityProvider->getAttribute(0, attrId);
				CIO_ASSERT_EQUAL("passengers", defn.getLabel());

				CIO_ASSERT_EQUAL(cio::Type::Text, defn.getValueType());
				CIO_ASSERT_EQUAL(0u, defn.getRange().lower);
				CIO_ASSERT_EQUAL(UINT32_MAX, defn.getRange().upper);


				// TODO offset tables are not yet being represented
			}

			// Validate "isElectric" definition
			{
				std::uint32_t attrId = entityProvider->findAttributeByLabel(0, geas::Label("isElectric"));
				const geas::Attribute defn = *entityProvider->getAttribute(0, attrId);
				CIO_ASSERT_EQUAL("isElectric", defn.getLabel());

				CIO_ASSERT_EQUAL(cio::Type::Boolean, defn.getValueType());
				CIO_ASSERT_EQUAL(1u, defn.getRange().lower);
				CIO_ASSERT_EQUAL(1u, defn.getRange().upper);
			}
		}

		// node #2: Elevations
		{
			const Node *node = nodes.get(geas::Label("elevations"));
			CIO_ASSERT(node);
			CIO_ASSERT_EQUAL("elevations", node->getLabel());
			CIO_ASSERT_EQUAL(3, node->getIndex());

			std::shared_ptr<const Provider> entityProvider = node->getProvider();
			CIO_ASSERT(entityProvider);

			const geas::Entity *entityType = entityProvider->getEntityType(0);
			CIO_ASSERT(entityType);
			CIO_ASSERT_EQUAL("elevation", entityType->getLabel());

			CIO_ASSERT_EQUAL(12u, entityProvider->size());

			CIO_ASSERT_EQUAL(12u, entityProvider->size());
			CIO_ASSERT_EQUAL(1u, entityProvider->getAttributeCount(0));

			// Validate "elevations" definition
			{
				std::uint32_t attrId = entityProvider->findAttributeByLabel(0, geas::Label("elevation"));
				const geas::Attribute defn = *entityProvider->getAttribute(0, attrId);
				CIO_ASSERT_EQUAL("elevation", defn.getLabel());

				CIO_ASSERT_EQUAL(cio::Type::Integer, defn.getValueType());
				CIO_ASSERT_EQUAL(1u, defn.getRange().lower);
				CIO_ASSERT_EQUAL(1u, defn.getRange().upper);
			}
		}
	}
}
