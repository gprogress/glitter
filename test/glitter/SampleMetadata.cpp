/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "SampleMetadata.h"

#include <geas/Application.h>
#include <geas/Attribute.h>
#include <geas/Entity.h>

#include <cio/Buffer.h>
#include <cio/Output.h>
#include <cio/Primitive.h>
#include <cio/ProtocolFactory.h>

namespace glitter
{
	const std::uint32_t SampleMetadata::Vertex::count = 12;

	const std::uint32_t SampleMetadata::Vertex::indices[12] = {
		0, 1, 4,
		1, 2, 4,
		2, 3, 4,
		3, 0, 4
	};

	const std::uint32_t SampleMetadata::VertexAttribute::count = 5;

	const float SampleMetadata::VertexAttribute::positions[5][3] = {
		{ 0.0f, 0.0f, 0.0f},		
		{ 100.0f, 0.0f, 0.0f },		
		{ 100.0f, 100.0f, 0.0f },
		{ 0.0f, 100.0f, 0.0f },
		{ 40.5f, 64.5f, 25.0f }
	};

	const float SampleMetadata::VertexAttribute::normals[5][3] = {
		{ -0.577f, -0.577f, 0.577f },	
		{ 0.577f, -0.577f, 0.577f},
		{ 0.577f, 0.577f, 0.577f},
		{ -0.577f, 0.577f, 0.577f},		
		{0.0f, 0.0f, 1.0f}
	};

	const std::uint8_t SampleMetadata::VertexAttribute::fids[5] = { 7, 2, 0, 1, 2 };

	const std::uint32_t SampleMetadata::Buildings::count = 8;
	const double SampleMetadata::Buildings::heights[8] = { 5.5, 10.0, 20.0, 15.0, 2.5, 5.0, 17.5, 1024.0 };
	const char *SampleMetadata::Buildings::addresses[8] = { "50 Foo Rd", "20 Bar Ave", "10 Foo Rd", "Circle Ln", "Lollygag Cir", "2000 N Text Pool", "2000 S Text Pool", "End of the Line" };
	const double SampleMetadata::Buildings::coordinates[8][2] = { { 4.5, 3.5 }, { 0.0, 0.0 }, { 75.25, 68.75 }, { 32.5, 28.0 }, { 65.0, 25.0 }, { -20.5, -1.0 }, {0.0, 0.0 }, { -5.0, 5.0 } };
	const std::uint32_t SampleMetadata::Buildings::occupants[8] = { 49213u, 0u, 1u, 12389712u, 45u, 100u, 1024u, 0u };

	const std::uint32_t SampleMetadata::Cars::count = 3;
	const char *SampleMetadata::Cars::passengers[] = { "me", "you", "everyone", "someone" };
	const std::uint32_t SampleMetadata::Cars::passengerCounts[3] = { 3, 0, 1 };
	const float SampleMetadata::Cars::seating[] = { 2.0f, 2.5f, 3.0f, 2.0f, 3.0f, 2.0f };
	const std::uint32_t SampleMetadata::Cars::seatingCounts[] = { 3, 2, 1 };

	const bool SampleMetadata::Cars::isElectric[3] = { true, true, false };

	const std::uint32_t SampleMetadata::Elevations::count = 12;
	const std::uint8_t SampleMetadata::Elevations::elevations[12] = { 255, 127, 96, 48, 0, 1, 50, 200, 9, 255, 99, 101 };

	const char* SampleMetadata::Elevations::name[2] = { "water", "tree" };
	const std::uint8_t SampleMetadata::Elevations::value[2] = { 225, 127 };

	std::vector<cio::SizeInterval> SampleMetadata::generateBufferRanges()
	{
		std::vector<cio::SizeInterval> ranges;

		ranges.emplace_back(0u, aligned(size(Vertex::indices)));
		ranges.emplace_back(ranges.back().upper, ranges.back().upper + aligned(size(VertexAttribute::positions)));
		ranges.emplace_back(ranges.back().upper, ranges.back().upper + aligned(size(VertexAttribute::normals)));
		ranges.emplace_back(ranges.back().upper, ranges.back().upper + aligned(size(VertexAttribute::fids)));

		ranges.emplace_back(ranges.back().upper, ranges.back().upper + aligned(size(Buildings::heights)));
		
		// Building address
		{
			std::size_t alength = 0;
			std::size_t aolength = sizeof(std::uint32_t) * count(Buildings::addresses);
			for (const char *a : Buildings::addresses)
			{
				alength += std::strlen(a);
			}

			ranges.emplace_back(ranges.back().upper, ranges.back().upper + aligned(alength));
			ranges.emplace_back(ranges.back().upper, ranges.back().upper + aligned(aolength));
		}

		ranges.emplace_back(ranges.back().upper, ranges.back().upper + aligned(size(Buildings::coordinates)));
		ranges.emplace_back(ranges.back().upper, ranges.back().upper + aligned(size(Buildings::occupants)));
		
		// Car passengers
		{
			std::size_t alength = 0;
			std::size_t aolength = sizeof(std::uint32_t) * count(Cars::passengers);
			std::size_t aoolength = sizeof(std::uint32_t) * count(Cars::passengerCounts);

			for (const char *a : Cars::passengers)
			{
				alength += std::strlen(a);
			}

			ranges.emplace_back(ranges.back().upper, ranges.back().upper + aligned(alength));
			ranges.emplace_back(ranges.back().upper, ranges.back().upper + aligned(aoolength));
			ranges.emplace_back(ranges.back().upper, ranges.back().upper + aligned(aolength));
		}

		// Car isElectric - note alignment to 4 bytes
		{
			ranges.emplace_back(ranges.back().upper, ranges.back().upper + aligned(size(Cars::isElectric)));
		}

		ranges.emplace_back(ranges.back().upper, ranges.back().upper + aligned(size(Elevations::elevations)));

		return ranges;
	}

	void SampleMetadata::generateBinFile(const cio::Path &path)
	{
		SampleMetadata::generateBinFile(path, *cio::ProtocolFactory::getDefault());
	}

	void SampleMetadata::generateBinFile(const cio::Path &path, cio::ProtocolFactory &protocols)
	{
		std::unique_ptr<cio::Output> output = protocols.openOutput(path);
		if (!output)
		{
			throw std::runtime_error("Failed to create output for " + path.get());
		}

		SampleMetadata::generateBinFile(*output);
	}

	void SampleMetadata::generateBinFile(cio::Output &output)
	{
		// Buffer 0: all data for gltf Model
		cio::Buffer buffer(4096);
		SampleMetadata::generateBin(buffer);
		output.drain(buffer);
	}

	void SampleMetadata::generateBin(cio::Buffer &buffer)
	{
		std::uint8_t zero[8] = { 0 };

		for (std::uint32_t i : Vertex::indices)
		{
			buffer.putLittle(i);
		}
		buffer.write(zero, padding(size(Vertex::indices)));

		for (const float (&v)[3] : VertexAttribute::positions)
		{
			for (float i : v)
			{
				buffer.putLittle(i);
			}
		}
		buffer.write(zero, padding(size(VertexAttribute::positions)));

		for (const float (&v)[3] : VertexAttribute::normals)
		{
			for (float i : v)
			{
				buffer.putLittle(i);
			}
		}
		buffer.write(zero, padding(size(VertexAttribute::normals)));

		buffer.write(VertexAttribute::fids, sizeof(VertexAttribute::fids));
		buffer.write(zero, padding(size(VertexAttribute::fids)));

		for (double h : Buildings::heights)
		{
			buffer.putLittle(h);
		}
		buffer.write(zero, padding(size(Buildings::heights)));

		// address array
		{
			std::size_t length = 0;
			std::uint32_t lengths[8];
			for (std::size_t n = 0; n < 8; ++n)
			{
				const char *a = Buildings::addresses[n];
				std::size_t len = buffer.putText(a);
				length += len;
				lengths[n] = static_cast<std::uint32_t>(len);
			}
			buffer.write(zero, padding(length));

			std::uint32_t offset = 0;
			for (std::uint32_t len : lengths)
			{
				buffer.putLittle(offset);
				offset += len;
			}
			buffer.write(zero, padding(size(lengths)));
		}

		for (const double (&v)[2] : Buildings::coordinates)
		{
			for (double i : v)
			{
				buffer.putLittle(i);
			}
		}
		buffer.write(zero, padding(size(Buildings::coordinates)));

		for (std::uint32_t i : Buildings::occupants)
		{
			buffer.putLittle(i);
		}
		buffer.write(zero, padding(size(Buildings::occupants)));

		// passengers array of array
		{
			// First write full list of text values
			std::size_t totalLength = 0;
			std::uint32_t lengths[sizeof(Cars::passengers) / sizeof(char *)];
			for (std::size_t n = 0; n < count(Cars::passengers); ++n)
			{
				const char *a = Cars::passengers[n];
				std::size_t len = buffer.putText(a);
				totalLength += len;
				lengths[n] = static_cast<std::uint32_t>(len);
			}
			buffer.write(zero, padding(totalLength));

			// Now we write relative offsets of each logical item into offset buffer
			std::uint32_t nextOffset = 0;
			std::size_t offsetComponentCount = 0;
			for (std::size_t off = 0; off < count(Cars::passengerCounts); ++off)
			{
				buffer.putLittle(nextOffset);
				nextOffset += 4 * Cars::passengerCounts[off];
				offsetComponentCount += 4;
			}
			buffer.write(zero, padding(offsetComponentCount));

			// Finally we write the string offsets into the last offset buffer
			std::uint32_t offset = 0;
			for (std::uint32_t len : lengths)
			{
				buffer.putLittle(offset);
				offset += len;
			}
			buffer.write(zero, padding(size(lengths)));
		}
		
		// isElectric bit packing
		{
			std::uint8_t packed = 0;
			std::size_t length = 0;
			for (std::size_t n = 0; n < count(Cars::isElectric); ++n)
			{
				std::uint8_t bit = static_cast<std::uint8_t>(Cars::isElectric[n]) << (7 - (n % 8));
				packed |= bit;

				if (n % 8 == 7)
				{ 
					buffer.putLittle(packed);
					++length;
				}
			}
			
			if (sizeof(Cars::isElectric) % 8 != 0)
			{
				buffer.putLittle(packed);
				++length;
			}
			buffer.write(zero, padding(length));
		}

		// Elevations - straight write
		buffer.write(Elevations::elevations, sizeof(Elevations::elevations));
		buffer.write(zero, padding(size(Elevations::elevations)));

		buffer.flip();
	}

	void SampleMetadata::generateMinimalSchema(geas::Application &schema)
	{
		geas::Dictionary<geas::Entity> &entities = schema.getEntities();
		geas::Dictionary<geas::Attribute> &datatypes = schema.getAttributes();

		geas::Entity *building = entities.create(geas::Label("building"));
		geas::Entity *car = entities.create(geas::Label("car"));
		geas::Entity *elevation = entities.create(geas::Label("elevation"));

		geas::Attribute *color = datatypes.create(geas::Label("color"));
		car->addAttribute(color);
		color->setValueType(cio::Type::Text);

		geas::Attribute *driver = datatypes.create(geas::Label("driver"));
		driver->setValueType(cio::Type::Text);
		car->addAttribute(driver);

		geas::Attribute *passengers = datatypes.create(geas::Label("passengers"));
		passengers->setValueType(cio::Type::Text);
		car->addAttribute(passengers);

		geas::Attribute *fuelLevel = datatypes.create(geas::Label("fuelLevel"));
		fuelLevel->setValueType(cio::Type::Real);
		car->addAttribute(fuelLevel);

		geas::Attribute *electric = datatypes.create(geas::Label("isElectric"));
		electric->setValueType(cio::Type::Boolean);
		car->addAttribute(electric);

		geas::Attribute* seating = datatypes.create(geas::Label("seating"));
		car->addAttribute(seating);
		seating->setValueType(cio::Type::Real);

		geas::Attribute *height = datatypes.create(geas::Label("height"));
		height->setValueType(cio::Type::Real);
		building->addAttribute(height);

		geas::Attribute *address = datatypes.create(geas::Label("address"));
		address->setValueType(cio::Type::Text);
		building->addAttribute(address);

		geas::Attribute *coordinates = datatypes.create(geas::Label("coordinates"));
		coordinates->setValueType(cio::Type::Real);
		building->addAttribute(coordinates);

		geas::Attribute *occupants = datatypes.create(geas::Label("occupants"));
		occupants->setValueType(cio::Type::Integer);
		building->addAttribute(occupants);

		geas::Attribute *elevationv = datatypes.create(geas::Label("elevation"));
		elevationv->setValueType(cio::Type::Integer);
		elevation->addAttribute(elevationv);
	}
}
