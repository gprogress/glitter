/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "ArrayTest.h"

#include <cio/Buffer.h>
#include <cio/Primitive.h>
#include <glitter/Array.h>

namespace glitter
{
	ArrayTest::ArrayTest() :
		cio::test::Suite("glitter::Array")
	{
		this->bindTestMethods();
	}

	ArrayTest::~ArrayTest() noexcept = default;

	template <>
	void ArrayTest::testBind<void>()
	{
		cio::Encoding expectedLayout = cio::Encoding::create<std::uint8_t>();
		cio::Type expectedType = cio::Type::Blob;

		// Bind a single value
		{
			Array actual;
			std::uint8_t value = 0;
			actual.bind(static_cast<void *>(&value), 1);
			CIO_ASSERT_EQUAL(1u, actual.size());
			CIO_ASSERT_EQUAL(1u, actual.getElementCount());
			CIO_ASSERT_EQUAL(1u, actual.getStride());
			CIO_ASSERT_EQUAL(1u, actual.getElementStride());
			CIO_ASSERT_EQUAL(0u, actual.getInitialOffset());
			CIO_ASSERT_EQUAL(expectedLayout, actual.getEncoding());
			CIO_ASSERT_EQUAL(expectedType, actual.getType());
			CIO_ASSERT_EQUAL(static_cast<void *>(&value), actual.getBuffer().data());
		}

		// Bind a pointer and length
		{
			Array actual;
			float value[5];
			actual.bind(static_cast<void *>(value + 1), 16);
			CIO_ASSERT_EQUAL(16u, actual.size());
			CIO_ASSERT_EQUAL(1u, actual.getElementCount());
			CIO_ASSERT_EQUAL(1, actual.getStride());
			CIO_ASSERT_EQUAL(1, actual.getElementStride());
			CIO_ASSERT_EQUAL(0u, actual.getInitialOffset());
			CIO_ASSERT_EQUAL(expectedLayout, actual.getEncoding());
			CIO_ASSERT_EQUAL(expectedType, actual.getType());
			CIO_ASSERT_EQUAL(static_cast<void *>(value + 1), actual.getBuffer().data());
		}

		// Bind a pointer and length with multiple elements
		{
			Array actual;
			std::uint64_t value[10];
			actual.bind(static_cast<void *>(value + 2), 16, 4);
			CIO_ASSERT_EQUAL(16u, actual.size());
			CIO_ASSERT_EQUAL(4u, actual.getElementCount());
			CIO_ASSERT_EQUAL(4u, actual.getStride());
			CIO_ASSERT_EQUAL(1u, actual.getElementStride());
			CIO_ASSERT_EQUAL(0u, actual.getInitialOffset());
			CIO_ASSERT_EQUAL(expectedLayout, actual.getEncoding());
			CIO_ASSERT_EQUAL(expectedType, actual.getType());
			CIO_ASSERT_EQUAL(static_cast<void *>(value + 2), actual.getBuffer().data());
		}
	}

	template <typename T>
	void ArrayTest::testBind()
	{
		cio::Encoding expectedLayout = cio::Encoding::create<T>();
		cio::Type expectedType = cio::primitive<T>();

		// Bind a single value
		{
			Array actual;
			T value = T();
			actual.bind(&value, 1);
			CIO_ASSERT_EQUAL(1u, actual.size());
			CIO_ASSERT_EQUAL(1u, actual.getElementCount());
			CIO_ASSERT_EQUAL(sizeof(T), actual.getStride());
			CIO_ASSERT_EQUAL(sizeof(T), actual.getElementStride());
			CIO_ASSERT_EQUAL(0u, actual.getInitialOffset());
			CIO_ASSERT_EQUAL(expectedLayout, actual.getEncoding());
			CIO_ASSERT_EQUAL(expectedType, actual.getType());
			CIO_ASSERT_EQUAL(&value, actual.getBuffer().data());
		}

		// Bind a fixed size array
		{
			Array actual;
			T value[7];
			actual.bind(value);
			CIO_ASSERT_EQUAL(7u, actual.size());
			CIO_ASSERT_EQUAL(1u, actual.getElementCount());
			CIO_ASSERT_EQUAL(sizeof(T), actual.getStride());
			CIO_ASSERT_EQUAL(sizeof(T), actual.getElementStride());
			CIO_ASSERT_EQUAL(0u, actual.getInitialOffset());
			CIO_ASSERT_EQUAL(expectedLayout, actual.getEncoding());
			CIO_ASSERT_EQUAL(expectedType, actual.getType());
			CIO_ASSERT_EQUAL(value + 0u, actual.getBuffer().data());
		}

		// Bind a pointer and length
		{
			Array actual;
			T value[5];
			actual.bind(value + 1, 4);
			CIO_ASSERT_EQUAL(4u, actual.size());
			CIO_ASSERT_EQUAL(1u, actual.getElementCount());
			CIO_ASSERT_EQUAL(sizeof(T), actual.getStride());
			CIO_ASSERT_EQUAL(sizeof(T), actual.getElementStride());
			CIO_ASSERT_EQUAL(0u, actual.getInitialOffset());
			CIO_ASSERT_EQUAL(expectedLayout, actual.getEncoding());
			CIO_ASSERT_EQUAL(expectedType, actual.getType());
			CIO_ASSERT_EQUAL(value + 1u, actual.getBuffer().data());
		}

		// Bind fixed array with multiple elements
		{
			Array actual;
			T value[21];
			actual.bind(value, 3, 7);
			CIO_ASSERT_EQUAL(3u, actual.size());
			CIO_ASSERT_EQUAL(7u, actual.getElementCount());
			CIO_ASSERT_EQUAL(sizeof(T) * 7u, actual.getStride());
			CIO_ASSERT_EQUAL(sizeof(T), actual.getElementStride());
			CIO_ASSERT_EQUAL(0u, actual.getInitialOffset());
			CIO_ASSERT_EQUAL(expectedLayout, actual.getEncoding());
			CIO_ASSERT_EQUAL(expectedType, actual.getType());
			CIO_ASSERT_EQUAL(value + 0u, actual.getBuffer().data());
		}

		// Bind a pointer and length with multiple elements
		{
			Array actual;
			T value[17];
			actual.bind(value + 3, 7, 2);
			CIO_ASSERT_EQUAL(7u, actual.size());
			CIO_ASSERT_EQUAL(2u, actual.getElementCount());
			CIO_ASSERT_EQUAL(sizeof(T) * 2u, actual.getStride());
			CIO_ASSERT_EQUAL(sizeof(T), actual.getElementStride());
			CIO_ASSERT_EQUAL(0u, actual.getInitialOffset());
			CIO_ASSERT_EQUAL(expectedLayout, actual.getEncoding());
			CIO_ASSERT_EQUAL(expectedType, actual.getType());
			CIO_ASSERT_EQUAL(value + 3u, actual.getBuffer().data());
		}
	}

	void ArrayTest::testGetBinary()
	{
		// Test with small std::uint8_t inputs
		{
			std::uint8_t byte = 0xF0;
			Array varray;

			varray.bind(&byte, 1);

			CIO_ASSERT_EQUAL(0xF0u, varray.getBinary<std::uint8_t>(0));
			CIO_ASSERT_EQUAL(0xF0u, varray.getBinary<std::uint16_t>(0));
			CIO_ASSERT_EQUAL(0xF0u, varray.getBinary<std::uint32_t>(0));
			CIO_ASSERT_EQUAL(0xF0u, varray.getBinary<std::uint64_t>(0));
			CIO_ASSERT_EQUAL(-16, varray.getBinary<std::int8_t>(0));
			CIO_ASSERT_EQUAL(0xF0, varray.getBinary<std::int16_t>(0)); // not sign extended
			CIO_ASSERT_EQUAL(0xF0, varray.getBinary<std::int32_t>(0));
			CIO_ASSERT_EQUAL(0xF0, varray.getBinary<std::int64_t>(0));

		}
	}

	void ArrayTest::testSetBinary()
	{
		// Test some unsigned integer fun times
		{
			std::uint16_t input[5] = { 0xDEADu, 0xBEEFu, 0u, 0xFFFFu, 1u };
			Array varray;
			varray.bind(input);

			// Test writing in native format
			varray.setBinary<std::uint16_t>(0xF0F0, 0);
			CIO_ASSERT_EQUAL(0xF0F0u, input[0]);
			CIO_ASSERT_EQUAL(0xBEEFu, input[1]);
			CIO_ASSERT_EQUAL(0u, input[2]);
			CIO_ASSERT_EQUAL(0xFFFFu, input[3]);
			CIO_ASSERT_EQUAL(1u, input[4]);

			// Test writing a smaller integer
			varray.setBinary<std::uint8_t>(0x5A, 1);
			CIO_ASSERT_EQUAL(0xF0F0u, input[0]);
			CIO_ASSERT_EQUAL(0x005Au, input[1]);
			CIO_ASSERT_EQUAL(0u, input[2]);
			CIO_ASSERT_EQUAL(0xFFFFu, input[3]);
			CIO_ASSERT_EQUAL(1u, input[4]);

			// Test writing a larger integer
			varray.setBinary<std::uint32_t>(0xEADABADA, 2);
			CIO_ASSERT_EQUAL(0xF0F0u, input[0]);
			CIO_ASSERT_EQUAL(0x005Au, input[1]);
			CIO_ASSERT_EQUAL(0xBADAu, input[2]);
			CIO_ASSERT_EQUAL(0xFFFFu, input[3]);
			CIO_ASSERT_EQUAL(1u, input[4]);

			// Testing writing a signed integer
			varray.setBinary<std::int16_t>(-2, 3);
			CIO_ASSERT_EQUAL(0xF0F0u, input[0]);
			CIO_ASSERT_EQUAL(0x005Au, input[1]);
			CIO_ASSERT_EQUAL(0xBADAu, input[2]);
			CIO_ASSERT_EQUAL(0xFFFEu, input[3]);
			CIO_ASSERT_EQUAL(1u, input[4]);

			// Test writing the largest integer on the last element
			varray.setBinary<std::uint64_t>(0xFEEFABBA01103223ull, 4);
			CIO_ASSERT_EQUAL(0xF0F0u, input[0]);
			CIO_ASSERT_EQUAL(0x005Au, input[1]);
			CIO_ASSERT_EQUAL(0xBADAu, input[2]);
			CIO_ASSERT_EQUAL(0xFFFEu, input[3]);
			CIO_ASSERT_EQUAL(0x3223u, input[4]);

			// Go back and write a smaller signed integer. The underlying type is unsigned so this should not sign extend.
			varray.setBinary<std::int8_t>(-4, 0);
			CIO_ASSERT_EQUAL(0x00FCu, input[0]);
			CIO_ASSERT_EQUAL(0x005Au, input[1]);
			CIO_ASSERT_EQUAL(0xBADAu, input[2]);
			CIO_ASSERT_EQUAL(0xFFFEu, input[3]);
			CIO_ASSERT_EQUAL(0x3223u, input[4]);
		}

		// Test float
		{
			float value[3] = { 5.5, -1.25, 625.375 };

			Array varray;
			varray.bind(value);

			varray.setBinary<float>(-2.25, 0);
			CIO_ASSERT_EQUAL(-2.25, value[0]);
			CIO_ASSERT_EQUAL(-1.25, value[1]);
			CIO_ASSERT_EQUAL(625.375, value[2]);
		}

		// Test unaligned case with offset and stride
		{
			cio::Buffer buffer(21);
			buffer.put(std::uint32_t(0)); // Skip first five bytes
			buffer.put(std::uint8_t(0));
			buffer.put(std::uint32_t(40)); // first value
			buffer.put(std::uint16_t(1)); // padding
			buffer.put(std::uint32_t(0xDEADBEEF)); // second value
			buffer.put(std::uint16_t(2)); // padding
			buffer.put(std::uint32_t(0xABBA3223)); // third value
			buffer.flip();
			CIO_ASSERT_EQUAL(21u, buffer.limit()); // Verify we set up the buffer properly

			Array varray;
			varray.bind(buffer.current(), buffer.available());
			varray.resize(3);
			varray.setInitialOffset(5);
			varray.setStride(6);
			varray.setEncoding(cio::Encoding::create<std::uint32_t>());
			varray.setType(cio::Type::Integer);

			varray.setBinary<std::uint32_t>(512u, 0);
			CIO_ASSERT_EQUAL(0u, buffer.get<std::uint32_t>());
			CIO_ASSERT_EQUAL(0u, buffer.get<std::uint8_t>());
			CIO_ASSERT_EQUAL(512u, buffer.get<std::uint32_t>());
			CIO_ASSERT_EQUAL(1u, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(0xDEADBEEF, buffer.get<std::uint32_t>());
			CIO_ASSERT_EQUAL(2u, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(0xABBA3223, buffer.get<std::uint32_t>());
			CIO_ASSERT_EQUAL(buffer.size(), buffer.position());
			buffer.flip();

			varray.setBinary<std::uint16_t>(0xFA4Bu, 1);
			CIO_ASSERT_EQUAL(0u, buffer.get<std::uint32_t>());
			CIO_ASSERT_EQUAL(0u, buffer.get<std::uint8_t>());
			CIO_ASSERT_EQUAL(512u, buffer.get<std::uint32_t>());
			CIO_ASSERT_EQUAL(1u, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(0xFA4B, buffer.get<std::uint32_t>());
			CIO_ASSERT_EQUAL(2u, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(0xABBA3223, buffer.get<std::uint32_t>());
			CIO_ASSERT_EQUAL(buffer.size(), buffer.position());
			buffer.flip();

			varray.setBinary<std::uint64_t>(0x122345567889ABBCull, 2);
			CIO_ASSERT_EQUAL(0u, buffer.get<std::uint32_t>());
			CIO_ASSERT_EQUAL(0u, buffer.get<std::uint8_t>());
			CIO_ASSERT_EQUAL(512u, buffer.get<std::uint32_t>());
			CIO_ASSERT_EQUAL(1u, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(0xFA4B, buffer.get<std::uint32_t>());
			CIO_ASSERT_EQUAL(2u, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(0x7889ABBCu, buffer.get<std::uint32_t>());
			CIO_ASSERT_EQUAL(buffer.size(), buffer.position());
			buffer.flip();
		}

		// Test case with multiple elements
		{
			// Do it with dynamic memory so bad writes are likely to be caught by memory checkers
			const std::int16_t input[6] = { std::int16_t(0xFEAF), 0, 1, 0x3654, 0x1234, 2 };
			cio::Chunk<std::int16_t> values(6);
			std::memcpy(values.data(), input, sizeof(input));

			Array varray;
			varray.bind(values.data(), 2u, 3u);
			varray.setBinary<std::int16_t>(-1, 0, 0);
			CIO_ASSERT_EQUAL(-1, values[0]);
			CIO_ASSERT_EQUAL(0, values[1]);
			CIO_ASSERT_EQUAL(1, values[2]);
			CIO_ASSERT_EQUAL(0x3654, values[3]);
			CIO_ASSERT_EQUAL(0x1234, values[4]);
			CIO_ASSERT_EQUAL(2, values[5]);

			varray.setBinary<std::int16_t>(-2, 1, 0);
			CIO_ASSERT_EQUAL(-1, values[0]);
			CIO_ASSERT_EQUAL(0, values[1]);
			CIO_ASSERT_EQUAL(1, values[2]);
			CIO_ASSERT_EQUAL(-2, values[3]);
			CIO_ASSERT_EQUAL(0x1234, values[4]);
			CIO_ASSERT_EQUAL(2, values[5]);

			varray.setBinary<std::int16_t>(-3, 0, 1);
			CIO_ASSERT_EQUAL(-1, values[0]);
			CIO_ASSERT_EQUAL(-3, values[1]);
			CIO_ASSERT_EQUAL(1, values[2]);
			CIO_ASSERT_EQUAL(-2, values[3]);
			CIO_ASSERT_EQUAL(0x1234, values[4]);
			CIO_ASSERT_EQUAL(2, values[5]);

			varray.setBinary<std::int16_t>(-4, 1, 2);
			CIO_ASSERT_EQUAL(-1, values[0]);
			CIO_ASSERT_EQUAL(-3, values[1]);
			CIO_ASSERT_EQUAL(1, values[2]);
			CIO_ASSERT_EQUAL(-2, values[3]);
			CIO_ASSERT_EQUAL(0x1234, values[4]);
			CIO_ASSERT_EQUAL(-4, values[5]);
		}

		// Test with multiple elements with offset, and element and total stride
		{
			cio::Buffer buffer(16);
			buffer.put(std::uint16_t(0)); // Skip first three bytes
			buffer.put(std::uint8_t(0));
			buffer.put(std::int16_t(40)); // first value, first element
			buffer.put(std::uint16_t(1)); // padding between elements
			buffer.put(std::int16_t(0xDEAD)); // first value, second element
			buffer.put(std::uint8_t(2)); // padding between values
			buffer.put(std::int16_t(0xBEEF)); // second value, first element
			buffer.put(std::uint16_t(3)); // padding between elements
			buffer.put(std::int16_t(0x1234)); // second value, second element
			buffer.flip();
			CIO_ASSERT_EQUAL(16u, buffer.limit()); // Verify we set up the buffer properly

			Array varray;
			varray.bind((void *)buffer.data(), 2, 2);
			varray.setEncoding(cio::Encoding::create<std::int16_t>());
			varray.setInitialOffset(3);
			varray.setElementStride(4);
			varray.setStride(7);
			varray.setType(cio::Type::Integer);

			// Let's do it in reverse this time just to mix it up
			varray.setBinary<std::int16_t>(-1, 1, 1);
			CIO_ASSERT_EQUAL(0u, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(0u, buffer.get<std::uint8_t>());
			CIO_ASSERT_EQUAL(40, buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(1, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(std::int16_t(0xDEAD), buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(2u, buffer.get<std::uint8_t>());
			CIO_ASSERT_EQUAL(std::int16_t(0xBEEF), buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(3u, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(-1, buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(buffer.size(), buffer.position());
			buffer.flip();

			varray.setBinary<std::int16_t>(2, 0, 1);
			CIO_ASSERT_EQUAL(0u, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(0u, buffer.get<std::uint8_t>());
			CIO_ASSERT_EQUAL(40, buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(1, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(2, buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(2u, buffer.get<std::uint8_t>());
			CIO_ASSERT_EQUAL(std::int16_t(0xBEEF), buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(3u, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(-1, buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(buffer.size(), buffer.position());
			buffer.flip();

			varray.setBinary<std::int16_t>(-3, 0, 0);
			CIO_ASSERT_EQUAL(0u, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(0u, buffer.get<std::uint8_t>());
			CIO_ASSERT_EQUAL(-3, buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(1, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(2, buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(2u, buffer.get<std::uint8_t>());
			CIO_ASSERT_EQUAL(std::int16_t(0xBEEF), buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(3u, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(-1, buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(buffer.size(), buffer.position());
			buffer.flip();

			varray.setBinary<std::int16_t>(-4, 1, 0);
			CIO_ASSERT_EQUAL(0u, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(0u, buffer.get<std::uint8_t>());
			CIO_ASSERT_EQUAL(-3, buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(1, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(2, buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(2u, buffer.get<std::uint8_t>());
			CIO_ASSERT_EQUAL(-4, buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(3u, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(-1, buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(buffer.size(), buffer.position());
			buffer.flip();
		}
	}

	void ArrayTest::testGetText()
	{

	}

	void ArrayTest::testSetText()
	{

		// Test setting binary values with "0x" mode
		{
			std::uint16_t input[5] = { 0xDEADu, 0xBEEFu, 0u, 0xFFFFu, 1u };
			Array varray;
			varray.bind(input);

			// Test writing in native format
			varray.setText("0xF0F0", 0);
			CIO_ASSERT_EQUAL(0xF0F0u, input[0]);
			CIO_ASSERT_EQUAL(0xBEEFu, input[1]);
			CIO_ASSERT_EQUAL(0u, input[2]);
			CIO_ASSERT_EQUAL(0xFFFFu, input[3]);
			CIO_ASSERT_EQUAL(1u, input[4]);

			// Test writing a smaller integer
			varray.setText("0x5A", 1);
			CIO_ASSERT_EQUAL(0xF0F0u, input[0]);
			CIO_ASSERT_EQUAL(0x005Au, input[1]);
			CIO_ASSERT_EQUAL(0u, input[2]);
			CIO_ASSERT_EQUAL(0xFFFFu, input[3]);
			CIO_ASSERT_EQUAL(1u, input[4]);

			// Test writing a larger integer
			varray.setText("0xEADABADA", 2);
			CIO_ASSERT_EQUAL(0xF0F0u, input[0]);
			CIO_ASSERT_EQUAL(0x005Au, input[1]);
			CIO_ASSERT_EQUAL(0xDAEAu, input[2]); // little endian, first 4 bytes
			CIO_ASSERT_EQUAL(0xFFFFu, input[3]);
			CIO_ASSERT_EQUAL(1u, input[4]);

			// Testing writing a signed integer
			varray.setText("-2", 3);
			CIO_ASSERT_EQUAL(0xF0F0u, input[0]);
			CIO_ASSERT_EQUAL(0x005Au, input[1]);
			CIO_ASSERT_EQUAL(0xDAEAu, input[2]);
			CIO_ASSERT_EQUAL(0xFFFEu, input[3]);
			CIO_ASSERT_EQUAL(1u, input[4]);

			// Test writing the largest integer on the last element
			varray.setText("0xFEEFABBA01103223", 4);
			CIO_ASSERT_EQUAL(0xF0F0u, input[0]);
			CIO_ASSERT_EQUAL(0x005Au, input[1]);
			CIO_ASSERT_EQUAL(0xDAEAu, input[2]);
			CIO_ASSERT_EQUAL(0xFFFEu, input[3]);
			CIO_ASSERT_EQUAL(0xEFFEu, input[4]); // little endian, first 4 bytes

			// Go back and write a smaller signed integer. The underlying type is unsigned so this should not sign extend.
			varray.setText("-4", 0);
			CIO_ASSERT_EQUAL(0xFFFCu, input[0]);
			CIO_ASSERT_EQUAL(0x005Au, input[1]);
			CIO_ASSERT_EQUAL(0xDAEAu, input[2]);
			CIO_ASSERT_EQUAL(0xFFFEu, input[3]);
			CIO_ASSERT_EQUAL(0xEFFEu, input[4]);
		}

		// Test float
		{
			float value[3] = { 5.5, -1.25, 625.375 };

			Array varray;
			varray.bind(value);

			varray.setText("-2.25", 0);
			CIO_ASSERT_EQUAL(-2.25, value[0]);
			CIO_ASSERT_EQUAL(-1.25, value[1]);
			CIO_ASSERT_EQUAL(625.375, value[2]);

			varray.setText("1.0e10", 1);
			CIO_ASSERT_EQUAL(-2.25, value[0]);
			CIO_ASSERT_EQUAL(1.0e10, value[1]);
			CIO_ASSERT_EQUAL(625.375, value[2]);
		}

		// Test unaligned case with offset and stride
		{
			cio::Buffer buffer(21);
			buffer.put(std::uint32_t(0)); // Skip first five bytes
			buffer.put(std::uint8_t(0));
			buffer.put(std::uint32_t(40)); // first value
			buffer.put(std::uint16_t(1)); // padding
			buffer.put(std::uint32_t(0xDEADBEEF)); // second value
			buffer.put(std::uint16_t(2)); // padding
			buffer.put(std::uint32_t(0xABBA3223)); // third value
			buffer.flip();
			CIO_ASSERT_EQUAL(21u, buffer.limit()); // Verify we set up the buffer properly

			Array varray;
			varray.bind(buffer.current(), buffer.available());
			varray.resize(3);
			varray.setInitialOffset(5);
			varray.setStride(6);
			varray.setEncoding(cio::Encoding::create<std::uint32_t>());
			varray.setType(cio::Type::Integer);

			varray.setText("512", 0);
			CIO_ASSERT_EQUAL(0u, buffer.get<std::uint32_t>());
			CIO_ASSERT_EQUAL(0u, buffer.get<std::uint8_t>());
			CIO_ASSERT_EQUAL(512u, buffer.get<std::uint32_t>());
			CIO_ASSERT_EQUAL(1u, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(0xDEADBEEF, buffer.get<std::uint32_t>());
			CIO_ASSERT_EQUAL(2u, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(0xABBA3223, buffer.get<std::uint32_t>());
			CIO_ASSERT_EQUAL(buffer.size(), buffer.position());
			buffer.flip();

			varray.setText("0xFA4B", 1);
			CIO_ASSERT_EQUAL(0u, buffer.get<std::uint32_t>());
			CIO_ASSERT_EQUAL(0u, buffer.get<std::uint8_t>());
			CIO_ASSERT_EQUAL(512u, buffer.get<std::uint32_t>());
			CIO_ASSERT_EQUAL(1u, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(0x4BFA, buffer.get<std::uint32_t>()); // little endian, first 2 bytes, upper bytes 0
			CIO_ASSERT_EQUAL(2u, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(0xABBA3223, buffer.get<std::uint32_t>());
			CIO_ASSERT_EQUAL(buffer.size(), buffer.position());
			buffer.flip();

			varray.setText("0x122345567889ABBC", 2);
			CIO_ASSERT_EQUAL(0u, buffer.get<std::uint32_t>());
			CIO_ASSERT_EQUAL(0u, buffer.get<std::uint8_t>());
			CIO_ASSERT_EQUAL(512u, buffer.get<std::uint32_t>());
			CIO_ASSERT_EQUAL(1u, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(0x4BFA, buffer.get<std::uint32_t>());
			CIO_ASSERT_EQUAL(2u, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(0x56452312u, buffer.get<std::uint32_t>()); // little endian, first 4 bytes
			CIO_ASSERT_EQUAL(buffer.size(), buffer.position());
			buffer.flip();
		}

		// Test case with multiple elements
		{
			// Do it with dynamic memory so bad writes are likely to be caught by memory checkers
			const std::int16_t input[6] = { std::int16_t(0xFEAF), 0, 1, 0x3654, 0x1234, 2 };
			cio::Chunk<std::int16_t> values(6);
			std::memcpy(values.data(), input, sizeof(input));

			Array varray;
			varray.bind(values.data(), 2u, 3u);
			varray.setText("-1", 0, 0);
			CIO_ASSERT_EQUAL(-1, values[0]);
			CIO_ASSERT_EQUAL(0, values[1]);
			CIO_ASSERT_EQUAL(1, values[2]);
			CIO_ASSERT_EQUAL(0x3654, values[3]);
			CIO_ASSERT_EQUAL(0x1234, values[4]);
			CIO_ASSERT_EQUAL(2, values[5]);

			varray.setText("2", 1, 0);
			CIO_ASSERT_EQUAL(-1, values[0]);
			CIO_ASSERT_EQUAL(0, values[1]);
			CIO_ASSERT_EQUAL(1, values[2]);
			CIO_ASSERT_EQUAL(2, values[3]);
			CIO_ASSERT_EQUAL(0x1234, values[4]);
			CIO_ASSERT_EQUAL(2, values[5]);

			varray.setText("-3", 0, 1);
			CIO_ASSERT_EQUAL(-1, values[0]);
			CIO_ASSERT_EQUAL(-3, values[1]);
			CIO_ASSERT_EQUAL(1, values[2]);
			CIO_ASSERT_EQUAL(2, values[3]);
			CIO_ASSERT_EQUAL(0x1234, values[4]);
			CIO_ASSERT_EQUAL(2, values[5]);

			varray.setText("-4", 1, 2);
			CIO_ASSERT_EQUAL(-1, values[0]);
			CIO_ASSERT_EQUAL(-3, values[1]);
			CIO_ASSERT_EQUAL(1, values[2]);
			CIO_ASSERT_EQUAL(2, values[3]);
			CIO_ASSERT_EQUAL(0x1234, values[4]);
			CIO_ASSERT_EQUAL(-4, values[5]);
		}

		// Test with multiple elements with offset, and element and total stride
		{
			cio::Buffer buffer(16);
			buffer.put(std::uint16_t(0)); // Skip first three bytes
			buffer.put(std::uint8_t(0));
			buffer.put(std::int16_t(40)); // first value, first element
			buffer.put(std::uint16_t(1)); // padding between elements
			buffer.put(std::int16_t(0xDEAD)); // first value, second element
			buffer.put(std::uint8_t(2)); // padding between values
			buffer.put(std::int16_t(0xBEEF)); // second value, first element
			buffer.put(std::uint16_t(3)); // padding between elements
			buffer.put(std::int16_t(0x1234)); // second value, second element
			buffer.flip();
			CIO_ASSERT_EQUAL(16u, buffer.limit()); // Verify we set up the buffer properly

			Array varray;
			varray.bind((void *)buffer.data(), 2, 2);
			varray.setEncoding(cio::Encoding::create<std::int16_t>());
			varray.setInitialOffset(3);
			varray.setElementStride(4);
			varray.setStride(7);
			varray.setType(cio::Type::Integer);

			// Let's do it in reverse this time just to mix it up
			varray.setText("-1", 1, 1);
			CIO_ASSERT_EQUAL(0u, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(0u, buffer.get<std::uint8_t>());
			CIO_ASSERT_EQUAL(40, buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(1, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(std::int16_t(0xDEAD), buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(2u, buffer.get<std::uint8_t>());
			CIO_ASSERT_EQUAL(std::int16_t(0xBEEF), buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(3u, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(-1, buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(buffer.size(), buffer.position());
			buffer.flip();

			varray.setText("-2", 0, 1);
			CIO_ASSERT_EQUAL(0u, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(0u, buffer.get<std::uint8_t>());
			CIO_ASSERT_EQUAL(40, buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(1, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(-2, buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(2u, buffer.get<std::uint8_t>());
			CIO_ASSERT_EQUAL(std::int16_t(0xBEEF), buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(3u, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(-1, buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(buffer.size(), buffer.position());
			buffer.flip();

			varray.setText("-3", 0, 0);
			CIO_ASSERT_EQUAL(0u, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(0u, buffer.get<std::uint8_t>());
			CIO_ASSERT_EQUAL(-3, buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(1, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(-2, buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(2u, buffer.get<std::uint8_t>());
			CIO_ASSERT_EQUAL(std::int16_t(0xBEEF), buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(3u, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(-1, buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(buffer.size(), buffer.position());
			buffer.flip();

			varray.setText("-4", 1, 0);
			CIO_ASSERT_EQUAL(0u, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(0u, buffer.get<std::uint8_t>());
			CIO_ASSERT_EQUAL(-3, buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(1, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(-2, buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(2u, buffer.get<std::uint8_t>());
			CIO_ASSERT_EQUAL(-4, buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(3u, buffer.get<std::uint16_t>());
			CIO_ASSERT_EQUAL(-1, buffer.get<std::int16_t>());
			CIO_ASSERT_EQUAL(buffer.size(), buffer.position());
			buffer.flip();
		}
	}

	void ArrayTest::testVariableData()
	{
		//Text test
		Array varrayText;
		char text[] = { "Helloallpeople" };
		const char *tp = &text[0];
		int offsets[4] = { 0,   5, 8,    14 };
		//The matrix will see this as 3 columns, 1 row
		varrayText.bind(text, offsets);

		//Get a single character
		std::string val = varrayText.getText(0, 3);
		CIO_ASSERT_EQUAL(text[3], val.at(0));

		//Get the entire string
		val = varrayText.getText(0);
		for (auto v : val)
		{
			CIO_ASSERT_EQUAL(*tp++, v);
		}

		//Get some more data, bind a different way
		std::string strSet[7] = { "North America", "South America", "Asia", "Europe", "Africa", "Australia", "Antarctica" };
		std::vector<char> chr = varrayText.toChar(strSet);
		std::vector<std::uint32_t> off = varrayText.getOffsets(strSet);
		varrayText.bind(chr.data(), off.data(), off.size());
		std::size_t sz = varrayText.getColumns(0);

		//Get the 3rd column index of the only row "Europe"
		val = varrayText.getText(3);
		CIO_ASSERT_EQUAL(val, strSet[3]);

		//Set column zero to "South America"
		varrayText.setText("South America", 0);
		val = varrayText.getText(0);

		//Now lets make it a matrix:  say this data is split into the following matrix
		// [	{"North America", "South America"},
		//		{"Asia", "Europe", "Africa"},
		//		{"Australia"}
		//		{"Antarctica"}
		//Row offsets would be 0, 2, 5, 6,7
		std::uint32_t rowOffsets[5] = { 0, 2, 5, 6, 7 };
		varrayText.bind(chr.data(), off.data(), off.size(), rowOffsets, 5);

		//Get Column 0 of row 2 "Australia"
		val = varrayText.getText(0, 2);		//Col 0, row 2 Australia
		CIO_ASSERT_EQUAL(val, strSet[5]);

		//Test variable data array {1, 2}, {3}, {4, 5, 6}
		//Array Index          0  1    2    3  4  5  6
		std::uint32_t varArray[6] = { 1, 2, 3, 4, 5, 6 };
		std::uint32_t arrOffsets[4] = { 0, 2, 3, 6 };
		varrayText.clear();
		varrayText.bind(varArray, arrOffsets);
		val = varrayText.getText(2, 2);
		CIO_ASSERT_EQUAL("6", val);

	}

	void ArrayTest::bindTestMethods()
	{
		/*
		// test bind() first so we can use it for remaining test methods
		cio::test::Suite &bindSuite = this->addTestCase(cio::test::Suite("geas/Array/bind"));
		bindSuite.addTestCase("void", &ArrayTest::testBind<void>);
		bindSuite.addTestCase("bool", &ArrayTest::testBind<bool>);
		bindSuite.addTestCase("char", &ArrayTest::testBind<char>);
		bindSuite.addTestCase("wchar_t", &ArrayTest::testBind<wchar_t>);
		bindSuite.addTestCase("uint8_t", &ArrayTest::testBind<std::uint8_t>);
		bindSuite.addTestCase("int8_t", &ArrayTest::testBind<std::int8_t>);
		bindSuite.addTestCase("int16_t", &ArrayTest::testBind<std::int16_t>);
		bindSuite.addTestCase("uint16_t", &ArrayTest::testBind<std::uint16_t>);
		bindSuite.addTestCase("int32_t", &ArrayTest::testBind<std::int32_t>);
		bindSuite.addTestCase("uint32_t", &ArrayTest::testBind<std::uint32_t>);
		bindSuite.addTestCase("int64_t", &ArrayTest::testBind<std::int64_t>);
		bindSuite.addTestCase("uint64_t", &ArrayTest::testBind<std::uint64_t>);
		bindSuite.addTestCase("float", &ArrayTest::testBind<float>);
		bindSuite.addTestCase("double", &ArrayTest::testBind<double>);

		this->addTestCase("getBinary", &ArrayTest::testGetBinary, 1);
		this->addTestCase("setBinary", &ArrayTest::testSetBinary, 1);
		*/
		this->addTestCase("getText", &ArrayTest::testGetText, 1);
		this->addTestCase("setText", &ArrayTest::testSetText, 1);
		this->addTestCase("variableData", &ArrayTest::testVariableData, 1);
	}
}
