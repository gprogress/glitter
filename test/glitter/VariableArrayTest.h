/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_VARIABLEARRAYTEST_H
#define GLITTER_VARIABLEARRAYTEST_H

#include <cio/test/Suite.h>
#include <glitter/VariableArray.h>

namespace glitter
{
		class VariableArrayTest : public cio::test::Suite
		{
		public:
			VariableArrayTest();

			virtual ~VariableArrayTest() noexcept override;

			virtual cio::Severity setUp() override;

			void constructTest();
			void bindTest();
			void getSetParamsTest();
			void getSetValuesTest();
			//void variableArrayTest();
			//void addRemoveElementTest();

		private:
			cio::Chunk<std::uint8_t> mChunk;
			cio::Chunk<std::uint8_t> mOffsetChunk;

			std::vector<std::uint8_t> mByteDataArray;
			std::vector<std::uint8_t> mByteOffsetArray;
			std::vector<std::uint8_t> mStringArray;
			std::uint32_t mStringOffsets[4];

		};
}

#endif
