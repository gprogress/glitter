/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_SAMPLEMETADATA_H
#define GLITTER_SAMPLEMETADATA_H

#include <glitter/Types.h>

#include <cio/Interval.h>
#include <vector>

namespace glitter
{
	struct SampleMetadata
	{
		struct Vertex
		{
			static const std::uint32_t count;
			static const std::uint32_t indices[12];
		};

		struct VertexAttribute
		{
			static const std::uint32_t count;
			static const float positions[5][3];
			static const float normals[5][3];
			static const std::uint8_t fids[5];
		};
			
		struct Buildings
		{
			static const std::uint32_t count;
			static const double heights[8];
			static const char *addresses[8];
			static const double coordinates[8][2];
			static const std::uint32_t occupants[8];
		};

		struct Cars
		{
			static const std::uint32_t count;
			static const char *passengers[4];
			static const std::uint32_t passengerCounts[3];
			static const bool isElectric[3];
			static const float seating[];
			static const std::uint32_t seatingCounts[3];
		};

		struct Elevations
		{
			static const std::uint32_t count;
			static const std::uint8_t elevations[12];
			static const char* name[2];
			static const std::uint8_t value[2];
		};

		static inline std::size_t padding(std::size_t length);

		static inline std::size_t aligned(std::size_t length);

		template <typename T, std::size_t N>
		static inline std::size_t count(T(&value)[N]);

		template <typename T, std::size_t N>
		static inline std::size_t size(T (&value)[N]);
		
		template <std::size_t N>
		static inline std::size_t size(const bool (&value)[N]);

		static std::vector<cio::SizeInterval> generateBufferRanges();

		static void generateMinimalSchema(geas::Application &schema);

		static void generateBin(cio::Buffer &buffer);

		static void generateBinFile(const cio::Path &path);

		static void generateBinFile(const cio::Path &path, cio::ProtocolFactory &protocols);

		static void generateBinFile(cio::Output &output);
	};
}

/* Inline implementation */

namespace glitter
{
	std::size_t SampleMetadata::aligned(std::size_t length)
	{
		return 8 * ((length + 7) / 8);
	}

	std::size_t SampleMetadata::padding(std::size_t length)
	{
		// 0 -> 0
		// 1 -> 3
		// 2 -> 2
		// 3 -> 1
		return (8 - (length % 8)) & 7;
	}

	template <typename T, std::size_t N>
	inline std::size_t SampleMetadata::count(T(&value)[N])
	{
		return N;
	}

	template <typename T, std::size_t N>
	inline std::size_t SampleMetadata::size(T(&value)[N])
	{
		return N * sizeof(T);
	}

	template <std::size_t N>
	inline std::size_t SampleMetadata::size(const bool(&value)[N])
	{
		return (N + 7) / 8;
	}

}

#endif
