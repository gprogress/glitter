/************************************************************************
 * Copyright 2020-2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "VariableArrayTest.h"
#include <iostream>
#include <cio/test/Assert.h>

namespace glitter
{
		VariableArrayTest::VariableArrayTest() : cio::test::Suite("cio/data/VariableArray")
		{
			CIO_TEST_METHOD(VariableArrayTest, construct);
			CIO_TEST_METHOD(VariableArrayTest, bind);
			CIO_TEST_METHOD(VariableArrayTest, getSetParams);
			CIO_TEST_METHOD(VariableArrayTest, getSetValues);
		}

		VariableArrayTest::~VariableArrayTest() noexcept = default;

		cio::Severity VariableArrayTest::setUp()
		{
			//Construct variable data  { {1}, {2, 3}, {4, 5, 6}, {7, 8, 0, 0} }
			std::size_t sz = sizeof(std::uint32_t);
			mByteDataArray.resize(sz * 10);
			std::memcpy(mByteDataArray.data(), std::vector<std::uint32_t>{1}.data(), sz);
			std::memcpy(mByteDataArray.data() + sz, std::vector<std::uint32_t>({2, 3}).data(), sz * 2);
			std::memcpy(mByteDataArray.data() + sz * 3, std::vector<std::uint32_t>({4, 5, 6}).data(), sz * 3);
			std::memcpy(mByteDataArray.data() + sz * 6, std::vector<std::uint32_t>({7, 8, 0, 0}).data(), sz * 4);

			//Setup Chunk
			mChunk = cio::Chunk<std::uint8_t>(mByteDataArray.data(), sz * 10);

			//Offsets {0, 1, 3, 6, 10}
			std::vector<std::uint32_t> offset = { 0, 1, 3, 6, 10 };
			mByteOffsetArray.resize(sz * 5);
			std::memcpy(mByteOffsetArray.data(), offset.data(), sz * 5);
			mOffsetChunk = cio::Chunk<std::uint8_t>(mByteOffsetArray.data(), sz * 5);


			//String data
			char stringArray[] = { "RedYellowGreen" };
			std::size_t sz2 = sizeof(stringArray);
			std::uint32_t stringOffset[4] = { 0, 3, 9, 14 };
			mStringArray.resize(sz2);
			std::memcpy(mStringArray.data(), stringArray, sz2);
			std::memcpy(mStringOffsets, stringOffset, sizeof(stringOffset));
			return cio::Severity::None;
		}

		void VariableArrayTest::constructTest()
		{
			VariableArray<std::uint32_t, std::uint32_t, std::uint8_t> accessor;
			CIO_ASSERT_EQUAL(0u, accessor.size());
			CIO_ASSERT_EQUAL(0u, accessor.getRowCount());
			CIO_ASSERT_EQUAL(0u, accessor.getOffset());
			CIO_ASSERT(!accessor.getData());
			
			//Variable array, 4 byte data
			VariableArray<std::uint32_t, std::uint32_t, std::uint8_t> accessor2(mChunk);
			CIO_ASSERT_EQUAL(mChunk.size(), accessor2.size());
			CIO_ASSERT_EQUAL(1u, accessor2.getRowCount());
			CIO_ASSERT_EQUAL(0u, accessor2.getOffset());
			CIO_ASSERT_EQUAL(mChunk.data(), accessor2.data());

			Scalar<std::uint32_t, std::uint8_t> sc(mOffsetChunk, 5);
			VariableArray<std::uint32_t, std::uint32_t, std::uint8_t> accessor3(mChunk, sc);
			CIO_ASSERT_EQUAL(10u, accessor3.size());
			CIO_ASSERT_EQUAL(4u, accessor3.getRowCount());
			CIO_ASSERT_EQUAL(0u, accessor3.getOffset());
			CIO_ASSERT_EQUAL(0u, accessor3.getIndexData().getOffset());
			CIO_ASSERT_EQUAL(sizeof(std::uint32_t), accessor3.getIndexData().getStride());
			CIO_ASSERT_EQUAL(*mChunk.data(), *accessor3.data());
			CIO_ASSERT_EQUAL(mOffsetChunk.data(), accessor3.indexData());

			VariableArray<std::uint32_t, std::uint32_t, std::uint8_t> accessor4(mChunk, sc, 3);
			CIO_ASSERT_EQUAL(10u, accessor4.size());
			CIO_ASSERT_EQUAL(4u, accessor4.getRowCount());
			CIO_ASSERT_EQUAL(3u, accessor4.getOffset());
			CIO_ASSERT_EQUAL(0u, accessor4.getIndexData().getOffset());
			CIO_ASSERT_EQUAL(sizeof(std::uint32_t), accessor4.getIndexData().getStride());
			CIO_ASSERT_EQUAL(*mChunk.data(), *accessor4.data());
			CIO_ASSERT_EQUAL(mOffsetChunk.data(), accessor4.indexData());

			VariableArray<std::uint32_t, std::uint32_t, std::uint8_t> accessor5(accessor4);
			CIO_ASSERT_EQUAL(accessor5.size(), accessor4.size());
			CIO_ASSERT_EQUAL(accessor5.getRowCount(), accessor4.getRowCount());
			CIO_ASSERT_EQUAL(accessor5.getOffset(), accessor4.getOffset());
			CIO_ASSERT_EQUAL(accessor5.getIndexData().getOffset(), accessor4.getIndexData().getOffset());
			CIO_ASSERT_EQUAL(accessor5.getIndexData().getStride(), accessor4.getIndexData().getStride());
			CIO_ASSERT_EQUAL(*accessor5.data(), *accessor4.data());
			CIO_ASSERT_EQUAL(accessor5.indexData(), accessor4.indexData());

			VariableArray<std::uint32_t, std::uint32_t, std::uint8_t> accessor6 = accessor2;
			CIO_ASSERT_EQUAL(accessor2.size(), accessor6.size());
			CIO_ASSERT_EQUAL(accessor2.getRowCount(), accessor6.getRowCount());
			CIO_ASSERT_EQUAL(accessor2.getOffset(), accessor6.getOffset());
			CIO_ASSERT_EQUAL(accessor2.data(), accessor6.data());
			CIO_ASSERT_EQUAL(accessor2.indexData(), accessor6.indexData());

			VariableArray<std::uint32_t, std::uint16_t, std::uint8_t> accessorDiff;
			accessor2 = accessorDiff;
			CIO_ASSERT_EQUAL(0u, accessor2.size());
			CIO_ASSERT_EQUAL(0u, accessor2.getRowCount());
			CIO_ASSERT_EQUAL(0u, accessor2.getOffset());
			CIO_ASSERT_EQUAL(0u, accessor2.getIndexData().getOffset());
			CIO_ASSERT(!accessor2.getData());
			CIO_ASSERT(!accessor2.indexData());
		}

		void VariableArrayTest::bindTest()
		{
			//Construct variable data  { {1}, {2, 3}, {4, 5, 6}, {7, 8, 0, 0} }
			//Offsets {0, 1, 3, 6, 10}

			VariableArray<std::uint32_t, std::uint32_t, std::uint8_t> accessor;
			Scalar<std::uint32_t, std::uint8_t> sc(mOffsetChunk, 5);

			accessor.bind(mChunk, sc);
			CIO_ASSERT_EQUAL(10u, accessor.size());
			CIO_ASSERT_EQUAL(4u, accessor.getRowCount());
			CIO_ASSERT_EQUAL(0u, accessor.getOffset());
			CIO_ASSERT_EQUAL(5u, accessor.getIndexData().size());
			CIO_ASSERT_EQUAL(0u, accessor.getIndexData().getOffset());
			CIO_ASSERT_EQUAL(mChunk.data(), accessor.data());
			CIO_ASSERT_EQUAL(mOffsetChunk.data(), accessor.indexData());

			VariableArray<std::uint32_t, std::uint32_t, std::uint8_t> accessorb;
			accessorb.bind(mChunk, mOffsetChunk);
			CIO_ASSERT_EQUAL(10u, accessorb.size());
			CIO_ASSERT_EQUAL(4u, accessorb.getRowCount());
			CIO_ASSERT_EQUAL(0u, accessorb.getOffset());
			CIO_ASSERT_EQUAL(5u, accessorb.getIndexData().size());
			CIO_ASSERT_EQUAL(0u, accessorb.getIndexData().getOffset());
			CIO_ASSERT_EQUAL(mChunk.data(), accessorb.data());
			CIO_ASSERT_EQUAL(mOffsetChunk.data(), accessorb.indexData());
		}

		void VariableArrayTest::getSetParamsTest()
		{
			VariableArray<std::uint32_t, std::uint32_t, std::uint8_t> accessor;

			//Size
			CIO_ASSERT_EQUAL(0u, accessor.size());
			accessor.resize(24);
			CIO_ASSERT_EQUAL(24u, accessor.size());

			//Offsets
			CIO_ASSERT_EQUAL(0u, accessor.getOffset());
			CIO_ASSERT_EQUAL(0u, accessor.getIndexData().getOffset());

			accessor.setOffset(4);
			accessor.getIndexData().setOffset(8);
			CIO_ASSERT_EQUAL(4u, accessor.getOffset());
			CIO_ASSERT_EQUAL(8u, accessor.getIndexData().getOffset());
			
			//Stride of offset array
			CIO_ASSERT_EQUAL(sizeof(std::uint32_t), accessor.getIndexData().getStride());
			accessor.getIndexData().setStride(10);
			CIO_ASSERT_EQUAL(10u, accessor.getIndexData().getStride());
		}

		void VariableArrayTest::getSetValuesTest()
		{
			//Empty test
			VariableArray<uint16_t, uint16_t, uint16_t> ac;
			std::size_t empty = ac.getCount(0);
			CIO_ASSERT_EQUAL(0u, empty);
			
			// Data structure provided for convenience
			//{ {1}, {2, 3}, {4, 5, 6}, {7, 8, 0, 0} }
			//Get number of elements at a particular index
			Scalar<std::uint32_t, std::uint8_t> sc(mOffsetChunk, 5);
			VariableArray<std::uint32_t, std::uint32_t, std::uint8_t> accessor(mChunk, sc);
			std::size_t elements = accessor.getCount(2);
			
			CIO_ASSERT_EQUAL(3u, elements);
			elements = accessor.getCount(3);
			CIO_ASSERT_EQUAL(4u, elements);
			elements = accessor.getCount(0);
			CIO_ASSERT_EQUAL(1u, elements);
			//Out of range
			elements = accessor.getCount(10);
			CIO_ASSERT_EQUAL(0u, elements);
			
			//Get value at a particular index
			std::uint32_t val = accessor.get(1, 1);
			CIO_ASSERT_EQUAL(3u, val);
			accessor.set(1, 1, 4);
			val = accessor.get(1, 1);
			std::vector<std::uint32_t> vecVals = accessor.get(3);
			vecVals.resize(5);
			vecVals = { 8, 9, 10, 11, 12 };
			accessor.set(2, vecVals);
			vecVals = accessor.get(2);


			//Try with the String Data  "{Red, Yellow, Green}
			cio::Chunk<std::uint8_t> stringChunk(mStringArray.data(), 15, nullptr);
			cio::Chunk<std::uint8_t> sOffsetChunk(reinterpret_cast<std::uint8_t*>(mStringOffsets), sizeof(mStringOffsets));
			Scalar<std::uint32_t, std::uint8_t> stringScalar(sOffsetChunk, 4);
			VariableArray<std::uint32_t, std::uint8_t, std::uint8_t> sAccessor(stringChunk, stringScalar);

			//Number of elements at each index
			CIO_ASSERT_EQUAL(3u, sAccessor.getCount(0));  //"Red"
			CIO_ASSERT_EQUAL(6u, sAccessor.getCount(1));  //"Yellow"
			CIO_ASSERT_EQUAL(5u, sAccessor.getCount(2));  //"Green"

			//Get all values in row 0  "Red"
			std::vector<uint8_t> vals = sAccessor.get(0);
			for (std::size_t i = 0; i < 3; ++i)
			{
				CIO_ASSERT_EQUAL(vals.at(i), *(mStringArray.data() + i));
			}
			vals = sAccessor.get(1);
			for (std::size_t i = 0; i < 6; ++i)
			{
				CIO_ASSERT_EQUAL(vals.at(i), *(mStringArray.data() + i + 3));
			}
			vals = sAccessor.get(2);
			for (std::size_t i = 0; i < 5; ++i)
			{
				CIO_ASSERT_EQUAL(vals.at(i), *(mStringArray.data() + i + 9));
			}

			//RESIZING
			//Try a different size of elements for the first row
			sAccessor.setText(0, "vector");  //0, 6, 12, 17
			vals = sAccessor.get(0);
			std::string strCheck(vals.begin(), vals.end());
			CIO_ASSERT_EQUAL(strCheck, "vector");
			vals = sAccessor.get(1);
			strCheck.assign(vals.begin(), vals.end());
			CIO_ASSERT_EQUAL(strCheck, "Yellow");

			sAccessor.setText(1, "hi");
			vals = sAccessor.get(1);
			strCheck.assign(vals.begin(), vals.end());
			CIO_ASSERT_EQUAL(strCheck, "hi");

			vals = sAccessor.get(0);
			strCheck.assign(vals.begin(), vals.end());
			CIO_ASSERT_EQUAL(strCheck, "vector");
			strCheck.assign(vals.begin(), vals.end());

			vals = sAccessor.get(2);
			strCheck.assign(vals.begin(), vals.end());
			CIO_ASSERT_EQUAL(strCheck, "Green");
			strCheck.assign(vals.begin(), vals.end());

			sAccessor.setText(0, "Excellent");

			//Set something out of range, to be added to the array.
			sAccessor.setText(3, "Blue"); //ExcellenthiGreenBlue 0, 9, 11, 16, 20
			vals = sAccessor.get(3);
			vals = sAccessor.get(1);
			sAccessor.remove(1);
			vals = sAccessor.get(1);
		}
}
