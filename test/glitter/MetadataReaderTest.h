/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_METADATAREADERTEST_H
#define GLITTER_METADATAREADERTEST_H

#include <glitter/Types.h>

#include <cio/Path.h>
#include <cio/test/Suite.h>

namespace glitter
{
	class MetadataReaderTest : public cio::test::Suite
	{
		public:
			MetadataReaderTest();

			explicit MetadataReaderTest(cio::Path dataDir);

			virtual ~MetadataReaderTest() noexcept override;

			virtual const cio::Metaclass &getMetaclass() const noexcept override;

			virtual cio::Severity setUp() override;

			void testLoadFeatureTables();

			void testLoadFeatureMetadata();

			void testLoadFeatureMetadataFromText();

			void testLoadFeatureMetadataFromFile();

			// Support methods

			void assertFeatureClassesLoaded(const geas::Application &schema) const;

			void assertFeatureTablesLoaded(const Collection &collection) const;

		private:
			static cio::Class<MetadataReaderTest> sMetaclass;

			void bindTestMethods();

			cio::Path mDataDir;
			std::string mSampleClassesFile;
	};
}

#endif
