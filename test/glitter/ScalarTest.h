/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#ifndef GLITTER_SCALARTEST_H
#define GLITTER_SCALARTEST_H

#include <cio/test/Suite.h>
#include <glitter/Scalar.h>

using chunkType = std::uint8_t;
using dataType = std::uint32_t;

namespace glitter
{
		class ScalarTest : public cio::test::Suite
		{
		public:
			ScalarTest();

			virtual ~ScalarTest() noexcept override;

			void constructTest();
			void assignTest();
			void bindTest();

			void valueTest();

			void elementSizeTest();
			void offsetTest();
			void strideTest();
			void sizeTest();
		};
}

#endif
