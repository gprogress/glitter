/************************************************************************
 * Copyright 2020-2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "FixedArrayTest.h"
#include <iostream>
#include <string>
#include <cio/test/Assert.h>

namespace glitter
{
		FixedArrayTest::FixedArrayTest() : cio::test::Suite("cio/FixedArray")
		{
			CIO_TEST_METHOD(FixedArrayTest, construct);
			CIO_TEST_METHOD(FixedArrayTest, bind);
			CIO_TEST_METHOD(FixedArrayTest, get);
			CIO_TEST_METHOD(FixedArrayTest, set);
		}

		FixedArrayTest::~FixedArrayTest() noexcept = default;

		cio::Severity FixedArrayTest::setUp()
		{
			//Construct with data
			std::uint32_t inputData[3][2] = { {1, 2}, {3, 4}, {5, 6} };
			mByteArray.resize(sizeof(inputData));
			std::memcpy(mByteArray.data(), &inputData, sizeof(inputData));
			mChunk = cio::Chunk<std::uint8_t>(mByteArray.data(), sizeof(inputData));
			return cio::Severity::None;
		}

		void FixedArrayTest::constructTest()
		{
			//Default
			FixedArray<std::uint32_t, std::uint8_t> accessor;
			CIO_ASSERT_EQUAL(sizeof(std::uint32_t), accessor.getElementSize());
			CIO_ASSERT_EQUAL(0u, accessor.getColumnCount());
			CIO_ASSERT_EQUAL(0u, accessor.getRowCount());
			CIO_ASSERT_EQUAL(sizeof(std::uint32_t), accessor.getElementStride());
			CIO_ASSERT_EQUAL(0u, accessor.getOffset());
			CIO_ASSERT_EQUAL(sizeof(std::uint32_t), accessor.getStride());
			CIO_ASSERT_EQUAL(0u, accessor.size());
			CIO_ASSERT(!accessor.getData());

			FixedArray<std::uint32_t, std::uint8_t> accessor2(mChunk, 2, 3, 0);
			CIO_ASSERT_EQUAL(sizeof(std::uint32_t), accessor2.getElementSize());
			CIO_ASSERT_EQUAL(2u, accessor2.getColumnCount());
			CIO_ASSERT_EQUAL(3u, accessor2.getRowCount());
			CIO_ASSERT_EQUAL(sizeof(std::uint32_t), accessor2.getElementStride());
			CIO_ASSERT_EQUAL(0u, accessor2.getOffset());
			CIO_ASSERT_EQUAL(sizeof(std::uint32_t) * 2, accessor2.getStride());
			CIO_ASSERT_EQUAL(6u, accessor2.size());
			CIO_ASSERT_EQUAL(mChunk.data(), accessor2.getData().data());

			FixedArray<std::uint32_t, std::uint8_t> accessor3(mChunk, 2, 3, 0, 4, 8);
			CIO_ASSERT_EQUAL(8u, accessor3.getElementSize());
			CIO_ASSERT_EQUAL(2u, accessor3.getColumnCount());
			CIO_ASSERT_EQUAL(3u, accessor3.getRowCount());
			CIO_ASSERT_EQUAL(8u, accessor3.getElementStride());
			CIO_ASSERT_EQUAL(0u, accessor3.getOffset());
			CIO_ASSERT_EQUAL(4u, accessor3.getStride());
			CIO_ASSERT_EQUAL(6u, accessor3.size());
			CIO_ASSERT_EQUAL(mChunk.data(), accessor3.getData().data());
			
			FixedArray<std::uint32_t, std::uint8_t> accessor4(mChunk, 2, 3, 0, 4, 8, 2);
			CIO_ASSERT_EQUAL(8u, accessor4.getElementSize());
			CIO_ASSERT_EQUAL(2u, accessor4.getColumnCount());
			CIO_ASSERT_EQUAL(3u, accessor4.getRowCount());
			CIO_ASSERT_EQUAL(2u, accessor4.getElementStride());
			CIO_ASSERT_EQUAL(0u, accessor4.getOffset());
			CIO_ASSERT_EQUAL(4u, accessor4.getStride());
			CIO_ASSERT_EQUAL(6u, accessor4.size());
			CIO_ASSERT_EQUAL(mChunk.data(), accessor4.getData().data());

			//Copy Construct
			accessor = accessor4;
			CIO_ASSERT_EQUAL(8u, accessor.getElementSize());
			CIO_ASSERT_EQUAL(2u, accessor.getColumnCount());
			CIO_ASSERT_EQUAL(3u, accessor.getRowCount());
			CIO_ASSERT_EQUAL(2u, accessor.getElementStride());
			CIO_ASSERT_EQUAL(0u, accessor.getOffset());
			CIO_ASSERT_EQUAL(4u, accessor.getStride());
			CIO_ASSERT_EQUAL(6u, accessor.size());
			CIO_ASSERT_EQUAL(mChunk.data(), accessor.getData().data());

			//Copy construct, Different Types
			FixedArray<std::uint8_t, std::uint8_t> accessor5;
			CIO_ASSERT_EQUAL(sizeof(std::uint8_t), accessor5.getElementSize());
			CIO_ASSERT_EQUAL(0u, accessor5.getColumnCount());
			CIO_ASSERT_EQUAL(0u, accessor5.getRowCount());
			CIO_ASSERT_EQUAL(sizeof(std::uint8_t), accessor5.getElementStride());
			CIO_ASSERT_EQUAL(0u, accessor5.getOffset());
			CIO_ASSERT_EQUAL(sizeof(std::uint8_t), accessor5.getStride());
			CIO_ASSERT_EQUAL(0u, accessor5.size());
			CIO_ASSERT(!accessor5.getData());

			accessor5 = accessor;
			CIO_ASSERT_EQUAL(8u, accessor5.getElementSize());
			CIO_ASSERT_EQUAL(2u, accessor5.getColumnCount());
			CIO_ASSERT_EQUAL(3u, accessor5.getRowCount());
			CIO_ASSERT_EQUAL(2u, accessor5.getElementStride());
			CIO_ASSERT_EQUAL(0u, accessor5.getOffset());
			CIO_ASSERT_EQUAL(4u, accessor5.getStride());
			CIO_ASSERT_EQUAL(6u, accessor5.size());
			CIO_ASSERT_EQUAL(accessor.getData().data(), accessor5.getData().data());
		}

		void FixedArrayTest::bindTest()
		{
			FixedArray<std::uint32_t, std::uint8_t> accessor;
			CIO_ASSERT(!accessor.getData());

			accessor.bind(mChunk);
			CIO_ASSERT_EQUAL(sizeof(std::uint32_t), accessor.getElementSize());
			CIO_ASSERT_EQUAL(1u, accessor.getColumnCount());
			CIO_ASSERT_EQUAL(1u, accessor.getRowCount());
			CIO_ASSERT_EQUAL(sizeof(std::uint32_t), accessor.getElementStride());
			CIO_ASSERT_EQUAL(0u, accessor.getOffset());
			CIO_ASSERT_EQUAL(sizeof(std::uint32_t), accessor.getStride());
			CIO_ASSERT_EQUAL(1u, accessor.size());
			CIO_ASSERT_EQUAL(*accessor.getData().data(), *mChunk.data());
		}

		void FixedArrayTest::getTest()
		{
			FixedArray<std::uint32_t, std::uint8_t> accessor(mChunk, 2, 3, 0);
			cio::Value<std::uint32_t> result = accessor.get(1, 1);
			CIO_ASSERT_EQUAL(4u, result);
			result = accessor.get(2, 0);
			CIO_ASSERT_EQUAL(5u, result);
		}

		void FixedArrayTest::setTest()
		{
			FixedArray<std::uint32_t, std::uint8_t> accessor(mChunk, 2, 3, 0);
			accessor.set(2, 0, 7);
			accessor.set(1, 1, 8);
			cio::Value<std::uint32_t> result = accessor.get(1, 1);
			CIO_ASSERT_EQUAL(8u, result);
			result = accessor.get(2, 0);
			CIO_ASSERT_EQUAL(7u, result);
		}
}
