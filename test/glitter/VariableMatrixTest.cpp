/************************************************************************
 * Copyright 2020-2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "VariableMatrixTest.h"
#include <iostream>
#include <glitter/VariableArray.h>
#include <cio/test/Assert.h>


namespace glitter
{
		VariableMatrixTest::VariableMatrixTest() : cio::test::Suite("cio/data/VariableMatrix")
		{
			CIO_TEST_METHOD(VariableMatrixTest, construct);
			CIO_TEST_METHOD(VariableMatrixTest, bind);
			CIO_TEST_METHOD(VariableMatrixTest, getSetMembers);
			CIO_TEST_METHOD(VariableMatrixTest, getSetElements);
		}

		VariableMatrixTest::~VariableMatrixTest() noexcept = default;

		cio::Severity VariableMatrixTest::setUp()
		{
			//Index offsets	(5)		        0                           3                             6                    8              10
			// data offsets (11)            0      3         7         13         19        24        30       35        40         46    50
			//Construct variable data  { ["red", "blue", "yellow"], ["orange", "green", "purple"], ["black", "white"], ["silver", "gold"] }
			std::string row12[2][3] = { { "red", "blue", "yellow" }, { "orange", "green", "purple" } };
			std::string row34[2][2] = { { "black", "white" }, { "silver", "gold" } };
			std::string allStr;
			std::vector<uint32_t> dataOffsets(11,0), indexOffsets(5,0);
			std::size_t index = 0, row=0;
			for (std::size_t i = 0; i < 2; ++i)
			{				
				for (std::size_t j = 0; j < 3; ++j)
				{
					allStr += row12[i][j];
					dataOffsets[index+1] = static_cast<unsigned int>(row12[i][j].length()) + dataOffsets[index];
					index++;
				}				
				indexOffsets[row+1] = static_cast<unsigned int>(index);
				row++;
			}			
			for (std::size_t i = 0; i < 2; ++i)
			{
				for (std::size_t j = 0; j < 2; ++j)
				{
					allStr += row34[i][j];
					dataOffsets[index + 1] = static_cast<unsigned int>(row34[i][j].length()) + dataOffsets[index];
					index++;
				}
				indexOffsets[row+1] = static_cast<unsigned int>(index);
				row++;
			}
			const char* str = allStr.c_str();
			std::size_t sz = std::strlen(str);
			sz = allStr.length();
			
			//Data
			mByteDataArray.resize(std::strlen(str)+1);
			std::memcpy(mByteDataArray.data(), str, std::strlen(str));
			mChunk = cio::Chunk<std::uint8_t>(mByteDataArray.data(), std::strlen(str), nullptr);

			//Data Offsets
			
			std::size_t dataSize = sizeof(std::uint32_t) * dataOffsets.size();
			mByteOffsetArray.resize(dataSize);
			std::memcpy(mByteOffsetArray.data(), dataOffsets.data(), dataSize);
			mDataOffsetChunk = cio::Chunk<std::uint8_t>(mByteOffsetArray.data(), dataSize, nullptr);
			
			//Array index offsets
			std::size_t indexSize = sizeof(std::uint32_t) * indexOffsets.size();
			mArrayOffsets.resize(indexSize);
			std::memcpy(mArrayOffsets.data(), indexOffsets.data(), indexSize);
			mArrayOffsetChunk = cio::Chunk<std::uint8_t>(mArrayOffsets.data(), indexSize, nullptr);
			
			return cio::Severity::None;
		}

		void VariableMatrixTest::constructTest()
		{
			//Empty
			VariableMatrix<std::uint32_t, std::uint8_t, std::uint8_t> matrix;
			CIO_ASSERT_EQUAL(0u, matrix.size());
			CIO_ASSERT_EQUAL(0u, matrix.getRowCount());
			CIO_ASSERT(!matrix.getData());
			CIO_ASSERT(!matrix.getColumnData().data());
			CIO_ASSERT(!matrix.getRowOffsets().data());
			
			//Data Chunk only
			VariableMatrix<std::uint32_t, std::uint8_t, std::uint8_t> matrix2(mChunk);
			CIO_ASSERT_EQUAL(1u, matrix2.size());
			CIO_ASSERT_EQUAL(1u, matrix2.getRowCount());
			CIO_ASSERT_EQUAL(matrix2.getData().data(), matrix2.getColumnData().getData().data());
			CIO_ASSERT(!matrix2.getRowOffsets().data());
			
			//Data Chunk and Offsets
			Scalar<std::uint32_t, std::uint8_t> scData(mDataOffsetChunk, 11);
			Scalar<std::uint32_t, std::uint8_t> scArray(mArrayOffsetChunk, 5);
			VariableMatrix<std::uint32_t, std::uint8_t, std::uint8_t> matrix3(mChunk, scData, scArray);
			CIO_ASSERT_EQUAL(scData.size() - 1, matrix3.size());
			CIO_ASSERT_EQUAL(scArray.size() - 1, matrix3.getRowCount());
			CIO_ASSERT_EQUAL(matrix3.getData().data(), matrix3.getColumnData().getData().data());
			CIO_ASSERT_EQUAL(matrix3.getColumnData().getIndexData().data(), scData.getData().data());
			CIO_ASSERT_EQUAL(matrix3.getRowOffsets().data(), scArray.data());

			//VariableArray
			VariableArray<std::uint32_t, std::uint8_t, uint8_t> accessor(mChunk, scData);
			VariableMatrix<std::uint32_t, std::uint8_t, std::uint8_t> matrix4(accessor);
			CIO_ASSERT_EQUAL(accessor.size(), matrix4.size());
			CIO_ASSERT_EQUAL(1u, matrix4.getRowCount());
			CIO_ASSERT_EQUAL(matrix4.getData().data(), matrix4.getColumnData().getData().data());
			CIO_ASSERT_EQUAL(matrix4.getColumnData().getIndexData().data(), accessor.getIndexData().data());
			CIO_ASSERT(!matrix4.getRowOffsets().data());

			//VariableArray and Row Offsets
			VariableMatrix<std::uint32_t, std::uint8_t, std::uint8_t> matrix5(accessor, scArray);
			CIO_ASSERT_EQUAL(accessor.size(), matrix5.size());
			CIO_ASSERT_EQUAL(scArray.size() - 1, matrix5.getRowCount());
			CIO_ASSERT_EQUAL(matrix5.getData().data(), matrix5.getColumnData().getData().data());
			CIO_ASSERT_EQUAL(matrix5.getColumnData().getIndexData().data(), accessor.getIndexData().data());
			CIO_ASSERT_EQUAL(matrix5.getRowOffsets().data(), scArray.data());
		}

		void VariableMatrixTest::bindTest()
		{
			Scalar<std::uint32_t, std::uint8_t> scData(mDataOffsetChunk, 11);
			Scalar<std::uint32_t, std::uint8_t> scArray(mArrayOffsetChunk, 5);
			VariableMatrix<std::uint32_t, std::uint8_t, std::uint8_t> matrix;

			matrix.bind(mChunk, scData, scArray);
			CIO_ASSERT_EQUAL(matrix.size(), scData.size() - 1);
			CIO_ASSERT_EQUAL(matrix.getRowCount(), scArray.size() - 1);
			CIO_ASSERT_EQUAL(matrix.getData().data(), matrix.getColumnData().getData().data());
			CIO_ASSERT_EQUAL(matrix.getColumnData().getIndexData().data(), scData.getData().data());
			CIO_ASSERT_EQUAL(matrix.getRowOffsets().data(), scArray.data());

			VariableArray<std::uint32_t, std::uint8_t, uint8_t> accessor(mChunk, scData);
			matrix.bind(accessor, scArray);
			CIO_ASSERT_EQUAL(matrix.size(), scData.size() - 1);
			CIO_ASSERT_EQUAL(matrix.getRowCount(), scArray.size() - 1);
			CIO_ASSERT_EQUAL(matrix.getData().data(), matrix.getColumnData().getData().data());
			CIO_ASSERT_EQUAL(matrix.getColumnData().getIndexData().data(), scData.getData().data());
			CIO_ASSERT_EQUAL(matrix.getRowOffsets().data(), scArray.data());

			matrix.clear();
			matrix.bind(mChunk, mDataOffsetChunk, 11);
			CIO_ASSERT_EQUAL(matrix.size(), scData.size() - 1);
			CIO_ASSERT_EQUAL(1u, matrix.getRowCount());
			CIO_ASSERT_EQUAL(matrix.getData().data(), matrix.getColumnData().getData().data());
			CIO_ASSERT_EQUAL(matrix.getColumnData().getIndexData().data(), scData.getData().data());
			CIO_ASSERT(!matrix.getRowOffsets().data());

			matrix.bind(mChunk, mDataOffsetChunk, 11, mArrayOffsetChunk, 5);
			CIO_ASSERT_EQUAL(matrix.size(), scData.size() - 1);
			CIO_ASSERT_EQUAL(matrix.getRowCount(), scArray.size() - 1);
			CIO_ASSERT_EQUAL(matrix.getData().data(), matrix.getColumnData().getData().data());
			CIO_ASSERT_EQUAL(matrix.getColumnData().getIndexData().data(), scData.getData().data());
			CIO_ASSERT_EQUAL(matrix.getRowOffsets().data(), scArray.data());
		}

		void VariableMatrixTest::getSetMembersTest()
		{
			std::size_t sz = sizeof(mArrayOffsets);
			Scalar<std::uint32_t, std::uint8_t> sc(mDataOffsetChunk, 11);
			Scalar<std::uint32_t, std::uint8_t> scArray(mArrayOffsetChunk, 5);
			VariableArray<std::uint32_t, std::uint8_t, uint8_t> accessor(mChunk, sc);
			VariableMatrix<std::uint32_t, std::uint8_t, std::uint8_t> matrix(accessor, scArray);

			std::size_t val = matrix.getRowCount();						//4
			val = matrix.getColumns(0);					//3
			val = matrix.getColumns(1);					//3
			val = matrix.getColumns(2);					//2
			val = matrix.getColumns(3);					//2
			val = matrix.getColumns(6);					//0 out of range

		}

		void VariableMatrixTest::getSetElementsTest()
		{
			std::size_t sz = sizeof(mArrayOffsets);
			Scalar<std::uint32_t, std::uint8_t> sc(mDataOffsetChunk, 11);
			Scalar<std::uint32_t, std::uint8_t> scArray(mArrayOffsetChunk, 5);
			VariableArray<std::uint32_t, std::uint8_t, uint8_t> accessor(mChunk, sc);
			VariableMatrix<std::uint32_t, std::uint8_t, std::uint8_t> matrix(accessor, scArray);

			std::size_t val = matrix.getElementSize(0, 2);	//black, 5
			CIO_ASSERT_EQUAL(5u, val);
			val = matrix.getElementSize(0, 0);			//red, 3
			CIO_ASSERT_EQUAL(3u, val);
			val = matrix.getElementSize(2, 1);			//purple, 6
			CIO_ASSERT_EQUAL(6u, val);

			//Get all of the elements at the given row
			std::vector<std::vector<std::uint8_t>> vals = matrix.get(2); // black, white
			std::string vecVal(vals.at(0).begin(), vals.at(0).end());
			CIO_ASSERT_EQUAL("black", vecVal);
			vecVal.assign(vals.at(1).begin(), vals.at(1).end());
			CIO_ASSERT_EQUAL("white", vecVal);

			vals = matrix.get(0);  //red, blue, yellow
			vecVal.assign(vals.at(2).begin(), vals.at(2).end());
			CIO_ASSERT_EQUAL("yellow", vecVal);

			std::vector<std::uint8_t> singleVal = matrix.get(1, 3);  //gold
			vecVal.assign(singleVal.begin(), singleVal.end());
			CIO_ASSERT_EQUAL("gold", vecVal);

			std::vector<std::uint8_t> setVal{'s', 'u', 'n' };
			matrix.set(1, 3, setVal);
			singleVal = matrix.get(1, 3);
		}
}

