/************************************************************************
 * Copyright 2020-2021, Geometric Progress LLC
 *
 * This file is part of the Careful I/O Library (CIO).
 *
 * CIO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CIO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CIO.  If not, see <https://www.gnu.org/licenses/>
*************************************************************************/
#include "ScalarTest.h"
#include <cio/Buffer.h>
#include <iostream>
#include <cio/test/Assert.h>

namespace glitter
{
		ScalarTest::ScalarTest() : cio::test::Suite("cio/Scalar")
		{
			CIO_TEST_METHOD(ScalarTest, construct);
			CIO_TEST_METHOD(ScalarTest, assign);
			CIO_TEST_METHOD(ScalarTest, bind);
			CIO_TEST_METHOD(ScalarTest, value);
			CIO_TEST_METHOD(ScalarTest, elementSize);
			CIO_TEST_METHOD(ScalarTest, offset);
			CIO_TEST_METHOD(ScalarTest, stride);
			CIO_TEST_METHOD(ScalarTest, size);
		}
		ScalarTest::~ScalarTest() noexcept = default;

		void ScalarTest::constructTest()
		{
			//Default
			Scalar<dataType, chunkType> accessor;
			CIO_ASSERT_EQUAL(sizeof(dataType), accessor.getElementSize());
			CIO_ASSERT_EQUAL(0u, accessor.getOffset());
			CIO_ASSERT_EQUAL(sizeof(dataType), accessor.getStride());
			CIO_ASSERT_EQUAL(0u, accessor.size());
			CIO_ASSERT(!accessor.getData());

			//Construct with data
			dataType inputData[] = { 1, 2, 3, 4, 5, 6 };
			chunkType byteArray[sizeof(inputData)] = {};
			std::memcpy(byteArray, &inputData, sizeof(inputData));
			cio::Chunk<chunkType> chunk(byteArray, sizeof(byteArray), nullptr);

			Scalar<dataType, chunkType> accessor2(chunk);
			CIO_ASSERT_EQUAL(sizeof(dataType), accessor2.getElementSize());
			CIO_ASSERT_EQUAL(0u, accessor2.getOffset());
			CIO_ASSERT_EQUAL(sizeof(dataType), accessor2.getStride());
			CIO_ASSERT_EQUAL(sizeof(inputData) / sizeof(dataType), accessor2.size());
			CIO_ASSERT_EQUAL(accessor2.getData().size(), chunk.size());

			//Construct with data, size, elements, offset
			Scalar<dataType, chunkType> accessor3(chunk, sizeof(inputData) / sizeof(dataType), 0, sizeof(dataType), sizeof(dataType));
			CIO_ASSERT_EQUAL(sizeof(dataType), accessor3.getElementSize());
			CIO_ASSERT_EQUAL(0u, accessor3.getOffset());
			CIO_ASSERT_EQUAL(sizeof(dataType), accessor3.getStride());
			CIO_ASSERT_EQUAL(sizeof(inputData) / sizeof(dataType), accessor3.size());
			CIO_ASSERT_EQUAL(accessor3.getData().size(), chunk.size());

			//Copy Constructor
			Scalar<dataType, chunkType> accessor4(accessor3);
			CIO_ASSERT_EQUAL(sizeof(dataType), accessor4.getElementSize());
			CIO_ASSERT_EQUAL(0u, accessor4.getOffset());
			CIO_ASSERT_EQUAL(sizeof(dataType), accessor4.getStride());
			CIO_ASSERT_EQUAL(sizeof(inputData) / sizeof(dataType), accessor4.size());
			CIO_ASSERT_EQUAL(accessor4.getData().size(), chunk.size());

			//Copy, Different Types
			Scalar<std::uint8_t, chunkType> accessor5(accessor3);
			CIO_ASSERT_EQUAL(sizeof(dataType), accessor5.getElementSize());
			CIO_ASSERT_EQUAL(0u, accessor5.getOffset());
			CIO_ASSERT_EQUAL(sizeof(dataType), accessor5.getStride());
			CIO_ASSERT_EQUAL(sizeof(inputData) / sizeof(dataType), accessor5.size());
			CIO_ASSERT_EQUAL(*accessor5.getData().data(), *accessor3.getData().data());

			
			//Reference
			Scalar<dataType, chunkType> accessor6;
			accessor6 = accessor6.reference(accessor5);
			CIO_ASSERT_EQUAL(sizeof(dataType), accessor6.getElementSize());
			CIO_ASSERT_EQUAL(0u, accessor6.getOffset());
			CIO_ASSERT_EQUAL(sizeof(dataType), accessor6.getStride());
			CIO_ASSERT_EQUAL(sizeof(inputData) / sizeof(dataType), accessor6.size());
			CIO_ASSERT_EQUAL(accessor5.getData().data(), accessor6.getData().data());
			
		}

		void ScalarTest::assignTest()
		{
			//Generic
			Scalar<dataType, chunkType> accessor;
			Scalar<dataType, chunkType> accessor2 = accessor;
			CIO_ASSERT_EQUAL(accessor2.getElementSize(), accessor.getElementSize());
			CIO_ASSERT_EQUAL(accessor2.getOffset(), accessor.getOffset());
			CIO_ASSERT_EQUAL(accessor2.getStride(), accessor.getStride());
			CIO_ASSERT_EQUAL(accessor2.size(), accessor.size());
			CIO_ASSERT_EQUAL(accessor2.data(), accessor.data());

			//Add some data
			dataType inputData[] = { 1, 2, 3, 4, 5, 6 };
			chunkType byteArray[sizeof(inputData)] = {};
			std::memcpy(byteArray, &inputData, sizeof(inputData));
			cio::Chunk<chunkType> chunk(byteArray, sizeof(byteArray), nullptr);
			Scalar<dataType, chunkType> accessor3(chunk);
			accessor = accessor3;
			CIO_ASSERT_EQUAL(accessor3.getElementSize(), accessor.getElementSize());
			CIO_ASSERT_EQUAL(accessor3.getOffset(), accessor.getOffset());
			CIO_ASSERT_EQUAL(accessor3.getStride(), accessor.getStride());
			CIO_ASSERT_EQUAL(accessor3.size(), accessor.size());
			CIO_ASSERT_EQUAL(accessor3.data(), accessor.data());
		}

		void ScalarTest::bindTest()
		{
			//Create some data
			dataType inputData[] = { 1, 2, 3, 4, 5, 6 };

			//Setup the byte array.
			chunkType byteArray[sizeof(inputData)] = {};
			std::memcpy(byteArray, &inputData, sizeof(inputData));

			cio::Chunk<chunkType> chunk(byteArray, sizeof(byteArray), nullptr);

			Scalar<dataType, chunkType> accessor;
			accessor.bind(chunk);

			CIO_ASSERT_EQUAL(sizeof(inputData) / sizeof(dataType), accessor.size());
			CIO_ASSERT_EQUAL(0u, accessor.getOffset());
			CIO_ASSERT_EQUAL(sizeof(dataType), accessor.getStride());
			CIO_ASSERT_EQUAL(sizeof(dataType), accessor.getElementSize());
			CIO_ASSERT_EQUAL(chunk.size(), accessor.getData().size());
			CIO_ASSERT(*chunk.data() == *accessor.getData().data());
		}

		void ScalarTest::valueTest()
		{
			//Create some data
			dataType inputData[] = { 1, 2, 3, 4, 5, 6 };

			//Setup the byte array.
			chunkType byteArray[sizeof(inputData)] = {};
			std::memcpy(byteArray, &inputData, sizeof(inputData));

			//Set the byte array as a chunk of data.
			cio::Chunk<chunkType> chunk(byteArray, sizeof(inputData), nullptr);
			Scalar<dataType, chunkType> accessor(chunk);

			//Get and set without offset
			cio::Value<dataType> retResult = accessor.get(3);
			CIO_ASSERT_EQUAL(4, retResult);
			accessor.set(3, 7);
			retResult = accessor.get(3);
			CIO_ASSERT_EQUAL(7, retResult);

			//Get and set with offset (and trailing bytes)
			dataType data[10] = { 0, 0, 0, 1, 2, 3, 4, 5 };
			chunkType byteArray2[sizeof(data)] = {};
			std::memcpy(byteArray2, &data, sizeof(data));
			cio::Chunk<std::uint8_t> chunk2(byteArray2, sizeof(byteArray2), nullptr);

			Scalar<dataType, chunkType> accessor2(chunk2, 5, 3 * sizeof(dataType), sizeof(dataType), sizeof(dataType));
			retResult = accessor2.get(3);
			CIO_ASSERT_EQUAL(4, retResult);
			accessor2.set(3, 7);
			retResult = accessor2.get(3);
			CIO_ASSERT_EQUAL(7, retResult);

			//Get and set with offset and custom stride
			accessor2.setStride(8);
			retResult = accessor2.get(1);
			CIO_ASSERT_EQUAL(3, retResult);
			accessor2.set(2, 7);
			retResult = accessor2.get(2);
			CIO_ASSERT_EQUAL(7, retResult);
		}

		void ScalarTest::elementSizeTest()
		{
			Scalar<std::uint16_t, void> mScalarData;
			CIO_ASSERT_EQUAL(sizeof(std::uint16_t), mScalarData.getElementSize());

			mScalarData.setElementSize(sizeof(float));
			CIO_ASSERT_EQUAL(sizeof(float), mScalarData.getElementSize());
		}

		void ScalarTest::offsetTest()
		{
			Scalar<std::uint32_t, void> mScalarData;
			CIO_ASSERT_EQUAL(0u, mScalarData.getOffset());

			mScalarData.setOffset(8);
			CIO_ASSERT_EQUAL(8u, mScalarData.getOffset());
		}

		void ScalarTest::strideTest()
		{
			Scalar<double, void> mScalarData;
			CIO_ASSERT_EQUAL(sizeof(double), mScalarData.getStride());

			mScalarData.setStride(sizeof(float));
			CIO_ASSERT_EQUAL(sizeof(float), mScalarData.getStride());
		}

		void ScalarTest::sizeTest()
		{
			//Default
			Scalar<dataType, chunkType> accessor;
			CIO_ASSERT_EQUAL(0u, accessor.size());
			accessor.resize(10);
			CIO_ASSERT_EQUAL(10u, accessor.size());

			//Create some data
			dataType inputData[] = { 1, 2, 3, 4, 5, 6 };
			//Setup the byte array.
			chunkType byteArray[sizeof(inputData)] = {};
			std::memcpy(byteArray, &inputData, sizeof(inputData));
			std::size_t size = sizeof(byteArray);
			//Set the byte array as a chunk of data.
			cio::Chunk<chunkType> chunk(byteArray, sizeof(inputData), nullptr);
			accessor.bind(chunk);
			CIO_ASSERT_EQUAL(6u, accessor.size());
		}
}

