/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "ArrayTest.h"
#include "MetadataReaderTest.h"
#include "ModelTest.h"
#include "SampleMetadata.h"

#include <cio/test/Reporter.h>
#include <cio/Path.h>
#include <cio/Protocol.h>

#include <iostream>

int main(int argc, char **argv)
{
	cio::test::Reporter logger;

	cio::Path dataDir;
	int argn = 1;
	while (argn < argc)
	{
		if (std::strcmp(argv[argn], "--generate-bin") == 0)
		{
			if (++argn < argc)
			{
				cio::Path outputBin = argv[argn];
				if (!outputBin.getProtocol())
				{
					outputBin = cio::Path::fromNativeFile(argv[argn]);
				}

				glitter::SampleMetadata::generateBinFile(outputBin);
			}
		}
		else
		{
			dataDir = argv[argn];
		}
		++argn;
	}

	cio::test::Suite suite;
	suite.addTestCase(glitter::ArrayTest());
	suite.addTestCase(glitter::MetadataReaderTest(dataDir));
	suite.addTestCase(glitter::ModelTest(dataDir));
	cio::Severity result = suite.execute(logger);

	return result >= cio::Severity::Error;
}
