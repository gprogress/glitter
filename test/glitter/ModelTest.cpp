/*=====================================================================================================================
 * Copyright 2020-2023 Geometric Progress LLC
 *
 * This file is part of the Glitter project.
 *
 * Glitter is free software : you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Glitter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Glitter. If not, see < https://www.gnu.org/licenses/ >.
=====================================================================================================================*/
#include "ModelTest.h"

#include "MetadataReaderTest.h"
#include "SampleMetadata.h"

#include <glitter/Model.h>
#include <glitter/Print.h>

#include <geas/Entity.h>

#include <cio/Buffer.h>
#include <cio/Primitive.h>
#include <cio/Protocol.h>
#include <cio/ProtocolFactory.h>
#include <cio/Class.h>

#include <cio/Filesystem.h>

#include <fx/gltf.h>
#include <nlohmann/json.hpp>

#include <fstream>

namespace glitter
{
	cio::Class<ModelTest> ModelTest::sMetaclass("glitter::ModelTest");

	void ModelTest::createSampleBuffers(Model &model)
	{
		fx::gltf::Document *document = model.getOrCreateDocument();
		document->buffers.resize(4);

		// Test a normal buffer
		document->buffers[0].byteLength = 12288;
		document->buffers[0].data.resize(12288);

		// Test smallest possible aligned buffer
		document->buffers[1].byteLength = 4;
		document->buffers[1].data.resize(4);

		// leave buffer 2 empty

		// Test case where byte length is smaller than actual data content (shouldn't happen but still)
		document->buffers[3].byteLength = 450;
		document->buffers[3].data.resize(468);
	}

	void ModelTest::createSampleViews(Model &model)
	{
		fx::gltf::Document *document = model.getOrCreateDocument();
		document->bufferViews.resize(9);

		// Intentionally setting up views to reference buffers out of order so we don't make the assumption of buffer index always increasing
		// Buffer 1 is set up notionally for an array of UINT8 index values
		document->bufferViews[0].buffer = 1;
		document->bufferViews[0].byteLength = 4;
		document->bufferViews[0].target = fx::gltf::BufferView::TargetType::ElementArrayBuffer;

		// Test view set up with invalid buffer
		document->bufferViews[1].buffer = -1;
		document->bufferViews[1].byteLength = 2000;

		// Buffer 0 is set up notionally for interleaved position and normal data stored as Vec3s
		document->bufferViews[2].buffer = 0;
		document->bufferViews[2].byteLength = 12288;
		document->bufferViews[2].byteStride = 12;
		document->bufferViews[2].target = fx::gltf::BufferView::TargetType::ArrayBuffer;

		// Buffer 3 is set up notionally for various feature attribute values, it will use different offsets and access pattern through views 3-5
		document->bufferViews[3].buffer = 3;
		document->bufferViews[3].byteLength = 24;
		document->bufferViews[3].byteOffset = 0;

		document->bufferViews[4].buffer = 3;
		document->bufferViews[4].byteLength = 32;
		document->bufferViews[4].byteOffset = 412;

		document->bufferViews[5].buffer = 3;
		document->bufferViews[5].byteLength = 16;
		document->bufferViews[5].byteOffset = 48;

		// Sprinkle in a reference to a nonexistent buffer
		document->bufferViews[6].buffer = 5;
		document->bufferViews[6].byteLength = 200;
		document->bufferViews[6].byteOffset = 4096;

		// Go back to Buffer 3 for more attributes, just for fun set a stride and a target so we can interpret this as two sets of normalized texture coordinate int16 pairs
		document->bufferViews[7].buffer = 3;
		document->bufferViews[7].byteLength = 64;
		document->bufferViews[7].byteOffset = 128;
		document->bufferViews[7].target = fx::gltf::BufferView::TargetType::ArrayBuffer;
		document->bufferViews[7].byteStride = 4;

		// Reference empty buffer 2
		document->bufferViews[8].buffer = 2;
		document->bufferViews[8].byteLength = 0;
	}

	void ModelTest::createSampleAccessors(Model &model)
	{
		fx::gltf::Document *document = model.getOrCreateDocument();
		document->accessors.resize(8);

		// Accessor 0 - let's do a strided vec3f access to buffer view 2
		document->accessors[0].bufferView = 2;
		document->accessors[0].count = 512;
		document->accessors[0].type = fx::gltf::Accessor::Type::Vec3;
		document->accessors[0].componentType = fx::gltf::Accessor::ComponentType::Float;

		// Accessor 1 - let's do the other strided vec3f access to buffer view 2
		document->accessors[1].bufferView = 2;
		document->accessors[1].count = 512;
		document->accessors[1].byteOffset = 12;
		document->accessors[1].type = fx::gltf::Accessor::Type::Vec3;
		document->accessors[1].componentType = fx::gltf::Accessor::ComponentType::Float;

		// Accesor 2 - let's do 4 bytes of uint8 access to buffer view 0
		document->accessors[2].bufferView = 0;
		document->accessors[2].count = 4;
		document->accessors[2].type = fx::gltf::Accessor::Type::Scalar;
		document->accessors[2].componentType = fx::gltf::Accessor::ComponentType::UnsignedByte;

		// Accessor 3 - let's do a strided normalized uint16 vec2 access to buffer view 7
		document->accessors[3].bufferView = 7;
		document->accessors[3].count = 8;
		document->accessors[3].type = fx::gltf::Accessor::Type::Vec2;
		document->accessors[3].componentType = fx::gltf::Accessor::ComponentType::UnsignedShort;
		document->accessors[3].normalized = true;

		// Accessor 4 - let's do the other strided normalized int16 vec2 access to buffer view 7 (notice signed instead of unsigned)
		document->accessors[4].bufferView = 7;
		document->accessors[4].count = 8;
		document->accessors[4].type = fx::gltf::Accessor::Type::Vec2;
		document->accessors[4].byteOffset = 4;
		document->accessors[4].componentType = fx::gltf::Accessor::ComponentType::Short;
		document->accessors[4].normalized = true;

		// Accessor 5 - reference invalid buffer view -1
		document->accessors[5].bufferView = -1;
		document->accessors[5].count = 256;
		document->accessors[5].byteOffset = 24;
		document->accessors[5].type = fx::gltf::Accessor::Type::Vec3;
		document->accessors[5].componentType = fx::gltf::Accessor::ComponentType::Float;

		// Accessor 6 - reference empty buffer view 8
		document->accessors[6].bufferView = 8;
		document->accessors[6].count = 0;
		document->accessors[6].byteOffset = 0;
		document->accessors[6].type = fx::gltf::Accessor::Type::Vec3;
		document->accessors[6].componentType = fx::gltf::Accessor::ComponentType::Float;

		// Accessor 7 - reference nonexistent buffer view 9
		document->accessors[7].bufferView = 9;
		document->accessors[7].count = 16;
		document->accessors[7].byteOffset = 512;
		document->accessors[7].type = fx::gltf::Accessor::Type::Vec2;
		document->accessors[7].componentType = fx::gltf::Accessor::ComponentType::Short;
	}

	ModelTest::ModelTest() :
		cio::test::Suite("glitter/Model")
	{
		this->bindTestMethods();
	}

	ModelTest::ModelTest(cio::Path dataDir) :
		cio::test::Suite("glitter/Model"),
		mDataDir(std::move(dataDir))
	{
		this->bindTestMethods();
	}

	ModelTest::~ModelTest() noexcept = default;

	void ModelTest::bindTestMethods()
	{
		// Core access methods to load glTF and work with buffers
		this->addTestCase("loadFromFile", &ModelTest::testLoadFromFile,  0);
		this->addTestCase("getBuffer", &ModelTest::testGetBuffer, 0);
		this->addTestCase("getBufferView", &ModelTest::testGetBufferView, 0);
		this->addTestCase("getDataView", &ModelTest::testGetDataView, 0);

		// getEntityData requires the core access methods to be working to be useful
		this->addTestCase("getEntityData", &ModelTest::testGetEntityData, 1);
	}

	const cio::Metaclass &ModelTest::getMetaclass() const noexcept
	{
		return sMetaclass;
	}

	cio::Severity ModelTest::setUp()
	{
		cio::Filesystem fs;
		cio::Path dataDir = mDataDir;
		if (!dataDir)
		{
			cio::Path appDir = fs.getApplicationDirectory();
			dataDir = appDir / "../share/test/glitter";
		}

		cio::Path sampleModel = dataDir / "SampleMetadata.gltf";
		mSampleModelFile = sampleModel.toNativeFile();

		cio::Path sampleBin = dataDir / "SampleMetadata.bin";
		mSampleModelBinFile = sampleBin.toNativeFile();

		CIO_ASSERT_EQUAL(cio::Resource::File, fs.getNativeType(mSampleModelFile.c_str()));
		CIO_ASSERT_EQUAL(cio::Resource::File, fs.getNativeType(mSampleModelBinFile.c_str()));

		return cio::Severity::None;
	}

	void ModelTest::testLoadFromFile()
	{
		// Temporary: do a JSON load test to see if we messed up raw formatting on sample data
		nlohmann::json parsed;
		std::ifstream f(mSampleModelFile.c_str());
		f >> parsed;

		Model model;
		model.loadFromFile(mSampleModelFile);

		this->assertSampleModel(model);
	}

	void ModelTest::testGetBuffer()
	{
		Model model;

		// Test getting a buffer on a null document
		{
			cio::Chunk<void> chunk = model.getBuffer(0);
			CIO_ASSERT(!chunk.data());
			CIO_ASSERT_EQUAL(0u, chunk.size());
		}

		fx::gltf::Document *document = model.getOrCreateDocument();
		
		// Test getting a buffer on an empty document
		{
			cio::Chunk<void> chunk = model.getBuffer(0);
			CIO_ASSERT(!chunk.data());
			CIO_ASSERT_EQUAL(0u, chunk.size());
		}

		ModelTest::createSampleBuffers(model);

		// Test buffer 0
		{
			cio::Chunk<void> chunk = model.getBuffer(0);
			CIO_ASSERT_EQUAL((void *) document->buffers[0].data.data(), chunk.data());
			CIO_ASSERT_EQUAL(12288u, chunk.size());
			CIO_ASSERT(!chunk.allocator());
		}

		// Test buffer 1
		{
			cio::Chunk<void> chunk = model.getBuffer(1);
			CIO_ASSERT_EQUAL((void *) document->buffers[1].data.data(), chunk.data());
			CIO_ASSERT_EQUAL(4u, chunk.size());
			CIO_ASSERT(!chunk.allocator());
		}

		// Test buffer 2
		{
			cio::Chunk<void> chunk = model.getBuffer(2);
			CIO_ASSERT_EQUAL((void *) nullptr, chunk.data());
			CIO_ASSERT_EQUAL(0u, chunk.size());
		}

		// Test buffer 3
		{
			cio::Chunk<void> chunk = model.getBuffer(3);
			CIO_ASSERT_EQUAL((void *) document->buffers[3].data.data(), chunk.data());
			CIO_ASSERT_EQUAL(450u, chunk.size());
			CIO_ASSERT(!chunk.allocator());
		}

		// Test buffer 4 (does not exist!)
		{
			cio::Chunk<void> chunk = model.getBuffer(4);
			CIO_ASSERT_EQUAL((void *) nullptr, chunk.data());
			CIO_ASSERT_EQUAL(0u, chunk.size());
		}

		// Test invalid buffer -1
		{
			cio::Chunk<void> chunk = model.getBuffer(-1);
			CIO_ASSERT_EQUAL((void *) nullptr, chunk.data());
			CIO_ASSERT_EQUAL(0u, chunk.size());
		}
	}

	void ModelTest::testGetBufferView()
	{
		Model model;

		// Test getting a buffer view on a null document
		{
			cio::Chunk<void> chunk = model.getBufferView(0);
			CIO_ASSERT(!chunk.data());
			CIO_ASSERT_EQUAL(0u, chunk.size());
		}

		fx::gltf::Document *document = model.getOrCreateDocument();

		// Test getting a buffer view on an empty document
		{
			cio::Chunk<void> chunk = model.getBufferView(0);
			CIO_ASSERT(!chunk.data());
			CIO_ASSERT_EQUAL(0u, chunk.size());
		}

		ModelTest::createSampleBuffers(model);
		ModelTest::createSampleViews(model);

		// Test buffer view 0 - should pull in 4 bytes from buffer 1
		{
			cio::Chunk<void> chunk = model.getBufferView(0);
			CIO_ASSERT_EQUAL((void *) document->buffers[1].data.data(), chunk.data());
			CIO_ASSERT_EQUAL(4u, chunk.size());
			CIO_ASSERT(!chunk.allocator());
		}

		// Test buffer view 1 - should be null since buffer view 1 itself is misconfigured
		{
			cio::Chunk<void> chunk = model.getBufferView(1);
			CIO_ASSERT(!chunk.data());
			CIO_ASSERT_EQUAL(0u, chunk.size());
		}

		// Test buffer view 2 - should reference all of buffer 0
		{
			cio::Chunk<void> chunk = model.getBufferView(2);
			CIO_ASSERT_EQUAL((void *) document->buffers[0].data.data(), chunk.data());
			CIO_ASSERT_EQUAL(12288u, chunk.size());
			CIO_ASSERT(!chunk.allocator());
		}

		// Test buffer view 3 - should reference buffer 3 bytes [0, 24)
		{
			cio::Chunk<void> chunk = model.getBufferView(3);
			CIO_ASSERT_EQUAL((void *)document->buffers[3].data.data(), chunk.data());
			CIO_ASSERT_EQUAL(24u, chunk.size());
			CIO_ASSERT(!chunk.allocator());
		}

		// Test buffer view 4 - should reference buffer 3 bytes [412, 444)
		{
			cio::Chunk<void> chunk = model.getBufferView(4);
			CIO_ASSERT_EQUAL((void *)(document->buffers[3].data.data() + 412), chunk.data());
			CIO_ASSERT_EQUAL(32u, chunk.size());
			CIO_ASSERT(!chunk.allocator());
		}

		// Test buffer view 5 - should reference buffer 3 bytes [48, 64)
		{
			cio::Chunk<void> chunk = model.getBufferView(5);
			CIO_ASSERT_EQUAL((void *)(document->buffers[3].data.data() + 48), chunk.data());
			CIO_ASSERT_EQUAL(16u, chunk.size());
			CIO_ASSERT(!chunk.allocator());
		}

		// Test buffer view 6 - should be null since buffer view 6 references a nonexistent buffer
		{
			cio::Chunk<void> chunk = model.getBufferView(6);
			CIO_ASSERT(!chunk.data());
			CIO_ASSERT_EQUAL(0u, chunk.size());
		}

		// Test buffer view 7 - should reference buffer 3 bytes [128, 192)
		{
			cio::Chunk<void> chunk = model.getBufferView(7);
			CIO_ASSERT_EQUAL((void *)(document->buffers[3].data.data() + 128), chunk.data());
			CIO_ASSERT_EQUAL(64u, chunk.size());
			CIO_ASSERT(!chunk.allocator());
		}

		// Test buffer view 8 - should be null because view 8 references empty view 1
		{
			cio::Chunk<void> chunk = model.getBufferView(8);
			CIO_ASSERT(!chunk.data());
			CIO_ASSERT_EQUAL(0u, chunk.size());
		}
	}

	void ModelTest::testGetDataView()
	{
		Model model;

		// Test getting an accessor on a null document
		{
			Array chunk = model.getDataView(0);
			CIO_ASSERT_EQUAL(0u, chunk.size());
		}

		fx::gltf::Document *document = model.getOrCreateDocument();

		// Test getting an accessor on an empty document
		{
			Array chunk = model.getDataView(0);
			CIO_ASSERT_EQUAL(0u, chunk.size());
		}

		ModelTest::createSampleBuffers(model);
		ModelTest::createSampleViews(model);
		ModelTest::createSampleAccessors(model);

		// Test getting accessor 0
		{
			Array chunk = model.getDataView(0);
			CIO_ASSERT_EQUAL(cio::Type::Real, chunk.getType());
			CIO_ASSERT_EQUAL(3u, chunk.getElementCount());
			CIO_ASSERT_EQUAL(512u, chunk.size());
			CIO_ASSERT_EQUAL(cio::Encoding::create<float>(), chunk.getEncoding());
			CIO_ASSERT_EQUAL(4u, chunk.getElementStride());
			CIO_ASSERT_EQUAL(24u, chunk.getStride());
			CIO_ASSERT_EQUAL(0u, chunk.getInitialOffset());
			CIO_ASSERT_EQUAL((void *) document->buffers[0].data.data(), chunk.getBuffer().data());
		}

		// Test getting accessor 1 - same layout as 0, but offset of 12
		{
			Array chunk = model.getDataView(1);
			CIO_ASSERT_EQUAL(cio::Type::Real, chunk.getType());
			CIO_ASSERT_EQUAL(3u, chunk.getElementCount());
			CIO_ASSERT_EQUAL(512u, chunk.size());
			CIO_ASSERT_EQUAL(cio::Encoding::create<float>(), chunk.getEncoding());
			CIO_ASSERT_EQUAL(4u, chunk.getElementStride());
			CIO_ASSERT_EQUAL(24u, chunk.getStride());
			CIO_ASSERT_EQUAL(12u, chunk.getInitialOffset());
			CIO_ASSERT_EQUAL((void *) document->buffers[0].data.data(), chunk.getBuffer().data());
		}

		// Test getting accessor 2 - 4 bytes of scalar uint8 access to buffer view 0
		{
			Array chunk = model.getDataView(2);
			CIO_ASSERT_EQUAL(cio::Type::Integer, chunk.getType());
			CIO_ASSERT_EQUAL(1u, chunk.getElementCount());
			CIO_ASSERT_EQUAL(4u, chunk.size());
			CIO_ASSERT_EQUAL(cio::Encoding::create<std::uint8_t>(), chunk.getEncoding());
			CIO_ASSERT_EQUAL(1u, chunk.getElementStride());
			CIO_ASSERT_EQUAL(1u, chunk.getStride());
			CIO_ASSERT_EQUAL(0u, chunk.getInitialOffset());
			CIO_ASSERT_EQUAL((void *)document->buffers[1].data.data(), chunk.getBuffer().data());
		}

		// Test getting accessor 3 - normalized uint16 strided access to buffer view 3
		{
			Array chunk = model.getDataView(3);
			CIO_ASSERT_EQUAL(cio::Type::Real, chunk.getType());
			CIO_ASSERT_EQUAL(2u, chunk.getElementCount());
			CIO_ASSERT_EQUAL(8u, chunk.size());
			CIO_ASSERT_EQUAL(cio::Encoding::createNormalized<std::uint16_t>(), chunk.getEncoding());
			CIO_ASSERT_EQUAL(2u, chunk.getElementStride());
			CIO_ASSERT_EQUAL(8u, chunk.getStride());
			CIO_ASSERT_EQUAL(0u, chunk.getInitialOffset());
			CIO_ASSERT_EQUAL((void *) (document->buffers[3].data.data() + 128), chunk.getBuffer().data());
		}

		// Test getting accessor 4 - same as accessor 3, but signed and the other part of the stride
		{
			Array chunk = model.getDataView(4);
			CIO_ASSERT_EQUAL(cio::Type::Real, chunk.getType());
			CIO_ASSERT_EQUAL(2u, chunk.getElementCount());
			CIO_ASSERT_EQUAL(8u, chunk.size());
			CIO_ASSERT_EQUAL(cio::Encoding::createNormalized<std::int16_t>(), chunk.getEncoding());
			CIO_ASSERT_EQUAL(2u, chunk.getElementStride());
			CIO_ASSERT_EQUAL(8u, chunk.getStride());
			CIO_ASSERT_EQUAL(4u, chunk.getInitialOffset());
			CIO_ASSERT_EQUAL((void *)(document->buffers[3].data.data() + 128), chunk.getBuffer().data());
		}

		// TODO accessors 5-8 are invalid cases, test them eventually
	}

	void ModelTest::testGetEntityData()
	{
		// @pre testLoadFromFile has passed
		Model model;

		// Test case of doing this on an empty model
		{
			// Test invalid ID
			{
				EntityWeight entity = model.getEntityData(cio::UniqueId(), 0);
				CIO_ASSERT(!entity.value);
				CIO_ASSERT_EQUAL(0u, entity.count);
				CIO_ASSERT_EQUAL(0u, entity.total);

				std::vector<EntityWeight> entities = model.getEntityData(cio::UniqueId());
				CIO_ASSERT_EQUAL(0u, entities.size());
			}
			
			// Test a made-up ID that doesn't actually exist
			{
				cio::UniqueId id = model.makePrimitiveIndexId(0, 0, 0);
				EntityWeight entity = model.getEntityData(id, 0);
				CIO_ASSERT(!entity.value);
				CIO_ASSERT_EQUAL(0u, entity.count);
				CIO_ASSERT_EQUAL(0u, entity.total);

				std::vector<EntityWeight> entities = model.getEntityData(id);
				CIO_ASSERT_EQUAL(0u, entities.size());
			}
		}

		model.loadFromFile(mSampleModelFile);

		// First round of test cases, test EntityWeight getEntityData(const cio::UniqueId &id, std::uint32_t layer)
		// Each of these tests will fully validate the returned entity reference and weights
		{
			// Case 0: give it the empty UniqueId, get back null
			{
				EntityWeight entity = model.getEntityData(cio::UniqueId(), 0);
				CIO_ASSERT(!entity.value);
				CIO_ASSERT_EQUAL(0u, entity.count);
				CIO_ASSERT_EQUAL(0u, entity.total);
			}

			// Case 1: let's test getting entities with their own ID
			{
				for (unsigned i = 0; i < SampleMetadata::Buildings::count; ++i)
				{		
					cio::UniqueId id = model.makeEntityId("buildings", i);
					EntityWeight entity = model.getEntityData(id, 0);
					CIO_ASSERT(entity.value.getSchema() == model.getCatalog()->getApplicationSchema());
					CIO_ASSERT(entity.value.getNode());
					CIO_ASSERT_EQUAL(i, entity.value.getId());

					const geas::Entity *type = entity.value.getType();
					CIO_ASSERT(type);
					CIO_ASSERT_EQUAL("building", type->getLabel());

					CIO_ASSERT_EQUAL(4u, entity.value.getAttributeCount());
					CIO_ASSERT_EQUAL(4u, type->getAttributes().size());
				}
			}

			// Case 2: let's test the attribute table (buildings)
			{
				for (unsigned i = 0; i < SampleMetadata::Vertex::count; ++i)
				{
					unsigned v = SampleMetadata::Vertex::indices[i];
					std::uint32_t fid = SampleMetadata::VertexAttribute::fids[v];

					cio::UniqueId id = model.makePrimitiveIndexId(0, 0, i);
					EntityWeight entity = model.getEntityData(id, 0);
					CIO_ASSERT(entity.value.getSchema() == model.getCatalog()->getApplicationSchema());
					CIO_ASSERT(entity.value.getNode());
					CIO_ASSERT_EQUAL(fid, entity.value.getId());
					const geas::Entity *type = entity.value.getType();
					CIO_ASSERT(type);
					CIO_ASSERT_EQUAL("building", type->getLabel());

					CIO_ASSERT_EQUAL(4u, entity.value.getAttributeCount());
					CIO_ASSERT_EQUAL(4u, type->getAttributes().size());

					Array height = entity.value.getValue(geas::Label("height"));
					CIO_ASSERT_EQUAL(SampleMetadata::Buildings::heights[fid], height.getBinary<double>());

					Array addresses = entity.value.getValue(geas::Label("address"));
					// TODO until text attributes are implemented, this will return the empty string, we test here just to make sure it doesn't crash
					CIO_ASSERT_EQUAL("", addresses.getText(0));

					Array coords = entity.value.getValue(geas::Label("coordinates"));
					CIO_ASSERT_EQUAL(2u, coords.getElementCount());
					CIO_ASSERT_EQUAL(SampleMetadata::Buildings::coordinates[fid][0], coords.getBinary<double>(0, 0));
					CIO_ASSERT_EQUAL(SampleMetadata::Buildings::coordinates[fid][1], coords.getBinary<double>(0, 1));

					Array occupants = entity.value.getValue(geas::Label("occupants"));
					CIO_ASSERT_EQUAL(1u, occupants.getElementCount());
					CIO_ASSERT_EQUAL(SampleMetadata::Buildings::occupants[fid], occupants.getBinary<std::uint32_t>());
				}
			}

			// Case 2: let's test the procedural table (elevations)
			{
				for (unsigned i = 0; i < SampleMetadata::Vertex::count; ++i)
				{
					cio::UniqueId id = model.makePrimitiveIndexId(0, 0, i);
					EntityWeight entity = model.getEntityData(id, 1);
					std::uint32_t fid = i / 2;
					CIO_ASSERT(entity.value.getSchema() == model.getCatalog()->getApplicationSchema());
					CIO_ASSERT(entity.value.getNode());
					CIO_ASSERT_EQUAL(fid, entity.value.getId());
					const geas::Entity *type = entity.value.getType();
					CIO_ASSERT(type);
					CIO_ASSERT_EQUAL("elevation", type->getLabel());

					CIO_ASSERT_EQUAL(1u, entity.value.getAttributeCount());
					CIO_ASSERT_EQUAL(2u, type->getAttributes().size());

					std::uint8_t elev = entity.value.getValue(geas::Label("elevation")).getBinary<std::uint8_t>();
					CIO_ASSERT_EQUAL(static_cast<std::uint32_t>(SampleMetadata::Elevations::elevations[fid]), static_cast<std::uint32_t>(elev));
				}
			}

			// Case 3: let's test the constant table (cars)
			{
				for (unsigned i = 0; i < SampleMetadata::Vertex::count; ++i)
				{
					cio::UniqueId id = model.makePrimitiveIndexId(0, 0, i);
					EntityWeight entity = model.getEntityData(id, 2);
					CIO_ASSERT(entity.value.getSchema() == model.getCatalog()->getApplicationSchema());
					CIO_ASSERT(entity.value.getNode());
					CIO_ASSERT_EQUAL(2, entity.value.getId());

					const geas::Entity *type = entity.value.getType();
					CIO_ASSERT(type);
					CIO_ASSERT_EQUAL("car", type->getLabel());

					CIO_ASSERT_EQUAL(2u, entity.value.getAttributeCount());
					CIO_ASSERT_EQUAL(6u, type->getAttributes().size());

					Array passengers = entity.value.getValue(geas::Label("passengers"));
					// TODO we support neither text nor variable-length arrays yet, so for now it is the empty string but we test to make sure it doesn't crash
					CIO_ASSERT_EQUAL("", passengers.getText(0));

					Array isElectric = entity.value.getValue(geas::Label("isElectric"));
					// TODO we do not yet support unaligned reads such as Boolean bits, the method is hard-coded to return 0 in this case
					CIO_ASSERT_EQUAL(0, isElectric.getBinary<int>());

					// The "car" entity has an attribute color, but it doesn't have actual values
					// Verify we get back the empty value array in this case
					Array color = entity.value.getValue(geas::Label("color"));
					CIO_ASSERT_EQUAL(0u, color.size());
				}
			}

			// We have tested all modes for vertex index support, now let's test triangle level query
			{
				// Buildings with per-vertex feature IDs
				{
					// test triangle 0
					// this is vertices (0, 1, 4) that map to features (7, 2, 2)
					cio::UniqueId id = model.makePrimitiveId(0, 0, 0);
					EntityWeight entity = model.getEntityData(id, 0);
					CIO_ASSERT_EQUAL(2u, entity.value.getId());
					CIO_ASSERT_EQUAL(2u, entity.count);
					CIO_ASSERT_EQUAL(3u, entity.total);

					// test triangle 1
					// this is vertices (1, 2, 4) that map to features (2, 0, 2)
					id = model.makePrimitiveId(0, 0, 1);
					entity = model.getEntityData(id, 0);
					CIO_ASSERT_EQUAL(2u, entity.value.getId());
					CIO_ASSERT_EQUAL(2u, entity.count);
					CIO_ASSERT_EQUAL(3u, entity.total);

					// test triangle 2
					// this is vertices (2, 3, 4) that map to features (0, 1, 2)
					id = model.makePrimitiveId(0, 0, 2);
					entity = model.getEntityData(id, 0);
					CIO_ASSERT_EQUAL(0u, entity.value.getId());
					CIO_ASSERT_EQUAL(1u, entity.count);
					CIO_ASSERT_EQUAL(3u, entity.total);

					// test triangle 3
					// this is vertices (3, 0, 4) that map to features (1, 2, 7)
					id = model.makePrimitiveId(0, 0, 3);
					entity = model.getEntityData(id, 0);
					CIO_ASSERT_EQUAL(1u, entity.value.getId());
					CIO_ASSERT_EQUAL(1u, entity.count);
					CIO_ASSERT_EQUAL(3u, entity.total);
				}

				// Elevations have sequential IDs, so every triangle will just have the feature ID of its first vertex at weight 1/3
				{
				for (unsigned i = 2; i < SampleMetadata::Vertex::count; i += 3)
				{
					cio::UniqueId id = model.makePrimitiveId(0, 0, i / 3);
					EntityWeight entity = model.getEntityData(id, 1);
					std::uint32_t efid = (i - 1) / 2; // will round up or down as appropriate
					CIO_ASSERT_EQUAL(efid, entity.value.getId());
					CIO_ASSERT_EQUAL(2u, entity.count);
					CIO_ASSERT_EQUAL(3u, entity.total);
				}
				}

				// Cars have a constant procedural ID, so every triangle will have that ID at weight 3/3
				{
					for (unsigned i = 2; i < SampleMetadata::Vertex::count; i += 3)
					{
						cio::UniqueId id = model.makePrimitiveId(0, 0, i / 3);
						EntityWeight entity = model.getEntityData(id, 2);
						CIO_ASSERT_EQUAL(2u, entity.value.getId());
						CIO_ASSERT_EQUAL(3u, entity.count);
						CIO_ASSERT_EQUAL(3u, entity.total);
					}
				}
			}

			// Now let's try primitive set level query
			{
			cio::UniqueId id = model.makePrimitiveSetId(0, 0);
			// For buildings per-vertex features, the only repeated one is FID #2 which is repeated on 6 out of 12 vertex indices
			{
				EntityWeight entity = model.getEntityData(id, 0);
				CIO_ASSERT_EQUAL(2u, entity.value.getId());
				CIO_ASSERT_EQUAL(6u, entity.count);
				CIO_ASSERT_EQUAL(12u, entity.total);
			}

			// For elevations procedurally generated, every FID is unique so the primitive set result is FID #0 with 1 out of 12 vertex indices
			{
				EntityWeight entity = model.getEntityData(id, 1);
				CIO_ASSERT_EQUAL(0u, entity.value.getId());
				CIO_ASSERT_EQUAL(1u, entity.count);
				CIO_ASSERT_EQUAL(12u, entity.total);
			}

			// Cars are a constant FID, so every vertex has that ID
			{
				EntityWeight entity = model.getEntityData(id, 2);
				CIO_ASSERT_EQUAL(2u, entity.value.getId());
				CIO_ASSERT_EQUAL(12u, entity.count);
				CIO_ASSERT_EQUAL(12u, entity.total);
			}
			}

			// Now let's try mesh level query
			// Currently since the mesh only has one primitive set, these have the same results as primitive set
			{
				cio::UniqueId id = model.makeMeshId(0);
				// For buildings per-vertex features, the only repeated one is FID #2 which is repeated on 6 out of 12 vertex indices
				{
					EntityWeight entity = model.getEntityData(id, 0);
					CIO_ASSERT_EQUAL(2u, entity.value.getId());
					CIO_ASSERT_EQUAL(6u, entity.count);
					CIO_ASSERT_EQUAL(12u, entity.total);
				}

				// For elevations procedurally generated, every FID is unique so the primitive set result is FID #0 with 1 out of 12 vertex indices
				{
					EntityWeight entity = model.getEntityData(id, 1);
					CIO_ASSERT_EQUAL(0u, entity.value.getId());
					CIO_ASSERT_EQUAL(1u, entity.count);
					CIO_ASSERT_EQUAL(12u, entity.total);
				}

				// Cars are a constant FID, so every vertex has that ID
				{
					EntityWeight entity = model.getEntityData(id, 2);
					CIO_ASSERT_EQUAL(2u, entity.value.getId());
					CIO_ASSERT_EQUAL(12u, entity.count);
					CIO_ASSERT_EQUAL(12u, entity.total);
				}
			}
		}

		// Second round of test cases, test std::vector<EntityWeight> getEntityData(const cio::UniqueId &id)
		// This test just validates that the std::vector<EntityWeight> is equivalent to calling each layer individually and then putting them in a vector
		{
			// Case 0: give it the empty UniqueId, get back empty vector
			{
				std::vector<EntityWeight> entities = model.getEntityData(cio::UniqueId());
				CIO_ASSERT_EQUAL(0u, entities.size());
			}

			// Case 1: let's test getting entities with their own ID
			{
				for (unsigned i = 0; i < SampleMetadata::Buildings::count; ++i)
				{
					cio::UniqueId id = model.makeEntityId("buildings", i);
					EntityWeight expected = model.getEntityData(id, 0);
					std::vector<EntityWeight> entities = model.getEntityData(id);
					CIO_ASSERT_EQUAL(1u, entities.size());
					CIO_ASSERT_EQUAL(expected, entities.front());
				}
			}

			// Case 2: let's test the vertex index values of the primitive set
			{
				for (unsigned i = 0; i < SampleMetadata::Vertex::count; ++i)
				{
					std::uint32_t expectedLayers = 3;
					cio::UniqueId id = model.makePrimitiveIndexId(0, 0, i);

					
					std::vector<EntityWeight> entities = model.getEntityData(id);
					CIO_ASSERT_EQUAL(expectedLayers, entities.size());
					for (std::uint32_t n = 0; n < expectedLayers; ++n)
					{
						CIO_ASSERT_EQUAL(model.getEntityData(id, n), entities[n]);
					}
				}
			}
		}
	}

	void ModelTest::assertSampleModel(const Model &model) const
	{
		CIO_ASSERT(model.getDocument());
		this->assertDocumentContent(*model.getDocument());

		CIO_ASSERT(model.getCatalog());
		CIO_ASSERT(model.getEntityCollection());

		MetadataReaderTest readerTest;
		readerTest.assertFeatureClassesLoaded(*model.getCatalog()->getApplicationSchema());
		readerTest.assertFeatureTablesLoaded(*model.getEntityCollection());
	}

	void ModelTest::assertDocumentContent(const fx::gltf::Document &document) const
	{
		CIO_ASSERT_EQUAL(1u, document.scenes.size());
		CIO_ASSERT_EQUAL(0u, document.scene);

		CIO_ASSERT_EQUAL(1u, document.nodes.size());
		CIO_ASSERT_EQUAL(1u, document.materials.size());
		CIO_ASSERT_EQUAL(1u, document.meshes.size());

		// Validate sole material
		{
			const fx::gltf::Material &material = document.materials.front();
			CIO_ASSERT_EQUAL("gold", material.name);

			CIO_ASSERT_EQUAL(1.0f, material.pbrMetallicRoughness.baseColorFactor[0]);
			CIO_ASSERT_EQUAL(0.75f, material.pbrMetallicRoughness.baseColorFactor[1]);
			CIO_ASSERT_EQUAL(0.375f, material.pbrMetallicRoughness.baseColorFactor[2]);
			CIO_ASSERT_EQUAL(1.0f, material.pbrMetallicRoughness.baseColorFactor[3]);
			CIO_ASSERT_EQUAL(-1, material.pbrMetallicRoughness.baseColorTexture.index);

			CIO_ASSERT_EQUAL(0.5f, material.pbrMetallicRoughness.metallicFactor);
			CIO_ASSERT_EQUAL(0.75f, material.pbrMetallicRoughness.roughnessFactor);
			CIO_ASSERT_EQUAL(-1, material.pbrMetallicRoughness.metallicRoughnessTexture.index);

			CIO_ASSERT_EQUAL(-1, material.emissiveTexture.index);
			CIO_ASSERT_EQUAL(-1, material.normalTexture.index);
			CIO_ASSERT_EQUAL(-1, material.occlusionTexture.index);
			
			// TODO check expected values for remaining material stuff
		}

		// Validate sole node
		{
			const fx::gltf::Node &node = document.nodes.front();
			CIO_ASSERT_EQUAL("root", node.name);
			CIO_ASSERT_EQUAL(0, node.mesh);
		}

		// Validate sole primitive on sole mesh
		{
			const fx::gltf::Mesh &mesh = document.meshes.front();
			CIO_ASSERT_EQUAL("", mesh.name);
			CIO_ASSERT_EQUAL(1u, mesh.primitives.size());

			{
				const fx::gltf::Primitive &prim = mesh.primitives.front();
				CIO_ASSERT_EQUAL(fx::gltf::Primitive::Mode::Triangles, prim.mode);
				CIO_ASSERT_EQUAL(0, prim.material);
				CIO_ASSERT_EQUAL(0, prim.indices);
				CIO_ASSERT_EQUAL(3u, prim.attributes.size());
				CIO_ASSERT_EQUAL(1u, prim.attributes.count("POSITION"));
				CIO_ASSERT_EQUAL(1, prim.attributes.at("POSITION"));
				CIO_ASSERT_EQUAL(1u, prim.attributes.count("NORMAL"));
				CIO_ASSERT_EQUAL(2, prim.attributes.at("NORMAL"));
				CIO_ASSERT_EQUAL(1u, prim.attributes.count("_FEATURE_ID_0"));
				CIO_ASSERT_EQUAL(3, prim.attributes.at("_FEATURE_ID_0"));
				CIO_ASSERT_EQUAL(0u, prim.targets.size());

				// Validate FID extension is present in JSON
				CIO_ASSERT_EQUAL(1u, prim.extensionsAndExtras.count("extensions"));
				const nlohmann::json &extensions = prim.extensionsAndExtras.at("extensions");
				CIO_ASSERT_EQUAL(1u, extensions.count("EXT_feature_metadata"));
				const nlohmann::json &metadata = extensions.at("EXT_feature_metadata");
				CIO_ASSERT_EQUAL(1u, metadata.count("featureIdAttributes"));
				const nlohmann::json &attributes = metadata.at("featureIdAttributes");
				CIO_ASSERT_EQUAL(0u, metadata.count("featureTextures"));

				CIO_ASSERT_EQUAL(3u, attributes.size());
				{
					// First binding: a feature ID attribute
					{
						const nlohmann::json &attribute = attributes.at(0);
						CIO_ASSERT_EQUAL(1u, attribute.count("featureTable"));
						CIO_ASSERT_EQUAL("buildings", attribute.at("featureTable").get<std::string>());
						CIO_ASSERT_EQUAL(1u, attribute.count("featureIds"));
						const nlohmann::json &fids = attribute.at("featureIds");
						CIO_ASSERT_EQUAL(1u, fids.count("attribute"));
						CIO_ASSERT_EQUAL("_FEATURE_ID_0", fids.at("attribute").get<std::string>());
						CIO_ASSERT_EQUAL(0u, fids.count("constant"));
						CIO_ASSERT_EQUAL(0u, fids.count("vertexStride"));
					}

					// Second binding: sequential elevation indices
					{
						const nlohmann::json &attribute = attributes.at(1);
						CIO_ASSERT_EQUAL(1u, attribute.count("featureTable"));
						CIO_ASSERT_EQUAL("elevations", attribute.at("featureTable").get<std::string>());
						CIO_ASSERT_EQUAL(1u, attribute.count("featureIds"));
						const nlohmann::json &fids = attribute.at("featureIds");
						CIO_ASSERT_EQUAL(0u, fids.count("attribute"));
						CIO_ASSERT_EQUAL(0u, fids.count("constant"));
						CIO_ASSERT_EQUAL(1u, fids.count("divisor"));
						CIO_ASSERT_EQUAL(2, fids.at("divisor").get<int>());
					}

					// Third binding: const car indices
					{
						const nlohmann::json &attribute = attributes.at(2);
						CIO_ASSERT_EQUAL(1u, attribute.count("featureTable"));
						CIO_ASSERT_EQUAL("cars", attribute.at("featureTable").get<std::string>());
						CIO_ASSERT_EQUAL(1u, attribute.count("featureIds"));
						const nlohmann::json &fids = attribute.at("featureIds");
						CIO_ASSERT_EQUAL(0u, fids.count("attribute"));
						CIO_ASSERT_EQUAL(1u, fids.count("constant"));
						CIO_ASSERT_EQUAL(2u, fids.at("constant").get<std::uint32_t>());
						CIO_ASSERT_EQUAL(1u, fids.count("divisor"));
						CIO_ASSERT_EQUAL(0u, fids.at("divisor").get<std::uint32_t>());
					}
				}
			}
		}

		// Validate accessors
		{
			CIO_ASSERT_EQUAL(4u, document.accessors.size());
			// #0 indices
			std::int32_t accIdx = 0;
			{
				const fx::gltf::Accessor &indices = document.accessors[accIdx];
				CIO_ASSERT_EQUAL(0, indices.bufferView);
				CIO_ASSERT_EQUAL(0u, indices.byteOffset);
				CIO_ASSERT_EQUAL(12u, indices.count);
				CIO_ASSERT_EQUAL(1u, indices.min.size());
				CIO_ASSERT_EQUAL(0.0f, indices.min[0]);
				CIO_ASSERT_EQUAL(1u, indices.max.size());
				CIO_ASSERT_EQUAL(11.0f, indices.max[0]);
				CIO_ASSERT_EQUAL(fx::gltf::Accessor::Type::Scalar, indices.type);
				CIO_ASSERT_EQUAL(fx::gltf::Accessor::ComponentType::UnsignedInt, indices.componentType);
				CIO_ASSERT_EQUAL(false, indices.normalized);
				CIO_ASSERT_EQUAL(0u, indices.sparse.count);
			}

			// #1 positions
			++accIdx;
			{
				const fx::gltf::Accessor &accessor = document.accessors[accIdx];
				CIO_ASSERT_EQUAL(1, accessor.bufferView);
				CIO_ASSERT_EQUAL(0u, accessor.byteOffset);
				CIO_ASSERT_EQUAL(12u, accessor.count);
				CIO_ASSERT_EQUAL(3u, accessor.min.size());
				CIO_ASSERT_EQUAL(0.0f, accessor.min[0]);
				CIO_ASSERT_EQUAL(0.0f, accessor.min[1]);
				CIO_ASSERT_EQUAL(0.0f, accessor.min[2]);
				CIO_ASSERT_EQUAL(3u, accessor.max.size());
				CIO_ASSERT_EQUAL(100.0f, accessor.max[0]);
				CIO_ASSERT_EQUAL(100.0f, accessor.max[1]);
				CIO_ASSERT_EQUAL(25.0f, accessor.max[2]);
				CIO_ASSERT_EQUAL(fx::gltf::Accessor::Type::Vec3, accessor.type);
				CIO_ASSERT_EQUAL(fx::gltf::Accessor::ComponentType::Float, accessor.componentType);
				CIO_ASSERT_EQUAL(false, accessor.normalized);
				CIO_ASSERT_EQUAL(0u, accessor.sparse.count);
			}

			// #2 normals
			++accIdx;
			{
				const fx::gltf::Accessor &accessor = document.accessors[accIdx];
				CIO_ASSERT_EQUAL(2, accessor.bufferView);
				CIO_ASSERT_EQUAL(0u, accessor.byteOffset);
				CIO_ASSERT_EQUAL(12u, accessor.count);
				CIO_ASSERT_EQUAL(3u, accessor.min.size());
				CIO_ASSERT_EQUAL(-1.0f, accessor.min[0]);
				CIO_ASSERT_EQUAL(-1.0f, accessor.min[1]);
				CIO_ASSERT_EQUAL(-1.0f, accessor.min[2]);
				CIO_ASSERT_EQUAL(3u, accessor.max.size());
				CIO_ASSERT_EQUAL(1.0f, accessor.max[0]);
				CIO_ASSERT_EQUAL(1.0f, accessor.max[1]);
				CIO_ASSERT_EQUAL(1.0f, accessor.max[2]);
				CIO_ASSERT_EQUAL(fx::gltf::Accessor::Type::Vec3, accessor.type);
				CIO_ASSERT_EQUAL(fx::gltf::Accessor::ComponentType::Float, accessor.componentType);
				CIO_ASSERT_EQUAL(false, accessor.normalized);
				CIO_ASSERT_EQUAL(0u, accessor.sparse.count);
			}
			// #3 feature IDs
			++accIdx;
			{
				const fx::gltf::Accessor &accessor = document.accessors[accIdx];
				CIO_ASSERT_EQUAL(3, accessor.bufferView);
				CIO_ASSERT_EQUAL(0u, accessor.byteOffset);
				CIO_ASSERT_EQUAL(5u, accessor.count);
				CIO_ASSERT_EQUAL(1u, accessor.min.size());
				CIO_ASSERT_EQUAL(0.0f, accessor.min[0]);
				CIO_ASSERT_EQUAL(1u, accessor.max.size());
				CIO_ASSERT_EQUAL(7.0f, accessor.max[0]);
				CIO_ASSERT_EQUAL(fx::gltf::Accessor::Type::Scalar, accessor.type);
				CIO_ASSERT_EQUAL(fx::gltf::Accessor::ComponentType::UnsignedByte, accessor.componentType);
				CIO_ASSERT_EQUAL(false, accessor.normalized);
				CIO_ASSERT_EQUAL(0u, accessor.sparse.count);
			}
		}

		// Vaildate buffer views and buffers
		{
			std::vector<cio::SizeInterval> viewRanges = SampleMetadata::generateBufferRanges();
			
			// Validate buffer views
			{
				CIO_ASSERT_EQUAL(viewRanges.size(), document.bufferViews.size());

				// Validate index view
				std::uint32_t viewIdx = 0;
				{
					cio::SizeInterval range = viewRanges[viewIdx];
					const fx::gltf::BufferView &indexView = document.bufferViews[viewIdx];
					CIO_ASSERT_EQUAL(0, indexView.buffer);
					CIO_ASSERT_EQUAL(range.lower, indexView.byteOffset);
					CIO_ASSERT_EQUAL(range.upper - range.lower, indexView.byteLength);
					CIO_ASSERT_EQUAL(0, indexView.byteStride);
					CIO_ASSERT_EQUAL(fx::gltf::BufferView::TargetType::ElementArrayBuffer, indexView.target);
				}

				// Validate position view
				++viewIdx;
				{
					cio::SizeInterval range = viewRanges[viewIdx];
					const fx::gltf::BufferView &indexView = document.bufferViews[viewIdx];
					CIO_ASSERT_EQUAL(0, indexView.buffer);
					CIO_ASSERT_EQUAL(range.lower, indexView.byteOffset);
					CIO_ASSERT_EQUAL(range.upper - range.lower, indexView.byteLength);
					CIO_ASSERT_EQUAL(0, indexView.byteStride);
					CIO_ASSERT_EQUAL(fx::gltf::BufferView::TargetType::ArrayBuffer, indexView.target);
				}

				// Validate normal view
				++viewIdx;
				{
					cio::SizeInterval range = viewRanges[viewIdx];
					const fx::gltf::BufferView &indexView = document.bufferViews[viewIdx];
					CIO_ASSERT_EQUAL(0, indexView.buffer);
					CIO_ASSERT_EQUAL(range.lower, indexView.byteOffset);
					CIO_ASSERT_EQUAL(range.upper - range.lower, indexView.byteLength);
					CIO_ASSERT_EQUAL(0, indexView.byteStride);
					CIO_ASSERT_EQUAL(fx::gltf::BufferView::TargetType::ArrayBuffer, indexView.target);
				}

				// Validate feature IDs view
				++viewIdx;
				{
					cio::SizeInterval range = viewRanges[viewIdx];
					const fx::gltf::BufferView &indexView = document.bufferViews[viewIdx];
					CIO_ASSERT_EQUAL(0, indexView.buffer);
					CIO_ASSERT_EQUAL(range.lower, indexView.byteOffset);
					CIO_ASSERT_EQUAL(range.upper - range.lower, indexView.byteLength);
					CIO_ASSERT_EQUAL(0, indexView.byteStride);
					CIO_ASSERT_EQUAL(fx::gltf::BufferView::TargetType::ElementArrayBuffer, indexView.target);
				}

				// Validate heights view
				++viewIdx;
				{
					cio::SizeInterval range = viewRanges[viewIdx];
					const fx::gltf::BufferView &indexView = document.bufferViews[viewIdx];
					CIO_ASSERT_EQUAL(0, indexView.buffer);
					CIO_ASSERT_EQUAL(range.lower, indexView.byteOffset);
					CIO_ASSERT_EQUAL(range.upper - range.lower, indexView.byteLength);
					CIO_ASSERT_EQUAL(0, indexView.byteStride);
					CIO_ASSERT_EQUAL(fx::gltf::BufferView::TargetType::None, indexView.target);
				}

				// Validate addresses view
				++viewIdx;
				{
					cio::SizeInterval range = viewRanges[viewIdx];
					const fx::gltf::BufferView &indexView = document.bufferViews[viewIdx];
					CIO_ASSERT_EQUAL(0, indexView.buffer);
					CIO_ASSERT_EQUAL(range.lower, indexView.byteOffset);
					CIO_ASSERT_EQUAL(range.upper - range.lower, indexView.byteLength);
					CIO_ASSERT_EQUAL(0, indexView.byteStride);
					CIO_ASSERT_EQUAL(fx::gltf::BufferView::TargetType::None, indexView.target);
				}

				// Validate addresses text offsets view
				++viewIdx;
				{
					cio::SizeInterval range = viewRanges[viewIdx];
					const fx::gltf::BufferView &indexView = document.bufferViews[viewIdx];
					CIO_ASSERT_EQUAL(0, indexView.buffer);
					CIO_ASSERT_EQUAL(range.lower, indexView.byteOffset);
					CIO_ASSERT_EQUAL(range.upper - range.lower, indexView.byteLength);
					CIO_ASSERT_EQUAL(0, indexView.byteStride);
					CIO_ASSERT_EQUAL(fx::gltf::BufferView::TargetType::None, indexView.target);
				}

				// Validate coordinates view
				++viewIdx;
				{
					cio::SizeInterval range = viewRanges[viewIdx];
					const fx::gltf::BufferView &indexView = document.bufferViews[viewIdx];
					CIO_ASSERT_EQUAL(0, indexView.buffer);
					CIO_ASSERT_EQUAL(range.lower, indexView.byteOffset);
					CIO_ASSERT_EQUAL(range.upper - range.lower, indexView.byteLength);
					CIO_ASSERT_EQUAL(0, indexView.byteStride);
					CIO_ASSERT_EQUAL(fx::gltf::BufferView::TargetType::None, indexView.target);
				}

				// Validate occupants view
				++viewIdx;
				{
					cio::SizeInterval range = viewRanges[viewIdx];
					const fx::gltf::BufferView &indexView = document.bufferViews[viewIdx];
					CIO_ASSERT_EQUAL(0, indexView.buffer);
					CIO_ASSERT_EQUAL(range.lower, indexView.byteOffset);
					CIO_ASSERT_EQUAL(range.upper - range.lower, indexView.byteLength);
					CIO_ASSERT_EQUAL(0, indexView.byteStride);
					CIO_ASSERT_EQUAL(fx::gltf::BufferView::TargetType::None, indexView.target);
				}

				// Validate passengers view
				++viewIdx;
				{
					cio::SizeInterval range = viewRanges[viewIdx];
					const fx::gltf::BufferView &indexView = document.bufferViews[viewIdx];
					CIO_ASSERT_EQUAL(0, indexView.buffer);
					CIO_ASSERT_EQUAL(range.lower, indexView.byteOffset);
					CIO_ASSERT_EQUAL(range.upper - range.lower, indexView.byteLength);
					CIO_ASSERT_EQUAL(0, indexView.byteStride);
					CIO_ASSERT_EQUAL(fx::gltf::BufferView::TargetType::None, indexView.target);
				}

				// Validate passengers array offsets view
				++viewIdx;
				{
					cio::SizeInterval range = viewRanges[viewIdx];
					const fx::gltf::BufferView &indexView = document.bufferViews[viewIdx];
					CIO_ASSERT_EQUAL(0, indexView.buffer);
					CIO_ASSERT_EQUAL(range.lower, indexView.byteOffset);
					CIO_ASSERT_EQUAL(range.upper - range.lower, indexView.byteLength);
					CIO_ASSERT_EQUAL(0, indexView.byteStride);
					CIO_ASSERT_EQUAL(fx::gltf::BufferView::TargetType::None, indexView.target);
				}

				// Validate passengers text offsets view
				++viewIdx;
				{
					cio::SizeInterval range = viewRanges[viewIdx];
					const fx::gltf::BufferView &indexView = document.bufferViews[viewIdx];
					CIO_ASSERT_EQUAL(0, indexView.buffer);
					CIO_ASSERT_EQUAL(range.lower, indexView.byteOffset);
					CIO_ASSERT_EQUAL(range.upper - range.lower, indexView.byteLength);
					CIO_ASSERT_EQUAL(0, indexView.byteStride);
					CIO_ASSERT_EQUAL(fx::gltf::BufferView::TargetType::None, indexView.target);
				}

				// Validate isElectric view
				++viewIdx;
				{
					cio::SizeInterval range = viewRanges[viewIdx];
					const fx::gltf::BufferView &indexView = document.bufferViews[viewIdx];
					CIO_ASSERT_EQUAL(0, indexView.buffer);
					CIO_ASSERT_EQUAL(range.lower, indexView.byteOffset);
					CIO_ASSERT_EQUAL(range.upper - range.lower, indexView.byteLength);
					CIO_ASSERT_EQUAL(0, indexView.byteStride);
					CIO_ASSERT_EQUAL(fx::gltf::BufferView::TargetType::None, indexView.target);
				}

				// Validate elevations view
				++viewIdx;
				{
					cio::SizeInterval range = viewRanges[viewIdx];
					const fx::gltf::BufferView &indexView = document.bufferViews[viewIdx];
					CIO_ASSERT_EQUAL(0, indexView.buffer);
					CIO_ASSERT_EQUAL(range.lower, indexView.byteOffset);
					CIO_ASSERT_EQUAL(range.upper - range.lower, indexView.byteLength);
					CIO_ASSERT_EQUAL(0, indexView.byteStride);
					CIO_ASSERT_EQUAL(fx::gltf::BufferView::TargetType::None, indexView.target);
				}

				// Sanity check to make sure we looked at all of them
				CIO_ASSERT_EQUAL(viewRanges.size(), viewIdx + 1);
			}

			// Validate buffer content
			{
				CIO_ASSERT_EQUAL(1u, document.buffers.size());
				const fx::gltf::Buffer &buffer = document.buffers.front();
				CIO_ASSERT_EQUAL(viewRanges.back().upper, buffer.byteLength);
				CIO_ASSERT_EQUAL(buffer.byteLength, buffer.data.size());
				CIO_ASSERT_EQUAL("SampleMetadata.bin", buffer.uri);

				const std::uint8_t *bytes = buffer.data.data();
				
				// Validate index buffer data
				std::uint32_t viewIdx = 0;
				{
					std::size_t offset = viewRanges[viewIdx].lower;
					for (std::uint32_t index : SampleMetadata::Vertex::indices)
					{
						std::uint32_t actual;
						std::memcpy(&actual, bytes + offset, 4);
						CIO_ASSERT_EQUAL(index, actual);
						offset += 4;
					}
				}

				// Validate position buffer data
				++viewIdx;
				{
					std::size_t offset = viewRanges[viewIdx].lower;
					for (const float (&v)[3] : SampleMetadata::VertexAttribute::positions)
					{
						for (float x : v)
						{
							float actual;
							std::memcpy(&actual, bytes + offset, 4);
							CIO_ASSERT_EQUAL(x, actual);
							offset += 4;
						}
					}
				}

				// Validate normal buffer data
				++viewIdx;
				{
					std::size_t offset = viewRanges[viewIdx].lower;
					for (const float(&v)[3] : SampleMetadata::VertexAttribute::normals)
					{
						for (float x : v)
						{
							float actual;
							std::memcpy(&actual, bytes + offset, 4);
							CIO_ASSERT_EQUAL(x, actual);
							offset += 4;
						}
					}
				}

				// Validate feature IDs buffer data
				++viewIdx;
				{
					std::size_t offset = viewRanges[viewIdx].lower;
					for (std::uint8_t n : SampleMetadata::VertexAttribute::fids)
					{
						std::uint8_t actual = bytes[offset];
						CIO_ASSERT_EQUAL(static_cast<std::uint32_t>(n), static_cast<std::uint32_t>(actual));
						offset += 1;
					}
				}

				// Validate heights data
				++viewIdx;
				{
					std::size_t offset = viewRanges[viewIdx].lower;
					for (double h : SampleMetadata::Buildings::heights)
					{
						double actual;
						std::memcpy(&actual, bytes + offset, 8);
						CIO_ASSERT_EQUAL(h, actual);
						offset += 8;
					}
				}

				// Validate addresses text character data
				++viewIdx;
				{
					std::size_t offset = viewRanges[viewIdx].lower;
					for (const char *address : SampleMetadata::Buildings::addresses)
					{
						std::size_t len = std::strlen(address);
						std::string value(reinterpret_cast<const char *>(bytes + offset), len);
						CIO_ASSERT_EQUAL(address, value);
						offset += len;
					}
				}

				// Validate addresses text offsets
				++viewIdx;
				{
					std::size_t offset = viewRanges[viewIdx].lower;
					std::size_t cumulativeTextOffset = 0;
					for (const char *address : SampleMetadata::Buildings::addresses)
					{
						std::size_t len = std::strlen(address);
						std::uint32_t textOffsetValue = 0;
						std::memcpy(&textOffsetValue, bytes + offset, 4);
						CIO_ASSERT_EQUAL(cumulativeTextOffset, textOffsetValue);
						offset += 4;
						cumulativeTextOffset += len;
					}
				}

				// Validate coordinates buffer data
				++viewIdx;
				{
					std::size_t offset = viewRanges[viewIdx].lower;
					for (const double(&v)[2] : SampleMetadata::Buildings::coordinates)
					{
						for (double x : v)
						{
							double actual;
							std::memcpy(&actual, bytes + offset, 8);
							CIO_ASSERT_EQUAL(x, actual);
							offset += 8;
						}
					}
				}

				// Validate occupants buffer data
				++viewIdx;
				{
					std::size_t offset = viewRanges[viewIdx].lower;
					for (std::uint32_t n : SampleMetadata::Buildings::occupants)
					{
						std::uint32_t actual;
						std::memcpy(&actual, bytes + offset, 4);
						CIO_ASSERT_EQUAL(n, actual);
						offset += 4;
					}
				}

				// Validate passengers text character data
				++viewIdx;
				{
					std::size_t offset = viewRanges[viewIdx].lower;
					for (const char *passenger : SampleMetadata::Cars::passengers)
					{
						std::size_t len = std::strlen(passenger);
						std::string value(reinterpret_cast<const char *>(bytes + offset), len);
						CIO_ASSERT_EQUAL(passenger, value);
						offset += len;
					}
				}

				// Validate passenger list index offsets
				++viewIdx;
				{
					std::size_t offset = viewRanges[viewIdx].lower;
					std::size_t cumulativeIndexOffset = 0;
					for (std::uint32_t count : SampleMetadata::Cars::passengerCounts)
					{
						std::uint32_t textOffsetValue = 0;
						std::memcpy(&textOffsetValue, bytes + offset, 4);
						CIO_ASSERT_EQUAL(cumulativeIndexOffset, textOffsetValue);
						offset += 4;
						cumulativeIndexOffset += count * 4;
					}
				}

				// Validate passenger text offsets
				++viewIdx;
				{
					std::size_t offset = viewRanges[viewIdx].lower;
					std::size_t cumulativeTextOffset = 0;
					for (const char *passenger : SampleMetadata::Cars::passengers)
					{
						std::size_t len = std::strlen(passenger);
						std::uint32_t textOffsetValue = 0;
						std::memcpy(&textOffsetValue, bytes + offset, 4);
						CIO_ASSERT_EQUAL(cumulativeTextOffset, textOffsetValue);
						offset += 4;
						cumulativeTextOffset += len;
					}
				}

				// Validate isElectric buffer data
				++viewIdx;
				{
					std::size_t offset = viewRanges[viewIdx].lower;
					std::size_t i = 0;
					std::uint8_t bits = 0;

					for (bool value : SampleMetadata::Cars::isElectric)
					{
						if (i % 8 == 0)
						{
							bits = bytes[offset];
							++offset;
						}

						std::uint32_t mask = 1u << (7 - (i % 8));

						bool actual = (bits & mask) != 0;
						CIO_ASSERT_EQUAL(value, actual);
						++i;
					}
				}

				// Validate elevations buffer data
				++viewIdx;
				{
					std::size_t offset = viewRanges[viewIdx].lower;
					for (std::uint8_t n : SampleMetadata::Elevations::elevations)
					{
						std::uint8_t actual = bytes[offset];
						CIO_ASSERT_EQUAL(static_cast<std::uint32_t>(n), static_cast<std::uint32_t>(actual));
						offset += 1;
					}
				}

				// Sanity check, make sure our viewIdx is the last valid one
				CIO_ASSERT_EQUAL(viewRanges.size(), viewIdx + 1);
			}
		}
	}
}
