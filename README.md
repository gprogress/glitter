# Glitter README #

Glitter - Geospatial Layered Information Transfer Technique for Environment Representation

### What is this repository for? ###

The Glitter library is a C++ library to read, write, edit, and validate 3D geospatial repositories organized according to profiles of an application schema.
It is intended to provide a comprehensive technique for working with vector, raster, voxel, mesh, and point cloud products with attribution and topology.
For those familiar with LTF, Glitter is intended to reenvision the LTF object model for 3D repository management, with Geas and CIO replacing the lower level ACSL
components of LTF.

The name was originally chosen just for fun as a wordplay on the Khronos glTF format, which was originally the only supported format. It can be expanded
with the backronym Geospatial Layered Information Transfer Technique for Environment Representation if so desired.

Glitter 0.4.0 is the latest version that works with Geas 0.4.0 and CIO 0.4.0. 

### How do I get set up? ###

Glitter is a conventional CMake project that should cleanly compile on any C++14 compiler with CMake 3.0 or newer
following the standard instructions for a CMake build process. 

In most cases, this means:
* Clone from the Geas repository at https://bitbucket.org/gprogress/glitter.git
* Create a build directory (conventionally "build" under the clone directory, but can be anywhere)
* From that build directory, run cmake (or launch the CMake GUI) with the top-level cloned directory as the source directory
* Specify all configuration variables such as CMAKE_BUILD_TYPE, CMAKE_INSTALL_PREFIX, and any hints to find dependencies
* Configure and generate your native build system (typically a Visual Studio IDE on Windows and Makefiles or Ninja everywhere else)
* Use your native build system as normal

Currently tested configurations include:
* Windows 10 and Windows 11 x64 with Visual Studio 2017 IDE, Visual Studio 2019 IDE, or Visual Studio 2022 IDE,
* Ubuntu Linux 18.04 amd64 and aarch64 with GCC 7.5 using Makefiles
* Ubuntu Linux 20.10 amd64 with GCC 10 using Makefiles

Glitter has a mandatory dependency on CIO library at https://bitbucket.org/gprogress/cio.git. CIO itself has the following optional dependencies:
* libexpat 2.0 or higher for XML read/write
* DBus for Bluetooth support on Linux
* ZLib for deflate compression and decompression

Gitter has a mandatory dependency on Geas library at https://bitbucket.org/gprogress/geas.git. Geas itself has the following optional dependencies:
* XLNT 1.50 for Excel read/write
* SQLite3 for SQLite database and Geopackage read/write

All optional components may be enabled or disabled via CMake cache variables. The external dependency locations may also be
specified by CMake variables to provide hints to the CMake Find scripts, or by editing the "cmake/DefaultManifest.cmake" which specifies default
locations for dependencies.

The following CMake Cache variables are particularly useful:
* BUILD_SHARED_LIBS - if true, build as a shared (DLL/so) library, if false build as a static library
* CMAKE_BUILD_TYPE - for non-IDE platforms, set Release, Debug, or RelWithDebInfo build type (no effect on IDE builds) per CMake norm 
* CMAKE_INSTALL_PREFIX - final install directory to copy all built artifacts, per CMake norm
* CIO_ROOT - base directory contining lib, bin, and include directories of CIO if in a non-standard location
* EXPAT_ROOT - base directory contining lib, bin, and include directories of libexpat if in a non-standard location
* Geas_ROOT - base directory contining lib, bin, and include directories of Geas if in a non-standard location 
* SQLite3_ROOT - base directory contining lib, bin, and include directories of sqlite if in a non-standard location
* XLNT_ROOT - base directory contining lib, bin, and include directories of XLNT if in a non-standard location
* ZLIB_ROOT - base directory contining lib, bin, and include directories of zlib if in a non-standard location

### Contribution guidelines ###

We welcome contributions to Glitter, especially to add new format read/write capabilities.
All contributions should be submitted as a pull request where they will be reviewed and any concerns addressed.
The contributions must cleanly compile on standard C++14 compilers, provide a general utility related to 3D geospatial environments, be licensed
under the LGPL v3 or a compatible license such as MIT/BSD, and (where applicable) assign the copyright to Geometric Progress LLC.

Exceptions to the guidelines can be discussed on a case by case basis for unusual circumstances.

### Who do I talk to? ###

Glitter is owned and maintained by Geometric Progress LLC. Please contact admin@geometricprogress.com for more information.

Copyright (c) 2020-2023 Geometric Progress LLC.